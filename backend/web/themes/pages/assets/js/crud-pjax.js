function FormShowModal(link="") {
    var link = (link || paramJs.urlFormShowModal);
    $('#detail').html("loading...");
    $.ajax({
        url: link,
        data:  $('#crud-form').serialize(),
        method: "POST",
        dataType: 'html',
        success: function(data) {
            $('#detail').html(data);
        },
    });
}

function FormCreate() {
    FormShowModal();
    $('#crud-modal').modal('show');
}
function FormUpdate(link) {
    FormShowModal(link);
    $('#crud-modal').modal('show');
}
function FormDuplicate(link) {
  FormShowModal(link);
  $('#crud-modal').modal('show');
}
function FormDelete(link) {
  if (confirm("Are you sure want to delete this item ?")) {
    FormShowModal(link);
    reload();
  }
}
window.closeModal = function(){
    reload();
};
function reload(){
    $.pjax.defaults.timeout = false;
    $.pjax.reload({
        container: '#crud-pjax'
    })
    $('#crud-form').modal('hide');
    $('#crud-form').data('modal', null);
}

