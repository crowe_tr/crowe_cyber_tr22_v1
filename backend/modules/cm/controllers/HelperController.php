<?php

namespace backend\modules\cm\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use common\models\hr\Employee;
use common\models\cl\Client;
use common\models\cm\Dept;
use common\models\tr\Job;
use common\models\tr\JobList;
use common\models\st\Task;
use common\components\CommonHelper;
use yii\filters\VerbFilter;

class HelperController extends Controller
{
    public $layout;
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    public function actionSrcemployee($q = null, $id = null)
    {
        //SrcSupplierOpt
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $model = Employee::find()->where("
                full_name like '%" . $q . "%'
                or user_id = '" . $q . "'
            ")->asArray()->all();

            $out['results'] = array();
            foreach ($model as $data) {
                $array = array(
                    'id' => $data['user_id'],
                    'text' => $data['user_id'] . ' - ' . $data['full_name'],
                );
                array_push($out['results'], $array);
            }
        } elseif ($id > 0) {
            $model = Employee::find()->where("
                user_id = " . $id . "
            ")->asArray()->one();

            $out['results'] = ['id' => $id, 'text' => $data['user_id'] . ' - ' . $data['full_name']];
        }

        return $out;
    }

    public function actionSrcjob_status($q = null, $id = null)
    {
        //SrcSupplierOpt
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            // $model = Employee::find()->where("
            //     full_name like '%" . $q . "%'
            //     or user_id = '" . $q . "'
            // ")->asArray()->all();

            $out['results'] = array();

            $data_array = ['DRAFT','DRAFT REVISION','SUBMITTED','SUBMITTED REVISION','ONGOING','EXPIRED','REJECTED','CLOSED'];
            $count = count($data_array);
            for ($i = 0; $i < $count; $i++) {
                $array = array(
                    'id' => $data_array[$i],
                    'text' => $data_array[$i],
                );
                array_push($out['results'], $array);
            }
            
            // foreach ($model as $data) {
            //     $array = array(
            //         'id' => $data['user_id'],
            //         'text' => $data['user_id'] . ' - ' . $data['full_name'],
            //     );
            //     array_push($out['results'], $array);
            // }
        // } elseif ($id > 0) {
        //     $model = Employee::find()->where("
        //         user_id = " . $id . "
        //     ")->asArray()->one();

        //     $out['results'] = ['id' => $id, 'text' => $data['user_id'] . ' - ' . $data['full_name']];
        }

        return $out;
    }
    public function actionSrcclient($q = null, $id = null)
    {
        //SrcSupplierOpt
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $model = Client::find()->where("
                client_name like '%" . $q . "%'
                or id = '" . $q . "'
            ")->asArray()->all();

            $out['results'] = array();
            foreach ($model as $data) {
                $array = array(
                    'id' => $data['id'],
                    'text' => $data['id'] . ' - ' . $data['client_name'],
                );
                array_push($out['results'], $array);
            }
        } elseif ($id > 0) {
            $model = Client::find()->where("
                id = " . $id . "
            ")->asArray()->one();

            $out['results'] = ['id' => $id, 'text' => $data['id'] . ' - ' . $data['client_name']];
        }

        return $out;
    }
    public function actionSrcclient_code($q = null, $id = null)
    {
        //SrcSupplierOpt
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $model = Client::find()->where("
                client_name like '%" . $q . "%'
                or client_code = '" . $q . "'
            ")->asArray()->all();

            $out['results'] = array();
            foreach ($model as $data) {
                $array = array(
                    'id' => $data['client_code'],
                    'text' => $data['client_code'] . ' - ' . $data['client_name'],
                );
                array_push($out['results'], $array);
            }
        } elseif ($id > 0) {
            $model = Client::find()->where("
            client_code = " . $id . "
            ")->asArray()->one();

            $out['results'] = ['id' => $id, 'text' => $data['client_code'] . ' - ' . $data['client_name']];
        }

        return $out;
    }

    public function find_client($clientID)
    {
        $client = Client::findOne($clientID);

        return $client['Name'];
    }
    public function actionSrcjob($q = null, $id = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $results = [];

        $list = Job::find()
            ->select([
            'tr_job.job_code',
            'tr_job.description',
            'tr_job.client_id',
            'cl_client.client_name as client_name'
            ])->innerJoin('cl_client', 'cl_client.id = tr_job.client_id');

        if (!empty($q)) {
            $list = $list->where(
                "
                tr_job.description like '%" . $q . "%'
                or tr_job.job_code like '%" . $q . "%'
                or cl_client.client_name like '%" . $q . "%'
                "
            );
        }

        if (!empty($id)) {
            $list = $list->andWhere(["JobCode = " . $id]);
        }


        $list = $list->asArray()->all();
        $results['results'] = [];
        foreach ($list as $data) {
            $array = array(
                'id' => $data['job_code'],
                'text' => $data['job_code'] . ' - ' . $data['client_name'] . ' - ' . $data['description'],
            );
            array_push($results['results'], $array);
        }

        return $results;
    }

    public function actionLoaddept()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $id = str_replace('+', ' ', end($_POST['depdrop_parents']));;
            $list = Dept::find()->andWhere(['divID' => $id])->asArray()->all();

            $results = [];
            $selected = null;
            if ($id != null && count($list) > 0) {
                $selected = '';
                foreach ($list as $i => $ls) {
                    $out[] = ['id' => $ls['id'], 'name' => $ls['deptName']];
                }
                $results = json_encode(['output' => $out, 'selected' => $selected]);
            } else {
                $results = json_encode(['output' => [], 'selected' => '']);
            }
        } else {
            $results = json_encode(['output' => [], 'selected' => '']);
        }
        return $results;
    }

    public function actionLoaddept_ver2()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $id = str_replace('+', ' ', end($_POST['depdrop_parents']));;
            $list = Dept::find()->joinwith('div')->andWhere(['cm_division.div_name' => $id])->asArray()->all();

            $results = [];
            $selected = null;
            if ($id != null && count($list) > 0) {
                $selected = '';
                foreach ($list as $i => $ls) {
                    $out[] = ['id' => $ls['dept_name'], 'name' => $ls['dept_name']];
                }
                $results = json_encode(['output' => $out, 'selected' => $selected]);
            } else {
                $results = json_encode(['output' => [], 'selected' => '']);
            }
        } else {
            $results = json_encode(['output' => [], 'selected' => '']);
        }
        return $results;
    }

    public function actionLoadtimereportjobs($itr_date)
    {
        $user = CommonHelper::getUserIndentity();
        $return = json_encode(['output' => array(), 'selected' => '']);

        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $type = end($_POST['depdrop_parents']);
            $sql_list = "call job_list('tr-job-list', '" . $user->user_id . ";" . $itr_date . "');";
            // $sql_list = "call tr_job_list(" . $id . ");";
            // $sql_list = "call TrJobList('".$id."', ".$job.")";
            $list = Yii::$app->db->createCommand($sql_list)->queryAll();

            $selected = null;
            if ($itr_date != null && count($list) > 0) {
                if ($type == 1) { //if PROJECTS
                    $selected = '';
                    foreach ($list as $i => $ls) {
                        $out[] = ['id' => $ls['job_id'], 'name' => $ls['job_client_description']];
                    }
                    // var_dump($out);
                    $return = json_encode(['output' => $out, 'selected' => $selected]);
                } else {
                    $return = json_encode(['output' => array(), 'selected' => '']);
                }

            }
        } else {
            $return = json_encode(['output' => array(), 'selected' => '']);
        }
        return $return;
    }

    public function actionLoadtimereportjobs_old($id, $job = 0)
    {
        $user = CommonHelper::getUserIndentity();
        $return = json_encode(['output' => array(), 'selected' => '']);

        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $type = end($_POST['depdrop_parents']);
            // $sql_list = "call job_list('tr-job-list', " . $id . ");";
            $sql_list = "call tr_job_list(" . $id . ");";
            // $sql_list = "call TrJobList('".$id."', ".$job.")";
            $list = Yii::$app->db->createCommand($sql_list)->queryAll();


            $selected = null;
            if ($id != null && count($list) > 0) {
                if ($type == 1) { //if PROJECTS
                    $selected = '';
                    foreach ($list as $i => $ls) {
                        $out[] = ['id' => $ls['JobID'], 'name' => $ls['Description']];
                    }
                    $return = json_encode(['output' => $out, 'selected' => $selected]);
                } else {
                    $return = json_encode(['output' => array(), 'selected' => '']);
                }
            }
        } else {
            $return = json_encode(['output' => array(), 'selected' => '']);
        }
        return $return;
    }
    public function actionLoadtimereportzone($TimeReportID)
    {
        $user = CommonHelper::getUserIndentity();

        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $id = end($_POST['depdrop_parents']);
            $sql_lookups = "call job_list('tr-job-ope','{$TimeReportID};{$id}')";
            $list = Yii::$app->db->createCommand($sql_lookups)->queryAll();
            // $list = Yii::$app->db->createCommand("call trdet_update_list('ope','{$TimeReportID}', '{$id}', null)")->queryAll();
            // $list = Yii::$app->db->createCommand("call TrJobOutOffList('{$TimeReportID}', '{$id}')")->queryAll();

            $selected = null;
            if ($id != null && count($list) > 0) {
              // var_dump('a');
              // die();
                $selected = '';
                foreach ($list as $i => $ls) {
                    if ($id == $ls['tr_det_id']) {
                        if (!empty($ls['job_area'])) {
                            $JobArea = explode(',', $ls['job_area']);
                            foreach ($JobArea as $Area) {
                                $out[] = ['id' => $Area, 'name' => $Area];
                            }
                            $out_of_zone = ['id' => 'OUT OF TOWN', 'name' => 'OUT OF TOWN'];
                            array_push($out, $out_of_zone);

                        }
                    }
                }
                echo json_encode(['output' => $out, 'selected' => $selected]);
            }
        } else {
            echo json_encode(['output' => array(), 'selected' => '']);
        }
    }
    public function actionLoadtimereporttask()
    {
        $user = CommonHelper::getUserIndentity();

        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $id = end($_POST['depdrop_parents']);
            $list = Task::find()->where(['task_type_id' => $id])->orderby(['seq' => SORT_ASC])->asArray()->all();
            $sql_list = "call common_list('tr-task','" . $user->user_id . ";" . $id . "')";


            $list = Yii::$app->db->createCommand($sql_list)->queryAll();
            // $list = Yii::$app->db->createCommand("call common_list('task','" . $user->Id . "," . $id . "')")->queryAll();
            // $list = Yii::$app->db->createCommand("call getTask('".$user->Id."', $id)")->queryAll();

            $selected = null;
            if ($id != null && count($list) > 0) {
                $selected = '';
                foreach ($list as $i => $ls) {
                    $out[] = ['id' => $ls['id'], 'name' => $ls['task_name']];
                }
                echo json_encode(['output' => $out, 'selected' => $selected]);
            }
        } else {
            echo json_encode(['output' => array(), 'selected' => '']);
        }
    }

    public function actionLoadtaskreport()
    {
        $user = CommonHelper::getUserIndentity();

        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $id = end($_POST['depdrop_parents']);
            $list = Task::find()->where(['task_type_id' => $id])->orderby(['seq' => SORT_ASC])->asArray()->all();
            $sql_list = "call common_list('tr-task-report'," . $id . ")";


            $list = Yii::$app->db->createCommand($sql_list)->queryAll();
            // $list = Yii::$app->db->createCommand("call common_list('task','" . $user->Id . "," . $id . "')")->queryAll();
            // $list = Yii::$app->db->createCommand("call getTask('".$user->Id."', $id)")->queryAll();

            $selected = null;
            if ($id != null && count($list) > 0) {
                $selected = '';
                foreach ($list as $i => $ls) {
                    $out[] = ['id' => $ls['id'], 'name' => $ls['task_name']];
                }
                echo json_encode(['output' => $out, 'selected' => $selected]);
            }
        } else {
            echo json_encode(['output' => array(), 'selected' => '']);
        }
    }

    public function actionLoadtimereporttasktaxi()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $id = end($_POST['depdrop_parents']);
            $sql_list = "call getTrTaxiClassList('" . $id . "')";
            $list = Yii::$app->db->createCommand($sql_list)->queryAll();


            $selected = null;
            if ($id != null && count($list) > 0) {
                $selected = '';
                foreach ($list as $i => $ls) {
                    $out[] = ['id' => $ls['ID'], 'name' => $ls['taskTypeName']];
                }
                echo json_encode(['output' => $out, 'selected' => $selected]);
            }
        } else {
            echo json_encode(['output' => array(), 'selected' => '']);
        }
    }

    public function actionLoadtimereportlookuptaxi($TimeReportID, $TrDetID)
    {

        $results = json_encode(['output' => array(), 'selected' => '']);
        $out = [];
        if (isset($_POST['depdrop_all_params'])) {
            $param = $_POST['depdrop_all_params'];
            $sql = "call job_list('tr-job-taxi-list','{$TimeReportID};{$TrDetID};" . $param['taxi_id'] ."')";

            // $sql = "call trdet_update_list('taxi'," . $TimeReportID . ", " . $TrDetID . ", " . $param['cmTaxiID'] . ")";
            // $sql = "call TrJobTaxiList(".$TimeReportID.", ".$param['cmTaxiID'].", '".$TrDetID."')";
            $list = Yii::$app->db->createCommand($sql)->queryAll();

            $selected = null;
            if (count($list) > 0) {
                $selected = '';
                foreach ($list as $i => $lookup) {
                    $out[] = ['id' => $lookup['tr_det_id'], 'name' => $lookup['description']];
                }
                $results = json_encode(['output' => $out, 'selected' => $selected]);
            }
        }
        return $results;
    }



    public function actionSearch_client_by_user_partner($q = null, $id = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $user = CommonHelper::getUserIndentity();
        $results = [];

        $sql_list = "call helper_hr_project_client(10, 0, '".$user->id."', '', '', 0, '".$q."');";
        $list = Yii::$app->db->createCommand($sql_list)->queryAll();

        $results['results'] = [];
        foreach ($list as $data) {
            $array = array(
                'id' => $data['ClientCode'],
                'text' => $data['ClientCode'] . ' - ' . $data['ClientName'],
            );
            array_push($results['results'], $array);
        }

        return $results;
    }
    public function actionSearch_job_by_user_partner($q = null, $id = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $user = CommonHelper::getUserIndentity();
        $results = [];

        $sql_list = "call helper_hr_project_job(10, 0, '".$user->id."', '', '', 0, '".$q."');";
        $list = Yii::$app->db->createCommand($sql_list)->queryAll();

        $results['results'] = [];
        foreach ($list as $data) {
            $array = array(
                'id' => $data['JobCode'],
                'text' => $data['JobCode'] . ' - ' . $data['ClientName'] . ' - ' . $data['Description'],
            );
            array_push($results['results'], $array);
        }

        return $results;
    }

    public function actionSearch_manager_by_user_partner($q = null, $id = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $user = CommonHelper::getUserIndentity();
        $results = [];

        $sql_list = "call helper_hr_project_manager(10, 0, '".$user->id."', '', '".$q."');";
        $list = Yii::$app->db->createCommand($sql_list)->queryAll();

        $results['results'] = [];
        foreach ($list as $data) {
            $array = array(
                'id' => $data['ManagerInitial'],
                'text' => $data['ManagerInitial'] . ' - ' . $data['ManagerName'],
            );
            array_push($results['results'], $array);
        }

        return $results;
    }

    public function actionSearch_client_by_user_manager($q = null, $id = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $user = CommonHelper::getUserIndentity();
        $results = [];

        // $sql_list = "call helper_hr_project_client(10, 0, '', '".$user->id."', '', 0, '".$q."');";
        $sql_list = "call search_client('manager','".$user->id."','".$q."',10,0);";
        $list = Yii::$app->db->createCommand($sql_list)->queryAll();

        $results['results'] = [];
        foreach ($list as $data) {
            $array = array(
                'id' => $data['client_code'],
                'text' => $data['client_code'] . ' - ' . $data['client_name'],
            );
            array_push($results['results'], $array);
        }

        return $results;
    }
    public function actionSearch_job_by_user_manager($q = null, $id = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $user = CommonHelper::getUserIndentity();
        $results = [];

        // $sql_list = "call helper_hr_project_job(10, 0, '', '".$user->id."', '', 0, '".$q."');";
        $sql_list = "call search_job('manager','".$user->id."','".$q."',10,0);";
        $list = Yii::$app->db->createCommand($sql_list)->queryAll();

        $results['results'] = [];
        foreach ($list as $data) {
            $array = array(
                'id' => $data['job_code'],
                // 'text' => $data['JobCode'] . ' - ' . $data['ClientName'] . ' - ' . $data['Description'],
                'text' => $data['job_client_description'],
            );
            array_push($results['results'], $array);
        }

        return $results;
    }

}
