<?php

namespace backend\modules\cm\controllers;

use Yii;
use yii\helpers\Html;
use common\components\HelperDB;

use common\models\cm\Options;
use common\models\cm\search\OptionsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\MethodNotAllowedHttpException;

/**
 * OptionsController implements the CRUD actions for Options model.
 */
class OptionsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new OptionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


     public function actionForm($id = null, $action=null)
     {
         if (Yii::$app->request->isAjax) {
             $post = Yii::$app->request->post();

             $model = new Options();
             $model->load($post);
             if (!empty($id)) {
                 $model = $this->findModel($id);

                 if($action=="duplicate"){
                     $model->Seq = null;
                     $model->isNewRecord;
                     $model->scenario = 'new';
                 }
             } else {
                 $model = new Options();
             }

             return $this->renderAjax('_form', [
                 'model' => $model,
                 'id'=> ($action=="duplicate") ? "" : $id,
             ]);
         } else {
             throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
         }
     }


     public function actionSave($id = null)
     {
         $model = empty($id) ? new Options() : $this->findModel($id);
         return HelperDB::saveAjax($model, Yii::$app->request->post());
     }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Options::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
