<?php

namespace backend\modules\cm\controllers;

use Yii;
use common\components\HelperDB;
use common\models\cm\Dept;
use common\models\cm\search\Dept as DeptSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\MethodNotAllowedHttpException;

/**
 * LevelController implements the CRUD actions for Dept model.
 */
class DeptController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return HelperDB::set_behaviors();
    }

    public function actionIndex()
    {
        return HelperDB::set_index($this, new DeptSearch());
    }

    public function actionForm($id = null, $action=null)
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();

            $model = new Dept();
            $model->load($post);
            if (!empty($id)) {
                $model = $this->findModel($id);

                if($action=="duplicate"){
                    $model->id = null;
                    $model->isNewRecord;
                }
            } else {
                $model = new Dept();
            }

            return $this->renderAjax('_form', [
                'model' => $model,
                'id'=> ($action=="duplicate") ? "" : $id,
            ]);
        } else {
            throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
        }
    }

    public function actionSave($id = null)
    {
        $model = empty($id) ? new Dept() : $this->findModel($id);
        return HelperDB::saveAjax($model, Yii::$app->request->post());
    }


    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }
    protected function findModel($id)
    {
        if (($model = Dept::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
