<?php

namespace backend\modules\cm\controllers;

use Yii;
use yii\helpers\Html;
use common\components\HelperDB;

use common\models\cm\Entity;
use common\models\cm\search\Entity as EntitySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\MethodNotAllowedHttpException;
use common\components\CommonHelper;

class EntityController extends Controller
{

    public function behaviors()
    {
        return HelperDB::set_behaviors();
    }

    public function actionIndex()
    {
        return HelperDB::set_index($this, new EntitySearch());
    }

    public function actionForm($id = null, $action = null)
    {
        Yii::$app->cache->flush();
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();

            $model = new Entity();
            $model->load($post);
            if (!empty($id)) {
                $model = $this->findModel($id);

                if ($action == "duplicate") {
                    $model->id = null;
                    $model->entityCode = null;
                    $model->isNewRecord;
                }
            } else {
                $model = new Entity();
            }

            return $this->renderAjax('_form', [
                'model' => $model,
                'id' => ($action == "duplicate") ? "" : $id,
            ]);
        } else {
            throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
        }
    }

    public function actionSave($id = null)
    {
        $model = empty($id) ? new Entity() : $this->findModel($id);
        return HelperDB::saveAjax($model, Yii::$app->request->post());
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Entity::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
