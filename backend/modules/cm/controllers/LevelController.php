<?php

namespace backend\modules\cm\controllers;

use Yii;
use common\models\cm\Level;
use common\models\st\RuleTaskAE;
use common\models\st\TaskLevel;
use common\models\cm\search\Level as levelSearch;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\CommonHelper;
use common\components\HelperDB;

/**
 * LevelController implements the CRUD actions for level model.
 */
class LevelController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new levelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionForm($id = null, $action=null)
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();

            $model = new Level();
            $model->load($post);
            if (!empty($id)) {
                $model = $this->findModel($id);
                $oldid = $model->id;

                if($action=="duplicate"){
                    $model->id = null;
                    $model->isNewRecord;
                }

                $modelDetail = new RuleTaskAE();
                $tasks = TaskLevel::find()->where(["level_id" => $id])->orderBy('task_id')->all();

                // $tasks = TaskLevel::find()->where(["level_id" => $id])->all();
                // $sql = "call getRuleTaskAE(".$oldid.")";
                // $tasks = Yii::$app->db->createCommand($sql)->queryAll();
            } else {
                $model = new Level();
                $modelDetail = new RuleTaskAE();
                $tasks = TaskLevel::find()->where(["level_id" => null])->orderBy('task_id')->all();

                // $tasks = TaskLevel::find()->where(["level_id" => null])->all();

                // $sql = "call getRuleTaskAE(null)";
                // $tasks = Yii::$app->db->createCommand($sql)->queryAll();
            }

            $tmpTasks = [];
            $i = 0;
            foreach($tasks as $task){
                $tmpTasks[$i]['task_id'] = $task['task_id'];
                $tmpTasks[$i]['task_name'] = $task['task_name'];
                $tmpTasks[$i]['percentage'] = $task['percentage'];
                $i++;
            }

            // foreach($tasks as $task){
            //     $tmpTasks[$i]['taskID'] = $task['task_id'];
            //     $tmpTasks[$i]['taskName'] = $task['task_name'];
            //     $tmpTasks[$i]['percentage'] = $task['percentage'];
            //     $i++;
            // }
            $modelDetail->TabularInput = $tmpTasks;

            return $this->renderAjax('_form', [
                'model' => $model,
                'modelDetail' => $modelDetail,
                'id'=> ($action=="duplicate") ? "" : $id,
            ]);
        } else {
            throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
        }
    }

    public function actionSave()
    {
        $result = HelperDB::result_default();

        $model = new Level();
        $modelDetail = new RuleTaskAE();

        $post = Yii::$app->request->post();
        if ($model->load($post)) {
            $modelFind = Level::findOne($model->id);
            if ($modelFind !== null) {
                $model = $modelFind;
            }

            $model->load($post);
            $modelDetail->load($post);
            $valid = $model->validate();
            if ($valid) {
                $tasks = $modelDetail->TabularInput;
                $total_percentage = 0;
                foreach($tasks as $task){
                    $total_percentage += !empty($task['percentage']) ? $task['percentage'] : 0;
                }


                if($total_percentage == 100){
                    $transaction = \Yii::$app->db->beginTransaction();
                    try {
                        if (!($flag = $model->save(false))) {
                            $transaction->rollBack();
                            $result['state']['status'] = false;
                            $result['state']['message'] = 'error : rollback';
                        } else {
                            $tasks = $modelDetail->TabularInput;
                            foreach($tasks as $task){
                              $task['percentage'] = !empty($task['percentage']) ? $task['percentage'] : 0;
                               $modelDetail = RuleTaskAE::find()->where([
                                   'level_id' => $model->id,
                                   'task_id' => $task['task_id']
                               ])->one();

                               if(empty($modelDetail->task_id)){
                                   $modelDetail = new RuleTaskAE();
                               }
                               $modelDetail->level_id = $model->id;
                               $modelDetail->task_id = $task['task_id'];
                               $modelDetail->percentage = CommonHelper::ResetDecimal($task['percentage']);

                               if (!($flag = $modelDetail->save(false))) {
                                   $transaction->rollBack();
                                   $result['state']['status'] = false;
                                   $result['state']['message'] = 'error : when saving target data';
                               }
                                // $task['percentage'] = !empty($task['percentage']) ? $task['percentage'] : 0;
                                // $modelDetail = RuleTaskAE::find()->where([
                                //     'levelID' => $model->id,
                                //     'taskID' => $task['taskID']
                                // ])->one();
                                //
                                // if(empty($modelDetail->taskID)){
                                //     $modelDetail = new RuleTaskAE();
                                // }
                                // $modelDetail->levelID = $model->id;
                                // $modelDetail->taskID = $task['taskID'];
                                // $modelDetail->percentage = CommonHelper::ResetDecimal($task['percentage']);
                                //
                                // if (!($flag = $modelDetail->save(false))) {
                                //     $transaction->rollBack();
                                //     $result['state']['status'] = false;
                                //     $result['state']['message'] = 'error : when saving target data';
                                // }

                            }
                        }

                        if ($flag) {
                            $transaction->commit();
                            $result['state']['status'] = true;
                            $result['state']['message'] = 'success';
                        }
                    } catch (\yii\db\Exception $e) {
                        $result['state']['status'] = false;
                        $result['state']['message'] = static::db_trigger_error($e->getMessage());
                        $transaction->rollBack();
                    }
                }else{
                    $result['state']['status'] = false;
                    $result['state']['message'] = 'Total Current Percentage is '.$total_percentage.'%, Total Percentage must be 100%';
                }

            } else {
                $result['state']['status'] = false;
                $result['state']['message'] = static::db_field_error(Html::errorSummary($model));
            }
        } else {
            $result = HelperDB::result_noloaded();
        }

        return json_encode($result['state']);
    }
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    protected function findModel($id)
    {
        if (($model = Level::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
