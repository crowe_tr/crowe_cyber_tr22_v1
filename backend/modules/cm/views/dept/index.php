
<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\web\View;
use common\models\cm\Division;
use common\components\CmHelper;

CmHelper::set_title($this, 'Department', 'Setup');
// $this->title = 'DEPARTMENT';
// $this->params['breadcrumbs'][] = 'SETUP';
// $this->params['breadcrumbs'][] = $this->title;
// $this->params['breadcrumbs_btn'] = Html::a(
//     '<i class="pg-plus"></i> <span class="hidden-xs">CREATE NEW</span>',
//     false,
//     [
//       'onclick' => 'FormCreate()',
//       'class' => 'btn btn-warning text-white ',
//     ]
//   );

$this->params['crud_ajax'] = true;

$column = [
              CmHelper::set_column_no(),
              CmHelper::set_column('dept_code', 'Dept Code', 2),
              CmHelper::set_column('dept_name', 'Dept Name', 4),
              CmHelper::set_column_search('div_id', 'Division', 4,
                      ArrayHelper::map(Division::find()->distinct()->all(), 'id', 'div_name'),
                      'div.div_name'
                  ),
              CmHelper::set_column_search('Flag', 'Status', 2,
                      [1 => 'Active' , 0 => 'Non Active' ],
                      function($model){
                            if ($model->flag == 1) {
                              return 'Active';
                            }
                            else{
                              return 'Non Active';
                            }
                      }
                  ),
              [
                  'header' => '<i class="fa fa-cogs"></i>',
                  'headerOptions' => ['class' => 'bg-success text-center'],
                  'filterOptions' => ['class' => 'b-b b-grey'],
                  'contentOptions' => ['class' => 'no-padding'],
                  'format' => 'raw',
                  'value' => function($data){
                    return CmHelper::set_icon_cm($data->id, 'dept');
                  },
              ],
          ];

          \yii\widgets\Pjax::begin(['id' => 'crud-pjax', 'enablePushState' => false]); //1. perhatiin idnya
          echo CmHelper::widget_pjax();
          echo GridView::widget( CmHelper::grid_view($dataProvider, $searchModel, $column) );
          \yii\widgets\Pjax::end();

          $js = CmHelper::set_script('dept');
          $this->registerjs($js, View::POS_END);
?>
