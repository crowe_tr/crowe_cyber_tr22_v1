<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use common\models\cm\Division;



$form = ActiveForm::begin([
  'id' => 'crud-form',
  'enableClientValidation' => true,
  'validateOnSubmit' => true,
  'validateOnChange' => true,
  'validateOnType' => true,
  'fieldConfig' => [
    'template' => '{label}{input}',
    'options' => [
      'class' => 'form-group form-group-default',
    ],
  ],
  'errorSummaryCssClass' => 'alert alert-danger'
]);
echo $form->errorSummary($model);;
echo $form->field($model, 'id', ['options' => ['class' => '']])->hiddenInput()->label(false);

?>
<div class="alert alert-danger" id="error" style="display:none"></div>

<div class="row">
  <div id="error-summary" class="col-md-12 ">

  </div>
</div>
<div class="form-group-attached">
  <div class="row">
    <div class="col-md-8">
      <?php
      echo $form->field($model, 'div_id', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
        Select2::classname(),
        [
          'data' => ArrayHelper::map(Division::find()->all(), 'id', 'div_name'),
          'options' => ['id' => 'div_name', 'placeholder' => 'Select ...'],
        ]
      );
      ?>
    </div>
    <div class="col-md-4">
      <?= $form->field($model, 'dept_code')->textInput(['maxlength' => true]) ?>
    </div>
  </div>
  <div class="row">
    <div class="col-md-8">
      <?= $form->field($model, 'dept_name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-4">
      <?php $model->flag = 1; ?>
      <?= $form->field($model, 'flag')->checkbox()->label('status'); ?>
    </div>
  </div>
</div>
<br />
<div class="row">
  <div class="col-md-12 text-right m-t-5">
    <hr class="m-b-5" />
    <?= Html::submitButton('UPDATE', ['class' => 'btn btn-warning p-t-10 p-b-10']) ?>
    <button type="button" class="btn btn-info p-t-10 p-b-10" style="font-size: 12px" data-dismiss="modal">CANCEL</button>
  </div>
</div>

<?php ActiveForm::end();  ?>
<script type="text/javascript">
  $('#crud-form').on('beforeSubmit', function() {
    var url = '<?= Yii::$app->urlManager->createAbsoluteUrl(['cm/dept/save', 'id' => $id]); ?>';
    var form_data = new FormData($('#crud-form')[0]);
    return HelperSaveAjax(url, form_data);
  });
</script>
