<?php
  use yii\helpers\Html;
  use yii\helpers\ArrayHelper;
  use kartik\grid\GridView;
  use yii\helpers\Url;
  use yii\web\View;
  use common\components\CmHelper;

  CmHelper::set_title($this, 'Division', 'Data');
  $column = [
              // CmHelper::set_column_no(),
              // CmHelper::set_column('divCode', 'Division Code', 3),
              // CmHelper::set_column('divName', 'Division Name', 9),
              // [
              //     'header' => '<i class="fa fa-cogs"></i>',
              //     'headerOptions' => ['class' => 'bg-success text-center'],
              //     'filterOptions' => ['class' => 'b-b b-grey'],
              //     'contentOptions' => ['class' => 'no-padding'],
              //     'format' => 'raw',
              //     'value' => function($data){
              //       return CmHelper::set_icon($data->id, 'division');
              //     },
              // ],

              CmHelper::set_column_no(),
              CmHelper::set_column('div_code', 'Division Code', 3),
              CmHelper::set_column('div_name', 'Division Name', 7),
              CmHelper::set_column('recovery_rate', 'Min. Recovery Rate', 2, null, 'text-center'),
              // CmHelper::set_column_bool('overtime_project','2',1),
              [
                  'header' => '<i class="fa fa-cogs"></i>',
                  'headerOptions' => ['class' => 'bg-success text-center'],
                  'filterOptions' => ['class' => 'b-b b-grey'],
                  'contentOptions' => ['class' => 'no-padding'],
                  'format' => 'raw',
                  'value' => function($data){
                    return CmHelper::set_icon_cm($data->id, 'division');
                  },
              ],

          ];
  \yii\widgets\Pjax::begin(['id' => 'crud-pjax', 'enablePushState' => false]); //1. perhatiin idnya
  echo CmHelper::widget_pjax();
  echo GridView::widget( CmHelper::grid_view($dataProvider, $searchModel, $column) );
  \yii\widgets\Pjax::end();

  $js = CmHelper::set_script('division');
  $this->registerjs($js, View::POS_END);
?>
