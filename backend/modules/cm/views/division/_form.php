<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;


/* @var $this yii\web\View */
/* @var $model common\models\trms\Division */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Create Division';
$this->params['breadcrumbs'][] = ['label' => 'Entities', 'url' => ['index']];

$form = ActiveForm::begin([
  'id' => 'crud-form', //perhatiin
  'enableClientValidation' => true,
  'validateOnSubmit' => true,
  'validateOnChange' => true,
  'validateOnType' => true,
  'fieldConfig' => [
      'template' => '{label}{input}',
      'options' => [
          'class' => 'form-group form-group-default',
      ],
],
'errorSummaryCssClass'=> 'alert alert-danger'
]);
echo $form->errorSummary($model);;
?>

<div class="alert alert-danger" id="error" style="display:none"></div>

<div class="form-group-attached">
  <div class="row">
    <div class="col-md-2">
      <?= $form->field($model, 'div_code')->label('Code')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-6">
      <?= $form->field($model, 'div_name')->label('Division Name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-4">
      <?= $form->field($model, 'recovery_rate')->label('Min. Recovery Rate')->textInput(['type' => 'number', 'min' => 0, 'max' => 100]) ?>
    </div>
  </div>
</div>
<br/>

<div class="row">
<div class="col-md-12 text-right m-t-5">
  <hr class="m-b-5"/>
  <?= Html::submitButton('UPDATE', ['class' => 'btn btn-warning p-t-10 p-b-10']) ?>
  <button type="button" class="btn btn-info p-t-10 p-b-10" style="font-size: 12px" data-dismiss="modal">CANCEL</button>
</div>
</div>

<?php ActiveForm::end(); ?>


<script type="text/javascript">
  $('#crud-form').on('beforeSubmit', function() {
    var url = '<?= Yii::$app->urlManager->createAbsoluteUrl(['cm/division/save', 'id' => $id]); ?>';
    var form_data = new FormData($('#crud-form')[0]);
    return HelperSaveAjax(url, form_data);
  });
</script>
