<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\web\View;
use common\models\st\RuleTaskAE;


$this->title = 'Level';
$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'][] = "DATA";
$this->params['breadcrumbs_btn'] = Html::a(
                                    '<i class="pg-plus"></i> <span class="hidden-xs">CREATE NEW</span>',
                                    false,
                                    [
                                      'onclick' => 'FormCreate()',
                                      'class' => 'btn btn-warning text-white ',
                                    ]
                                  );

$column = [
        [
          'class' => 'kartik\grid\ExpandRowColumn',
          'enableRowClick' => true,
          'mergeHeader' => false,
          'headerOptions' => ['class' => 'kartik-sheet-style b-l b-r  b-success bg-success'],
          'filterOptions' => ['class' => ''],
          'contentOptions' => ['class' => 'kv-align-middle bg-warning-lighter b-l b-r b-grey', 'style' => 'height: 50px'],
          'expandOneOnly' => true,
          'width' => '50px',
          'expandIcon'=>'<i class="fa fa-angle-right"></i>',
          'collapseIcon'=>'<i class="fa fa-angle-up"></i>',
          'value' => function ($model, $key, $index, $column) {
              return GridView::ROW_COLLAPSED;
          },
          'detail' => function ($model, $key, $index, $column) {
            $taskaes = RuleTaskAE::find()->where(['level_id'=>$model->id])->all();
            if(!empty($taskaes)){
              $html = '
              <div class="row">
                <div class="col-md-6">
                  <table class="table table-default kv-grid-table table table-hover kv-table-wrap">
                    <tr>
                      <th class="bg-primary">
                        Task Name
                      </th>
                      <th class="bg-primary">
                        Percentage
                      </th>
                    </tr>';
                    foreach($taskaes as $taskae){
                      $html .= '<tr>
                                  <td>'.$taskae->task->task_name.'</td>
                                  <td>'.$taskae->percentage.' %</td>
                                </tr>';

                      // $html .= '<tr>
                      //             <td>'.$taskae->task->taskName.'</td>
                      //             <td>'.$taskae->percentage.' %</td>
                      //           </tr>';
                    }
              $html .='
                  </div>
                </div>
              </table>
              ';
            }else{
              $html = "empty data";
            }

              return $html;
          },
        ],
        [
            'attribute' => 'level_code',
            'headerOptions' => ['class' => 'col-sm-2 bg-success'],
            'filterOptions' => ['class' => 'b-b b-grey'],
            'contentOptions' => ['class' => 'kv-align-middle'],
            'value'=>function($data){
              return empty($data->level_code) ? "" : $data->level_code;
            }
        ],
        [
            'attribute' => 'level_name',
            'headerOptions' => ['class' => 'col-sm-5 bg-success'],
            'filterOptions' => ['class' => 'b-b b-grey'],
            'contentOptions' => ['class' => 'kv-align-middle'],
            'value'=>function($data){
              return empty($data->level_name) ? "" : $data->level_name;
            }
        ],
        [
            'attribute' => 'level_notes',
            'headerOptions' => ['class' => 'col-sm-5 bg-success'],
            'filterOptions' => ['class' => 'b-b b-grey'],
            'contentOptions' => ['class' => 'kv-align-middle'],
            'value'=>function($data){
              return empty($data->level_notes) ? "" : $data->level_notes;
            }
        ],
        [
            'header' => '',
            'headerOptions' => ['class' => 'bg-success text-center'],
            'filterOptions' => ['class' => 'b-b b-grey'],
            'contentOptions' => ['class' => 'no-padding'],

            'format' => 'raw',
            'value' => function ($data) {
                $val = '<div class="tooltip-action">
                          <div class="trigger">
                            '.Html::a('<i class="fa fa-ellipsis-h"></i>', false, ['class' => 'text-info', 'data-toggle' => 'tooltip', 'data-original-title' => 'Up here!']).'
                          </div>
                          <div class="action-mask">
                            <div class="action">
                              '.Html::a(
                                '<i class="fa fa-edit"></i>',
                                false,
                                [
                                    'onclick' => "FormUpdate('".Yii::$app->urlManager->createAbsoluteUrl(['cm/level/form', 'id' => $data->id])."')",
                                    'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
                                    'style' => 'bjob-radius: 5px !important',
                                ]
                              ).'
                              '.Html::a(
                                '<i class="fa fa-copy"></i>',
                                false,
                                [
                                    'onclick' => "FormDuplicate('".Yii::$app->urlManager->createAbsoluteUrl(['cm/level/form', 'id' => $data->id, 'action'=>'duplicate'])."')",
                                    'class' => 'btn btn-info text-white padding-5 p-l-10 p-r-10',
                                    'style' => 'bjob-radius: 5px !important',
                                ]
                              ).'
                            '.Html::a('<i class="pg-trash"></i>',
                                                    ['delete', 'id' => $data->id],
                                                    [
                                                      'data-method' => 'post',
                                                      'data-confirm' => 'Are you sure to delete this item?',
                                                      'class' => 'btn btn-danger text-white padding-5 p-l-10 p-r-10',
                                                    ]).'
                            </div>
                          </div>
                        </div>';

                return $val;
            },
        ],
    ];
?>
<?php \yii\widgets\Pjax::begin(['id' => 'crud-pjax', 'enablePushState' => false]); //1. perhatiin idnya ?>
    <div class="modal fade stick-up " id="crud-modal" role="dialog" aria-hidden="false" data-keyboard="false" data-backdrop="static">
    	<div class="modal-dialog modal-lg ">
    		<div class="modal-content no-border">
    			<div class="modal-header bg-success text-white">
    			</div>
    			<div id="detail" class="modal-body padding-20">
    			</div>
    		</div>
    	</div>
    </div>

    <?php
    echo GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $column,
            'layout'=>'
                <div class="card card-default">
                    <div class="row ">
                    <div class="col-md-12">
                        {items}
                    </div>
                    </div>
                    <div class="row padding-10">
                    <div class="col-md-4">{summary}</div>
                    <div class="col-md-8">{pager}</div>
                    </div>
                </div>
                ',
            'emptyText' => '
                    <div class="text-center" style="padding: 2em 0">
                      <i class="fa fa-exclamation-circle fa-5x text-warning"></i>
                      <br>
                      <br>
                      '.Yii::t('backend', 'You do not have any data within your Filters.').'
                      <br>
                      '.Yii::t('backend', 'To create a new data, click <b><i class="icon-plus-circle"></i> ADD NEW</b> on the right top of corner').'
                  </div>',
            'resizableColumns' => true,
            'bordered' => false,
            'striped' => false,
            'condensed' => false,
            'responsive' => false,
            'hover' => true,
            'persistResize' => false,
        ]
    );
    ?>
<?php \yii\widgets\Pjax::end(); ?>

<script type="text/javascript">
  var paramJs = (paramJs || {});
  paramJs.urlFormShowModal = '<?= Yii::$app->urlManager->createAbsoluteUrl(['/cm/level/form']); ?>';

	function FormShowModal(link="") {
    $('#detail').html("loading ...");

		var link = (link || paramJs.urlFormShowModal);
		$.ajax({
			url: link,
			data:  $('#crud-form').serialize(),
			method: "POST",
			dataType: 'html',
			success: function(data) {
				$('#detail').html(data);
			},
		});
	}

	function FormCreate() {
		FormShowModal();
		$('#crud-modal').modal('show');
	}
	function FormUpdate(link) {
		FormShowModal(link);
		$('#crud-modal').modal('show');
	}
	function FormDuplicate(link) {
		FormShowModal(link);
		$('#crud-modal').modal('show');
	}

	window.closeModal = function(){
		reload();
	};
	function reload(){
		$.pjax.defaults.timeout = false;
		$.pjax.reload({
			container: '#crud-pjax'
		})
		$('#crud-form').modal('hide');
		$('#crud-form').data('modal', null);
    $('.modal-backdrop').modal('hide');
	}

</script>
