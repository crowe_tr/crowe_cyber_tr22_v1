<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use unclead\multipleinput\MultipleInput;
use kartik\widgets\Select2;
use yii\widgets\MaskedInput;

$this->title = 'Create Entity';
$this->params['breadcrumbs'][] = ['label' => 'Entities', 'url' => ['index']];

$form = ActiveForm::begin([
  'id' => 'crud-form', //perhatiin
  'enableClientValidation' => true,
  'validateOnSubmit' => true,
  'validateOnChange' => true,
  'validateOnType' => true,
  'fieldConfig' => [
    'template' => '{label}{input}',
    'options' => [
      'class' => 'form-group form-group-default',
    ],
  ],
  'errorSummaryCssClass' => 'alert alert-danger'
]);
echo $form->errorSummary($model);
echo $form->field($model, 'id', ['options' => ['class' => '']])->hiddenInput()->label(false);

?>
<div class="alert alert-danger" id="error" style="display:none"></div>

<div class="form-group-attached">
  <div class="row">
    <div class="col-md-2">
      <?= $form->field($model, 'level_code')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-5">
      <?= $form->field($model, 'level_name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-5">
      <?= $form->field($model, 'level_notes')->textInput(['maxlength' => true]) ?>
    </div>
  </div>
</div>
<br />
<div class="row">
  <div class="col-md-12">
    <?php
    echo $form->field($modelDetail, 'TabularInput', ['options' => ['class' => 'table']])->widget(
      MultipleInput::className(),
      [
        'iconSource' => 'fa',
        'theme' => 'default',
        'sortable' => false,
        'addButtonPosition' => MultipleInput::POS_HEADER,
        'removeButtonOptions' => [
          'class' => 'btn btn-danger btn-sm',
        ],
        'addButtonOptions' => [
          'class' => 'btn btn-warning btn-sm',
        ],
        'columns' => [
          [
            'name'  => 'task_id',
            'title' => '',
            'value' => function ($data) {
              return !empty($data['task_id']) ? $data['task_id'] : "";
            },
            'options' => [
              'class' => 'hidden',
              'style' => 'color: black;'
            ],
            'columnOptions' => [
              'class' => '',
              'style' => 'padding:0 !Important; border:0',

            ],
            'headerOptions' => [
              'class' => '',
              'style' => 'padding:0 !Important; border:0',
              'width' => '10px'
            ]
          ],
          [
            'name'  => 'task_name',
            'title' => 'TASK NAME',
            'value' => function ($data) {
              return !empty($data['task_name']) ? $data['task_name'] : "";
            },
            'options' => [
              'class' => 'form-control',
              'disabled' => true,
              'style' => 'color: black;'
            ],
            'columnOptions' => [
              'class' => 'col-md-9',

            ],
            'headerOptions' => [
              'class' => 'bg-info text-white text-center'
            ]

          ],
          [
            'name'  => 'percentage',
            'title' => 'PERCENTAGE',
            'type' => MaskedInput::className(),
            'value' => function ($data) {
              return !empty($data['percentage']) ? $data['percentage'] : 0;

            },
            'options' => [
              'clientOptions' => [
                'alias' => 'decimal',
                'groupSeparator' => '',
                'autoGroup' => true,
              ],
              'options' => [
                'class' => 'form-control',
              ]
            ],
            'columnOptions' => [
              'class' => 'col-md-3',

            ],
            'headerOptions' => [
              'class' => 'bg-info text-white text-center'
            ]

          ],

        ]
      ]
    )->label(false);
    ?>
  </div>
</div>

<div class="row">
  <div class="col-md-12 text-right m-t-5">
    <hr class="m-b-5" />
    <?= Html::submitButton('UPDATE', ['class' => 'btn btn-warning p-t-10 p-b-10']) ?>
    <button type="button" class="btn btn-info p-t-10 p-b-10" style="font-size: 12px" data-dismiss="modal">CANCEL</button>
  </div>
</div>

<?php ActiveForm::end(); ?>

<script type="text/javascript">
  $('#crud-form').on('beforeSubmit', function() {
    var url = '<?= Yii::$app->urlManager->createAbsoluteUrl(['cm/level/save', 'id' => $id]); ?>';
    var form_data = new FormData($('#crud-form')[0]);
    return HelperSaveAjax(url, form_data);
  });
</script>
