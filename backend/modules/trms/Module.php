<?php

namespace backend\modules\trms;

class Module extends \yii\base\Module
{

    public $controllerNamespace = 'backend\modules\trms\controllers';

    public function init()
    {
        parent::init();
    }
}
