<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\trms\AreaEffective */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="area-effective-form">

  <?php
  // $form = ActiveForm::begin();
  $form = ActiveForm::begin([
          'id' => 'job-form',
          'enableClientValidation' => true,
          'validateOnSubmit' => true,
          'validateOnChange' => true,
          'validateOnType' => true,
          'fieldConfig' => [
              'template' => '{label}{input}',
              'options' => [
                  'class' => 'form-group form-group-default',
              ],
          ],
      ]);

  ?>
  <div class="card card-default m-b-10">
    <div class="card-boy">
      <div class="form-group-attached">
        <div class="row">
          <div class="col-md-3">
            <?=
							$form->field($model, 'effectivedate', [
								'template' => '{label}{input}', 'options' => ['onchange' => 'ReqSave()', 'class' => 'form-group form-group-default input-group'],
							]
							)->widget(DatePicker::classname(), [
								'type' => DatePicker::TYPE_INPUT,
								'value' => date('Y-m-d'),
								'pluginOptions' => [
									'autoclose' => true,
									'format' => 'yyyy-mm-dd',
								],
							])
						?>
          </div>
          <div class="col-md-9">

              <?php

              echo $form->field($model, 'description')->textInput(['maxlength' => true])
              ?>
          </div>
        </div>
      </div>
    </div>
  </div>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
