<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\trms\BillingRateDetail */

$this->title = 'Update Billing Rate Detail: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Billing Rate Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="billing-rate-detail-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
