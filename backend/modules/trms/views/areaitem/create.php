<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\trms\AreaItem */

$this->title = 'Create Area Item';
$this->params['breadcrumbs'][] = ['label' => 'Area Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="area-item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
