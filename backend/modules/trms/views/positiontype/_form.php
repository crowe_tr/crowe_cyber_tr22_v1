<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\trms\PositionType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="position-type-form">

  <?php
  // $form = ActiveForm::begin();
  $form = ActiveForm::begin([
          'id' => 'job-form',
          'enableClientValidation' => true,
          'validateOnSubmit' => true,
          'validateOnChange' => true,
          'validateOnType' => true,
          'fieldConfig' => [
              'template' => '{label}{input}',
              'options' => [
                  'class' => 'form-group form-group-default',
              ],
          ],
      ]);

  ?>

  <div class="card card-default m-b-10">
    <div class="card-boy">
      <div class="form-group-attached">
        <div class="row">
          <div class="col-md-9">
            <?php
            echo   $form->field($model, 'name')->textInput(['maxlength' => true])
            ?>
          </div>
          <div class="col-md-3">
              <?php
                echo $form->field($model, 'suspended')->checkbox()->label('status');
              // echo $form->field($model, 'suspended')->dropdownList([
              //         1 => 'Actve',
              //         0 => 'Non Active'
              //     ],
              //     ['prompt'=>'Select suspended']
              // );

              ?>
          </div>
        </div>
      </div>
    </div>
  </div>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
