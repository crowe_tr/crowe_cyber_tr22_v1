<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\trms\AreaZone */

$this->title = 'Create Area Zone';
$this->params['breadcrumbs'][] = ['label' => 'Area Zones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="area-zone-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
