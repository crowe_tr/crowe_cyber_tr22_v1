<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\trms\RuleField */

$this->title = 'Update Rule Field: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Rule Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rule-field-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
