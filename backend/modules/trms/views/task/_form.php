<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use common\models\trms\TaskType;

/* @var $this yii\web\View */
/* @var $model common\models\trms\Task */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-form">

  <?php
  $form = ActiveForm::begin([
          'id' => 'job-form',
          'enableClientValidation' => true,
          'validateOnSubmit' => true,
          'validateOnChange' => true,
          'validateOnType' => true,
          'fieldConfig' => [
              'template' => '{label}{input}',
              'options' => [
                  'class' => 'form-group form-group-default',
              ],
          ],
      ]);

  ?>

  <div class="card card-default m-b-10">
    <div class="card-boy">
      <div class="form-group-attached">
        <div class="row">
          <div class="col-md-8">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
          </div>
          <div class="col-md-4">
            <?php
                echo $form->field($model, 'taskType', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
                        Select2::classname(),
                        [
                                'data' => ArrayHelper::map(TaskType::find()->all(), 'id', 'taskTypeName'),
                                'options' => ['id' => 'taskType', 'placeholder' => 'Select ...'],
                        ]
                );
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
