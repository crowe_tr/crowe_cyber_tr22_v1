<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $searchModel common\models\trms\search\Rule */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rules';
$this->params['breadcrumbs'][] = $this->title;

$column = [
            [
              'class' => 'kartik\grid\SerialColumn',
              'header' => '#',
              'mergeHeader' => false,
              // 'rowspan' => '1',
              // 'attribute' => 'id',
              'headerOptions' => ['class' => 'bg-success b-r'],
              'contentOptions' => ['class' => 'text-right'],
              'width' => '36px',
              'filterOptions' => ['class' => 'b-b b-grey'],
                // 'filterType' => GridView::FILTER_SELECT2,
                // 'filterWidgetOptions' => [
                //     'pluginOptions' => ['allowClear' => true],
                // ],
                // 'filterInputOptions' => ['placeholder' => ''],
                // 'format' => 'raw',
                // 'filter' => ArrayHelper::map(CmDept::find()->distinct()->all(), 'Id', 'Departement'),
                // 'value' => 'id',
            ],
            [
                'attribute' => 'name',
                'headerOptions' => ['class' => 'col-sm-5 bg-success'],
                'filterOptions' => ['class' => 'b-b b-grey'],
                'contentOptions' => ['class' => 'kv-align-middle'],
                 // 'dateControlDisplayTimezone'=>  'Europe/Rome',
            ],
            [
                'attribute' => 'ruleType',
                'headerOptions' => ['class' => 'col-sm-5 bg-success'],
                'filterOptions' => ['class' => 'b-b b-grey'],
                'contentOptions' => ['class' => 'kv-align-middle'],
            ],
            // [
            //     'attribute' => 'claimMeal',
            //     'headerOptions' => ['class' => 'col-sm-5 bg-success'],
            //     'filterOptions' => ['class' => 'b-b b-grey'],
            //     'contentOptions' => ['class' => 'kv-align-middle'],
            // ],
            // [
            //     'attribute' => 'useTaxi',
            //     'headerOptions' => ['class' => 'col-sm-5 bg-success'],
            //     'filterOptions' => ['class' => 'b-b b-grey'],
            //     'contentOptions' => ['class' => 'kv-align-middle'],
            // ],
            // [
            //     'attribute' => 'claimTransportation',
            //     'headerOptions' => ['class' => 'col-sm-5 bg-success'],
            //     'filterOptions' => ['class' => 'b-b b-grey'],
            //     'contentOptions' => ['class' => 'kv-align-middle'],
            // ],
            // [
            //     'attribute' => 'claimOutOffice',
            //     'headerOptions' => ['class' => 'col-sm-5 bg-success'],
            //     'filterOptions' => ['class' => 'b-b b-grey'],
            //     'contentOptions' => ['class' => 'kv-align-middle'],
            // ],
            // [
            //     'attribute' => 'day',
            //     'headerOptions' => ['class' => 'col-sm-5 bg-success'],
            //     'filterOptions' => ['class' => 'b-b b-grey'],
            //     'contentOptions' => ['class' => 'kv-align-middle'],
            // ],
            // [
            //     'attribute' => 'area',
            //     'headerOptions' => ['class' => 'col-sm-5 bg-success'],
            //     'filterOptions' => ['class' => 'b-b b-grey'],
            //     'contentOptions' => ['class' => 'kv-align-middle'],
            // ],
            // [
            //     'attribute' => 'workHour',
            //     'headerOptions' => ['class' => 'col-sm-5 bg-success'],
            //     'filterOptions' => ['class' => 'b-b b-grey'],
            //     'contentOptions' => ['class' => 'kv-align-middle'],
            // ],
            // [
            //     'attribute' => 'totalOvertimeHour',
            //     'headerOptions' => ['class' => 'col-sm-5 bg-success'],
            //     'filterOptions' => ['class' => 'b-b b-grey'],
            //     'contentOptions' => ['class' => 'kv-align-middle'],
            // ],
            // [
            //     'attribute' => 'itemOvertime',
            //     'headerOptions' => ['class' => 'col-sm-5 bg-success'],
            //     'filterOptions' => ['class' => 'b-b b-grey'],
            //     'contentOptions' => ['class' => 'kv-align-middle'],
            // ],
            // [
            //     'attribute' => 'isMealProvided',
            //     'headerOptions' => ['class' => 'col-sm-5 bg-success'],
            //     'filterOptions' => ['class' => 'b-b b-grey'],
            //     'contentOptions' => ['class' => 'kv-align-middle'],
            // ],
            // [
            //     'attribute' => 'isTransportationProvided',
            //     'headerOptions' => ['class' => 'col-sm-5 bg-success'],
            //     'filterOptions' => ['class' => 'b-b b-grey'],
            //     'contentOptions' => ['class' => 'kv-align-middle'],
            // ],
            // [
            //     'attribute' => 'isAccomodationProvided',
            //     'headerOptions' => ['class' => 'col-sm-5 bg-success'],
            //     'filterOptions' => ['class' => 'b-b b-grey'],
            //     'contentOptions' => ['class' => 'kv-align-middle'],
            // ],
            // [
            //     'attribute' => 'claimTransportationAmount',
            //     'headerOptions' => ['class' => 'col-sm-5 bg-success'],
            //     'filterOptions' => ['class' => 'b-b b-grey'],
            //     'contentOptions' => ['class' => 'kv-align-middle'],
            // ],
            [
                'attribute' => 'withOvertimeTransport',
                'headerOptions' => ['class' => 'col-sm-5 bg-success'],
                'filterOptions' => ['class' => 'b-b b-grey'],
                'contentOptions' => ['class' => 'kv-align-middle'],
            ],
            [
                'attribute' => 'applicablePosition',
                'headerOptions' => ['class' => 'col-sm-5 bg-success'],
                'filterOptions' => ['class' => 'b-b b-grey'],
                'contentOptions' => ['class' => 'kv-align-middle'],
            ],
            [
                'attribute' => 'source',
                'headerOptions' => ['class' => 'col-sm-5 bg-success'],
                'filterOptions' => ['class' => 'b-b b-grey'],
                'contentOptions' => ['class' => 'kv-align-middle'],
            ],
            [
                'attribute' => 'allowance',
                'headerOptions' => ['class' => 'col-sm-5 bg-success'],
                'filterOptions' => ['class' => 'b-b b-grey'],
                'contentOptions' => ['class' => 'kv-align-middle'],
            ],
            [
                'header' => '<i class="fa fa-cogs"></i>',
                'headerOptions' => ['class' => 'bg-success text-center'],
                'filterOptions' => ['class' => 'b-b b-grey'],
                'contentOptions' => ['class' => 'no-padding'],
                'format' => 'raw',
                'value' => function ($data) {
                    $val = '<div class="tooltip-action">
              <div class="trigger">
                '.Html::a('<i class="fa fa-ellipsis-h"></i>', false, ['class' => 'text-info', 'data-toggle' => 'tooltip', 'data-original-title' => 'Up here!']).'
              </div>
              <div class="action-mask">
                <div class="action">
                  '.Html::a('<i class="fa fa-edit"></i>', ['update', 'id' => $data->id], [
                                            'class' => 'btn btn-default b-rounded padding-5 p-l-10 p-r-10',
                                            'style' => 'border-radius: 5px !important',
                                        ]).'
                  '.Html::a('<i class="pg-trash"></i>',
                                        ['delete', 'id' => $data->id],
                                        [
                                        'data-method' => 'post',
                                        'data-confirm' => 'Are you sure to delete this item?',
                                        'class' => 'btn btn-default b-rounded padding-5 p-l-10 p-r-10',
                                        'style' => 'border-radius: 5px !important',
                                        ]).'
                </div>
              </div>
            </div>';

                    return $val;
                },
            ],

        ];
?>
<div class="panel panel-default p-t-20 p-b-10 p-l-20 p-r-20" style="background: white">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="row">
      <div class="col-md-6">
        <div class="pull-left">
          <h1><?= Html::encode($this->title) ?></h1>
        </div>
      </div>
      <div class="col-md-6">
        <div class="pull-right">
          <?= Html::a('<i class="pg-plus"></i> <span class="hidden-xs">ADD NEW</span>', ['create'], ['class' => 'btn btn-warning']) ?>
        </div>
      </div>
    </div>

    <?php
        echo GridView::widget(
            [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $column,
            'emptyText' => '
                    <div class="text-center" style="padding: 2em 0">
                      <i class="fa fa-exclamation-circle fa-5x text-warning"></i>
                      <br>
                      <br>
                      '.Yii::t('backend', 'You do not have any data within your Filters.').'
                      <br>
                      '.Yii::t('backend', 'To create a new data, click <b><i class="icon-plus-circle"></i> ADD NEW</b> on the right top of corner').'
                  </div>',



            'resizableColumns' => true,
            'bordered' => false,
            'striped' => false,
            'condensed' => false,
            'responsive' => false,
            'hover' => true,
            'persistResize' => false,
            ]
        );
    ?>
</div>
