<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\trms\Rule */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rule-form">

  <?php
  // $form = ActiveForm::begin();
  $form = ActiveForm::begin([
          'id' => 'job-form',
          'enableClientValidation' => true,
          'validateOnSubmit' => true,
          'validateOnChange' => true,
          'validateOnType' => true,
          'fieldConfig' => [
              'template' => '{label}{input}',
              'options' => [
                  'class' => 'form-group form-group-default',
              ],
          ],
      ]);

  ?>

  <div class="card card-default m-b-10">
    <div class="card-boy">
      <div class="form-group-attached">
        <div class="row">
          <div class="col-md-10">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
          </div>
          <div class="col-md-2">
            <?= $form->field($model, 'ruleType')->textInput(['maxlength' => true]) ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3">
            <?= $form->field($model, 'claimMeal')->textInput(['maxlength' => true]) ?>
          </div>
          <div class="col-md-3">
            <?= $form->field($model, 'useTaxi')->textInput(['maxlength' => true]) ?>
          </div>
          <div class="col-md-3">
            <?= $form->field($model, 'claimTransportation')->textInput(['maxlength' => true]) ?>
          </div>
          <div class="col-md-3">
            <?= $form->field($model, 'claimOutOffice')->textInput(['maxlength' => true]) ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <?= $form->field($model, 'day')->textInput(['maxlength' => true]) ?>
          </div>
          <div class="col-md-6">
            <?= $form->field($model, 'area')->textInput(['maxlength' => true]) ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3">
            <?= $form->field($model, 'workHour')->textInput(['maxlength' => true]) ?>
          </div>
          <div class="col-md-3">
            <?= $form->field($model, 'totalOvertimeHour')->textInput(['maxlength' => true]) ?>
          </div>
          <div class="col-md-3">
            <?= $form->field($model, 'itemOvertime')->textInput(['maxlength' => true]) ?>
          </div>
          <div class="col-md-3">
            <?= $form->field($model, 'isMealProvided')->textInput(['maxlength' => true]) ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3">
            <?= $form->field($model, 'isTransportationProvided')->textInput(['maxlength' => true]) ?>
          </div>
          <div class="col-md-3">
            <?= $form->field($model, 'isAccomodationProvided')->textInput(['maxlength' => true]) ?>
          </div>
          <div class="col-md-3">
            <?= $form->field($model, 'claimTransportationAmount')->textInput(['maxlength' => true]) ?>
          </div>
          <div class="col-md-3">
            <?= $form->field($model, 'withOvertimeTransport')->textInput(['maxlength' => true]) ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <?= $form->field($model, 'applicablePosition')->textInput(['maxlength' => true]) ?>
          </div>
          <div class="col-md-4">
            <?= $form->field($model, 'source')->textInput(['maxlength' => true]) ?>
          </div>
          <div class="col-md-4">
            <?= $form->field($model, 'allowance')->textInput(['maxlength' => true]) ?>
          </div>
        </div>
      </div>
    </div>
  </div>



    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
