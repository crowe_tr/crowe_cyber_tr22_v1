<?php

    namespace backend\modules\st\controllers;
    
    use Yii;
    use yii\helpers\Html;
    use common\models\st\HolidayType;
    use common\models\st\HolidayTimeOff;
    use common\models\st\search\HolidayTimeOff as HolidayTimeOffSearch;
    use yii\web\Controller;
    use yii\web\NotFoundHttpException;
    use yii\filters\VerbFilter;
    use yii\web\MethodNotAllowedHttpException;
    
    class HolidayController extends Controller
    {
    
        public function behaviors()
        {
            return [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ];
        }
        public function actionIndex()
        {
            $searchModel = new HolidayTimeOffSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    
        public function actionForm($id = null, $action=null)
        {
            if (Yii::$app->request->isAjax) {
                $post = Yii::$app->request->post();
    
                $model = new HolidayTimeOff();
                $model->load($post);
                if (!empty($id)) {
                    $model = $this->findModel($id);
    
                    if($action=="duplicate"){
                        $model->id = null;
                        $model->isNewRecord;
                    }
                } else {
                    $model = new HolidayTimeOff();
                }
    
                return $this->renderAjax('_form', [
                    'model' => $model,
                    'id'=> ($action=="duplicate") ? "" : $id,
                ]);
            } else {
                throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
            }
        }
    
        public function actionSave()
        {
            $return = false;
            $model = new HolidayTimeOff();
            $post = Yii::$app->request->post();
            if ($model->load($post)) {
                $modelFind = HolidayTimeOff::findOne($model->id);
                if ($modelFind !== null) {
                    $model = $modelFind;
                }
                
                $model->load($post);
                $valid = $model->validate();
                if ($valid) {
                    $transaction = \Yii::$app->db->beginTransaction();
                    try {
                        if (!($flag = $model->save(false))) {
                            $transaction->rollBack();
                            $return = 'error : rollback';
                        }
                        if ($flag) {
                            $transaction->commit();
                            $return = true;
    
                        }
                    } catch (Exception $e) {
                        $transaction->rollBack();
                    }
                } else {
                    $return = \yii\helpers\Html::errorSummary($model, ['encode' => true]);
                }
            } else {
                $return = 'error : model not loaded';
            }
    
            return $return;
        }
    
        public function actionDelete($id)
        {
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
        }
        protected function findModel($id)
        {
            if (($model = HolidayTimeOff::findOne($id)) !== null) {
                return $model;
            }
    
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    
    }
    