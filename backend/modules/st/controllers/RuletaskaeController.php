<?php

namespace backend\modules\st\controllers;

use Yii;
use common\models\st\RuleTaskAE;
use common\models\st\search\RuleTaskAE as RuleTaskAESearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RuletaskaeController implements the CRUD actions for RuleTaskAE model.
 */
class RuletaskaeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RuleTaskAE models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RuleTaskAESearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RuleTaskAE model.
     * @param integer $levelID
     * @param integer $taskID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($levelID, $taskID)
    {
        return $this->render('view', [
            'model' => $this->findModel($levelID, $taskID),
        ]);
    }

    /**
     * Creates a new RuleTaskAE model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RuleTaskAE();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'levelID' => $model->levelID, 'taskID' => $model->taskID]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing RuleTaskAE model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $levelID
     * @param integer $taskID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($levelID, $taskID)
    {
        $model = $this->findModel($levelID, $taskID);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'levelID' => $model->levelID, 'taskID' => $model->taskID]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing RuleTaskAE model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $levelID
     * @param integer $taskID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($levelID, $taskID)
    {
        $this->findModel($levelID, $taskID)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RuleTaskAE model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $levelID
     * @param integer $taskID
     * @return RuleTaskAE the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($levelID, $taskID)
    {
        if (($model = RuleTaskAE::findOne(['levelID' => $levelID, 'taskID' => $taskID])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
