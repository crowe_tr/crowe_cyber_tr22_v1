<?php

namespace backend\modules\st\controllers;

use Yii;
use common\models\st\HolidayType;
use common\models\st\search\HolidayType as HolidayTypeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class HolidaytypeController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new HolidayTypeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionForm($id = null, $action=null)
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();

            $model = new HolidayType();
            $model->load($post);
            if (!empty($id)) {
                $model = $this->findModel($id);

                if($action=="duplicate"){
                    $model->id = null;
                    $model->isNewRecord;
                }
            } else {
                $model = new HolidayType();
            }

            return $this->renderAjax('_form', [
                'model' => $model,
                'id'=> ($action=="duplicate") ? "" : $id,
            ]);
        } else {
            throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
        }
    }

    public function actionSave()
    {
        $return = false;
        $model = new HolidayType();
        $post = Yii::$app->request->post();
        if ($model->load($post)) {
            $modelFind = HolidayType::findOne($model->id);
            if ($modelFind !== null) {
                $model = $modelFind;
            }
            
            $model->load($post);
            $valid = $model->validate();
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if (!($flag = $model->save(false))) {
                        $transaction->rollBack();
                        $return = 'error : rollback';
                    }
                    if ($flag) {
                        $transaction->commit();
                        $return = true;

                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            } else {
                $return = \yii\helpers\Html::errorSummary($model, ['encode' => true]);
            }
        } else {
            $return = 'error : model not loaded';
        }

        return $return;
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
    }
    protected function findModel($id)
    {
        if (($model = HolidayType::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
