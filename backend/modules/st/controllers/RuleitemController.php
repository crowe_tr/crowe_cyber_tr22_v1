<?php

namespace backend\modules\st\controllers;

use Yii;
use common\models\st\RuleItem;
use common\models\st\search\RuleItem as RuleItemSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class RuleitemController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new RuleItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionForm($id = null, $action=null)
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $data = array();

            $model = new RuleItem();
            $model->load($post);
            if (!empty($id)) {
                $model = $this->findModel($id);

                if($action=="duplicate"){
                    $model->id = null;
                    $model->isNewRecord;
                }
                $model->term_zone_name = explode(',', $model->term_zone_name);

                // $model->termZoneName = explode(',', $model->termZoneName);

            } else {
                $model = new RuleItem();
            }
            return $this->renderAjax('_form', [
                'data' => $data,
                'model' => $model,
                'id'=> ($action=="duplicate") ? "" : $id,
            ]);
        } else {
            throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
        }
    }

    public function actionSave()
    {
        $return = false;
        $model = new RuleItem();
        $post = Yii::$app->request->post();
        if ($model->load($post)) {
            $modelFind = RuleItem::findOne($model->id);
            if ($modelFind !== null) {
                $model = $modelFind;
            }

            $model->load($post);
            $model->term_zone_name = implode(',', $model->term_zone_name);

            // $model->termZoneName = implode(',', $model->termZoneName);
            $valid = $model->validate();
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if (!($flag = $model->save(false))) {
                        $transaction->rollBack();
                        $return = 'error : rollback';
                    }
                    if ($flag) {
                        $transaction->commit();
                        $return = true;

                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            } else {
                $return = \yii\helpers\Html::errorSummary($model, ['encode' => true]);
            }
        } else {
            $return = 'error : model not loaded';
        }

        return $return;
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
    }
    protected function findModel($id)
    {
        if (($model = RuleItem::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
