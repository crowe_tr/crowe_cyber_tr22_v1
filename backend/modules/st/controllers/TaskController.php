<?php

namespace backend\modules\st\controllers;

use Yii;
use common\models\st\Task;
use common\models\st\search\Task as TaskSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class TaskController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    public function actionIndex()
    {
        $searchModel = new TaskSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionForm($id = null, $action=null)
    {
        Yii::$app->cache->flush();

        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $model = new Task();

            $model->load($post);
            if (!empty($id)) {
                $model = $this->findModel($id);

                if($action=="duplicate"){
                    $model->id = null;
                    $model->isNewRecord;
                }
            }

            $model->emp_dept = explode(',', $model->emp_dept);
            $model->emp_division = explode(',', $model->emp_division);
            $model->emp_level = explode(',', $model->emp_level);

            return $this->renderAjax('_form', [
                'model' => $model,
                'id'=> ($action=="duplicate") ? "" : $id,
            ]);
        } else {
            throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
        }
    }

    public function actionSave()
    {
        $return = false;
        $model = new Task();
        $post = Yii::$app->request->post();
        if ($model->load($post)) {
            $modelFind = Task::findOne($model->id);
            if ($modelFind !== null) {
                $model = $modelFind;
            }

            $model->load($post);
            $model->emp_dept = empty($model->emp_dept) ? null : implode(',', $model->emp_dept);
            $model->emp_division = empty($model->emp_division) ? null : implode(',', $model->emp_division);
            $model->emp_level = empty($model->emp_level) ? null : implode(',', $model->emp_level);
            $model->manager_id = empty($model->manager_id) ? null : $model->manager_id;
            $model->supervisor_id = empty($model->supervisor_id) ? null : $model->supervisor_id;

            $valid = $model->validate();
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if (!($flag = $model->save(false))) {
                        $transaction->rollBack();
                        $return = 'error : rollback';
                    }
                    if ($flag) {
                        $transaction->commit();
                        $return = true;

                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            } else {
                $return = \yii\helpers\Html::errorSummary($model, ['encode' => true]);
            }
        } else {
            $return = 'error : model not loaded';
        }

        return $return;
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
    }
    protected function findModel($id)
    {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
