<?php

    namespace backend\modules\st\controllers;

    use Yii;
    use yii\helpers\Html;
    use common\models\st\Rules;
    use common\models\st\RulesLevel;
    use common\models\st\RulesDetail;
    use common\models\st\RuleItem;
    use common\models\st\RuleTermZone;
    use common\models\st\search\Rules as RulesSearch;
    use yii\web\Controller;
    use yii\web\NotFoundHttpException;
    use yii\filters\VerbFilter;
    use yii\web\MethodNotAllowedHttpException;
    use yii\helpers\ArrayHelper;
    use common\components\CommonHelper;

    class RulesController extends Controller
    {

        public function behaviors()
        {
            return [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ];
        }
        public function actionIndex()
        {		Yii::$app->cache->flush();

            $searchModel = new RulesSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }

        public function actionForm($id = null, $action=null)
        {
                $post = Yii::$app->request->post();

                $model = new Rules();
                $model->load($post);
                if (!empty($id)) {
                    $model = $this->findModel($id);

                    if($action=="duplicate"){
                        $model->id = null;
                        $model->isNewRecord;
                    }

                    $modelLevel = new RulesLevel();
                    $levels = RulesLevel::find()->where([
                        'rules_id' => $model->id,
                    ])->all();

                    // $levels = RulesLevel::find()->where([
                    //     'rulesID' => $model->id,
                    // ])->all();
                    $tmpLevel = [];
                    $i = 0;
                    // var_dump($levels);
                    foreach($levels as $level){
                        $tmpLevel[$i]['item_id'] = $level->item_id;
                        $tmpLevel[$i]['level_description'] = $level->level_description;

                        $tmpLevel[$i]['term_item_name'] = $level->ruleItem->term_item_name;
                        $tmpLevel[$i]['qty_claim'] = $level->qty_claim;
                        $tmpLevel[$i]['qty_claim_stayed'] = $level->qty_claim_stayed;
                        $tmpLevel[$i]['stayed_allowance'] = $level->stayed_allowance;
                        $i++;
                    }

                    // foreach($levels as $level){
                    //     $tmpLevel[$i]['itemID'] = $level->itemID;
                    //     $tmpLevel[$i]['levelDescription'] = $level->levelDescription;
                    //
                    //     $tmpLevel[$i]['termItemName'] = $level->ruleItem->termItemName;
                    //     $tmpLevel[$i]['qtyClaim'] = $level->qtyClaim;
                    //     $tmpLevel[$i]['qtyClaimStayed'] = $level->qtyClaimStayed;
                    //     $tmpLevel[$i]['stayedAllowance'] = $level->stayedAllowance;
                    //     $i++;
                    // }

                    $modelDetail = new RulesDetail();
                    $details = RulesDetail::find()->where([
                        'rules_id' => $model->id,
                    ])->all();

                    // $details = RulesDetail::find()->where([
                    //     'rulesID' => $model->id,
                    // ])->all();
                    $tmpDetail = [];
                    $i = 0;
                    foreach($details as $detail){
                        $tmpDetail[$i]['item_id'] = $detail->item_id;
                        $tmpDetail[$i]['term_id'] = $detail->term_id;
                        $tmpDetail[$i]['term_item_name'] = $detail->ruleItem->term_item_name;
                        $tmpDetail[$i]['term_zone_name'] = $detail->ruleTermZone->term_zone_name;

                        $tmpDetail[$i]['min_overtime'] = $detail->min_overtime;
                        $tmpDetail[$i]['use_taxi'] = $detail->use_taxi;
                        $tmpDetail[$i]['min_clock'] = $detail->min_clock;
                        $tmpDetail[$i]['allowance_amount'] = $detail->allowance_amount;

                        $i++;
                    }

                    // foreach($details as $detail){
                    //     $tmpDetail[$i]['itemID'] = $detail->itemID;
                    //     $tmpDetail[$i]['termID'] = $detail->termID;
                    //     $tmpDetail[$i]['termItemName'] = $detail->ruleItem->termItemName;
                    //     $tmpDetail[$i]['termZoneName'] = $detail->ruleTermZone->termZoneName;
                    //
                    //     $tmpDetail[$i]['minOvertime'] = $detail->minOvertime;
                    //     $tmpDetail[$i]['useTaxi'] = $detail->useTaxi;
                    //     $tmpDetail[$i]['minClock'] = $detail->minClock;
                    //     $tmpDetail[$i]['allowanceAmount'] = $detail->allowanceAmount;
                    //
                    //     $i++;
                    // }
                    $modelLevel->TabularInput = $tmpLevel;
                    $modelDetail->TabularInput = $tmpDetail;


                } else {
                    $model = new Rules();
                    $modelLevel = new RulesLevel();
                    $modelDetail = new RulesDetail();

                    $items = RuleItem::find()->all();
                    $TermZones = ArrayHelper::map(RuleTermZone::find()->asArray()->all(), 'term_zone_name', 'id');

                    // $TermZones = ArrayHelper::map(RuleTermZone::find()->asArray()->all(), 'termZoneName', 'id');


                    $tmpLevel = [];
                    $tmpDetail = [];
                    $i = 0;
                    $j = 0;
                    foreach($items as $item){
                        $tmpLevel[$i]['item_id'] = $item->id;
                        $tmpLevel[$i]['term_item_name'] = $item->term_item_name;
                        $tmpLevel[$i]['qty_claim'] = 0;
                        $tmpLevel[$i]['qty_claim_stayed'] = 0;
                        $tmpLevel[$i]['stayed_allowance'] = 0;

                        $terms = explode(',', $item->term_zone_name);
                        foreach($terms as $term){

                            $tmpDetail[$j]['item_id'] = $item->id;
                            $tmpDetail[$j]['term_item_name'] = $item->term_item_name;

                            $tmpDetail[$j]['term_id'] = $TermZones[$term];
                            $tmpDetail[$j]['term_zone_name'] = $term;
                            $j++;
                        }
                        $i++;
                    }

                    // foreach($items as $item){
                    //     $tmpLevel[$i]['itemID'] = $item->id;
                    //     $tmpLevel[$i]['termItemName'] = $item->termItemName;
                    //     $tmpLevel[$i]['qtyClaim'] = 0;
                    //     $tmpLevel[$i]['qtyClaimStayed'] = 0;
                    //     $tmpLevel[$i]['stayedAllowance'] = 0;
                    //
                    //     $terms = explode(',', $item->termZoneName);
                    //     foreach($terms as $term){
                    //
                    //         $tmpDetail[$j]['itemID'] = $item->id;
                    //         $tmpDetail[$j]['termItemName'] = $item->termItemName;
                    //
                    //         $tmpDetail[$j]['termID'] = $TermZones[$term];
                    //         $tmpDetail[$j]['termZoneName'] = $term;
                    //         $j++;
                    //     }
                    //     $i++;
                    // }
                    $modelLevel->TabularInput = $tmpLevel;
                    $modelDetail->TabularInput = $tmpDetail;

                }

                return $this->renderAjax('_form', [
                    'model' => $model,
                    'modelLevel'=>$modelLevel,
                    'modelDetail'=>$modelDetail,
                    'id'=> ($action=="duplicate") ? "" : $id,
                ]);
        }

        public function actionSave()
        {
            $return = false;
            $model = new Rules();
            $modelLevel = new RulesLevel();
            $modelDetail = new RulesDetail();
            $post = Yii::$app->request->post();
            if ($model->load($post)) {
                $modelFind = Rules::findOne($model->id);
                if (!empty($modelFind->id)) {
                    $model = $modelFind;
                }else{
                    $model = new Rules();
                }

                $model->load($post);
                $modelLevel->load($post);
                $modelDetail->load($post);

                $valid = $model->validate();
                if ($valid) {
                    $transaction = \Yii::$app->db->beginTransaction();
                    try {
                        if (!($flag = $model->save(false))) {
                            $transaction->rollBack();
                            $return = 'error : rollback';
                        } else {
                          if ($flag) {
                              $details = $modelDetail->TabularInput;
                              if (is_array($details) || is_object($details)) {
                                  foreach($details as $d){
                                      $modelDetail = RulesDetail::find()->where([
                                          'rules_id' => $model->id,
                                          'item_id' => $d['item_id'],
                                          'term_id' => $d['term_id']
                                      ])->one();
                                      if(empty($modelDetail->item_id)){
                                          $modelDetail = new RulesDetail();
                                      }

                                      $modelDetail->rules_id = $model->id;
                                      $modelDetail->item_id = $d['item_id'];
                                      $modelDetail->term_id = $d['term_id'];
                                      $modelDetail->min_overtime = CommonHelper::ResetDecimal($d['min_overtime']);
                                      $modelDetail->use_taxi = $d['use_taxi'];
                                      $modelDetail->min_clock = $d['min_clock'];
                                      $modelDetail->allowance_amount = CommonHelper::ResetDecimal($d['allowance_amount']);


                                      if (!($flag = $modelDetail->save(false))) {
                                          $transaction->rollBack();
                                          $return = 'error : when saving details data'.$d['term_id'].' '.$d['term_id'];
                                          break;
                                      }

                                  }
                              }

                          }

                            // if ($flag) {
                            //     $details = $modelDetail->TabularInput;
                            //     if (is_array($details) || is_object($details)) {
                            //         foreach($details as $d){
                            //             $modelDetail = RulesDetail::find()->where([
                            //                 'rulesID' => $model->id,
                            //                 'itemID' => $d['itemID'],
                            //                 'termID' => $d['termID']
                            //             ])->one();
                            //
                            //             if(empty($modelDetail->itemID)){
                            //                 $modelDetail = new RulesDetail();
                            //             }
                            //
                            //             $modelDetail->rulesID = $model->id;
                            //             $modelDetail->itemID = $d['itemID'];
                            //             $modelDetail->termID = $d['termID'];
                            //             $modelDetail->minOvertime = CommonHelper::ResetDecimal($d['minOvertime']);
                            //             $modelDetail->useTaxi = $d['useTaxi'];
                            //             $modelDetail->minClock = $d['minClock'];
                            //             $modelDetail->allowanceAmount = CommonHelper::ResetDecimal($d['allowanceAmount']);
                            //
                            //             if (!($flag = $modelDetail->save(false))) {
                            //                 $transaction->rollBack();
                            //                 $return = 'error : when saving details data'.$d['itemID'].' '.$d['termID'];
                            //                 break;
                            //             }
                            //
                            //         }
                            //     }
                            // }
                            //-------------------------
                            if ($flag) {
                                $levels = $modelLevel->TabularInput;
                                if (is_array($levels) || is_object($levels)) {
                                    foreach($levels as $level){
                                        $modelLevel = RulesLevel::find()->where([
                                            'rules_id' => $model->id,
                                            'item_id' => $level['item_id']
                                        ])->one();
                                        if(empty($modelLevel->item_id)){
                                            $modelLevel = new RulesLevel();
                                        }

                                        $modelLevel->rules_id = $model->id;
                                        $modelLevel->item_id = $level['item_id'];
                                        $modelLevel->level_description = empty($level['level_description']) ? "" : implode(',', $level['level_description']);
                                        $modelLevel->qty_claim = CommonHelper::ResetDecimal($level['qty_claim']);
                                        $modelLevel->qty_claim_stayed = CommonHelper::ResetDecimal($level['qty_claim_stayed']);
                                        $modelLevel->stayed_allowance = CommonHelper::ResetDecimal($level['stayed_allowance']);

                                        if (!($flag = $modelLevel->save(false))) {
                                            $transaction->rollBack();
                                            $return = 'error : when saving level data'.$level['item_id'];
                                            break;
                                        }
                                    }
                                }
                            }

                            // if ($flag) {
                            //     $levels = $modelLevel->TabularInput;
                            //     if (is_array($levels) || is_object($levels)) {
                            //         foreach($levels as $level){
                            //             $modelLevel = RulesLevel::find()->where([
                            //                 'rulesID' => $model->id,
                            //                 'itemID' => $level['itemID']
                            //             ])->one();
                            //             if(empty($modelLevel->itemID)){
                            //                 $modelLevel = new RulesLevel();
                            //             }
                            //
                            //             $modelLevel->rulesID = $model->id;
                            //             $modelLevel->itemID = $level['itemID'];
                            //             $modelLevel->levelDescription = empty($level['levelDescription']) ? "" : implode(',', $level['levelDescription']);
                            //             $modelLevel->qtyClaim = CommonHelper::ResetDecimal($level['qtyClaim']);
                            //             $modelLevel->qtyClaimStayed = CommonHelper::ResetDecimal($level['qtyClaimStayed']);
                            //             $modelLevel->stayedAllowance = CommonHelper::ResetDecimal($level['stayedAllowance']);
                            //
                            //             if (!($flag = $modelLevel->save(false))) {
                            //                 $transaction->rollBack();
                            //                 $return = 'error : when saving level data'.$level['itemID'];
                            //                 break;
                            //             }
                            //         }
                            //     }
                            // }

                        }

                        if ($flag) {
                            $transaction->commit();
                            $return = true;
                        }
                    } catch (Exception $e) {
                        $transaction->rollBack();
                    }
                } else {
                    $return = \yii\helpers\Html::errorSummary($model, ['encode' => true]);
                    $return .= \yii\helpers\Html::errorSummary($modelLevel, ['encode' => true]);
                    $return .= \yii\helpers\Html::errorSummary($modelDetail, ['encode' => true]);
                }
            } else {
                $return = 'error : model not loaded';
            }

            return $return;
        }

        public function actionDelete($id)
        {
            $transaction = \Yii::$app->db->beginTransaction();
            try {
              RulesDetail::deleteAll([
                  'rules_id' => $id,
              ]);
              RulesLevel::deleteAll([
                  'rules_id' => $id,
              ]);

                // RulesDetail::deleteAll([
                //     'rulesID' => $id,
                // ]);
                // RulesLevel::deleteAll([
                //     'rulesID' => $id,
                // ]);

                if (!($flag = $this->findModel($id)->delete() )) {
                    $transaction->rollBack();
                    $return = 'error : rollback';
                }
                if ($flag) {
                    $transaction->commit();
                    $return = true;
                }

            } catch (Exception $e) {
                $transaction->rollBack();
            }
        }
        protected function findModel($id)
        {
            if (($model = Rules::findOne($id)) !== null) {
                return $model;
            }

            throw new NotFoundHttpException('The requested page does not exist.');
        }

    }
