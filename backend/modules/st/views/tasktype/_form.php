<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use common\models\st\TaskType;
use kartik\widgets\SwitchInput;
use common\models\cm\Division;
use common\models\cm\Dept;

$form = ActiveForm::begin([
  'id' => 'crud-form',
  'enableClientValidation' => true,
  'validateOnSubmit' => true,
  'validateOnChange' => true,
  'validateOnType' => true,
  'fieldConfig' => [
    'template' => '{label}{input}',
    'options' => [
      'class' => 'form-group form-group-default',
    ],
  ],
  'errorSummaryCssClass' => 'alert alert-danger'
]);
echo $form->errorSummary($model);;
echo $form->field($model, 'id', ['options' => ['class' => '']])->hiddenInput()->label(false);

?>
<div class="row">
  <div id="error-summary" class="col-md-12 ">

  </div>
</div>
<div class="form-group-attached">
  <div class="row">
    <div class="col-md-9">
      <?= $form->field($model, 'task_type_name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-3">
      <?php
      echo $form->field($model, 'flag', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
        Select2::classname(),
        [
          'data' => [1 => 'ACTIVE', 0 => 'INACTIVE'],
          'options' => ['id' => 'type', 'placeholder' => 'Select ...'],
        ]
      );
      ?>
    </div>

  </div>
</div>
<div class="form-group-attached mb-3">
  <div class="row p-b-5">
    <div class="col-sm-3">
      <?php
      echo $form->field(
        $model,
        'is_meal',
        ['options' => ['class' => ' form-group form-group-default']]
      )->widget(
        SwitchInput::className(),
        ['pluginOptions' => [
          'size' => 'small',
          'onText' => 'Yes',
          'offText' => 'No',
        ]]
      );
      ?>
    </div>
    <div class="col-sm-3">
      <?php
      echo $form->field(
        $model,
        'is_ope',
        ['options' => ['class' => ' form-group form-group-default']]
      )->widget(
        SwitchInput::className(),
        ['pluginOptions' => [
          'size' => 'small',
          'onText' => 'Yes',
          'offText' => 'No',
        ]]
      );
      ?>

    </div>
    <div class="col-sm-3">
      <?php
      echo $form->field(
        $model,
        'is_taxi',
        ['options' => ['class' => ' form-group form-group-default']]
      )->widget(
        SwitchInput::className(),
        ['pluginOptions' => [
          'size' => 'small',
          'onText' => 'Yes',
          'offText' => 'No',
        ]]
      );
      ?>

    </div>
    <div class="col-sm-3">
      <?php
      echo $form->field(
        $model,
        'used_project',
        ['options' => ['class' => ' form-group form-group-default']]
      )->widget(
        SwitchInput::className(),
        ['pluginOptions' => [
          'size' => 'small',
          'onText' => 'Yes',
          'offText' => 'No',
        ]]
      );
      ?>

    </div>
  </div>
</div>
<b>ASSIGN THIS TASK ONLY TO : </b>

<div class="form-group-attached mt-1">
  <div class="row">

    <div class="col-12">
      <?php
      echo $form->field($model, 'emp_dept')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Dept::find()->all(), 'dept_name', 'dept_name'),
        'options' => ['placeholder' => 'Sign to Departement ...', 'multiple' => true],
        'pluginOptions' => [
          'tokenSeparators' => [','],
        ],
      ]);
      ?>
    </div>
  </div>

  <div class="row">
    <div class="col-12">
      <?php
      echo $form->field($model, 'emp_division')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Division::find()->all(), 'div_name', 'div_name'),
        'options' => ['placeholder' => 'Sign to Division ...', 'multiple' => true],
        'pluginOptions' => [
          'tokenSeparators' => [','],
        ],
      ]);
      ?>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12 text-right m-t-5">
    <hr class="m-b-5" />
    <?= Html::submitButton('UPDATE', ['class' => 'btn btn-warning p-t-10 p-b-10']) ?>
    <button type="button" class="btn btn-info p-t-10 p-b-10" style="font-size: 12px" data-dismiss="modal">CANCEL</button>
  </div>
</div>

<?php ActiveForm::end();  ?>
<script type="text/javascript">
  $('#crud-form').on('beforeSubmit', function() {
    var $form = new FormData($('#crud-form')[0]);
    $.ajax({
      url: paramJs.urlFormSave,
      type: 'POST',
      data: $form,
      async: false,
      cache: false,
      contentType: false,
      processData: false,

      success: function(data) {
        if (data != 1) {
          $('#error-summary').html('<div class="alert alert-danger">' + data + '</div>');
        } else {
          $('#crud-modal').modal('hide');
          reload();
        }
      },
      error: function(jqXHR, errMsg) {
        alert(errMsg);
      }
    });
    return false;
  });
</script>
