<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use unclead\multipleinput\MultipleInput;
use kartik\widgets\Select2;
use yii\widgets\MaskedInput;
use common\models\cm\Level;
use yii\helpers\ArrayHelper;

$form = ActiveForm::begin([
  'id' => 'crud-form',
  'enableClientValidation' => true,
  'validateOnSubmit' => true,
  'validateOnChange' => true,
  'validateOnType' => true,
  'fieldConfig' => [
    'template' => '{label}{input}',
    'options' => [
      'class' => 'form-group form-group-default',
    ],
  ],
  'errorSummaryCssClass' => 'alert alert-danger'
]);
echo $form->errorSummary($model);;
echo $form->field($model, 'id', ['options' => ['class' => '']])->hiddenInput()->label(false);

?>
<div class="row">
  <div id="error-summary" class="col-md-12 ">
  </div>
</div>
<div class="form-group-attached">
  <div class="row">
    <div class="col-md-3">
      <?=
      $form->field(
        $model,
        'effective_date',
        [
          'template' => '{label}{input}', 'options' => ['onchange' => 'ReqSave()', 'class' => 'form-group form-group-default input-group'],
        ]
      )->widget(DatePicker::classname(), [
        'type' => DatePicker::TYPE_INPUT,
        'value' => date('Y-m-d'),
        'pluginOptions' => [
          'autoclose' => true,
          'format' => 'yyyy-mm-dd',
        ],
      ])
      ?>
    </div>
    <div class="col-md-9">
      <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
    </div>

  </div>
</div>
<br />
<div class="row">
  <div class="col-md-12">
    <?php
    echo $form->field($modelDetail, 'TabularInput', ['options' => ['class' => 'table']])->widget(
      MultipleInput::className(),
      [
        'iconSource' => 'fa',
        'theme' => 'default',
        'sortable' => false,
        'addButtonPosition' => MultipleInput::POS_HEADER,
        'removeButtonOptions' => [
          'class' => 'btn btn-danger btn-sm',
        ],
        'addButtonOptions' => [
          'class' => 'btn btn-warning btn-sm',
        ],
        'columns' => [
          [
            'name'  => 'level_id',
            'title' => '',
            'value' => function ($data) {
              return !empty($data['level_id']) ? $data['level_id'] : "";
            },
            'options' => [
              'class' => 'hidden',
              'style' => 'color: black;'
            ],
            'columnOptions' => [
              'class' => '',
              'style' => 'padding:0 !Important; border:0',

            ],
            'headerOptions' => [
              'class' => '',
              'style' => 'padding:0 !Important; border:0',
              'width' => '10px'
            ]
          ],
          [
            'name'  => 'level_name',
            'title' => 'LEVEL',
            'value' => function ($data) {
              return !empty($data['level_name']) ? $data['level_name'] : "";
            },
            'options' => [
              'class' => 'form-control',
              'disabled' => true,
              'style' => 'color: black;'
            ],
            'columnOptions' => [
              'class' => 'col-md-7',

            ],
            'headerOptions' => [
              'class' => 'bg-info text-white text-center'
            ]

          ],
          [
            'name'  => 'billing_rate_value',
            'title' => 'Rate',
            'type' => MaskedInput::className(),
            'value' => function ($data) {
              return !empty($data['billing_rate_value']) ? $data['billing_rate_value'] : 0;

            },
            'options' => [
              'clientOptions' => [
                'alias' => 'decimal',
                'groupSeparator' => ',',
                'autoGroup' => true,
              ],
              'options' => [
                'class' => 'form-control',
              ]
            ],
            'columnOptions' => [
              'class' => 'col-md-5',

            ],
            'headerOptions' => [
              'class' => 'bg-info text-white text-center'
            ]

          ],

        ]
      ]
    )->label(false);
    ?>
  </div>
</div>
<div class="row">
  <div class="col-md-12 text-right m-t-5">
    <hr class="m-b-5" />
    <?= Html::submitButton('UPDATE', ['class' => 'btn btn-warning p-t-10 p-b-10']) ?>
    <button type="button" class="btn btn-info p-t-10 p-b-10" style="font-size: 12px" data-dismiss="modal">CANCEL</button>
  </div>
</div>

<?php ActiveForm::end();  ?>
<script type="text/javascript">
  $('#crud-form').on('beforeSubmit', function() {
    var $form = new FormData($('#crud-form')[0]);
    $.ajax({
      url: paramJs.urlFormSave,
      type: 'POST',
      data: $form,
      async: false,
      cache: false,
      contentType: false,
      processData: false,

      success: function(data) {
        if (data != 1) {
          $('#error-summary').html('<div class="alert alert-danger">' + data + '</div>');
        } else {
          $('#crud-modal').modal('hide');
          reload();
        }
      },
      error: function(jqXHR, errMsg) {
        alert(errMsg);
      }
    });
    return false;
  });
</script>
