<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use common\models\st\HolidayType;

$form = ActiveForm::begin([
    'id' => 'crud-form',
    'enableClientValidation' => true,
    'validateOnSubmit' => true,
    'validateOnChange' => true,
    'validateOnType' => true,
    'fieldConfig' => [
        'template' => '{label}{input}',
        'options' => [
            'class' => 'form-group form-group-default',
        ],
  ],
  'errorSummaryCssClass'=> 'alert alert-danger'
  ]);
  echo $form->errorSummary($model);;
  echo $form->field($model, 'id', ['options' => ['class' => '']])->hiddenInput()->label(false);

?>
    <div class="row">
        <div id="error-summary" class="col-md-12 ">

        </div>
    </div>
    <div class="form-group-attached">
      <div class="row">
        <div class="col-md-3">
          <?=
            $form->field($model, 'holiday_date', [
              'template' => '{label}{input}', 'options' => ['onchange' => 'ReqSave()', 'class' => 'form-group form-group-default input-group'],
            ]
            )->widget(DatePicker::classname(), [
              'type' => DatePicker::TYPE_INPUT,
              'value' => date('Y-m-d'),
              'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd',
              ],
            ])
          ?>
        </div>
        <div class="col-md-3">
          <?php
              echo $form->field($model, 'type_id', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
                      Select2::classname(),
                      [
                              'data' => ArrayHelper::map(HolidayType::find()->all(), 'id', 'holiday_type_name'),
                              'options' => ['id' => 'type', 'placeholder' => 'Select ...'],
                      ]
              );
          ?>
        </div>
        <div class="col-md-6">
          <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
        </div>
      </div>
    </div>
  <div class="row">
    <div class="col-md-12 text-right m-t-5">
      <hr class="m-b-5"/>
      <?= Html::submitButton('UPDATE', ['class' => 'btn btn-warning p-t-10 p-b-10']) ?>
      <button type="button" class="btn btn-info p-t-10 p-b-10" style="font-size: 12px" data-dismiss="modal">CANCEL</button>
    </div>
  </div>

<?php ActiveForm::end();  ?>
<script type="text/javascript">
$('#crud-form').on('beforeSubmit', function() {
  var $form = new FormData($('#crud-form')[0]);
  $.ajax({
    url: paramJs.urlFormSave,
    type: 'POST',
    data: $form,
    async: false,
    cache: false,
    contentType: false,
    processData: false,

    success: function (data) {
      if(data != 1){
        $('#error-summary').html('<div class="alert alert-danger">' + data + '</div>');
      }else{
        $('#crud-modal').modal('hide');
        reload();
      }
    },
    error: function(jqXHR, errMsg) {
      alert(errMsg);
    }
  });
  return false;
});
</script>
