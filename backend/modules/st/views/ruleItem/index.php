<?php

use yii\helpers\Html;
use yii\web\View;
use kartik\grid\GridView;
use yii\helpers\Url;


$this->title = 'Item';
$this->params['breadcrumbs'][] = 'SETUP';
$this->params['breadcrumbs'][] = 'RULES';
$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs_btn'] = Html::a(
    'BACK',
    ['/st/rules'],
    [
      'class' => 'btn btn-info text-white ',
    ]
  );

$this->params['crud_ajax'] = true;

$column = [
        [
            'class' => 'kartik\grid\SerialColumn',
            'header' => 'No.',
            'mergeHeader' => false,
            'headerOptions' => ['class' => 'bg-success b-r'],
            'contentOptions' => ['class' => 'text-right b-r'],
            'filterOptions' => ['class' => 'b-b b-grey b-r'],
            'width' => '36px',
        ],
        [
            'attribute' => 'term_item_name',
            'headerOptions' => ['class' => 'col-sm-3 bg-success'],
            'filterOptions' => ['class' => 'b-b b-grey'],
            'contentOptions' => ['class' => 'kv-align-middle'],
            'value' => function ($data){
                return empty($data->term_item_name) ? "" : $data->term_item_name;
            }
        ],
        [
            'attribute' => 'term_zone_name',
            'headerOptions' => ['class' => 'col-sm-4 bg-success'],
            'filterOptions' => ['class' => 'b-b b-grey'],
            'contentOptions' => ['class' => 'kv-align-middle'],
            'value' => function ($data){
                return empty($data->term_zone_name) ? "" : $data->term_zone_name;
            }
        ],

        [
            'attribute' => 'term_item_desc',
            'headerOptions' => ['class' => 'col-sm-5 bg-success'],
            'filterOptions' => ['class' => 'b-b b-grey'],
            'contentOptions' => ['class' => 'kv-align-middle'],
            'value' => function ($data){
                return empty($data->term_item_desc) ? "" : $data->term_item_desc;
            }
        ],
        [
            'header' => '',
            'headerOptions' => ['class' => 'bg-success text-center'],
            'filterOptions' => ['class' => 'b-b b-grey'],
            'contentOptions' => ['class' => 'no-padding'],

            'format' => 'raw',
            'value' => function ($data) {
                $val = '<div class="tooltip-action">
                          <div class="trigger">
                            '.Html::a('<i class="fa fa-ellipsis-h"></i>', false, ['class' => 'text-info', 'data-toggle' => 'tooltip', 'data-original-title' => 'Up here!']).'
                          </div>
                          <div class="action-mask">
                            <div class="action">
                              '.Html::a(
                                '<i class="fa fa-edit"></i>',
                                false,
                                [
                                    'onclick' => "FormUpdate('".Yii::$app->urlManager->createUrl([Url::current(['form']), 'id' => $data->id])."')",
                                    'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
                                    'style' => 'bjob-radius: 5px !important',
                                ]
                              ).'

                            </div>
                          </div>
                        </div>';

                return $val;
            },
        ],
    ];
?>
<?php \yii\widgets\Pjax::begin(['id' => 'crud-pjax', 'enablePushState' => false]); ?>
    <div class="modal fade stick-up " id="crud-modal" role="dialog" aria-hidden="false" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-lg ">
            <div class="modal-content no-border">
                <div class="modal-header bg-success text-white">
                </div>
                <div id="detail" class="modal-body padding-20">
                </div>
            </div>
        </div>
    </div>

    <?php
    echo GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $column,
            'layout'=>'
                <div class="card card-default">
                    <div class="row ">
                    <div class="col-md-12">
                        {items}
                    </div>
                    </div>
                    <div class="row padding-10">
                    <div class="col-md-4">{summary}</div>
                    <div class="col-md-8">{pager}</div>
                    </div>
                </div>
                ',
            'resizableColumns' => true,
            'bordered' => false,
            'striped' => true,
            'condensed' => false,
            'responsive' => false,
            'hover' => true,
            'persistResize' => false,
        ]
    );
    ?>
<?php \yii\widgets\Pjax::end(); ?>
