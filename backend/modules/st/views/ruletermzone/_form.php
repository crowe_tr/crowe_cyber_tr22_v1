<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use unclead\multipleinput\MultipleInput;
use kartik\widgets\Select2;
use yii\widgets\MaskedInput;
use common\models\cm\Level;
use yii\helpers\ArrayHelper;

$form = ActiveForm::begin([
    'id' => 'crud-form',
    'enableClientValidation' => true,
    'validateOnSubmit' => true,
    'validateOnChange' => true,
    'validateOnType' => true,
    'fieldConfig' => [
        'template' => '{label}{input}',
        'options' => [
            'class' => 'form-group form-group-default',
        ],
  ],
  'errorSummaryCssClass'=> 'alert alert-danger'
  ]);
  echo $form->errorSummary($model);;
  echo $form->field($model, 'id', ['options' => ['class' => '']])->hiddenInput()->label(false);

?>
    <div class="row">
        <div id="error-summary" class="col-md-12 ">
        </div>
    </div>
    <div class="form-group-attached">
      <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'term_zone_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'term_zone_description')->textInput(['maxlength' => true]) ?>
        </div>
      </div>
    </div>
  <div class="row">
    <div class="col-md-12 text-right m-t-5">
      <hr class="m-b-5"/>
      <?= Html::submitButton('UPDATE', ['class' => 'btn btn-warning p-t-10 p-b-10']) ?>
      <button type="button" class="btn btn-info p-t-10 p-b-10" style="font-size: 12px" data-dismiss="modal">CANCEL</button>
    </div>
  </div>

<?php ActiveForm::end();  ?>
<script type="text/javascript">
$('#crud-form').on('beforeSubmit', function() {
  var $form = new FormData($('#crud-form')[0]);
  $.ajax({
    url: paramJs.urlFormSave,
    type: 'POST',
    data: $form,
    async: false,
    cache: false,
    contentType: false,
    processData: false,

    success: function (data) {
      if(data != 1){
        $('#error-summary').html('<div class="alert alert-danger">' + data + '</div>');
      }else{
        $('#crud-modal').modal('hide');
        reload();
      }
    },
    error: function(jqXHR, errMsg) {
      alert(errMsg);
    }
  });
  return false;
});
</script>
