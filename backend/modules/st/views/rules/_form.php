<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use unclead\multipleinput\MultipleInput;
use kartik\widgets\Select2;
use yii\widgets\MaskedInput;
use yii\helpers\ArrayHelper;
use kartik\checkbox\CheckboxX;
use kartik\widgets\TimePicker;

use common\models\cm\Level;
use common\models\st\RuleItem;
use common\models\st\RuleTermZone;

$this->title = 'FORM';
$this->params['breadcrumbs'][] = 'SETUP';
$this->params['breadcrumbs'][] = 'RULES';
$this->params['breadcrumbs'][] = $this->title;

$form = ActiveForm::begin([
    'id' => 'crud-form',
    'enableClientValidation' => true,
    'validateOnSubmit' => true,
    'validateOnChange' => true,
    'validateOnType' => true,
    'fieldConfig' => [
        'template' => '{label}{input}',
        'options' => [
            'class' => 'form-group form-group-default',
        ],
  ],
  'errorSummaryCssClass'=> 'alert alert-danger'
  ]);
  echo $form->errorSummary($model);;
  echo $form->field($model, 'id', ['options' => ['class' => '']])->hiddenInput()->label(false);

?>
    <div class="row">
        <div id="error-summary" class="col-md-12 ">
        </div>
    </div>
    <div class="form-group-attached">
      <div class="row">
        <div class="col-md-2">
            <?=
                $form->field($model, 'effective_date', [
                    'template' => '{label}{input}', 'options' => ['onchange' => 'ReqSave()', 'class' => 'form-group form-group-default input-group'],
                ]
                )->widget(DatePicker::classname(), [
                    'type' => DatePicker::TYPE_INPUT,
                    'value' => date('Y-m-d'),
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                    ],
                ])
            ?>
        </div>

        <div class="col-md-10">
            <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
      <br/>
      <?php
      echo $form->field($modelLevel, 'TabularInput', ['options'=>['class'=>'table table-bordered']])->widget(
        MultipleInput::className(), [
          'iconSource' => 'fa',
          'theme'=>'default',
          'sortable'=>false,
          'addButtonPosition' => MultipleInput::POS_HEADER,
          'removeButtonOptions'=>[
            'class'=>'btn btn-danger btn-sm',
          ],
          'addButtonOptions'=>[
            'class'=>'btn btn-warning btn-sm',
          ],

          'columns' => [
            [
              'name'  => 'item_id',
              'title' => '',
              'value' => function($data) {
                  return !empty($data['item_id']) ? $data['item_id'] : "";

              },
              'options' => [
                'class' => 'hidden',
                'style'=>'color: black;'
              ],
              'columnOptions' => [
                'class' => '',
                'style'=>'padding:0 !Important; border:0',

              ],
              'headerOptions' => [
                'class'=>'',
                'style'=>'padding:0 !Important; border:0',
                'width'=>'10px'
              ]

            ],
            [
              'name'  => 'term_item_name',
              'title' => 'RULE',
              'value' => function($data) {
                  return !empty($data['term_item_name']) ? $data['term_item_name'] : "";

              },
              'options' => [
                'class' => 'form-control',
                'disabled'=>true,
                'style'=>'color: black;'
              ],
              'columnOptions' => [
                'class' => 'col-md-3',

              ],
              'headerOptions' => [
                'class'=>'bg-info text-white text-center'
              ]

            ],
            [
              'name'  => 'qty_claim',
              'title' => 'Claim Qty',
              'type'=>MaskedInput::className(),
              'value' => function($data) {
                  return !empty($data['qty_claim']) ? $data['qty_claim'] : 0;
              },
              'headerOptions' => [
                'class'=>'bg-success text-center'
              ],
              'columnOptions' => [
                'class' => 'col-md-1',
              ],
              'options' => [
                'clientOptions' => [
                  'alias' => 'decimal',
                  'groupSeparator' => ',',
                  'autoGroup' => true,
                ],
                'options' => [
                  'class' => 'form-control',
                ]
              ],
            ],
            [
              'name'  => 'qty_claim_stayed',
              'title' => 'Claim Stayed',
              'type'=>MaskedInput::className(),
              'value' => function($data) {
                  return !empty($data['qty_claim_stayed']) ? $data['qty_claim_stayed'] : 0;
              },
              'headerOptions' => [
                'class'=>'bg-success text-center'
              ],
              'columnOptions' => [
                'class' => 'col-md-1',
              ],
              'options' => [
                'clientOptions' => [
                  'alias' => 'decimal',
                  'groupSeparator' => ',',
                  'autoGroup' => true,
                ],
                'options' => [
                  'class' => 'form-control',
                ]
              ],
            ],
            [
              'name'  => 'stayed_allowance',
              'title' => 'Stayed Allowance',
              'type'=>MaskedInput::className(),
              'value' => function($data) {
                  return !empty($data['stayed_allowance']) ? $data['stayed_allowance'] : 0;
              },
              'headerOptions' => [
                'class'=>'bg-success text-center'
              ],
              'columnOptions' => [
                'class' => 'col-md-1',
              ],
              'options' => [
                'clientOptions' => [
                  'alias' => 'decimal',
                  'groupSeparator' => ',',
                  'autoGroup' => true,
                ],
                'options' => [
                  'class' => 'form-control',
                ]
              ],
            ],

            [
              'name'  => 'level_description',
              'type'  => Select2::classname(),
              'title' => 'APPLIED TO',

              'value' => function($data) {
                return !empty($data['level_description']) ? explode(',', $data['level_description']) : "";
              },
              'options' => [
                'data' => ArrayHelper::map(Level::find()->asArray()->all(), 'level_name', 'level_name'),
                'options' => ['placeholder' => '', 'multiple' => true],
                'pluginOptions' => [
                  'tokenSeparators' => [','],
                ],

              ],
              'columnOptions' => [
                'class' => 'col-md-9',
              ],
              'headerOptions' => [
                'class'=>'bg-success text-white text-center'
              ]
            ],
        ]
      ])->label(false);
      ?>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
      <br/>
      <?php
      echo $form->field($modelDetail, 'TabularInput', ['options'=>['class'=>'table table-bordered']])->widget(
        MultipleInput::className(), [
          'iconSource' => 'fa',
          'theme'=>'default',
          'sortable'=>false,
          'addButtonPosition' => MultipleInput::POS_HEADER,
          'removeButtonOptions'=>[
            'class'=>'btn btn-danger btn-sm',
          ],
          'addButtonOptions'=>[
            'class'=>'btn btn-warning btn-sm',
          ],

          'columns' => [
            [
              'name'  => 'item_id',
              'title' => '',
              'value' => function($data) {
                  return !empty($data['item_id']) ? $data['item_id'] : "";

              },
              'options' => [
                'class' => 'hidden',
                'style'=>'color: black;'
              ],
              'columnOptions' => [
                'class' => '',
                'style'=>'padding:0 !Important; border:0',

              ],
              'headerOptions' => [
                'class'=>'',
                'style'=>'padding:0 !Important; border:0',
                'width'=>'10px'
              ]

            ],

            [
              'name'  => 'term_item_name',
              'title' => 'RULE',
              'value' => function($data) {
                  return !empty($data['term_item_name']) ? $data['term_item_name'] : "";

              },
              'options' => [
                'class' => 'form-control',
                'disabled'=>true,
                'style'=>'color: black;'
              ],
              'columnOptions' => [
                'class' => 'col-md-5',
              ],
              'headerOptions' => [
                'class'=>'bg-info text-white text-center'
              ]

            ],
            [
              'name'  => 'term_id',
              'title' => '',
              'value' => function($data) {
                  return !empty($data['term_id']) ? $data['term_id'] : "";

              },
              'options' => [
                'class' => 'hidden',
                'style'=>'color: black;'
              ],
              'columnOptions' => [
                'class' => 'hidden',
              ],
              'headerOptions' => [
                'class'=>'hidden'
              ]

            ],
            [
              'name'  => 'term_zone_name',
              'title' => 'RULE',
              'value' => function($data) {
                  return !empty($data['term_zone_name']) ? $data['term_zone_name'] : "";

              },
              'options' => [
                'class' => 'form-control',
                'disabled'=>true,
                'style'=>'color: black;'
              ],
              'columnOptions' => [
                'class' => 'col-md-2',
              ],
              'headerOptions' => [
                'class'=>'bg-info text-white text-center'
              ]

            ],

            [
              'name'  => 'min_overtime',
              'title' => 'Min Overtime',
              'type'=>MaskedInput::className(),
              'value' => function($data) {
                  return !empty($data['min_overtime']) ? $data['min_overtime'] : 0;
              },
              'headerOptions' => [
                'class'=>'bg-success text-center'
              ],
              'columnOptions' => [
                'class' => 'col-md-1',
              ],
              'options' => [
                'clientOptions' => [
                  'alias' => 'decimal',
                  'groupSeparator' => ',',
                  'autoGroup' => true,
                ],
                'options' => [
                  'class' => 'form-control',
                ]
              ],

            ],
            [
              'name'  => 'use_taxi',
              'title' => 'Use Taxi?',
              'type'  => Select2::classname(),
              'value' => function($data) {
                  return !empty($data['use_taxi']) ? $data['use_taxi'] : 0;
              },
              'headerOptions' => [
                'class'=>'bg-success text-center'
              ],
              'columnOptions' => [
                'class' => 'col-md-1',
              ],
              'options' => [
                'class' => 'form-control',
                'data' => [1=>"Yes", 0=>"No"],

              ],
            ],
            [
              'name'  => 'min_clock',
              'title' => 'Min Clock',
              'type'=> TimePicker::classname(),
              'value' => function($data) {
                  return !empty($data['min_clock']) ? $data['min_clock'] : 0;
              },
              'headerOptions' => [
                'class'=>'bg-success text-center'
              ],
              'columnOptions' => [
                'class' => 'col-md-1',
              ],
              'options' => [
                'class' => '',
                'pluginOptions' => [
                  'showSeconds' => false,
                  'showMeridian' => false,
                  'minuteStep' => 1,
                  'secondStep' => 5,
                  'template' => false
                ],

              ],
            ],
            [
              'name'  => 'allowance_amount',
              'title' => 'Allowance Amount',
              'type'=>MaskedInput::className(),
              'value' => function($data) {
                  return !empty($data['allowance_amount']) ? $data['allowance_amount'] : 0;
              },
              'headerOptions' => [
                'class'=>'bg-success text-center'
              ],
              'columnOptions' => [
                'class' => 'col-md-2',
              ],
              'options' => [
                'clientOptions' => [
                  'alias' => 'decimal',
                  'groupSeparator' => ',',
                  'autoGroup' => true,
                ],
                'options' => [
                  'class' => 'form-control',
                ]
              ],
            ],
        ]
      ])->label(false);
      ?>
      </div>
    </div>
  <div class="row">
    <div class="col-md-12 text-right m-t-5">
      <hr class="m-b-5"/>
      <?= Html::submitButton('UPDATE', ['class' => 'btn btn-warning p-t-10 p-b-10']) ?>
      <button type="button" class="btn btn-info p-t-10 p-b-10" style="font-size: 12px" data-dismiss="modal">CANCEL</button>
    </div>
  </div>

<?php ActiveForm::end();  ?>
<script type="text/javascript">
$('#crud-form').on('beforeSubmit', function() {
  var $form = new FormData($('#crud-form')[0]);
  $.ajax({
    url: paramJs.urlFormSave,
    type: 'POST',
    data: $form,
    async: false,
    cache: false,
    contentType: false,
    processData: false,

    success: function (data) {
      if(data != 1){
        $('#error-summary').html('<div class="alert alert-danger">' + data + '</div>');
      }else{
        $('#crud-modal').modal('hide');
        reload();
      }
    },
    error: function(jqXHR, errMsg) {
      alert(errMsg);
    }
  });
  return false;
});
</script>
