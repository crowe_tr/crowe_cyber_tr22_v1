<?php

use yii\helpers\Html;
use yii\web\View;
use kartik\grid\GridView;
use yii\helpers\Url;


$this->title = 'Rules';
$this->params['breadcrumbs'][] = 'SETUP';
$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs_btn'] = Html::a(
    'TERM ZONE',
    ['/st/ruletermzone'],
    [
      'class' => 'btn btn-info text-white ',
    ]
  ).'&nbsp;'.Html::a(
    'ITEM',
    ['/st/ruleitem'],
    [
      'class' => 'btn btn-info text-white ',
    ]
  ).'&nbsp;'.Html::a(
    '<i class="pg-plus"></i> CREATE NEW',
    false,
    [
      'onclick' => 'FormCreate()',
      'class' => 'btn btn-warning text-white ',
    ]
  );

$this->params['crud_ajax'] = true;

$column = [
        [
            'class' => 'kartik\grid\SerialColumn',
            'header' => 'No.',
            'mergeHeader' => false,
            'headerOptions' => ['class' => 'bg-success b-r'],
            'contentOptions' => ['class' => 'text-right b-r'],
            'filterOptions' => ['class' => 'b-b b-grey b-r'],
            'width' => '36px',
        ],
        [
            'attribute' => 'description',
            'headerOptions' => ['class' => 'col-sm-10 bg-success'],
            'filterOptions' => ['class' => 'b-b b-grey'],
            'contentOptions' => ['class' => 'kv-align-middle'],
        ],
        [
            'attribute' => 'effective_date',
            'headerOptions' => ['class' => 'col-sm-2 bg-success'],
            'filterOptions' => ['class' => 'b-b b-grey'],
            'contentOptions' => ['class' => 'kv-align-middle'],
        ],
        [
            'header' => '',
            'headerOptions' => ['class' => 'bg-success text-center'],
            'filterOptions' => ['class' => 'b-b b-grey'],
            'contentOptions' => ['class' => 'no-padding'],

            'format' => 'raw',
            'value' => function ($data) {
                $val = '<div class="tooltip-action">
                          <div class="trigger">
                            '.Html::a('<i class="fa fa-ellipsis-h"></i>', false, ['class' => 'text-info', 'data-toggle' => 'tooltip', 'data-original-title' => 'Up here!']).'
                          </div>
                          <div class="action-mask">
                            <div class="action">
                              '.Html::a(
                                '<i class="fa fa-edit"></i>',
                                false,
                                [
                                    'onclick' => "FormUpdate('".Yii::$app->urlManager->createUrl([Url::current(['form']), 'id' => $data->id])."')",
                                    'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
                                    'style' => 'bjob-radius: 5px !important',
                                ]
                              ).'
                            '.Html::a('<i class="pg-trash"></i>',
                              false,
                              [
                                  'onclick' => "FormDelete('".Yii::$app->urlManager->createUrl([Yii::$app->request->getPathInfo()."/delete", 'id' => $data->id])."')",
                                  'class' => 'btn btn-danger text-white padding-5 p-l-10 p-r-10',
                              ]).'
                            </div>
                          </div>
                        </div>';

                return $val;
            },
        ],
    ];
?>
<?php \yii\widgets\Pjax::begin(['id' => 'crud-pjax', 'enablePushState' => false]); ?>
    <div class="modal" id="crud-modal" role="dialog" aria-hidden="false" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-xlg ">
            <div class="modal-content no-border  modal-xlg ">
                <div class="modal-header bg-success text-white">
                </div>
                <div id="detail" class="modal-body padding-20">
                </div>
            </div>
        </div>
    </div>

    <?php
    echo GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $column,
            'layout'=>'
                <div class="card card-default">
                    <div class="row ">
                    <div class="col-md-12">
                        {items}
                    </div>
                    </div>
                    <div class="row padding-10">
                    <div class="col-md-4">{summary}</div>
                    <div class="col-md-8">{pager}</div>
                    </div>
                </div>
                ',
            'resizableColumns' => true,
            'bordered' => false,
            'striped' => true,
            'condensed' => false,
            'responsive' => false,
            'hover' => true,
            'persistResize' => false,
        ]
    );
    ?>
<?php \yii\widgets\Pjax::end(); ?>
