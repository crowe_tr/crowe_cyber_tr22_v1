<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use common\models\st\TaskType;
use common\models\cm\Division;
use common\models\cm\Dept;
use common\models\cm\Level;
use common\components\HrHelper;
use yii\web\JsExpression;

$form = ActiveForm::begin([
  'id' => 'crud-form',
  'enableClientValidation' => true,
  'validateOnSubmit' => true,
  'validateOnChange' => true,
  'validateOnType' => true,
  'fieldConfig' => [
    'template' => '{label}{input}',
    'options' => [
      'class' => 'form-group form-group-default',
    ],
  ],
  'errorSummaryCssClass' => 'alert alert-danger'
]);
echo $form->errorSummary($model);;
echo $form->field($model, 'id', ['options' => ['class' => '']])->hiddenInput()->label(false);

?>
<div class="row">
  <div id="error-summary" class="col-md-12 ">

  </div>
</div>
<div class="form-group-attached mb-3">
  <div class="row">
    <div class="col-md-2">
      <?= $form->field($model, 'seq')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-3">
      <?php
      echo $form->field($model, 'task_type_id', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
        Select2::classname(),
        [
          'data' => ArrayHelper::map(TaskType::find()->all(), 'id', 'task_type_name'),
          'options' => ['id' => 'type', 'placeholder' => 'Select ...'],
        ]
      );
      ?>
    </div>
    <div class="col-md-7">
      <?= $form->field($model, 'task_name')->textInput(['maxlength' => true]) ?>
    </div>
  </div>
</div>

<b>ASSIGN APPROVAL TO : </b>

<div class="form-group-attached mb-3 mt-1">
  <div class="row">
    <div class="col-md-6">
      <?php
      $manager = HrHelper::getEmployee($model->manager_id);
      echo $form->field($model, 'manager_id', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])
        ->widget(Select2::classname(), [
          'initValueText' => !empty($manager->full_name) ? $manager->user_id . ' - ' . $manager->full_name : "",
          'options' => ['placeholder' => 'Search Manager ...'],
          'pluginOptions' => [
            'tags' => false,
            'allowClear' => true,
            'minimumInputLength' => 2,
            'ajax' => [
              'url' => Url::to(['/hr/helper/srcemployee']),
              'dataType' => 'json',
              'data' => new JsExpression('function(params) { return {q:params.term}; }'),
            ],
          ],
        ]);
      ?>
    </div>
    <div class="col-md-6">
      <?php
      $supervisor = HrHelper::getEmployee($model->supervisor_id);
      echo $form->field($model, 'supervisor_id', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])
        ->widget(Select2::classname(), [
          'initValueText' => !empty($supervisor->full_name) ? $supervisor->user_id . ' - ' . $supervisor->full_name : "",
          'options' => ['placeholder' => 'Search Supervisor ...'],
          'pluginOptions' => [
            'tags' => false,
            'allowClear' => true,
            'minimumInputLength' => 2,
            'ajax' => [
              'url' => Url::to(['/hr/helper/srcemployee']),
              'dataType' => 'json',
              'data' => new JsExpression('function(params) { return {q:params.term}; }'),
            ],
          ],
        ]);
      ?>
    </div>
  </div>
</div>

<b>ASSIGN THIS TASK ONLY TO : </b>

<div class="form-group-attached mt-1">
  <div class="row">

    <div class="col-12">
      <?php
      echo $form->field($model, 'emp_dept')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Dept::find()->all(), 'dept_name', 'dept_name'),
        'options' => ['placeholder' => 'Sign to Departement ...', 'multiple' => true],
        'pluginOptions' => [
          'tokenSeparators' => [','],
        ],
      ]);
      ?>
    </div>
  </div>

  <div class="row">
    <div class="col-12">
      <?php
      echo $form->field($model, 'emp_division')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Division::find()->all(), 'div_name', 'div_name'),
        'options' => ['placeholder' => 'Sign to Division ...', 'multiple' => true],
        'pluginOptions' => [
          'tokenSeparators' => [','],
        ],
      ]);
      ?>
    </div>
  </div>
  <div class="row">

    <div class="col-12">
      <?php
      echo $form->field($model, 'emp_level')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Level::find()->all(), 'level_name', 'level_name'),
        'options' => ['placeholder' => 'Sign to Level ...', 'multiple' => true],
        'pluginOptions' => [
          'tokenSeparators' => [','],
        ],
      ]);
      ?>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12 text-right m-t-5">
    <hr class="m-b-5" />
    <?= Html::submitButton('UPDATE', ['class' => 'btn btn-warning p-t-10 p-b-10']) ?>
    <button type="button" class="btn btn-info p-t-10 p-b-10" style="font-size: 12px" data-dismiss="modal">CANCEL</button>
  </div>
</div>

<?php ActiveForm::end();  ?>
<script type="text/javascript">
  $('#crud-form').on('beforeSubmit', function() {
    var $form = new FormData($('#crud-form')[0]);
    $.ajax({
      url: paramJs.urlFormSave,
      type: 'POST',
      data: $form,
      async: false,
      cache: false,
      contentType: false,
      processData: false,

      success: function(data) {
        if (data != 1) {
          $('#error-summary').html('<div class="alert alert-danger">' + data + '</div>');
        } else {
          $('#crud-modal').modal('hide');
          reload();
        }
      },
      error: function(jqXHR, errMsg) {
        alert(errMsg);
      }
    });
    return false;
  });
</script>
