<?php
use mdm\admin\components\MenuHelper;
use yii\bootstrap\Nav;
use common\components\CommonHelper; //helper dipanggil disni :O ASIIIKK

?>

<nav class="page-sidebar" data-pages="sidebar">
	<div class="sidebar-overlay-slide from-top" id="appMenu">
	</div>
	<div class="sidebar-header">
		<img src="<?php echo $this->theme->baseUrl; ?>/assets/img/logo_white.png" alt="logo" class="brand">
	</div>

<div class="sidebar-menu">
		<?php
        $session = Yii::$app->session;
        if (!isset($session['__leftmenu'])) {
            $callback = function ($menu) {
                return [
              'label' => $menu['name'],
              'url' => $menu['route'],
              'data' => json_decode($menu['data'], true),
              'items' => $menu['children'],
          ];
            };
            $menus = MenuHelper::getAssignedMenu(Yii::$app->user->id, null, $callback, true);
            $html_menu = '';
            $left_menu = CommonHelper::CreateLeftMenuBasedOnMDM($menus, $html_menu, 1, 1, '', 'pcr');

            $session->set('__leftmenu', $left_menu);
            echo $session['__leftmenu'];
        } else {
            echo $session['__leftmenu'];
        }
    ?>
		<div class="clearfix"></div>
	</div>
</nav>
