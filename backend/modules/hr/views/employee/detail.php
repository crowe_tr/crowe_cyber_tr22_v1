<?php

use common\models\hr\HrAnnualLeave;

$leader_id = !empty($model->leader->user_id) ? $model->leader->user_id : "";
$leader_fullName = !empty($model->leader->full_name) ? $model->leader->full_name : "";
$manager_id = !empty($model->manager->user_id) ? $model->manager->user_id : "";
$manager_name = !empty($model->manager->full_name) ? $model->manager->full_name : "";
$project_manager = !empty($model->projectmanager[0]['project_manager']) ? $model->projectmanager[0]['project_manager'] : "";
// $project_manager = $model->projectmanager[0]['project_manager'];
$project_supervisor = !empty($model->projectsupervisor[0]['project_supervisor']) ? $model->projectsupervisor[0]['project_supervisor'] : "";
$supervisor_id = !empty($model->supervisor->user_id) ? $model->supervisor->user_id : "";
$supervisor_name = !empty($model->supervisor->full_name) ? $model->supervisor->full_name : "";

?>
<div id="wrapper-<?= $model->user_id; ?>">
    <div class="row">
        <div class="col-md-6">
            <p class="m-t-15"><b>BIODATA</b></p>
            <table class="table table-bordered table-striped ">
                <tr>
                    <th class="col-md-4">
                        User Name
                    </th>
                    <td class="col-md-8">
                        <?= $model->user_name; ?>
                    </td>
                </tr>
                <tr>
                    <th class="col-md-4">
                        Initial
                    </th>
                    <td class="col-md-8">
                        <?= $model->initial; ?>
                    </td>
                </tr>
                <tr>
                    <th class="col-md-4">
                        Gender
                    </th>
                    <td class="col-md-8">
                        <?= ($model->gender == 'm') ? "MALE" : (($model->gender == 'f') ? "FEMALE" : ""); ?>
                    </td>
                </tr>
                <tr>
                    <th class="col-md-4">
                        Place Of Birth
                    </th>
                    <td class="col-md-8">
                        <?= $model->place_of_birth; ?>
                    </td>
                </tr>
                <tr>
                    <th class="col-md-4">
                        Date Of Birth
                    </th>
                    <td class="col-md-8">
                        <?= $model->date_of_birth; ?>
                    </td>
                </tr>
                <tr>
                    <th class="col-md-4">
                        Religion
                    </th>
                    <td class="col-md-8">
                        <?= $model->religion; ?>
                    </td>
                </tr>
                <tr>
                    <th class="col-md-4">
                        Marital Status
                    </th>
                    <td class="col-md-8">
                        <?= $model->marital_status; ?>
                    </td>
                </tr>
                <tr>
                    <th class="col-md-4">
                        Nationality
                    </th>
                    <td class="col-md-8">
                        <?= $model->nationality; ?>
                    </td>
                </tr>
                <tr>
                    <th class="col-md-4">
                        Education
                    </th>
                    <td class="col-md-8">
                        <?= $model->education; ?>
                    </td>
                </tr>
            </table>
            <p class="m-t-15"><b>CONTACT</b></p>
            <table class="table table-bordered table-striped ">
                <tr>
                    <th class="col-md-4">
                        Mobile Phone
                    </th>
                    <td class="col-md-8">
                        <?= $model->mobile_phone; ?>
                        </th>
                </tr>
                <tr>
                    <th class="col-md-4">
                        Email
                    </th>
                    <td class="col-md-8">
                        <?= $model->user_email; ?>
                        </th>
                </tr>
                <tr>
                    <th class="col-md-4">
                        Address
                    </th>
                    <td class="col-md-8">
                        <?= $model->address; ?>
                        </th>
                </tr>
            </table>
        </div>
        <div class="col-md-6">
            <p class="m-t-15"><b>EMPLOYMENT STATUS</b></p>
            <table class="table table-bordered table-striped ">
                <tr>
                    <th class="col-md-4">
                        Group
                    </th>
                    <td class="col-md-8">
                        <?= $leader_id; ?> - <?= $leader_fullName; ?>
                    </td>
                </tr>
                <tr>
                    <th class="col-md-4">
                        Manager
                    </th>
                    <td class="col-md-8">
                        <?= $manager_id; ?> - <?= $manager_name; ?>
                    </td>
                </tr>
                <tr>
                    <th class="col-md-4">
                        Project Manager
                    </th>
                    <td class="col-md-8">
                        <?= $project_manager; ?>
                    </td>
                </tr>
                <tr>
                    <th class="col-md-4">
                        Supervisor
                    </th>
                    <td class="col-md-8">
                        <?= $supervisor_id; ?> - <?= $supervisor_name; ?>
                    </td>
                </tr>
                <tr>
                    <th class="col-md-4">
                        Project Supervisor
                    </th>
                    <td class="col-md-8">
                        <?= $project_supervisor; ?>
                    </td>
                </tr>
                <tr>
                    <th class="col-md-4">
                        Join Date
                    </th>
                    <td class="col-md-8">
                        <?= $model->join_date; ?>
                    </td>
                </tr>
                <tr>
                    <th class="col-md-4">
                        Resign Date
                    </th>
                    <td class="col-md-8">
                        <?= $model->resign_date; ?>
                    </td>
                </tr>
                <tr>
                    <th class="col-md-4">
                        Resign Description
                    </th>
                    <td class="col-md-8">
                        <?= $model->resign_description; ?>
                    </td>
                </tr>
            </table>
            <p class="m-t-15"><b>ANNUAL VACATION</b></p>
            <table class="table-bordered table-striped ">
                <tr>
                    <th class="col-md-4">
                        keepVacationBalance
                    </th>
                    <td class="col-md-8">
                        <?= $model->keep_vacation_balance; ?>
                    </td>
                </tr>
                <tr>
                    <th class="col-md-4">
                        annualVacation
                    </th>
                    <td class="col-md-8">
                        <?= $model->annual_vacation; ?>
                    </td>
                </tr>
            </table>
            <p class="m-t-15"><b>ANNUAL LEAVE</b></p>
            <table class="table-bordered table-striped ">
                <tr>
                    <th class="col-1">
                        Year
                    </th>
                    <th class="col-2">
                        Days
                    </th>
                    <th class="col-2">
                        OB Hours
                    </th>
                    <th class="col-2">
                        Used Hours
                    </th>
                    <th class="col-2">
                        Balance Hours
                    </th>
                    <th class="col-2">
                        Balance Days
                    </th>
                    <th>
                    </th>
                </tr>
                <?php
                $annual_leave = HrAnnualLeave::find()->where(['employee_id' => $model->user_id])->all();
                foreach ($annual_leave as $al) {
                    echo "<tr>
                    <td>" . $al->annual_year . "</td>
                    <td>" . intval($al->annual_days) . " Days</td>
                    <td>" . intval($al->annual_used_hours_ob) . " Hours</td>
                    <td>" . intval($al->annual_used_hours) . " Hours</td>
                    <td>" . intval($al->annual_balance_hours) . " Hours</td>
                    <td>" . intval($al->annual_balance_days) . " Days</td>
                    <td><a class='btn btn-default btn-sm' onclick='FormShowModalAnnual(" . $al->id . ");'><i class='fa fa-edit'></i></a></td>
                </tr>";
                }
                ?>
            </table>
        </div>
    </div>
</div>
