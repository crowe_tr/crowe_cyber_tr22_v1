<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\MaskedInput;
use kartik\widgets\ActiveForm;
use kartik\widgets\DepDrop;
use kartik\widgets\FileInput;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use yii\web\JsExpression;

use common\models\cm\CmMaster;
use common\models\cm\Entity;
use common\models\cm\Dept;
use common\models\cm\Division;
use common\models\cm\Level;
use common\models\hr\Employee;

use common\components\HrHelper;

$this->title = 'Employees';
$this->params['breadcrumbs'][] = "Setup";
$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs_btn'] = Html::a(
    'BACK',
    ['index'],
    [
        //'onclick' => 'FormCreate()',
        'class' => 'btn btn-info text-white ',
    ]
);


?>

<div class="row">
    <div class="col-md-12">
        <div class="card ">
            <div class="card-body">
                <?php
                $form = ActiveForm::begin([
                    'id' => 'crud-form',
                    //'action'=>['save'],
                    'enableClientValidation' => true,
                    'validateOnSubmit' => true,
                    'validateOnChange' => true,
                    'validateOnType' => true,
                    'fieldConfig' => [
                        'template' => '{label}{input}',
                        'options' => [
                            'class' => 'form-group form-group-default',
                        ],
                    ],
                    'errorSummaryCssClass' => 'alert alert-danger'
                ]);
                echo $form->errorSummary($model);
                echo $form->field($model, 'guid', ['options' => ['class' => '']])->hiddenInput()->label(false);

                ?>
                <div id="error" class="alert alert-danger" style="display:none"></div>
                <div class="row m-b-30">
                    <div class="col-md-12">
                        <div class="form-group-attached">
                            <div class="row">
                                <div class="col-md-2">
                                    <?php
                                    echo $form->field($model, 'user_id')->textInput(['maxlength' => 32]);
                                    ?>
                                </div>
                                <div class="col-md-7">
                                    <?= $form->field($model, 'full_name')->textInput(['maxlength' => 32]) ?>
                                </div>
                                <div class="col-md-3">
                                    <?= $form->field($model, 'initial')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <?= $form->field(
                                        $model,
                                        'gender',
                                        [
                                            'options' => [
                                                'class' => ' form-group form-group-default form-group-default-select2',
                                            ],
                                        ]
                                    )->widget(
                                        Select2::classname(),
                                        [
                                            'data' => [
                                                'm' => Yii::t('backend', 'Male'),
                                                'f' => Yii::t('backend', 'Female'),
                                            ],
                                            'options' => ['placeholder' => 'Select ...'],
                                        ]
                                    ) ?>

                                </div>
                                <div class="col-md-3">
                                    <?= $form->field($model, 'date_of_birth')->widget(MaskedInput::className(), ['clientOptions' => ['alias' => 'dd/mm/yyyy']]) ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($model, 'place_of_birth')->textInput() ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <?php
                                    echo $form->field($model, 'religion', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
                                        Select2::classname(),
                                        [
                                            'data' => [
                                                'MOSLEM' => 'MOSLEM',
                                                'CATHOLIC' => 'CATHOLIC',
                                                'CHRISTIAN' => 'CHRISTIAN',
                                                'HINDU' => 'HINDU',
                                                'BUDHA' => 'BUDHA',
                                                'KHONGHUCU' => 'KHONGHUCU',
                                            ],
                                            'options' => ['placeholder' => 'Select ...'],
                                        ]
                                    );
                                    ?>
                                </div>
                                <div class="col-md-3">
                                    <?php
                                    echo $form->field($model, 'marital_status', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
                                        Select2::classname(),
                                        [
                                            'data' => [
                                                'S/0' => 'S/0',
                                                'S/1' => 'S/1',
                                                'S/2' => 'S/2',
                                                'S/3' => 'S/3',
                                                'M/0' => 'M/0',
                                                'M/1' => 'M/1',
                                                'M/2' => 'M/2',
                                                'M/3' => 'M/3',
                                            ],
                                            'options' => ['placeholder' => 'Select ...'],
                                        ]
                                    );
                                    ?>
                                </div>
                                <div class="col-md-4">
                                    <?php
                                    echo $form->field($model, 'nationality', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
                                        Select2::classname(),
                                        [
                                            'options' => ['placeholder' => 'Select...'],
                                            'data' => [
                                              'INDONESIA'  =>  'INDONESIA',
                                              // 'AFGHANISTAN'  =>  'AFGHANISTAN',
                                              // 'SOUTH AFRICA'  =>  'SOUTH AFRICA',
                                              // 'CENTRAL AFRICAN'  =>  'CENTRAL AFRICAN',
                                              // 'ALBANIA'  =>  'ALBANIA',
                                              // 'ALGERIA'  =>  'ALGERIA',
                                              // 'UNITED STATES OF AMERICA'  =>  'UNITED STATES OF AMERICA',
                                              // 'ANDORRA'  =>  'ANDORRA',
                                              // 'ANGOLA'  =>  'ANGOLA',
                                              // 'ANTIGUA DAN BARBUDA'  =>  'ANTIGUA DAN BARBUDA',
                                              // 'SAUDI ARABIA'  =>  'SAUDI ARABIA',
                                              // 'ARGENTINA'  =>  'ARGENTINA',
                                              // 'ARMENIA'  =>  'ARMENIA',
                                              // 'AUSTRALIA'  =>  'AUSTRALIA',
                                              // 'AUSTRIA'  =>  'AUSTRIA',
                                              // 'AZERBAIJAN'  =>  'AZERBAIJAN',
                                              // 'BAHAMA'  =>  'BAHAMA',
                                              // 'BAHRAIN'  =>  'BAHRAIN',
                                              // 'BANGLADESH'  =>  'BANGLADESH',
                                              // 'BARBADOS'  =>  'BARBADOS',
                                              // 'NETHERLANDS'  =>  'NETHERLANDS',
                                              // 'BELARUS'  =>  'BELARUS',
                                              // 'BELGIUM'  =>  'BELGIUM',
                                              // 'BELIZE'  =>  'BELIZE',
                                              // 'BENIN'  =>  'BENIN',
                                              // 'BHUTAN'  =>  'BHUTAN',
                                              // 'BOLIVIA'  =>  'BOLIVIA',
                                              // 'BOSNIA AND HERZEGOVINA'  =>  'BOSNIA AND HERZEGOVINA',
                                              // 'BOTSWANA'  =>  'BOTSWANA',
                                              // 'BRAZIL'  =>  'BRAZIL',
                                              // 'GREAT BRITAIN'  =>  'GREAT BRITAIN',
                                              'BRUNEI DARUSSALAM'  =>  'BRUNEI DARUSSALAM',
                                              // 'BULGARIA'  =>  'BULGARIA',
                                              // 'BURKINA FASO'  =>  'BURKINA FASO',
                                              // 'BURUNDI'  =>  'BURUNDI',
                                              // 'CZECH REPUBLIC'  =>  'CZECH REPUBLIC',
                                              // 'CHAD'  =>  'CHAD',
                                              // 'CHILE'  =>  'CHILE',
                                              // 'CHINA'  =>  'CHINA',
                                              // 'DENMARK'  =>  'DENMARK',
                                              // 'DJIBOUTI'  =>  'DJIBOUTI',
                                              // 'DOMINICA'  =>  'DOMINICA',
                                              // 'ECUADOR'  =>  'ECUADOR',
                                              // 'EL SALVADOR'  =>  'EL SALVADOR',
                                              // 'ERITREA'  =>  'ERITREA',
                                              // 'ESTONIA'  =>  'ESTONIA',
                                              // 'ETHIOPIA'  =>  'ETHIOPIA',
                                              // 'FIJI'  =>  'FIJI',
                                              'PHILIPPINES'  =>  'PHILIPPINES',
                                              // 'FINLANDIA'  =>  'FINLANDIA',
                                              // 'GABON'  =>  'GABON',
                                              // 'GAMBIA'  =>  'GAMBIA',
                                              // 'GEORGIA'  =>  'GEORGIA',
                                              // 'GHANA'  =>  'GHANA',
                                              // 'GRENADA'  =>  'GRENADA',
                                              // 'GUATEMALA'  =>  'GUATEMALA',
                                              // 'GUINEA'  =>  'GUINEA',
                                              // 'GUINEA-BISSAU'  =>  'GUINEA-BISSAU',
                                              // 'GUINEA KHATULISTIWA'  =>  'GUINEA KHATULISTIWA',
                                              // 'GUYANA'  =>  'GUYANA',
                                              // 'HAITI'  =>  'HAITI',
                                              // 'HONDURAS'  =>  'HONDURAS',
                                              // 'HUNGARY'  =>  'HUNGARY',
                                              // 'INDIA'  =>  'INDIA',
                                              // 'UNITED KINGDOM'  =>  'UNITED KINGDOM',
                                              // 'IRAK'  =>  'IRAK',
                                              // 'IRAN'  =>  'IRAN',
                                              // 'IRELAND'  =>  'IRELAND',
                                              // 'ICELAND'  =>  'ICELAND',
                                              // 'ISRAEL'  =>  'ISRAEL',
                                              // 'ITALY'  =>  'ITALY',
                                              // 'JAMAICA'  =>  'JAMAICA',
                                              // 'JAPAN'  =>  'JAPAN',
                                              // 'GERMANY'  =>  'GERMANY',
                                              'CAMBODIA'  =>  'CAMBODIA',
                                              // 'CAMEROON'  =>  'CAMEROON',
                                              // 'CANADA'  =>  'CANADA',
                                              // 'KAZAKHSTAN'  =>  'KAZAKHSTAN',
                                              // 'KENYA'  =>  'KENYA',
                                              // 'KYRGYZSTAN'  =>  'KYRGYZSTAN',
                                              // 'KIRIBATI'  =>  'KIRIBATI',
                                              // 'COLOMBIA'  =>  'COLOMBIA',
                                              // 'COMOROS'  =>  'COMOROS',
                                              // 'CONGO REPUBLIC'  =>  'CONGO REPUBLIC',
                                              // 'SOUTH KOREA'  =>  'SOUTH KOREA',
                                              // 'NORTH KOREA'  =>  'NORTH KOREA',
                                              // 'COSTA RICA'  =>  'COSTA RICA',
                                              // 'CROATIA'  =>  'CROATIA',
                                              // 'CUBA'  =>  'CUBA',
                                              // 'KUWAIT'  =>  'KUWAIT',
                                              'LAOS'  =>  'LAOS',
                                              // 'LATVIA'  =>  'LATVIA',
                                              // 'LEBANON'  =>  'LEBANON',
                                              // 'LESOTHO'  =>  'LESOTHO',
                                              // 'LIBERIA'  =>  'LIBERIA',
                                              // 'LIBYA'  =>  'LIBYA',
                                              // 'LIECHTENSTEIN'  =>  'LIECHTENSTEIN',
                                              // 'LITHUANIA'  =>  'LITHUANIA',
                                              // 'LUXEMBOURG'  =>  'LUXEMBOURG',
                                              // 'MADAGASCAR'  =>  'MADAGASCAR',
                                              // 'MACEDONIA'  =>  'MACEDONIA',
                                              // 'MALDIVES'  =>  'MALDIVES',
                                              // 'MALAWI'  =>  'MALAWI',
                                              'MALAYSIA'  =>  'MALAYSIA',
                                              // 'MALI'  =>  'MALI',
                                              // 'MALTA'  =>  'MALTA',
                                              // 'MOROCCO'  =>  'MOROCCO',
                                              // 'MARSHALL ISLANDS'  =>  'MARSHALL ISLANDS',
                                              // 'MAURITANIA'  =>  'MAURITANIA',
                                              // 'MAURITIUS'  =>  'MAURITIUS',
                                              // 'MEXICO'  =>  'MEXICO',
                                              // 'EGYPT'  =>  'EGYPT',
                                              // 'MICRONESIA'  =>  'MICRONESIA',
                                              // 'MOLDOVA'  =>  'MOLDOVA',
                                              // 'MONACO'  =>  'MONACO',
                                              // 'MONGOLIA'  =>  'MONGOLIA',
                                              // 'MONTENEGRO'  =>  'MONTENEGRO',
                                              // 'MOZAMBIQUE'  =>  'MOZAMBIQUE',
                                              'MYANMAR'  =>  'MYANMAR',
                                              // 'NAMIBIA'  =>  'NAMIBIA',
                                              // 'NAURU'  =>  'NAURU',
                                              // 'NEPAL'  =>  'NEPAL',
                                              // 'NIGER'  =>  'NIGER',
                                              // 'NIGERIA'  =>  'NIGERIA',
                                              // 'NICARAGUA'  =>  'NICARAGUA',
                                              // 'NORWAY'  =>  'NORWAY',
                                              // 'OMAN'  =>  'OMAN',
                                              // 'PAKISTAN'  =>  'PAKISTAN',
                                              // 'PALAU'  =>  'PALAU',
                                              // 'PANAMA'  =>  'PANAMA',
                                              // 'IVORY COAST'  =>  'IVORY COAST',
                                              // 'PAPUA NEW GUINEA'  =>  'PAPUA NEW GUINEA',
                                              // 'PARAGUAY'  =>  'PARAGUAY',
                                              // 'FRANCE'  =>  'FRANCE',
                                              // 'PERU'  =>  'PERU',
                                              // 'POLAND'  =>  'POLAND',
                                              // 'PORTUGAL'  =>  'PORTUGAL',
                                              // 'QATAR'  =>  'QATAR',
                                              // 'DEMOCRATIC REPUBLIC OF THE CONGO'  =>  'DEMOCRATIC REPUBLIC OF THE CONGO',
                                              // 'DOMINICAN REPUBLIC'  =>  'DOMINICAN REPUBLIC',
                                              // 'ROMANIA'  =>  'ROMANIA',
                                              // 'RUSSIA'  =>  'RUSSIA',
                                              // 'RWANDA'  =>  'RWANDA',
                                              // 'SAINT KITTS AND NEVIS'  =>  'SAINT KITTS AND NEVIS',
                                              // 'SAINT LUCIA'  =>  'SAINT LUCIA',
                                              // 'SAINT VINCENT AND THE GRENADINES'  =>  'SAINT VINCENT AND THE GRENADINES',
                                              // 'SAMOA'  =>  'SAMOA',
                                              // 'SAN MARINO'  =>  'SAN MARINO',
                                              // 'SAO TOME AND PRINCIPE'  =>  'SAO TOME AND PRINCIPE',
                                              // 'NEW ZEALAND'  =>  'NEW ZEALAND',
                                              // 'SENEGAL'  =>  'SENEGAL',
                                              // 'SERBIA'  =>  'SERBIA',
                                              // 'SEYCHELLES'  =>  'SEYCHELLES',
                                              // 'SIERRA LEONE'  =>  'SIERRA LEONE',
                                              'SINGAPORE'  =>  'SINGAPORE',
                                              // 'CYPRUS'  =>  'CYPRUS',
                                              // 'SLOVENIA'  =>  'SLOVENIA',
                                              // 'SLOVAKIA'  =>  'SLOVAKIA',
                                              // 'SOLOMON ISLANDS'  =>  'SOLOMON ISLANDS',
                                              // 'SOMALIA'  =>  'SOMALIA',
                                              // 'SPAIN'  =>  'SPAIN',
                                              // 'SRI LANKA'  =>  'SRI LANKA',
                                              // 'SUDAN'  =>  'SUDAN',
                                              // 'SOUTH SUDAN'  =>  'SOUTH SUDAN',
                                              // 'SYRIA'  =>  'SYRIA',
                                              // 'SURINAME'  =>  'SURINAME',
                                              // 'SWAZILAND'  =>  'SWAZILAND',
                                              // 'SWEDEN'  =>  'SWEDEN',
                                              // 'SWISS'  =>  'SWISS',
                                              // 'TAJIKISTAN'  =>  'TAJIKISTAN',
                                              // 'CAPE VERDE'  =>  'CAPE VERDE',
                                              // 'TANZANIA'  =>  'TANZANIA',
                                              'THAILAND'  =>  'THAILAND',
                                              'TIMOR LESTE'  =>  'TIMOR LESTE',
                                              // 'TOGO'  =>  'TOGO',
                                              // 'TONGA'  =>  'TONGA',
                                              // 'TRINIDAD AND TOBAGO'  =>  'TRINIDAD AND TOBAGO',
                                              // 'TUNISIA'  =>  'TUNISIA',
                                              // 'TURKEY'  =>  'TURKEY',
                                              // 'TURKMENISTAN'  =>  'TURKMENISTAN',
                                              // 'TUVALU'  =>  'TUVALU',
                                              // 'UGANDA'  =>  'UGANDA',
                                              // 'UKRAINE'  =>  'UKRAINE',
                                              // 'UNITED ARAB EMIRATES'  =>  'UNITED ARAB EMIRATES',
                                              // 'URUGUAY'  =>  'URUGUAY',
                                              // 'UZBEKISTAN'  =>  'UZBEKISTAN',
                                              // 'VANUATU'  =>  'VANUATU',
                                              // 'VENEZUELA'  =>  'VENEZUELA',
                                              'VIETNAM'  =>  'VIETNAM',
                                              // 'YEMEN'  =>  'YEMEN',
                                              // 'JORDAN'  =>  'JORDAN',
                                              // 'YUGOSLAVIA'  =>  'YUGOSLAVIA',
                                              // 'GREEK'  =>  'GREEK',
                                              // 'ZAMBIA'  =>  'ZAMBIA',
                                              // 'ZIMBABWE'  =>  'ZIMBABWE',
                                            ],
                                            'pluginOptions' => [
                                                'placeholder' => Yii::t('backend', 'Select..'),
                                            ],

                                        ]
                                    );
                                    ?>
                                </div>
                                <div class="col-md-2">
                                    <?php
                                    echo $form->field($model, 'education', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
                                        Select2::classname(),
                                        [
                                            'options' => ['placeholder' => 'Select...'],
                                            'data' => [
                                                'SD' => 'SD',
                                                'SMP' => 'SMP',
                                                'SMA' => 'SMA',
                                                'SMK' => 'SMK',
                                                'D3' => 'D3',
                                                'S1' => 'S1',
                                                'S2' => 'S2',
                                                'S3' => 'S3',
                                            ],
                                            'pluginOptions' => [
                                                'placeholder' => Yii::t('backend', 'Select..'),
                                            ],

                                        ]
                                    );
                                    ?>
                                </div>
                            </div>
                        </div>

                        <p class="m-t-15"><b>CONTACT</b></p>
                        <div class="form-group-attached">
                            <div class="row">
                                <div class="col-md-6">
                                    <?= $form->field($model, 'mobile_phone')->widget(MaskedInput::className(), ['mask' => '(+99) 999 999 999 9999']) ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($model, 'user_email')->textInput() ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <?= $form->field($model, 'address')->textarea(['rows' => 2, 'style' => 'height: 72px']) ?>
                                </div>
                            </div>
                        </div>

                        <p class="m-t-15"><b>EMPLOYMENT STATUS</b></p>
                        <div class="form-group-attached">
                            <div class="row">
                                <div class="col-md-2">
                                    <?php
                                    echo $form->field($model, 'entity_id', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
                                        Select2::classname(),
                                        [
                                            'options' => ['id' => 'entity_id', 'placeholder' => 'Select...'],
                                            'data' => ArrayHelper::map(Entity::find()->asArray()->all(), 'id', 'entity_name'),
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'placeholder' => Yii::t('backend', 'Select..'),
                                            ],

                                        ]
                                    );
                                    ?>
                                </div>
                                <div class="col-md-3">
                                    <?php
                                    echo $form->field($model, 'div_id', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
                                        Select2::classname(),
                                        [
                                            'options' => ['id' => 'div_id', 'placeholder' => 'Select...'],
                                            'data' => ArrayHelper::map(Division::find()->distinct()->all(), 'id', 'div_name'),
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'placeholder' => Yii::t('backend', 'Select..'),
                                            ],
                                        ]
                                    );
                                    ?>
                                </div>
                                <div class="col-md-3">
                                    <?php
                                    echo $form->field($model, 'dept_id', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(DepDrop::classname(), [
                                        'data' => ArrayHelper::map(Dept::find()->asArray()->all(), 'id', 'dept_name'),
                                        'options' => ['id' => 'dept_id', 'placeholder' => 'Select ...'],
                                        'type' => DepDrop::TYPE_SELECT2,
                                        'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                                        'pluginOptions' => [
                                            'depends' => ['divisionID'],
                                            'url' => Url::to(['/cm/helper/loaddept']),
                                            'loadingText' => 'Loading ...',
                                            'allowClear' => true,
                                            'placeholder' => Yii::t('backend', 'Select..'),
                                        ],
                                    ]);
                                    ?>
                                </div>

                                <div class="col-md-4">
                                    <?php
                                    echo $form->field($model, 'level_id', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
                                        Select2::classname(),
                                        [
                                            'options' => ['id' => 'level_id', 'placeholder' => 'Select...'],
                                            'data' => ArrayHelper::map(Level::find()->asArray()->all(), 'id', 'level_name'),
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'placeholder' => Yii::t('backend', 'Select..'),
                                            ],
                                        ]
                                    );
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <?= $form->field($model, 'join_date')->widget(DatePicker::classname(), [
                                        'type' => DatePicker::TYPE_INPUT,
                                        'options' => ['placeholder' => 'Enter Join date ...'],
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format' => 'dd/mm/yyyy',
                                        ],
                                    ])
                                    ?>
                                </div>

                                <div class="col-md-2">
                                    <?= $form->field($model, 'resign_date')->widget(DatePicker::classname(), [
                                        'type' => DatePicker::TYPE_INPUT,
                                        'options' => ['placeholder' => 'Enter Resign date ...'],
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format' => 'dd/mm/yyyy',
                                        ],
                                    ])
                                    ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($model, 'resign_description')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-md-2">
                                    <?php
                                    echo $form->field($model, 'flag', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
                                        Select2::classname(),
                                        [
                                            'options' => ['id' => 'flag', 'placeholder' => 'Select...'],
                                            'data' => [1 => 'Active', 0 => 'Resign'],
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'placeholder' => Yii::t('backend', 'Select..'),
                                            ],

                                        ]
                                    );
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <?php
                                    $parent = HrHelper::getParent($model->parent_id);
                                    echo $form->field($model, 'parent_id', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])
                                        ->widget(Select2::classname(), [
                                            'initValueText' => !empty($parent->full_name) ? $parent->user_id . ' - ' . $parent->full_name : "",
                                            'options' => ['placeholder' => 'Search Leader ...'],
                                            'pluginOptions' => [
                                                'tags' => false,
                                                'allowClear' => true,
                                                //'minimumInputLength' => 1,
                                                'ajax' => [
                                                    'url' => Url::to(['/hr/helper/srcleader', 'id' => $model->user_id]),
                                                    'dataType' => 'json',
                                                    'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                                                ],
                                                //'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                                //'templateResult' => new JsExpression('formatSelect2SupplierId'),
                                                //'templateSelection' => new JsExpression('formatSelect2SupplierIdSelection'),
                                            ],
                                        ]);
                                    ?>
                                </div>
                                <div class="col-md-4">
                                    <?php
                                    $supervisor = HrHelper::getParent($model->supervisor_id);
                                    echo $form->field($model, 'supervisor_id', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])
                                        ->widget(Select2::classname(), [
                                            'initValueText' => !empty($supervisor->full_name) ? $supervisor->user_id . ' - ' . $supervisor->full_name : "",
                                            'options' => ['placeholder' => 'Search Leader ...'],
                                            'pluginOptions' => [
                                                'tags' => false,
                                                'allowClear' => true,
                                                //'minimumInputLength' => 1,
                                                'ajax' => [
                                                    'url' => Url::to(['/hr/helper/srcleader', 'id' => $model->user_id]),
                                                    'dataType' => 'json',
                                                    'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                                                ],
                                                //'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                                //'templateResult' => new JsExpression('formatSelect2SupplierId'),
                                                //'templateSelection' => new JsExpression('formatSelect2SupplierIdSelection'),
                                            ],
                                        ]);
                                    ?>
                                </div>
                                <div class="col-md-4">
                                    <?php
                                    $manager = HrHelper::getParent($model->manager_id);
                                    echo $form->field($model, 'manager_id', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])
                                        ->widget(Select2::classname(), [
                                            'initValueText' => !empty($manager->full_name) ? $manager->user_id . ' - ' . $manager->full_name : "",
                                            'options' => ['placeholder' => 'Search Leader ...'],
                                            'pluginOptions' => [
                                                'tags' => false,
                                                'allowClear' => true,
                                                //'minimumInputLength' => 1,
                                                'ajax' => [
                                                    'url' => Url::to(['/hr/helper/srcleader', 'id' => $model->user_id]),
                                                    'dataType' => 'json',
                                                    'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                                                ],
                                                //'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                                //'templateResult' => new JsExpression('formatSelect2SupplierId'),
                                                //'templateSelection' => new JsExpression('formatSelect2SupplierIdSelection'),
                                            ],
                                        ]);
                                    ?>
                                </div>
                            </div>
                        </div>

                        <p class="m-t-15"><b>ANNUAL VACATION</b></p>
                        <div class="form-group-attached">
                            <div class="row">
                                <div class="col-md-4" style="display:none;">
                                    <?php
                                    echo $form->field($model, 'keep_vacation_balance', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
                                        Select2::classname(),
                                        [
                                            'options' => ['placeholder' => 'Select...'],
                                            'data' => [
                                                1 => 'Yes',
                                                0 => 'No',
                                            ],
                                            'pluginOptions' => [
                                                'placeholder' => Yii::t('backend', 'Select..'),
                                            ],

                                        ]
                                    );
                                    ?>
                                </div>
                                <div class="col-md-3" style="display:none;">
                                    <?= $form->field($model, 'annual_vacation')->textInput(['maxlength' => true, 'type' => 'number']) ?>
                                </div>
                                <div class="col-md-3">
                                    <?= $form->field($model, 'max_view_time_report')->textInput(['maxlength' => true, 'type' => 'number']) ?>
                                </div>
                            </div>
                        </div>

                        <p class="m-t-15"><b>USER'S RIGHT</b></p>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group-attached">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <?= $form->field($model, 'user_name')->textInput(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row m-t-5">
                            <div class="col-md-12 well padding-10">
                                <?php
                                echo $form->field($model, 'Permissions', [
                                    'options' => ['class' => '']
                                ])->label(false)->checkboxButtonGroup($data['roles']['crowe'], [
                                    'class' => 'btn-group',
                                    'disabledItems' => [''],
                                    'itemOptions' => ['labelOptions' => ['class' => 'btn btn-warning', 'style' => 'box-shadow: unset !important']],
                                ]);
                                ?>
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 m-t-5">
                            <hr class="m-b-5" />
                            <button id="button" type="button" value="SAVE" class="btn btn-warning btn-lg">SAVE</button>
                            <?=
                            Html::a(
                                'BACK',
                                ['index'],
                                [
                                    'class' => 'btn btn-info btn-lg text-white ',
                                    'data-dismiss' => "modal"
                                ]
                            );
                            ?>
                        </div>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var paramJs = (paramJs || {});
    paramJs.urlFormSave = '<?= Yii::$app->urlManager->createAbsoluteUrl(['hr/employee/save', 'id' => $id]); ?>';

    //$('#crud-form').on('beforeSubmit', function() {
    $('#button').click(function(event) {
        event.preventDefault(); //this will prevent the default submit
        showFullLoading();
        var $form = new FormData($('#crud-form')[0]);
        $.ajax({
            url: paramJs.urlFormSave,
            type: 'POST',
            data: $form,
            async: false,
            cache: false,
            contentType: false,
            processData: false,

            success: function(data) {
                hideFullLoading();

                if (data != 1) {
                    $('#error').show();
                    $('#error').html(data);
                } else {
                    notif("Success Updating data !");
                    <?php
                    echo 'window.location.href = "' . Yii::$app->urlManager->createAbsoluteUrl(['/hr/employee/index']) . '";';
                    ?>
                }
            },
            error: function(jqXHR, errMsg) {
                hideFullLoading();
                $('#error').show();
                $('#error').html(data);
            }
        });
        return false;
        event.preventDefault(); //this will prevent the default submit

    });
</script>
