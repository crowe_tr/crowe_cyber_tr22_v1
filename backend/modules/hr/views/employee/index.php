<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\web\View;

use common\models\cm\Entity;
use common\models\cm\Dept;
use common\models\cm\Division;
use common\models\cm\Level;

$this->title = 'Employees';
$this->params['breadcrumbs'][] = "Setup";
$this->params['breadcrumbs'][] = $this->title;

$open_timereport = '';
if ($user->is_admin == 1) {
  $open_timereport = Html::a(
    '<i class="fa fa-calendar"></i> <span class="hidden-xs">Reopen TimeReport for someone</span>',
    ['open_tr'],
    [
      'class' => 'dropdown-item',
    ]
  );
}

// if ($user->IsAdmin == 1) {
//   $open_timereport = Html::a(
//     '<i class="fa fa-calendar"></i> <span class="hidden-xs">Reopen TimeReport for someone</span>',
//     ['open_tr'],
//     [
//       'class' => 'dropdown-item',
//     ]
//   );
// }


$this->params['breadcrumbs_btn'] =
  '<div class="btn-group dropdown dropdown-default">
  <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Utility
  </button>
  <div class="dropdown-menu" style="width: max-content">
    ' . Html::a(
    '<i class="fa fa-calendar"></i> <span class="hidden-xs">OPEN TIMEREPORT 1 MONTH</span>',
    'javascript:;',
    [
      'onclick' => 'resetAnnualVacation("Are you sure you want to open 1 Month TimeReport MaxView for all employees ?","' . Yii::$app->urlManager->createAbsoluteUrl(['hr/employee/open1month']) . '")',
      'class' => 'dropdown-item',
    ]
  ) . '
    ' . Html::a(
    '<i class="fa fa-calendar"></i> <span class="hidden-xs">RESET TIMEREPORT</span>',
    'javascript:;',
    [
      'onclick' => 'resetAnnualVacation("Are you sure you want to reset TimeReport MaxView for all employees ?", "' . Yii::$app->urlManager->createAbsoluteUrl(['hr/employee/resetmaxviewtimereport']) . '")',
      'class' => 'dropdown-item',
    ]
  ) . '
  ' . Html::a(
    '<i class="fa fa-refresh"></i> <span class="hidden-xs">RESET ANNUAL LEAVE NEW</span>',
    'javascript:;',
    [
      'onclick' => 'FormShowModalResetAnnual();',
      'class' => 'dropdown-item',
    ]
  ) . ' ' . $open_timereport . '
  </div>
</div>'
  . "&nbsp;"
  . Html::a(
    '<i class="pg-plus"></i> <span class="hidden-xs">ADD NEW</span>',
    ['form'],
    [
      //'onclick' => 'FormCreate()',
      'class' => 'btn btn-warning text-white ',
    ]
  );
$this->params['crud_ajax'] = false;


$column = [
  [
    'class' => 'kartik\grid\ExpandRowColumn',
    'enableRowClick' => true,
    'mergeHeader' => false,
    'headerOptions' => ['class' => 'kartik-sheet-style b-l b-r  b-success bg-success'],
    'filterOptions' => ['class' => ''],
    'contentOptions' => ['class' => 'kv-align-middle bg-warning-lighter b-l b-r b-grey', 'style' => 'height: 50px'],
    'expandOneOnly' => true,
    'width' => '50px',
    'expandIcon' => '<i class="fa fa-angle-right"></i>',
    'collapseIcon' => '<i class="fa fa-angle-up"></i>',
    'value' => function ($model, $key, $index, $column) {
      return GridView::ROW_COLLAPSED;
    },
    'detailUrl' => Url::to(['/hr/employee/detail']),
  ],
  [
    'attribute' => 'user_id',
    'headerOptions' => ['class' => 'col-md-1 bg-success'],
    'contentOptions' => ['class' => 'bg-warning-lighter kv-align-middle'],
    'filterOptions' => ['class' => 'b-b b-grey'],
  ],
  [
    'attribute' => 'full_name',
    'headerOptions' => ['class' => 'col-sm-4 bg-success'],
    'filterOptions' => ['class' => 'b-b b-grey'],
    'contentOptions' => ['class' => 'kv-align-middle'],
  ],
  [
    'attribute' => 'entity_id',
    'headerOptions' => ['class' => 'col-sm-2 bg-success'],
    'filterOptions' => ['class' => 'b-b b-grey'],
    'contentOptions' => ['class' => 'kv-align-middle'],
    'filterType' => GridView::FILTER_SELECT2,
    'filterWidgetOptions' => [
      'pluginOptions' => ['allowClear' => true],
    ],
    'filterInputOptions' => ['placeholder' => ''],
    'format' => 'raw',
    'filter' => ArrayHelper::map(Entity::find()->all(), 'id', 'entity_name'),
    'value' => function ($data) {
      return empty($data->entity->entity_name) ? '-' : $data->entity->entity_name;
    },

  ],
  [
    'attribute' => 'div_id',
    'headerOptions' => ['class' => 'col-sm-2 bg-success'],
    'filterOptions' => ['class' => 'b-b b-grey'],
    'contentOptions' => ['class' => 'kv-align-middle'],
    'filterType' => GridView::FILTER_SELECT2,
    'filterWidgetOptions' => [
      'pluginOptions' => ['allowClear' => true],
    ],
    'filterInputOptions' => ['placeholder' => ''],
    'format' => 'raw',
    'filter' => ArrayHelper::map(Division::find()->all(), 'id', 'div_name'),
    'value' => function ($data) {
      return empty($data->division->div_name) ? '-' : $data->division->div_name;
    },

  ],
  [
    'attribute' => 'dept_id',
    'headerOptions' => ['class' => 'col-sm-2 bg-success'],
    'filterOptions' => ['class' => 'b-b b-grey'],
    'contentOptions' => ['class' => 'kv-align-middle'],
    'filterType' => GridView::FILTER_SELECT2,
    'filterWidgetOptions' => [
      'pluginOptions' => ['allowClear' => true],
    ],
    'filterInputOptions' => ['placeholder' => ''],
    'format' => 'raw',
    'filter' => ArrayHelper::map(Dept::find()->all(), 'id', 'dept_name'),
    'value' => function ($data) {
      return empty($data->dept->dept_name) ? '-' : $data->dept->dept_name;
    },

  ],
  [
    'attribute' => 'level_id',
    'headerOptions' => ['class' => 'col-sm-3 bg-success'],
    'filterOptions' => ['class' => 'b-b b-grey'],
    'contentOptions' => ['class' => 'kv-align-middle'],
    'value' => function ($data) {
      return empty($data->level->level_name) ? '-' : $data->level->level_name;
    },

  ],
  [
    'attribute' => 'flag',
    'headerOptions' => ['class' => 'col-sm-3 bg-success'],
    'filterOptions' => ['class' => 'b-b b-grey'],
    'contentOptions' => ['class' => 'kv-align-middle'],
    'filterType' => GridView::FILTER_SELECT2,
    'filterWidgetOptions' => [
      'pluginOptions' => ['allowClear' => true],
    ],
    'filterInputOptions' => ['placeholder' => ''],
    'format' => 'raw',
    'filter' => [1 => 'ACTIVE', 0 => 'RESIGN'],
    'value' => function ($data) {
      return ($data->flag == 1) ? 'ACTIVE' : 'RESIGN';
    },
  ],
  [
    'header' => '<i class="fa fa-cogs"></i>',
    'headerOptions' => ['class' => 'bg-success text-center'],
    'filterOptions' => ['class' => 'b-b b-grey'],
    'contentOptions' => ['class' => 'no-padding'],

    'format' => 'raw',
    'value' => function ($data) {
      $val = '<div class="tooltip-action">
                              <div class="trigger">
                                ' . Html::a('<i class="fa fa-ellipsis-h"></i>', false, ['class' => 'text-info', 'data-toggle' => 'tooltip', 'data-original-title' => 'Up here!']) . '
                              </div>
                              <div class="action-mask">
                                <div class="action">
                                  ' . Html::a(
        '<i class="fa fa-edit"></i>',
        ['form', 'id' => $data->Id],
        [
          //'onclick' => "FormUpdate('".Yii::$app->urlManager->createAbsoluteUrl(['hr/employee/form', 'id' => $data->Id])."')",
          'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
          'style' => 'bjob-radius: 5px !important',
        ]
      ) . '
                                  ' . Html::a(
        '<i class="fa fa-copy"></i>',
        ['form', 'id' => $data->Id, 'action' => 'duplicate'],
        [
          //'onclick' => "FormDuplicate('".Yii::$app->urlManager->createAbsoluteUrl(['hr/employee/form', 'id' => $data->Id, 'action'=>'duplicate'])."')",
          'class' => 'btn btn-info text-white padding-5 p-l-10 p-r-10',
          'style' => 'bjob-radius: 5px !important',
        ]
      ) . '
                                  ' . Html::a(
        '<i class="fa fa-refresh"></i>',
        ['reset', 'id' => $data->Id],
        [
          'data-method' => 'post',
          'data-confirm' => 'Are you sure want to reset this user login ?',
          'class' => 'btn btn-info text-white padding-5 p-l-10 p-r-10',
        ]
      ) . '
                                </div>
                              </div>
                            </div>';

      return $val;
    },
  ],

];

?>
<div class="modal fade stick-up " id="crud-modal" role="dialog" aria-hidden="false" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content no-border">
      <div class="modal-header bg-success text-white">
      </div>
      <div id="detail" class="modal-body padding-20">
      </div>
    </div>
  </div>
</div>
<?php /* \yii\widgets\Pjax::begin(['id' => 'crud-pjax', 'enablePushState' => false]); */ ?>

<?php
echo GridView::widget(
  [
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => $column,
    'layout' => '
          <div class="card card-default">
            <div class="row ">
              <div class="col-md-12">
                {items}
              </div>
            </div>
            <div class="row padding-10">
              <div class="col-md-4">{summary}</div>
              <div class="col-md-8">{pager}</div>
            </div>
          </div>
            ',
    'resizableColumns' => false,
    'bordered' => false,
    'striped' => true,
    'condensed' => false,
    'responsive' => false,
    'hover' => false,
    'persistResize' => false,
  ]
);
?>

<?php /* \yii\widgets\Pjax::end(); */ ?>
<script type="text/javascript">
  var paramJs = (paramJs || {});
  paramJs.urlFormShowModalResetAnnual = '<?= Yii::$app->urlManager->createAbsoluteUrl(['/hr/employee/resetannualleave']); ?>';
  paramJs.urlFormShowModalAnnual = '<?= Yii::$app->urlManager->createAbsoluteUrl(['/hr/employee/annualleave']); ?>';
  paramJs.urlRefreshAnnual = '<?= Yii::$app->urlManager->createAbsoluteUrl(['/hr/employee/detail']); ?>';

  function FormShowModalResetAnnual(link = "") {
    $('#crud-modal').modal('show');
    $('#detail').html("loading ...");

    var link = (link || paramJs.urlFormShowModalResetAnnual);
    $.ajax({
      url: link,
      data: $('#crud-form').serialize(),
      method: "GET",
      dataType: 'html',
      success: function(data) {
        $('#detail').html(data);
      },
    });
  }

  function FormShowModalAnnual(id, link = "") {
    $('#crud-modal').modal('show');
    $('#detail').html("loading ...");

    var link = (link || paramJs.urlFormShowModalAnnual);
    $.ajax({
      url: link,
      data: {
        id: id
      },
      method: "POST",
      dataType: 'html',
      success: function(data) {
        $('#detail').html(data);
      },
    });
  }

  function RefreshAnnual(id, link = "") {
    showFullLoading();
    var link = (link || paramJs.urlRefreshAnnual);
    $.ajax({
      url: link,
      data: {
        expandRowKey: id
      },
      method: "POST",
      dataType: 'html',
      success: function(data) {
        $('#wrapper-' + id).html(data);
        hideFullLoading();
      },
    });
  }


  function resetAnnualVacation(txt, link) {
    showFullLoading();

    if (confirm(txt)) {
      $.ajax({
        url: link,
        data: {},
        method: "POST",
        dataType: 'json',
        success: function() {
          reload();
          notif("Success Updating data !");
          hideFullLoading();
        },
        error: function(XMLHttpRequest, textStatus, errorThrown, data) {
          $('#error').show();
          var err = unescape(XMLHttpRequest.responseText);
          err = err.split('&#039;');
          err = err[3];
          err = escape(err);

          err = err.split('%26gt%3B%26gt%3BERROR%26lt%3B%26lt%3B');
          err = unescape(err[1]);
          //alert(err);
          $('#error').html(err);
          $('html, body').animate({
            scrollTop: 0
          }, 0);
          hideFullLoading();
        }
      });
    } else {
      $('html, body').animate({
        scrollTop: 0
      }, 0);
      hideFullLoading();
    }
  }

  window.closeModal = function() {
    reload();
    hideFullLoading();
  };

  function reload() {
    $.pjax.defaults.timeout = false;
    $.pjax.reload({
      container: '#crud-pjax'
    })
    $('#ChildModal').modal('hide');
    $('#ChildModal').data('modal', null);
  }
</script>
