<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use yii\web\JsExpression;
use common\components\HrHelper;
use common\models\hr\Employee;

$this->title = 'Reopen TimeReport';
$this->params['breadcrumbs'][] = "Setup";
$this->params['breadcrumbs'][] = "Employees";
$this->params['breadcrumbs'][] = $this->title;

$form = ActiveForm::begin([
    'id' => 'crud-form',
    'enableClientValidation' => true,
    'validateOnSubmit' => true,
    'validateOnChange' => true,
    'validateOnType' => true,
    'fieldConfig' => [
        'template' => '{label}{input}',
        'options' => [
            'class' => 'form-group form-group-default',
        ],
    ],
    'errorSummaryCssClass' => 'alert alert-danger'
]);
echo $form->errorSummary($model);

?>
<div id="error" class="alert alert-danger" style="display:none"></div>

<div class="card ">
    <div class="card-body">
        <div class="form-group-attached m-b-10">
            <div class="row">
                <div class="col-md-8">
                    <?php
                    // var_dump($model);
                    // die();
                    $employee = Employee::findOne($model->employee_id);
                    echo $form->field($model, 'employee_id', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])
                        ->widget(Select2::classname(), [
                            'initValueText' => !empty($employee->full_name) ? $employee->user_id . ' - ' . $employee->full_name : "",
                            'options' => ['placeholder' => 'Search Leader ...'],
                            'pluginOptions' => [
                                'tags' => false,
                                'allowClear' => true,
                                'minimumInputLength' => 3,
                                'ajax' => [
                                    'url' => Url::to(['/hr/helper/srcemployee']),
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                                ],
                            ],
                        ]);
                    ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'tr_date')->widget(DatePicker::classname(), [
                        'type' => DatePicker::TYPE_INPUT,
                        'options' => ['placeholder' => 'Pick a Date ...'],
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd',
                        ],
                    ])
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-right">
                <button id="button" type="button" value="SAVE" class="btn btn-primary btn-lg">REOPEN TIMEREPORT</button>
                <?=
                Html::a(
                    'BACK',
                    ['index'],
                    [
                        'class' => 'btn btn-info btn-lg text-white ',
                        'data-dismiss' => "modal"
                    ]
                );
                ?>
            </div>
        </div>

    </div>
</div>
<?php ActiveForm::end(); ?>





<script type="text/javascript">
    var paramJs = (paramJs || {});
    paramJs.urlFormSave = '<?= Yii::$app->urlManager->createAbsoluteUrl(['hr/employee/open_tr_save']); ?>';

    //$('#crud-form').on('beforeSubmit', function() {4

    $('#button').click(function(event) {
        event.preventDefault(); //this will prevent the default submit
        showFullLoading();
        if (confirm("Are you sure want to open this TimeReport ?")) {
            var $form = new FormData($('#crud-form')[0]);
            $.ajax({
                url: paramJs.urlFormSave,
                type: 'POST',
                data: $form,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'text',
                success: function(data) {
                    hideFullLoading();
                    if (data != 1) {
                        $('#error').show();
                        $('#error').html(data);
                    } else {
                        notif("Success Reopen TimeReport");
                        <?php
                        echo 'window.location.href = "' . Yii::$app->urlManager->createAbsoluteUrl(['/hr/employee/open_tr']) . '";';
                        ?>
                    }
                },
                error: function(jqXHR, errMsg) {
                    hideFullLoading();
                    $('#error').show();
                    $('#error').html(data);
                }
            });
            return false;
            hideFullLoading()
        }
        hideFullLoading();
        event.preventDefault(); //this will prevent the default submit
    });
</script>
