<?php

    use yii\helpers\Html;
    use yii\widgets\DetailView;
    $button = "";
    if($detail['tr_status'] == 'APPROVAL2-PENDING' ){
        $button = Html::a(
            '<i class="fa fa-check-square"></i> Approve',
            false,
            [
                'onclick' => "approvalall('".Yii::$app->urlManager->createAbsoluteUrl(
                    ['tr/trmanagerapproval/detaillistapproveall'
                    , 'id' => $detail['tr_date']
                    , 'Description' => $detail['task_description']
                    , 'TimeReportID' => $detail['tr_id']
                    , 'TrDetID' => $detail['tr_det_id']
                ])

                ."')",
                'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
            ]);
    }


    $button .= Html::a(
        '<i class="fa fa-comment"></i>',
        false,
        [
            'onclick' => "Comments('".Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/comments', 'tr' => $detail['tr_id'], 'id' => $detail['tr_det_id'], 'type'=>1])."')",
            'class' => 'btn btn-info text-white padding-5 p-l-10 p-r-10',
        ]);
    $button .= Html::a(
        '<i class="fa fa-edit"></i> ',
        false,
        [
            'onclick' => "approvedetail('".Yii::$app->urlManager->createAbsoluteUrl(['tr/trmanagerapproval/view', 'id' => $detail['tr_id'] , 'TrDetID' => $detail['tr_det_id'] ])."')",
            'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
        ]
    );
    echo "
    <tr>
        <td class='text-right align-middle'>".$i.".</td>
        <td class='align-middle'>".$detail['employee_name']."</td>
        <td class='align-middle'>".$detail['task_description']."</td>
        <td class='align-middle'>".$detail['level_name']."</td>
        <td class='text-right align-middle'>".$detail['work_hour']."</td>
        <td class='text-right align-middle'>".$detail['over_time']."</td>
        <td class='text-right align-middle'>Rp. ".number_format($detail['meal_amount'], 2)."</td>
        <td class='text-right align-middle'>Rp. ".number_format($detail['ope_amount'], 2)."</td>
        <td class='text-right align-middle'>Rp. ".number_format($detail['taxi_amount'], 2)."</td>
        <td class='text-right align-middle'><span class='badge ".$detail['tr_statusCSS']."'>".$detail['tr_status_label']."</span></td>
        <td class='text-right align-middle'>"
        . "<div class='tooltip-action'>
            <div class='trigger'>
                ".Html::a('<i class="fa fa-ellipsis-h"></i>', false, ['class' => 'text-info', 'data-toggle' => 'tooltip', 'data-original-title' => 'Up here!'])."
            </div>
            <div class='action-mask'>
                <div class='action'>
                    '.$button.'
                </div>
            </div>
        </div>"
        ."</td>
    </tr>
    ";
?>
