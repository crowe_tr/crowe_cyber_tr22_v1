<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;

use yii\widgets\ListView;
use yii\widgets\Pjax;
use kartik\widgets\SwitchInput;
use yii\web\JsExpression;
use yii\widgets\MaskedInput;
use kartik\widgets\TimePicker;
use common\models\cl\viewClientZone;
use common\components\ant;

?>

<?php

$form = ActiveForm::begin([
    'id' => 'trdetail-form',
    'options' => ['class' => 'ant-form', 'enctype' => 'multipart/form-data'],
    'enableClientValidation' => true,
    'validateOnSubmit' => true,
    'validateOnChange' => true,
    'validateOnType' => true,
    'fieldConfig' => [
        'template' => '{label}{input}',
        'options' => [
            'class' => 'form-group form-group-default ',
        ],
    ],
    'errorSummaryCssClass' => 'alert alert-danger'
]);

echo $form->field($modelDetail, 'employee_id', ['options' => ['class' => '', 'id' => 'employee_id']])->hiddenInput()->label(false);
echo $form->field($modelDetail, 'tr_date', ['options' => ['class' => '']])->hiddenInput()->label(false);

echo $form->field($modelDetail, 'time_report_id', ['options' => ['class' => '']])->hiddenInput()->label(false);
echo $form->field($modelDetail, 'id', ['options' => ['class' => '']])->hiddenInput()->label(false);

?>
<div class="alert alert-danger" id="error" style="display:none">
</div>
<?= $form->errorSummary($modelDetail); ?>
<div class="form-group-attached p-b-5">
    <div class="row">
        <div class="col-sm-4">
            <?php
            echo $form->field($modelDetail, 'task_type_id', ['options' => ['onchange' => 'toggleJob();', 'class' => ' form-group form-group-default form-group-default-select2']])->widget(
                Select2::classname(),
                [
                    'options' => ['placeholder' => 'Select...'],
                    'data' => $data['task_type_id'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'placeholder' => Yii::t('backend', 'Select..'),
                    ],
                    'options' => ['id' => 'task_type_id', 'placeholder' => 'Select ...'],
                ]
            );
            ?>
        </div>
        <div class="col-sm-8">
            <?php
            // var_dump($modelDetail->task_type_id, $modelDetail->task_id);
            echo $form->field($modelDetail, 'task_id', ['options' => ['onchange' => 'toggleAttachment();', 'class' => ' form-group form-group-default form-group-default-select2']])->widget(DepDrop::classname(), [
                'data' => $data['task_id'],
                'options' => ['id' => 'task_id', 'placeholder' => 'Select ...'],
                'type' => DepDrop::TYPE_SELECT2,
                'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                'pluginOptions' => [
                    'allowClear' => false,
                    'depends' => ['task_type_id'],
                    'url' => Url::to(['/cm/helper/loadtimereporttask']),
                    'loadingText' => 'Loading ...',
                ],
            ]);
            ?>

        </div>
    </div>
</div>
<div class="form-group-attached p-b-5">

    <div class="row">
        <div class="col-sm-12" id="job-wrapper" style="<?php echo ($modelDetail->task_type_id <> 1) ? "display:none;" : ""; ?>">
            <?php
            // var_dump($data['job_id']);
            // die();
            echo $form->field($modelDetail, 'job_id', [
                'options' => [
                    'class' => ' form-group form-group-default form-group-default-select2'
                ]
            ])->widget(DepDrop::classname(), [
                'data' => $data['job_id'],
                'options' => ['id' => 'job_id', 'placeholder' => 'Select ...'],
                'type' => DepDrop::TYPE_SELECT2,
                'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                'pluginOptions' => [
                    'allowClear' => false,
                    'depends' => ['task_type_id', 'time_report_id'],
                    // 'url' => Url::to(['/cm/helper/loadtimereportjobs', 'id' => $modelDetail->time_report_id, 'job' => ($modelDetail->isNewRecord) ? 0 : $modelDetail->job_id]),
                    'url' => Url::to(['/cm/helper/loadtimereportjobs', 'itr_date' => $modelDetail->tr_date]),
                    'loadingText' => 'Loading ...',
                ],
            ]);
            ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12 TimeAvailable" style="<?php echo ($modelDetail->task_type_id <> 1) ? "display:none;" : ""; ?>">
        <?= !empty($TimeAvailable) ? $TimeAvailable : ""; ?>
    </div>
</div>
<p>
    <b>TASK & WORKHOURS</b>
</p>
<div class="form-group-attached">
    <div class="row p-b-5">
        <div class="col-sm-3">
            <?=
                $form->field($modelDetail, 'work_hour')->widget(MaskedInput::className(), [
                    'clientOptions' => [
                        'alias' => 'decimal',
                        'groupSeparator' => ',',
                        'autoGroup' => true,
                    ],
                ]);
            ?>
        </div>
        <div class="col-sm-9">
            <?= $form->field($modelDetail, 'description')->textInput(); ?>
        </div>
    </div>
</div>
<p>
</p>

<div class="row">
    <div class="col-md-12" id="attachment" style="display: none;">
        <?= ant::inputfile($form, $modelDetail, 'attachment', 'thumbnail2', 'Attach attachments if needed'); ?>
    </div>
</div>


<div class="row">
    <div class="col-md-12 text-right">
        <hr class="m-b-5" />
        <?= Html::submitButton('UPDATE', ['class' => 'btn btn-warning p-t-10 p-b-10']) ?>
        <button type="button" class="btn btn-info p-t-10 p-b-10" style="font-size: 12px" onclick="CloseModal()">CANCEL</button>
    </div>
</div>

<?php ActiveForm::end(); ?>

<script type="text/javascript">
    var paramJs = (paramJs || {});
    paramJs.urlFormSave = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/detailsave']); ?>';
    paramJs.urlJobTimeAvailable = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/getjobtimeavailable']); ?>';
    paramJs.urlAttachment = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/getattachment']); ?>';
    $('#trdetail-form').on('beforeSubmit', function() {
        showFullLoading();

        var form = new FormData($('#trdetail-form')[0]);
        $.ajax({
            url: paramJs.urlFormSave,
            type: 'POST',
            data: form,
            async: false,
            cache: false,
            contentType: false,
            processData: false,

            success: function(data) {
                hideFullLoading();
                console.log(data);
                // $('#trdetail-modal').modal('hide');
                // reload();
                if (data != 1) {
                    $('#error').show();
                    var err = unescape(XMLHttpRequest.responseText);
                    err = err.split('&#039;');
                    err = err[3];
                    err = escape(err);

                    // err = err.split('%26gt%3B%26gt%3BERROR%26lt%3B%26lt%3B');
                    err = err.split('___');
                    err = unescape(err[1]);

                    if (err === "undefined") {
                        $('#error').html(data);
                    } else {
                        $('#error').html(err);
                    }
                } else {
                    notif("Success Updating WH !");
                    $('#trdetail-modal').modal('hide');
                    reload();
                }
            },
            // error: function(data){
            //
            //   $('#error').show();
            //   $('#error').html(data);
            //   hideFullLoading();
            // }
            error: function(XMLHttpRequest, textStatus, errorThrown, data) {
                hideFullLoading();

                $('#error').show();
                var err = unescape(XMLHttpRequest.responseText);
                err = err.split('&#039;');
                err = err[3];
                err = escape(err);

                // err = err.split('%26gt%3B%26gt%3BERROR%26lt%3B%26lt%3B');
                err = err.split('___');
                err = unescape(err[1]);

                if (err === "undefined") {
                    $('#error').html(data);
                } else {
                    $('#error').html(err);
                }

            }
        });
        return false;
    });
    $('#job_id').on('change', function() {
        // alert($('#job_id').val());
        showFullLoading();
        $.ajax({
            url: paramJs.urlJobTimeAvailable,
            type: 'POST',
            data: {
                id: $('#job_id').val()
            },
            dataType: 'html',
            success: function(data) {
                hideFullLoading();
                $('.TimeAvailable').html(data);
            },
            error: function(data) {
                hideFullLoading();
            }
        });
    });

    function CloseModal() {
        $('#ChildModal').modal('hide');
    }

    function toggleJob() {
        if ($('#task_type_id').val() == 1) {
            $('#job-wrapper').show();
            $('.TimeAvailable').show();
        } else {
            $('#job-wrapper').hide();
            $('.TimeAvailable').hide();
        }
    }

    function toggleAttachment() {
        showFullLoading();
        $.ajax({
            url: paramJs.urlAttachment,
            type: 'POST',
            data: {
                id: $('#task_id').val()
            },
            dataType: 'html',
            success: function(data) {
                hideFullLoading();
                if (data == 1) {
                    $('#attachment').show();
                } else {
                    $('#attachment').hide();
                }
            },
            error: function(data) {
                hideFullLoading();
            }
        });

    }
    toggleAttachment();
</script>
