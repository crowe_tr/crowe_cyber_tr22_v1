<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\web\View;

use common\components\CommonHelper;

$user = CommonHelper::getUserIndentity();
// var_dump($modelView);

$this->title = 'HISTORY LOGS';
$this->params['breadcrumbs'][] = "TIME REPORT";
$this->params['breadcrumbs'][] = $modelView->employee_id . ' (' . $modelView->tr_date. ')';
$this->params['breadcrumbs'][] = $this->title;
// $this->params['breadcrumbs'][] = $modelView->id;
$this->params['breadcrumbs_btn'] =
	Html::a(
		'BACK',
		['/tr/timereport/update', 'id' => $modelView->tr_date],
		[
			'class' => 'btn btn-info text-white ',
		]
	);
$this->params['crud_ajax'] = false;
$this->registerCss('
	.kv-expand-detail-row td
     {
       background: notset !important;
     }
');

$column = [
	[
		'class' => 'kartik\grid\ExpandRowColumn',
		'enableRowClick' => true,
		'mergeHeader' => false,
		'headerOptions' => ['class' => 'kartik-sheet-style b-l b-r  b-success bg-success'],
		'filterOptions' => ['class' => ''],
		'contentOptions' => ['class' => 'kv-align-middle bg-warning-lighter b-l b-r b-grey', 'style' => 'height: 50px'],
		'expandOneOnly' => false,
		'width' => '50px',
		'expandIcon' => '<i class="fa fa-angle-right"></i>',
		'collapseIcon' => '<i class="fa fa-angle-up"></i>',
		'value' => function ($model, $key, $index, $column) {
			return GridView::ROW_COLLAPSED;
		},
		'detailUrl' => Url::to(['/tr/timereport/time_report_logs_detail']),
	],
	
	[
		'attribute' => 'tr_type',
		'headerOptions' => ['class' => 'col-3 bg-success'],
		'contentOptions' => ['class' => 'bg-warning-lighter kv-align-middle'],
		'filterOptions' => ['class' => 'b-b b-grey'],
	],
	[
		'attribute' => 'created_by',
		'headerOptions' => ['class' => 'col-3 bg-success'],
		'contentOptions' => ['class' => 'bg-warning-lighter kv-align-middle'],
		'filterOptions' => ['class' => 'b-b b-grey'],
	],
	[
		'attribute' => 'created_at',
		'headerOptions' => ['class' => 'col-3 bg-success'],
		'contentOptions' => ['class' => 'bg-warning-lighter kv-align-middle'],
		'filterOptions' => ['class' => 'b-b b-grey'],
		'value' => function ($model, $key, $index, $column) {
			return $model->created_at;
		}
	],
	[
		'attribute' => 'user_action',
		'headerOptions' => ['class' => 'col-3 bg-success'],
		'contentOptions' => ['class' => 'bg-warning-lighter kv-align-middle'],
		'filterOptions' => ['class' => 'b-b b-grey'],
	],

];

?>
<?php
echo GridView::widget(
	[
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => $column,
		'layout' => '
          <div class="card card-default">
            <div class="row ">
              <div class="col-md-12">
                {items}
              </div>
            </div>
            <div class="row padding-10">
              <div class="col-md-4">{summary}</div>
              <div class="col-md-8">{pager}</div>
            </div>
          </div>
            ',
		'resizableColumns' => false,
		'bordered' => false,
		'striped' => false,
		'condensed' => false,
		'responsive' => false,
		'hover' => false,
		'persistResize' => false,
	]
);
?>
