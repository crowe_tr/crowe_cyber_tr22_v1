<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use kartik\widgets\ActiveForm;
use common\components\CommonHelper;

$user = CommonHelper::getUserIndentity();
?>

<div class="padding-20">
	<div id="error" class="alert alert-danger" style="display:none;"></div>

	<?php
	echo '<table class="table table-bordered">';
	echo '<thead>'
		. '<tr>'
		. '<th class="bg-primary"></th>'
		. '<th class="bg-primary col-md-6">DESCRIPTION</th>'
		. '<th class="bg-primary col-md-1 text-center">WORKHOURS</th>'
		. '<th class="bg-primary col-md-1 text-center">OVERTIME</th>'
		. '<th class="bg-primary col-md-1 text-center">MEAL</th>'
		. '<th class="bg-primary col-md-1 text-center">OUT OF OFFICE</th>'
		. '<th class="bg-primary col-md-1 text-center">TAXI</th>'
		. '<th class="bg-primary col-md-1 text-center">STATUS</th>'
		. '</tr>'
		. '</thead>'
		. '<tbody>';
	if (!empty($header)) {

		// $header_desc = json_decode('[' . $header->audit_description . ']');
		$header_desc = json_decode($header->tr_memo);
		// var_dump($header_desc);
		// $header_desc = json_encode($header->audit_description);
		// $header_desc = json_decode($header_desc);
		// var_dump($header_desc);
		// var_dump(array($header->audit_description));
		// var_dump(json_decode(json_encode($header->audit_description)));
		if (!empty($header_desc)) {
			// echo "<tr>
			// 				<td class='bg-info text-white'></td>
			// 				<td class='bg-info text-white' colspan='4'>JOB</td>
			// 			</tr>";

			$no = 0;
			// echo "<tr><td>" . $header_desc . "</td></tr>";
			foreach ($header_desc as $val => $h) {
				$no++;
				echo "<tr>";
				echo "<td>" . $no . "</td>";
				echo "<td>" . $h->description . "</td>";
				echo "<td class='text-center'>" . number_format($h->work_hour,1, '.', ',') . "</td>";
				echo "<td class='text-center'>" . number_format($h->over_time,1, '.', ',') . "</td>";
				echo "<td class='text-right'>" . number_format($h->meal_amount,0, '.', ',') . "</td>";
				echo "<td class='text-right'>" . number_format($h->ope_amount,0, '.', ',') . "</td>";
				echo "<td class='text-right'>" . number_format($h->taxi_amount,0, '.', ',') . "</td>";
				echo "<td class='text-center'>" . $h->tr_status . "</td>";
				// if (
				// 	$h->description == 'FEE'
				// 	or $h->description == 'MEAL ALLOWANCE'
				// 	or $h->description == 'OPE ALLOWANCE'
				// 	or $h->description == 'TAXI ALLOWANCE'
				// 	or $h->description == 'TIME CHARGES'
				// 	or $h->description == 'ADMINISTRATIVE CHARGE'
				// 	or $h->description == 'OTHER EXPENSE'
				// ) {
				// 	echo "<td>" . number_format($h->before_value) . "</td>";
				// 	echo "<td>" . number_format($h->after_value) . "</td>";
				// } else {
				// 	echo "<td>" . $h->before_value . "</td>";
				// 	echo "<td>" . $h->after_value . "</td>";
				// }
				// number_format ( $d[$f], 0, '.', ',' )
				echo "</tr>";
			}
		}
	}

	echo '</tbody>';
	echo '</table>';
	?>
</div>
