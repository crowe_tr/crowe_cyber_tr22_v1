<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;

use yii\widgets\ListView;
use yii\widgets\Pjax;
use kartik\widgets\SwitchInput;
use yii\web\JsExpression;
use yii\widgets\MaskedInput;
use kartik\widgets\TimePicker;
use common\models\cl\viewClientZone;

?>

<?php

$form = ActiveForm::begin([
        'id' => 'trdetail-form',
        'enableClientValidation' => true,
        'validateOnSubmit' => true,
        'validateOnChange' => true,
        'validateOnType' => true,
        'fieldConfig' => [
            'template' => '{label}{input}',
            'options' => [
                'class' => 'form-group form-group-default',
            ],
		],
		'errorSummaryCssClass'=> 'alert alert-danger'
	]);

    echo $form->errorSummary($modelDetail);
    echo $form->field($modelDetail, 'employee_id', ['options' => ['class' => '', 'id'=>'employee_id']])->hiddenInput()->label(false);
    echo $form->field($modelDetail, 'tr_date', ['options' => ['class' => '']])->hiddenInput()->label(false);

    echo $form->field($modelDetail, 'time_report_id', ['options' => ['class' => '']])->hiddenInput()->label(false);
    echo $form->field($modelDetail, 'id', ['options' => ['class' => '']])->hiddenInput()->label(false);

?>
<div class="alert alert-danger" id="error" style="display:none">
</div>

<div class="form-group-attached p-b-5">
    <div class="row">
        <div class="col-sm-4">
            <?php
                echo $form->field($modelDetail, 'task_type_id', ['options' => ['onchange' => '', 'class' => ' form-group form-group-default form-group-default-select2']])->widget(
                    Select2::classname(),
                    [
                        'options' => ['placeholder' => 'Select...'],
                        'data' => $data['task_type_id'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'placeholder' => Yii::t('backend', 'Select..'),
                        ],
                        'options' => ['id' => 'task_type_id', 'disabled'=>'disabled', 'placeholder' => 'Select ...'],
                    ]
                );
            ?>
        </div>
        <div class="col-sm-8">
            <?php
                echo $form->field($modelDetail, 'task_id', ['options' => ['disabled'=>'disabled', 'class' => ' form-group form-group-default form-group-default-select2']])->widget(DepDrop::classname(), [
                    'data' => $data['task_id'],
                    'options' => ['id' => 'task_id', 'disabled'=>'disabled', 'placeholder' => 'Select ...'],
                    'type' => DepDrop::TYPE_SELECT2,
                    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                    'pluginOptions' => [
                        'allowClear' => false,
                        'depends' => ['task_type_id'],
                        'url' => Url::to(['/cm/helper/loadtimereporttask']),
                        'loadingText' => 'Loading ...',
                    ],
                ]);
            ?>

        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?php
                echo $form->field($modelDetail, 'job_id', ['options' => ['disabled'=>'disabled', 'class' => ' form-group form-group-default form-group-default-select2']])->widget(DepDrop::classname(), [
                    'data' => $data['job_id'],
                    'options' => ['id' => 'job_id', 'disabled'=>'disabled', 'placeholder' => 'Select ...'],
                    'type' => DepDrop::TYPE_SELECT2,
                    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                    'pluginOptions' => [
                        'allowClear' => false,
                        'depends' => ['task_type_id', 'time_report_id'],
                        'url' => Url::to(['/cm/helper/loadtimereportjobs', 'itr_date' => $modelDetail->tr_date]),
                        // 'url' => Url::to(['/cm/helper/loadtimereportjobs', 'id'=>$modelDetail->TimeReportID, 'job'=>($modelDetail->isNewRecord) ? 0 : $modelDetail->JobId]),
                        'loadingText' => 'Loading ...',
                    ],
                ]);
            ?>
        </div>
    </div>
</div>
<p><b>OVERTIME</b></p>
<div class="form-group-attached">
    <div class="row p-b-5">
        <div class="col-sm-3">
            <?=
                $form->field($modelDetail, 'over_time')->widget(MaskedInput::className(), [
                        'clientOptions' => [
                            'alias' => 'decimal',
                            'groupSeparator' => ',',
                            'autoGroup' => true,
                        ],
                    ]);
            ?>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12 text-right">
    <hr class="m-b-5"/>
    <?= Html::submitButton('UPDATE', ['class' => 'btn btn-warning p-t-10 p-b-10']) ?>
    <button type="button" class="btn btn-info p-t-10 p-b-10" style="font-size: 12px" onclick="CloseModal()">CANCEL</button>
    </div>
</div>

<?php ActiveForm::end(); ?>

<script type="text/javascript">
	var paramJs = (paramJs || {});
    paramJs.urlFormSave = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/detailsave']); ?>';
    $('#trdetail-form').on('beforeSubmit', function() {
        showFullLoading();

        var form = new FormData($('#trdetail-form')[0]);
        $.ajax({
            url: paramJs.urlFormSave,
            type: 'POST',
            data: form,
            async: false,
            cache: false,
            contentType: false,
            processData: false,

            success: function (data) {
                hideFullLoading();

                if(data != 1){
                    $('#error').show();
                    $('#error').html(data);
                }else{
                    $('#trdetail-modal').modal('hide');
                    notif("Success Updating Overtime !");
                    reload();
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown, data) {
                hideFullLoading();

                $('#error').show();
                var err = unescape(XMLHttpRequest.responseText);
                err = err.split('&#039;');
                err = err[3];
                err = escape(err);

                err = err.split('%26gt%3B%26gt%3BERROR%26lt%3B%26lt%3B');
                err = unescape(err[1]);

                $('#error').html(err);
            }
        });
        return false;
    });

    function CloseModal(){
        $('#ChildModal').modal('hide');
    }

</script>
