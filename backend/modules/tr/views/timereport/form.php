<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\grid\GridView;
use yii\web\View;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use kartik\widgets\SwitchInput;
use common\components\Helper;
?>
<div class="modal" id="ChildModal" role="dialog" aria-hidden="false" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content no-border">
            <div class="modal-header bg-success text-white">
            </div>
            <div id="ChildModaldetail" class="modal-body padding-20">
            </div>
        </div>
    </div>
</div>
<div class="modal fade slide-right" id="ModalComments" role="dialog" aria-hidden="false">
    <div class="modal-dialog no-border">
        <div class="modal-content no-border">
            <div class="modal-header no-border bg-success">
                <p class="text-white">
                    <b><i class="pg-comment fax2"></i> COMMENTS</b>
                </p>
            </div>
            <div id="ModalCommentsBody" class="modal-body no-padding">

            </div>
        </div>
    </div>
</div>

<?php \yii\widgets\Pjax::begin(['id' => 'trTimeReportDetail-pjax', 'enablePushState' => false]); ?>
<div class="padding-10">

    <?php

    $form = ActiveForm::begin([
        'id' => 'tr-form',
        'enableClientValidation' => true,
        'validateOnSubmit' => true,
        'validateOnChange' => true,
        'validateOnType' => true,
        'fieldConfig' => [
            'template' => '{label}{input}',
            'options' => [
                'class' => 'form-group form-group-default',
            ],
        ],
        'errorSummaryCssClass' => 'alert alert-danger'
    ]);

    echo $form->errorSummary($model);
    echo $form->field($model, 'id', ['options' => ['class' => '']])->hiddenInput()->label(false);

    echo $form->field($model, 'employee_id', ['options' => ['class' => '', 'user_id' => 'employee_id']])->hiddenInput()->label(false);
    echo $form->field($model, 'tr_date', ['options' => ['class' => '']])->hiddenInput()->label(false);
    echo $form->field($model, 'work_hour', ['options' => ['class' => '']])->hiddenInput()->label(false);

    $model->employee_name = !empty($model->employee->full_name) ? $model->employee->full_name : "";
    $model->employee_level = !empty($model->employee->level->level_name) ? $model->employee->level->level_name : "";
    $isMobile = Helper::getIsMobile();

    $model->Date_label = date('l, F d, Y', strtotime($model->tr_date));
    ?>

    <div class="alert alert-danger custom-error" id="custom-error">

    </div>


    <?php
    if ($model->tr_status == 1) {
        $status = '<span class="badge badge-success fs-13">SUBMITTED</span>';
    } else {
        $status = '<span class="badge badge-warning fs-13 text-black">PENDING</span>';
    }
    echo '
            <div class="row">
                <div class="col-sm-6">
                    <h3 class="bold fs-24">
                        TIME REPORT ' . $status . '
                    </h3>
                </div> 
                <div class="col-sm-6 text-right">';
    echo  Html::a('<i class="fa fa-history"></i> LOGS', ['time_report_logs', 'id' => $model->id], ['class' => 'btn btn-primary text-white active ']);

    // echo '      <div class="col-sm-6">
    //                 <h3 class="bold fs-24">
    //                     TIME REPORT ' . $status . '
    //                 </h3>
    //             </div> ';
    echo '
                </div> 
            </div>
            <div class="row">
                <div class="col-sm-12 b-b m-b-10" style="border-bottom: thin solid #ccc">
                </div>
            </div>
        ';
        // Html::a('<i class="fa fa-history"></i> LOGS', ['revise_log', 'id' => $model->id], ['class' => 'btn btn-primary text-white active ']);

    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="<?php echo ($isMobile) ? 'card card-default m-b-10' : ''; ?>">
                <div class="<?php echo ($isMobile) ? 'card-body p-b-0' : ''; ?>">
                    <div class="form-group-attached p-b-5">
                        <div class="row">
                            <div class="col-sm-5 col-md-3">
                                <?php
                                echo $form->field($model, 'Date_label')->textInput(['disabled' => true]);
                                ?>
                            </div>
                            <div class="col-sm-7 col-md-5">
                                <?php
                                echo $form->field($model, 'employee_name')->textInput(['disabled' => true]);
                                ?>
                            </div>
                            <div class="col-sm-5 col-md-2">
                                <?php
                                echo $form->field($model, 'employee_level')->textInput(['disabled' => true]);
                                ?>
                            </div>
                            <!-- <div class="col-sm-7 col-md-2"> -->
                            <?php
                            // echo $form->field(
                            //     $model,
                            //     'isStayed',
                            //     ['options' => ['class' => 'form-group form-group-default']]
                            // )->widget(
                            //     SwitchInput::className(),
                            //     [
                            //         'options' => [
                            //             'size' => 'small',
                            //             'onText' => 'Yes',
                            //             'offText' => 'No',
                            //             'disabled' => ($model->Status == 1) ? true : false,
                            //             //'onclick'=>'HeaderSave(0)',
                            //         ]
                            //     ]
                            // )->label(false);
                            ?>
                            <!-- </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    if ($model->tr_status != 1) {
        echo '<div class="row">
                    <div class="col-md-12 text-center m-t-20">';
        echo Html::a(
            '<i class="fa fa-list-ul"></i> <span class="hidden-xs">TASK</span>',
            false,
            [
                'onclick' => 'update(1)',
                'class' => 'btn btn-lg btn-warning text-white',
            ]
        );
        echo Html::a(
            '<i class="fa fa-spoon"></i> <span class="hidden-xs">MEALS</span>',
            false,
            [
                'onclick' => 'update(2)',
                'class' => 'btn btn-lg btn-primary text-white',
            ]
        );
        echo Html::a(
            '<i class="fa fa-building"></i> <span class="hidden-xs">OUT OF OFFICE</span>',
            false,
            [
                'onclick' => 'update(3)',
                'class' => 'btn btn-lg btn-primary text-white',
            ]
        );
        echo Html::a(
            '<i class="fa fa-taxi"></i> <span class="hidden-xs">TAXI</span>',
            false,
            [
                'onclick' => 'update(4)',
                'class' => 'btn btn-lg btn-primary text-white',
            ]
        );
        /*
        echo Html::a(
            '<i class="fa fa-building"></i> <span class="hidden-xs">OUT OF TOWN</span>',
            false,
            [
                'onclick' => 'update(5)',
                'class' => 'btn btn-lg btn-info text-white',
            ]
        );
        */
        echo '</div>
            </div>';
    }
    ?>


    <div class="card card-default m-t-10 m-b-10" style="border-top: thick solid #0062cc">
        <div class="card-body p-t-20">
            <p><b>TASK</b></p>
            <hr />
            <div class='m-t-5 m-b-0' style='overflow: scroll;'>
                <div class=''>
                    <table class='table table-bordered'>
                        <tr>
                            <th class='bg-primary col-10'>DESCRIPTION</th>
                            <th class='bg-primary col-2'>TASK</th>
                            <th class='bg-primary'>WH</th>
                            <th class='bg-primary'>OT</th>
                            <th class='bg-primary'>STATUS</th>
                            <th class='bg-primary'></th>
                        </tr>
                        <?php
                        if (!empty($data['Detail'])) {
                            foreach ($data['Detail'] as $task) {
                                $button = Html::a(
                                    '<i class="fa fa-comment"></i> <span class="d-none d-md-inline">Comments</span>',
                                    false,
                                    [
                                        'onclick' => "Comments('" . Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/comments', 'tr' => $task['time_report_id'], 'id' => $task['tr_det_id'], 'type' => 1]) . "')",
                                        'class' => 'btn btn-info text-white padding-5 p-l-10 p-r-10',
                                        'style' => 'bjob-radius: 5px !important',
                                    ]
                                );

                                // $button .= " " . Html::a(
                                //     '<i class="fa fa-trash"></i>',
                                //     false,
                                //     [
                                //         'onclick' => "itemdelete(1, '" . Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/detaildelete', 'id' => $task['TrDetID']]) . "')",
                                //         'class' => 'btn btn-danger text-white padding-5 p-l-10 p-r-10',
                                //         'style' => 'bjob-radius: 5px !important',
                                //     ]
                                // );

                                if (
                                    ($model->tr_status == 0
                                        // or $task["Status"] == "APPROVAL1-PENDING"
                                        or $task["tr_status"] == "APPROVAL1-REVISED"
                                        or $task["tr_status"] == "APPROVAL2-REVISED"
                                        or $task["tr_status"] == "REVISED")
                                    and ($task["tr_status"] != "APPROVAL2-PENDING"
                                        and $task["tr_status"] != "COMPLETED")
                                ) {
                                    $button .= " " . Html::a(
                                        '<i class="fa fa-clock-o"></i> <span class="d-none d-md-inline">OT</span>',
                                        false,
                                        [
                                            'onclick' => "update(1, '" . Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/overtime', 'id' => $task['tr_det_id']]) . "')",
                                            'class' => 'btn btn-info text-white padding-5 p-l-10 p-r-10',
                                            'style' => 'bjob-radius: 5px !important',
                                        ]
                                    );
                                    $button .= " " . Html::a(
                                        '<i class="fa fa-edit"></i> <span class="d-none d-md-inline">WH</span>',
                                        false,
                                        [
                                            'onclick' => "update(1, '" . Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/detail', 'id' => $task['tr_det_id']]) . "')",
                                            'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
                                            'style' => 'bjob-radius: 5px !important',
                                        ]
                                    );
                                    $button .= " " . Html::a(
                                        '<i class="fa fa-trash"></i>',
                                        false,
                                        [
                                            'onclick' => "itemdelete(1, '" . Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/detaildelete', 'id' => $task['tr_det_id']]) . "')",
                                            'class' => 'btn btn-danger text-white padding-5 p-l-10 p-r-10',
                                            'style' => 'bjob-radius: 5px !important',
                                        ]
                                    );
                                }

                                // var_dump($task);
                                // die();
                                echo "
                                        <tr>
                                            <td valign='center'>{$task['task_description']}</td>
                                            <td class='kv-align-middle'>{$task['task_name']}</td>
                                            <td align='right' class='kv-align-middle'>{$task['work_hour']}</td>
                                            <td align='right' class='kv-align-middle'>{$task['over_time']}</td>
                                            <td align='right' class='kv-align-middle'><span class='badge {$task['tr_status_label']}'>{$task['tr_status']}</span></td>
                                            <td>
                                                <div class='tooltip-action pull-right'>
                                                    <div class='trigger'>
                                                        " . Html::a('<i class="fa fa-ellipsis-h"></i>', false, ['class' => 'text-info', 'data-toggle' => 'tooltip']) . "
                                                    </div>
                                                    <div class='action-mask'>
                                                        <div class='action'>
                                                            " . $button . "
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    ";
                            }
                        }
                        /*
                                echo $this->render('viewDetail', [
                                    'model' => $model,
                                    'dataProvider' => $dataProvider,
                                    'dataProvider_Overtime' => $dataProvider_Overtime,

                                ]);
                            */
                        ?>
                    </table>
                </div>
            </div>

        </div>
    </div>

    <div class="card card-default m-t-10 m-b-10" style="border-top: thin solid #0062cc">
        <div class="card-body p-t-20">
            <p><b>MEALS</b></p>
            <hr />
            <?php
            if (!empty($data['Meal'])) {
                echo "
                    <div class='m-t-5 m-b-0' style='overflow: scroll;'>
                        <div class=''>
                            <table class='table table-bordered'>
                                <tr>
                                    <th class='bg-primary col-9'>DESCRIPTION</th>
                                    <th class='bg-primary'>AMOUNT</th>
                                    <th class='bg-primary'>STATUS</th>
                                    <th class='bg-primary'></th>
                                </tr>
                    ";
                foreach ($data['Meal'] as $meal) {
                    $button = "";
                    if (
                        ($model->tr_status == 0
                            or $meal["tr_status"] == "APPROVAL1-REVISED"
                            or $meal["tr_status"] == "APPROVAL2-REVISED"
                            or $meal["tr_status"] == "REVISED")
                        and ($meal["tr_status"] != "APPROVAL1-REJECTED"
                            and $meal["tr_status"] != "APPROVAL2-REJECTED"
                            and $meal["tr_status"] != "APPROVAL2-PENDING"
                            and $meal["tr_status"] != "COMPLETED")
                    ) {
                        $button .= Html::a(
                            '<i class="fa fa-edit"></i> <span class="d-none d-md-inline">Update</span>',
                            false,
                            [
                                'onclick' => "update(2, '" . Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/meals', 'id' => $meal['time_report_id'], 'seq' => $meal['seq']]) . "')",
                                'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
                                'style' => 'bjob-radius: 5px !important',
                            ]
                        );
                        $button .= " " . Html::a(
                            '<i class="fa fa-trash"></i> <span class="d-none d-md-inline">Delete</span>',
                            false,
                            [
                                'onclick' => "itemdelete(2, '" . Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/mealsdelete', 'id' => $meal['time_report_id'], 'seq' => $meal['seq']]) . "')",
                                'class' => 'btn btn-danger text-white padding-5 p-l-10 p-r-10',
                                'style' => 'bjob-radius: 5px !important',
                            ]
                        );
                    }
                    echo "
                                    <tr>
                                        <td class='bold'>{$meal['task_description']}</td>
                                        <td align='right'>Rp." . number_format($meal['meal_amount'], 2) . "</td>
                                        <td align='right'><span class='badge {$meal['tr_status_label']}'>{$meal['tr_status']}</span></td>
                                        <td  align='right'>
                                            <div class='tooltip-action pull-right'>
                                                <div class='trigger'>
                                                    " . Html::a('<i class="fa fa-ellipsis-h"></i>', false, ['class' => 'text-info', 'data-toggle' => 'tooltip']) . "
                                                </div>
                                                <div class='action-mask'>
                                                    <div class='action'>
                                                        " . $button . "
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>";
                }

                echo "</table>
                    </div>
                </div>
                ";
            }
            /*
                    echo $this->render('viewMeals', [
                        'model' => $model,
                        'dataProvider_Meals' => $dataProvider_Meals,

                    ]);
                */
            ?>
        </div>
    </div>
    <div class="card card-default m-t-10 m-b-10" style="border-top: thin solid #0062cc">
        <div class="card-body p-t-20">
            <p><b>OUT OF OFFICE</b></p>
            <hr />
            <?php
            if (!empty($data['OutOffice'])) {
                echo "
                    <div class='m-t-5 m-b-0' style='overflow: scroll;'>
                        <div class=''>
                        <table class='table table-bordered'>
                            <tr>
                                <th class='bg-primary col-9'>DESCRIPTION</th>
                                <th class='bg-primary'>ZONE</th>
                                <th class='bg-primary'>AMOUNT</th>
                                <th class='bg-primary'>STATUS</th>
                                <th class='bg-primary'></th>
                            </tr>";
                foreach ($data['OutOffice'] as $outofoffice) {
                    $button = "";
                    if (
                        ($model->tr_status == 0
                            or $outofoffice["tr_status"] == "APPROVAL1-REVISED"
                            or $outofoffice["tr_status"] == "APPROVAL2-REVISED"
                            or $outofoffice["tr_status"] == "REVISED")
                        and ($outofoffice["tr_status"] != "APPROVAL1-REJECTED"
                            and $outofoffice["tr_status"] != "APPROVAL2-REJECTED"
                            and $outofoffice["tr_status"] != "APPROVAL2-PENDING"
                            and $outofoffice["tr_status"] != "COMPLETED")
                    ) {
                        $button .= Html::a(
                            '<i class="fa fa-edit"></i> <span class="d-none d-md-inline">Update</span>',
                            false,
                            [
                                'onclick' => "update(3, '" . Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/ope', 'id' => $outofoffice['time_report_id'], 'seq' => $outofoffice['seq']]) . "')",
                                'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
                                'style' => 'bjob-radius: 5px !important',
                            ]
                        );
                        $button .= " " . Html::a(
                            '<i class="fa fa-trash"></i> <span class="d-none d-md-inline">Delete</span>',
                            false,
                            [
                                'onclick' => "itemdelete(3, '" . Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/opedelete', 'id' => $outofoffice['time_report_id'], 'seq' => $outofoffice['seq']]) . "')",
                                'class' => 'btn btn-danger text-white padding-5 p-l-10 p-r-10',
                                'style' => 'bjob-radius: 5px !important',
                            ]
                        );
                    }

                    echo "
                                <tr>
                                    <td class='bold'>{$outofoffice['task_description']}</td>
                                    <td align='right'>{$outofoffice['zone_id']} </td>
                                    <td align='right'>Rp." . number_format($outofoffice['ope_amount'], 2) . "</td>
                                    <td align='right'><span class='badge {$outofoffice['tr_status_label']}'>{$outofoffice['tr_status']}</span></td>
                                    <td align='right'>
                                        <div class='tooltip-action pull-right'>
                                            <div class='trigger'>
                                                " . Html::a('<i class="fa fa-ellipsis-h"></i>', false, ['class' => 'text-info', 'data-toggle' => 'tooltip']) . "
                                            </div>
                                            <div class='action-mask'>
                                                <div class='action'>
                                                    " . $button . "
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                        ";
                }
                echo "</table>
                    </div>
                </div>";
            }
            /*
                    echo $this->render('viewOPE', [
                        'model' => $model,
                        'dataProvider_OPE' => $dataProvider_OPE,

                    ]);
                */
            ?>
        </div>
    </div>
    <div class="card card-default m-t-10 m-b-10" style="border-top: thin solid #0062cc">
        <div class="card-body p-t-20">
            <p><b>TAXI</b></p>
            <hr />
            <?php
            if (!empty($data['Taxi'])) {
                echo "
                    <div class='m-t-5 m-b-0' style='overflow: scroll;'>
                        <table class='table table-bordered'>
                        <tr>
                            <th class='bg-primary col-9'>DESCRIPTION</th>
                            <th class='bg-primary'>TYPE</th>
                            <th class='bg-primary'>AMOUNT</th>
                            <th class='bg-primary'>DESTINATION</th>
                            <th class='bg-primary'>STATUS</th>
                            <th class='bg-primary'></th>
                        </tr>
                    ";
                foreach ($data['Taxi'] as $taxi) {
                    $button = "";

                    if (
                        ($model->tr_status == 0
                            or $taxi["tr_status"] == "APPROVAL1-REVISED"
                            or $taxi["tr_status"] == "APPROVAL2-REVISED"
                            or $taxi["tr_status"] == "REVISED")
                        and ($taxi["tr_status"] != "APPROVAL1-REJECTED"
                            and $taxi["tr_status"] != "APPROVAL2-REJECTED"
                            and $taxi["tr_status"] != "APPROVAL2-PENDING"
                            and $taxi["tr_status"] != "COMPLETED")
                    ) {
                        $button .= Html::a(
                            '<i class="fa fa-edit"></i> <span class="d-none d-md-inline">Update</span>',
                            false,
                            [
                                'onclick' => "update(4, '" . Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/taxi', 'id' => $taxi['time_report_id'], 'seq' => $taxi['seq']]) . "')",
                                'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
                                'style' => 'bjob-radius: 5px !important',
                            ]
                        );
                        $button .= " " . Html::a(
                            '<i class="fa fa-trash"></i> <span class="d-none d-md-inline">Delete</span>',
                            false,
                            [
                                'onclick' => "itemdelete(4, '" . Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/taxidelete', 'id' => $taxi['time_report_id'], 'seq' => $taxi['seq']]) . "')",
                                'class' => 'btn btn-danger text-white padding-5 p-l-10 p-r-10',
                                'style' => 'bjob-radius: 5px !important',
                            ]
                        );
                    }

                    echo "
                                <tr>
                                    <td class='bold'>{$taxi['task_description']}</td>
                                    <td>{$taxi['taxi_name']}</td>
                                    <td align='right'>Rp." . number_format($taxi['taxi_amount'], 0) . "</td>
                                    <td>{$taxi['taxi_destination']} </td>
                                    <td align='right'><span class='badge {$taxi['tr_status_label']}'>{$taxi['tr_status']}</span></td>
                                    <td align='right'>
                                        <div class='tooltip-action pull-right'>
                                            <div class='trigger'>
                                                " . Html::a('<i class="fa fa-ellipsis-h"></i>', false, ['class' => 'text-info', 'data-toggle' => 'tooltip']) . "
                                            </div>
                                            <div class='action-mask'>
                                                <div class='action'>
                                                    " . $button . "
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>";
                }
                echo "</table>
                </div>";
            }
            /*
                echo ListView::widget([
                    'dataProvider' => $dataProvider_TAXI,
                    'itemView' => 'viewTaxi',
                    'layout' => '{items}',
                ]);
                */
            ?>
        </div>
    </div>

    <!--
    <div class="card card-default m-t-10 m-b-10" style="border-top: thin solid #0062cc; ">
        <div class="card-body p-t-20">
            <p><b>OUT OF TOWN</b></p>
            <hr />
            <?php
            if (!empty($data['OutOfTown'])) {
                echo "
                    <div class='m-t-5 m-b-0' style='overflow: scroll;'>
                        <div class=''>
                        <table class='table table-bordered'>
                            <tr>
                                <th class='bg-primary col-9'>DESCRIPTION</th>
                                <th class='bg-primary'>AMOUNT</th>
                                <th class='bg-primary'>STATUS</th>
                                <th class='bg-primary'></th>
                            </tr>";
                foreach ($data['OutOfTown'] as $outoftown) {
                    $button = "";
                    if (
                        ($model->Status == 0
                            or $outoftown["Status"] == "APPROVAL1-REVISED"
                            or $outoftown["Status"] == "APPROVAL2-REVISED"
                            or $outoftown["Status"] == "REVISED")
                        and ($outoftown["Status"] != "APPROVAL1-REJECTED"
                            and $outoftown["Status"] != "APPROVAL2-REJECTED"
                            and $outoftown["Status"] != "APPROVAL2-PENDING"
                            and $outoftown["Status"] != "COMPLETED")
                    ) {
                        $button .= Html::a(
                            '<i class="fa fa-edit"></i> <span class="d-none d-md-inline">Update</span>',
                            false,
                            [
                                'onclick' => "update(3, '" . Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/oot', 'id' => $outoftown['TimeReportID'], 'job_id' => $outoftown['JobId']]) . "')",
                                'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
                                'style' => 'bjob-radius: 5px !important',
                            ]
                        );
                        $button .= " " . Html::a(
                            '<i class="fa fa-trash"></i> <span class="d-none d-md-inline">Delete</span>',
                            false,
                            [
                                'onclick' => "itemdelete(3, '" . Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/ootdelete', 'id' => $outoftown['TimeReportID'], 'job_id' => $outoftown['JobId']]) . "')",
                                'class' => 'btn btn-danger text-white padding-5 p-l-10 p-r-10',
                                'style' => 'bjob-radius: 5px !important',
                            ]
                        );
                    }

                    echo "
                                <tr>
                                    <td class='bold'>{$outoftown['Task']}</td>
                                    <td align='right'>Rp." . number_format($outoftown['OutOfTownValue'], 2) . "</td>
                                    <td align='right'><span class='badge {$outoftown['StatusLabel']}'>{$outoftown['Status']}</span></td>
                                    <td align='right'>
                                        <div class='tooltip-action pull-right'>
                                            <div class='trigger'>
                                                " . Html::a('<i class="fa fa-ellipsis-h"></i>', false, ['class' => 'text-info', 'data-toggle' => 'tooltip']) . "
                                            </div>
                                            <div class='action-mask'>
                                                <div class='action'>
                                                    " . $button . "
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                        ";
                }
                echo "</table>
                    </div>
                </div>";
            }
            /*
                    echo $this->render('viewOPE', [
                        'model' => $model,
                        'dataProvider_OPE' => $dataProvider_OPE,

                    ]);
                */
            ?>
        </div>
    </div>
    -->

    <div class="row m-b-10">
        <div class="col-md-12  <?php echo ($isMobile) ? 'text-center' : 'text-right'; ?>">

            <?php
            echo Html::a(
                'BACK',
                ['/tr/timereport/index'],
                [
                    'class' => 'btn btn-info text-white p-t-10 p-b-10',
                    'style' => 'bjob-radius: 5px !important',
                ]
            ) . '&nbsp;';
            if ($model->tr_status == 0) {
                echo '&nbsp;<a class="btn btn-info p-t-10 p-b-10" onclick="HeaderSave(0)" href="javascript:;"><i class="fa fa-edit"></i> SAVE<a/>';
                echo '&nbsp;<a class="btn btn-warning p-t-10 p-b-10" onclick="HeaderSave(1)" href="javascript:;">SUBMIT<a/>';
            }
            ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>


<script type="text/javascript">
    var paramJs = (paramJs || {});
    paramJs.urlDetailAdd = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/detail']); ?>';
    paramJs.urlMealAdd = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/meals']); ?>';
    paramJs.urlOPEAdd = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/ope']); ?>';
    paramJs.urlTaxiAdd = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/taxi']); ?>';
    paramJs.urlOOTAdd = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/oot']); ?>';

    paramJs.urlDetailDelete = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/detaildelete']); ?>';
    paramJs.urlMealDelete = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/mealsdelete']); ?>';
    paramJs.urlOPEDelete = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/opedelete']); ?>';
    paramJs.urlTaxiDelete = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/taxidelete']); ?>';
    paramJs.urlOOTDelete = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/ootdelete']); ?>';

    paramJs.urlComments = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/trsupervisorapproval/comments']); ?>';

    $('#tr-form').submit(function() {
        HeaderSave(0);
    });
    paramJs.urlFormHeaderSave = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/updatesave', 'id' => $model->tr_date]); ?>';
    paramJs.urlFormHeaderSubmitedSave = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/updatesave', 'id' => $model->tr_date, 'submit' => true]); ?>';

    function HeaderSave($submited) {
        showFullLoading();

        if ($submited == 1) {
            link = paramJs.urlFormHeaderSubmitedSave
        } else {
            link = paramJs.urlFormHeaderSave
        }

        $.ajax({
            url: link,
            type: 'POST',
            data: $('#tr-form').serialize(),
            success: function(data) {
                reload();
                hideFullLoading();

            },
            error: function(XMLHttpRequest, textStatus, errorThrown, data) {

                $('.custom-error').show();
                var err = (XMLHttpRequest.responseText);
                err = err.split('___');
                err = unescape(err[1]);
                if (err === "undefined") {
                    $('.custom-error').html(XMLHttpRequest.responseText);
                } else {
                    $('.custom-error').html(err);
                }


                $('html, body').animate({
                    scrollTop: 0
                }, 0);
                $('.switchery').trigger('click');
                hideFullLoading();

            }
        });
        return false;
    }

    function Comments(link) {
        $('#ModalComments').modal('show');
        $('#ModalCommentsBody').html("Loading ...");

        $.ajax({
            url: link,
            data: {},
            method: "POST",
            dataType: 'html',
            success: function(data) {
                $('#ModalCommentsBody').html(data);
                hideFullLoading();

            },
        });
    }

    function CommentsSave(id) {
        $('#ModalComments').modal('show');
        $('#ModalCommentsBody').html("Loading ...");
        var link = (link || paramJs.urlComments);

        $.ajax({
            url: link,
            data: {
                id: id
            },
            method: "POST",
            dataType: 'html',
            success: function(data) {
                $('#ModalCommentsBody').html(data);
                hideFullLoading();

            },
        });
    }

    function update(type, link = "") {
        if (link != "") {
            Modal(link);
        } else {
            if (type == 1) {
                Modal(paramJs.urlDetailAdd);
            } else if (type == 2) {
                Modal(paramJs.urlMealAdd);
            } else if (type == 3) {
                Modal(paramJs.urlOPEAdd);
            } else if (type == 4) {
                Modal(paramJs.urlTaxiAdd);
            } else if (type == 5) {
                Modal(paramJs.urlOOTAdd);
            }
        }
    }

    function itemdelete(type, link) {
        //showFullLoading();

        if (confirm("Are you sure want to delete this item permanently ?")) {
            $.ajax({
                url: link,
                data: {},
                method: "POST",
                dataType: 'json',
                success: function(data) {
                    reload();
                    hideFullLoading();
                    console.log(data);
                    var err = unescape(XMLHttpRequest.responseText);
                    err = err.split('&#039;');
                    err = err[3];
                    err = escape(err);

                    err = err.split('%26gt%3B%26gt%3BERROR%26lt%3B%26lt%3B');
                    err = unescape(err[1]);
                    //alert(err);
                    //$('.custom-error').html(err);
                    if (err === "undefined") {
                        //$('.custom-error').html(data);
                    } else {
                        $('.custom-error').show();
                        $('.custom-error').html(err);
                    }

                    $('html, body').animate({
                        scrollTop: 0
                    }, 0);
                    hideFullLoading();

                },
                error: function(XMLHttpRequest, textStatus, errorThrown, data) {
                    // console.log(XMLHttpRequest.responseText);
                    $('.custom-error').show();
                    var err = unescape(XMLHttpRequest.responseText);
                    // err = err.split('&#039;');
                    // err = err[3];
                    // err = escape(err);
                    //
                    // err = err.split('%26gt%3B%26gt%3BERROR%26lt%3B%26lt%3B');
                    // err = unescape(err[1]);
                    //alert(err);
                    $('.custom-error').html(err);
                    $('html, body').animate({
                        scrollTop: 0
                    }, 0);
                    hideFullLoading();

                }
            });
        } else {
            hideFullLoading();
        }
    }

    function Modal(link = "") {
        showFullLoading();
        var link = (link || paramJs.urlBudgetShowModal);
        $.ajax({
            url: link,
            data: $('#tr-form').serialize(),
            method: "POST",
            dataType: 'html',
            success: function(data) {
                hideFullLoading();
                $('#ChildModal').modal('show');
                $('#ChildModaldetail').html("Loading ...");

                $('#ChildModaldetail').html(data);
            },
        });
    }

    window.closeModal = function() {
        reload();
        hideFullLoading();
    };

    function reload() {
        $.pjax.defaults.timeout = false;
        $.pjax.reload({
            container: '#trTimeReportDetail-pjax'
        })
        $('#ChildModal').modal('hide');
        $('#ChildModal').data('modal', null);
    }
</script>
<?php \yii\widgets\Pjax::end(); ?>
