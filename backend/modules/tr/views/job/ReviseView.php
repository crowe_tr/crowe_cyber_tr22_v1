<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use kartik\widgets\ActiveForm;
use common\components\CommonHelper;

$user = CommonHelper::getUserIndentity();
?>

<div class="row">
	<div class="col-md-12">
		<div class="card card-default">
			<div class="card-body">
				<b class="fs-14">PENDING REVISION JOB : <?php echo $model->job_code . ' - ' . $model->client->client_name ?> </b></p>
				<div id="error" class="alert alert-danger" style="display:none;"></div>

				<?php
				echo '<table class="table table-bordered">';
				echo '<thead>'
					. '<tr>'
					. '<th class="bg-primary"></th>'
					. '<th class="bg-primary col-md-6" colspan="2">DESCRIPTION CHANGES</th>'
					. '<th class="bg-primary col-md-3">ORIGINAL</th>'
					. '<th class="bg-primary col-md-3">REVISION</th>'
					. '</tr>'
					. '</thead>';

				if (!empty($header)) {
					echo "<tr>
						<td colspan='5' class='bg-warning text-white'>" . date('d M Y', strtotime($header->created_at)) . " / " . $header->transaction_type . " - " . $header->id . "</td>
					</tr>";

					$json = "[".$header->audit_description."]";
					$header_desc = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $json));
					if (!empty($header_desc)) {
						echo "<tr>
							<td class='bg-info text-white'></td>
							<td class='bg-info text-white' colspan='4'>JOB</td>
						</tr>";

						$no = 0;
						foreach ($header_desc as $vals => $val) {
							$no++;
							echo "<tr>";
							echo "<td>" . $no . "</td>";
							echo "<td colspan='2'>" . $val->description . "</td>";

							if (
								$val->description == 'FEE'
								or $val->description == 'MEAL ALLOWANCE'
								or $val->description == 'OPE ALLOWANCE'
								or $val->description == 'TAXI ALLOWANCE'
								or $val->description == 'TIME CHARGES'
								or $val->description == 'ADMINISTRATIVE CHARGE'
								or $val->description == 'OTHER EXPENSE'
							) {
								echo "<td>" . number_format($val->before_value) . "</td>";
								echo "<td>" . number_format($val->after_value) . "</td>";
							} else {
								echo "<td>" . $val->before_value . "</td>";
								echo "<td>" . $val->after_value . "</td>";
							}
							echo "</tr>";
						}
					}
				}
				if (!empty($details)) {
					echo "<tr>
						<td class='bg-info text-white'></td>
						<td class='bg-info text-white' colspan='4'>JOB BUDGETING</td>
					</tr>";

					$no = 0;
					foreach ($details as $detail) {
						$desc = json_decode($detail->audit_description);
						if (!empty($desc)) {
							$no++;
							echo "<tr>";
							echo "<td>" . $no . "</td>";
							echo "<td colspan='2'>" . $desc->description . "</td>";
							echo "<td>" . $desc->before_value . "</td>";
							echo "<td>" . $desc->after_value . "</td>";
							echo "</tr>";
						}
					}
				}
				echo '</tbody>';
				echo '</table>';
				?>
			</div>
		</div>
		<div class="row">
			<div class="col-6">
				<?php
				echo Html::a(
					'<i class="fa fa-check"></i> APPROVE',
					false,
					[
						'class' => 'btn btn-primary text-white active approveklik',
						'onclick' => 'revise_approve();'
					]
				);
				echo Html::a(
					'<i class="fa fa-close"></i> REJECT ',
					false,
					[
						'class' => 'btn btn-danger text-white active ',
						'onclick' => 'revise_reject();',
					]
				);
				?>
			</div>
			<div class="col-6">
				<div class="pull-right">
					<?php
					echo Html::a('CLOSE', ['view', 'id' => $model->id], ['class' => 'btn btn-secondary text-white active ']);
					?>
				</div>

			</div>
		</div>


	</div>
</div>

<script type="text/javascript">
	var paramJs = (paramJs || {});
	paramJs.urlApprove = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/job/revise_approve', 'id' => $model->id]); ?>';
	paramJs.urlReject = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/job/revise_reject', 'id' => $model->id]); ?>';

	function revise_approve() {
		showFullLoading();
		if (confirm("Are you sure want to approve this Job ?")) {
			$.ajax({
				url: paramJs.urlApprove,
				type: 'POST',
				dataType: 'JSON',
				success: function(data) {
					if (data.status == true) {
						window.location.href = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/job/view', 'id' => $model->id]); ?>';
					} else {
						$('#error').html(data.message);
						$('#error').show();
					}

					hideFullLoading();
				},
				error: function(jqXHR, errMsg) {
					hideFullLoading();
					$('#error').html(errMsg);
					$('#error').show();
				}
			});
		} else {
			hideFullLoading();
		}
		return false;

	}

	function revise_reject() {
		showFullLoading();
		if (confirm("Are you sure want to reject this Job Revision ?")) {
			$.ajax({
				url: paramJs.urlReject,
				type: 'POST',
				dataType: 'JSON',
				success: function(data) {
					if (data.status == true) {
						window.location.href = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/job/view', 'id' => $model->id]); ?>';
					} else {
						$('#error').html(data.message);
						$('#error').show();
					}
					hideFullLoading();
				},
				error: function(jqXHR, errMsg) {
					hideFullLoading();
					$('#error').html(errMsg);
					$('#error').show();
				}
			});
		} else {
			hideFullLoading();
		}
		return false;

	}
</script>
