<?php

use yii\helpers\Url;
use yii\web\JsExpression;
use yii\helpers\Html;

use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\daterange\DateRangePicker;

use common\models\cl\Client;
use common\components\CommonHelper;

$user = CommonHelper::getUserIndentity();
?>
<?php
$form = ActiveForm::begin([
    'id' => 'jobsearch-form',
    'method' => 'post',
    'fieldConfig' => [
        'template' => '{label}{input}',
        'options' => [
            'class' => 'form-group form-group-default',
        ],
    ],
]);
?>

<div class="card m-t-10 m-b-5" style="background: #f2f2f2">
    <div class="card-body">
        <div class="form-group-attached">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-7">
                    <?php
                    $client = '';
                    if (!$model->isNewrecord || !empty($model->client_id)) {
                        $client = Client::findOne($model->client_id);
                        $client = empty($client->client_name) ? $model->client_id : $client->client_name;
                    }
                    echo $form->field($model, 'client_id', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])
                        ->widget(Select2::classname(), [
                            'initValueText' => $client,
                            'options' => ['placeholder' => 'Search Client ... '],
                            'pluginOptions' => [
                                'allowClear' => true,
                                // 'minimumInputLength' => 2,
                                'tags' => false,
                                'ajax' => [
                                    'url' => Url::to(['helper/srcclient']),
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) { return {q:params.term , job_id:$("#job_id").val()}; }'),
                                ],
                            ],
                        ])->label("Client");
                    ?>

                </div>
                <div class="col-lg-2 col-md-3 col-sm-5">
                    <?= $form->field($model, 'job_code', ['options' => ['onchange' => '$("#jobsearch-form").submit()', 'placeholder' => 'Search by Job Code']]); ?>
                </div>
                <div class="col-lg-3 col-md-4 ">
                    <?= $form->field($model, 'job_description', ['options' => ['onchange' => '$("#jobsearch-form").submit()', 'placeholder' => 'Search by Job Description']]); ?>
                </div>
                <div class="col-md-2">
                    <?php
                    echo $form->field($model, 'periode')->widget(DateRangePicker::classname(), [
                        'presetDropdown' => true,
                        'pluginOptions' => [
                            'opens' => 'right',
                            'locale' => [
                                'format' => 'Y-MM-DD',
                                'separator' => ' TO ',
                                'cancelLabel' => 'Clear'
                            ],
                        ],
                    ]);
                    ?>
                </div>
                <div class="col-md-1">
                    <?= Html::submitButton('SEARCH', ['class' => 'btn btn-success btn-block active padding-15 btn-lg']) ?>
                </div>

            </div>
        </div>
    </div>
</div>


<?php ActiveForm::end(); ?>
