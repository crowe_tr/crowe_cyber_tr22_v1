<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\grid\GridView;
use common\components\JobHelper;
use kartik\widgets\SwitchInput;


JobHelper::set_title_txt($this, ['label' => Yii::t('backend', 'JOB LIST'), 'url' => ['all']], ($model->isNewRecord) ? 'Create New Job' : 'Update Job  : ' . $model->job_code);
echo JobHelper::widget_pjax('DetailModal');
$form = ActiveForm::begin(JobHelper::begin_form('job-form'));
echo $form->errorSummary($model);
?>
<?php \yii\widgets\Pjax::begin(['id' => rand() . 'pjax', 'enablePushState' => false, 'id' => 'job-pjax']); ?>

<div class="alert alert-danger" id="error" style="display:none"></div>

<?php if (!$model->isNewRecord) {	?>
	<div class="card card-default m-b-10">
		<div class="card-body">
			<div class="form-group-attached">
				<div class="row">
					<div class="col-md-2">
						<?= $form->field($model, 'job_code', ['options' => ['onchange' => 'request_save()']])->textInput(); ?>
					</div>
					<div class="col-md-6">
						<?= $form->field($modelView, 'client_name', ['options' => []])->textInput(['disabled' => true]); ?>

					</div>
					<div class="col-md-2">
						<?= $form->field($modelView, 'entity_name', ['options' => []])->textInput(['disabled' => true]); ?>
					</div>
					<div class="col-md-2">
						<?= $form->field($modelView, 'div_name', ['options' => []])->textInput(['disabled' => true]); ?>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php } ?>
<div class="card card-default m-b-10">
	<div class="card-body">
		<?php
		if (!is_null($model->id)) {
			$form->field($model, 'id', ['options' => ['class' => '']])->hiddenInput(['id' => 'id'])->label(false);
			$form->field($model, 'client_id', ['options' => ['class' => '']])->hiddenInput()->label(false);
			$form->field($model, 'job_code', ['options' => ['class' => '']])->hiddenInput()->label(false);
		?>
			<div class="form-group-attached">
				<div class="row">
					<?php
					echo JobHelper::set_text_form('9', $form, $model, 'description', 'request_save()');
					echo JobHelper::set_number_form('3', $form, $model, 'job_fee', 'onchange', 'request_save()');
					?>
				</div>
				<div class="row">
					<?php
					echo JobHelper::set_date_form('2', $form, $model, 'start_date', 'request_save()');
					echo JobHelper::set_date_form('2', $form, $model, 'end_date', 'request_save()');
					echo JobHelper::set_area_form('5', $form, $model, 'job_area', $modelView->client_areas, 'request_save()');
					echo JobHelper::set_switch_form('3', $form, $model, 'include_ope', 'request_save()');
					?>

				</div>
			<?php
		} else {
			$form->field($model, 'id', ['options' => ['class' => '']])->hiddenInput()->label(false);
			?>
				<div class="form-group-attached">
					<div class="row">
						<?php
						echo JobHelper::set_combo_form_read('4', $form, $model, 'client_id', 'request_save()', $data['Client']);
						echo JobHelper::set_text_form_read('2', $form, $model, 'job_code');
						echo JobHelper::set_text_form('6', $form, $model, 'description', 'request_save()');
						?>
					</div>
					<div class="row">
						<?php
						echo JobHelper::set_date_form('3', $form, $model, 'start_date', 'request_save()');
						echo JobHelper::set_date_form('3', $form, $model, 'end_date', 'request_save()');
						echo JobHelper::set_number_form('4', $form, $model, 'job_fee', 'onchange', 'request_save()');
						echo JobHelper::set_switch_form('2', $form, $model, 'include_ope', 'request_save()');
						?>
					</div>
				</div>
			<?php
		}
			?>
			</div>
	</div>

</div>

<?php
if (!$model->isNewRecord) :
?>
	<div class="card card-default m-b-10">
		<div class="card-body">
			<p><b>ALLOWANCE</b></p>
			<div class="form-group-attached">
				<div class="row">
					<?php
					echo JobHelper::set_combo_allowance('3', $form, $model, 'is_meal', 'Meal', 'onchange', 'request_save()', 'change', 'job-mealallowance');
					echo JobHelper::set_combo_allowance('3', $form, $model, 'is_taxi', 'Transportation', 'onchange', 'request_save()', 'change', 'job-taxiallowance');
					echo JobHelper::set_combo_allowance('3', $form, $model, 'is_ope', 'OutOffice', 'onchange', 'request_save()', 'change', 'job-outofofficeallowance');
					?>
				</div>
			</div>
		</div>
	</div>

	<div class="card card-default m-b-10 m-t-20">
		<div class="card-body">
			<?= JobHelper::set_budget_register('REGISTER TEAM', 'onclick', 'BudgetCreate()'); ?>
			<p><b>JOB BUDGETING</b></p>
			<?=
			GridView::widget([
				'id' => 'job-details',
				'dataProvider' => $modelBudgeting,
				'emptyText' => JobHelper::set_empty_text_budgeting(),
				'columns' => [
					JobHelper::set_column_no(),
					JobHelper::set_column('employee_id', '3', '', '', 'employee.full_name'),
					JobHelper::set_column('level_id', '2', '', '', 'employee.level.level_name'),
					JobHelper::set_column('total_wh', '1', '', 'text-right'),
					JobHelper::set_column('planning', '1', 'text-center', 'text-right'),
					JobHelper::set_column('field_work', '1', 'text-center', 'text-right'),
					JobHelper::set_column('reporting', '1', 'text-center', 'text-right'),
					JobHelper::set_column('wrap_up', '1', 'text-center', 'text-right'),
					JobHelper::set_column('over_time', '1', 'text-center', 'text-right'),
					// JobHelper::set_column('employee_id', '3', '', '', 'employee.full_name'),
					// JobHelper::set_column('level_id', '2', '', '', 'employee.level.level_name'),
					// JobHelper::set_column2($modelBudgeting, 'Total WH', '1', 'text-right', 'text-right','total_wh'),
					// JobHelper::set_column2($modelBudgeting, 'Planning', '1', 'text-right', 'text-right', 'planning'),
					// JobHelper::set_column2($modelBudgeting, 'Field Work', '1', 'text-right', 'text-right', 'field_work'),
					// JobHelper::set_column2($modelBudgeting, 'Reporting', '1', 'text-right', 'text-right', 'reporting'),
					// JobHelper::set_column2($modelBudgeting, 'Wrap Up', '1', 'text-right', 'text-right', 'wrap_up'),
					// JobHelper::set_column2($modelBudgeting, 'Overtime', '1', 'text-right', 'text-right', 'over_time'),
					[
						'header' => '',
						'headerOptions' => ['class' => 'bg-success text-center'],
						'filterOptions' => ['class' => 'b-b b-grey'],
						'contentOptions' => ['class' => 'no-padding'],
						'format' => 'raw',
						'value' => function ($data, $model) {
							$edit = JobHelper::set_icon_register('fa fa-edit', 'onclick', "BudgetUpdate('" . Yii::$app->urlManager->createAbsoluteUrl(['tr/job/budget', 'id' => $data->employee_id]) . "')", 'btn-success');
							$delete = JobHelper::set_icon_register('pg-trash', 'onclick', "BudgetDelete('" . $data->employee_id . "','" . $data->job_id . "')", 'btn-danger');
							$val = JobHelper::set_icon_ellipse([$edit, $delete]);
							return $val;
						},
					],
				],
				'layout' => '{items}',
				'resizableColumns' => false,
				'bordered' => false,
				'striped' => false,
				'condensed' => false,
				'responsive' => false,
				'hover' => false,
				'persistResize' => false,
			]);
			?>
		</div>
	</div>

	<div class="card card-default m-b-10">
		<div class="card-body">
			<p><b>PIC PROJECT</b></p>
			<div class="form-group-attached">
				<div class="row">
					<?php
					echo JobHelper::set_pic_project('4', $model, $model->partner_id, $form, 'partner_id', $model->id);
					echo JobHelper::set_pic_project('4', $model, $model->manager_id, $form, 'manager_id', $model->id);
					echo JobHelper::set_pic_project('4', $model, $model->supervisor_id, $form, 'supervisor_id', $model->id);
					?>
				</div>
			</div>
		</div>
	</div>

	<div class="card card-default m-t-5 m-b-5">
		<div class="card-body">
			<div class="row">
				<div class="col-md-7 no-padding ">
					<div class="row">
						<div class="col-md-6">
							<b>JOB REALITATION</b>
						</div>
						<div class="col-md-6 pull-right">
							<a href="javascript:;" class="btn btn-info pull-right" onclick="estimated_default()">SET DEFAULT</a>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-7 no-padding card card-default">
					<table class="table table-striped ">
						<thead>
							<tr>
								<td class="btn-success text-white"></td>
								<td class="btn-success text-white">ESTIMATED</td>
								<td class="btn-success text-white">ACTUAL</td>
							</tr>
						</thead>
						<tbody>
							<?php
							echo $form->field($model, 'id', ['options' => ['class' => '']])->hiddenInput()->label(false);

							echo JobHelper::set_footer_form('TIME CHARGES', number_format($model->time_charges), isset($model->time_charges_actual) ? number_format($model->time_charges_actual) : "0");
							echo JobHelper::set_footer_form_allowance('MEALS ALLOWED', $model->is_meal, $form, $model, 'meal_allowance', 'meal_allowance', isset($model->meal_allowance_actual) ? number_format($model->meal_allowance_actual) : "0");
							echo JobHelper::set_footer_form_allowance('TAXI USED ALLOWED', $model->is_taxi, $form, $model, 'taxi_allowance', 'taxi_allowance', isset($model->taxi_allowance_actual) ? number_format($model->taxi_allowance_actual) : "0");
							echo JobHelper::set_footer_form_allowance('OUT OF OFFICE ALLOWANCE', $model->is_ope, $form, $model, 'ope_allowance', 'ope_allowance', isset($model->ope_allowance_actual) ? number_format($model->ope_allowance_actual) : "0");
							echo JobHelper::set_footer_form_expense('ADMINISTRATIVE CHARGE', $form, $model, 'administrative_charge', 'administrative_charge', isset($model->administrative_charge) ? number_format($model->administrative_charge) : "0");
							echo JobHelper::set_footer_form_expense('OTHER EXPENSE ALLOWED', $form, $model, 'other_expense_allowance', 'other_expense_allowance', isset($model->other_expense_allowance) ? number_format($model->other_expense_allowance) : "0");
							echo JobHelper::set_footer_form_recovery('RECOVERY RATE(%)', isset($model->percentage) ? $model->percentage : "0", isset($model->percentage_actual) ? $model->percentage_actual : "0");
							?>
						</tbody>
					</table>
				</div>

			</div>
		</div>
	</div>
<?php
endif;
?>

<div class="alert alert-danger" id="error-bottom" style="display:none"></div>

<div class="row m-b-70">
	<div class="col-md-12 text-right">
		<?php
		echo Html::a('BACK', ['view', 'id' => $model->id], ['class' => 'btn btn-info btn-lg mr-2']);
		echo '<input type="submit" name="submit-btn" value="SUBMIT REQUEST" class="btn btn-success btn-lg" />';
		?>
	</div>
</div>

<?php \yii\widgets\Pjax::end(); ?>

<?php ActiveForm::end(); ?>

<script type="text/javascript">
	var paramJs = (paramJs || {});
	paramJs.urlBudgetDelete = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/job/budgetdelete']); ?>';
	paramJs.urlBudgetShowModal = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/job/budget']); ?>';
	paramJs.urlDefault = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/job/estimated_default']) ?>'
	$(document).ready(function() {

		$('#job-form').on('beforeSubmit', function() {
			request_save(1);
			return false;
		});

		$('#job-include_ope').on('switchChange.bootstrapSwitch', function(e, s){
			request_save();
		});
		$('#Meal').on('switchChange.bootstrapSwitch', function(e, s){
			request_save();
		});
		$('#Transportation').on('switchChange.bootstrapSwitch', function(e, s){
			request_save();
		});
		$('#OutOffice').on('switchChange.bootstrapSwitch', function(e, s){
			request_save();
		});
	});

	function request_save(submit) {
		// alert('a');
		var url_data = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/job/request_save']); ?>';
		var form_data = new FormData($('#job-form')[0]);
		submit = (submit || 0);
		if (submit == 1) {
			url_data = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/job/request_save', 'submit' => 1]); ?>';
		}

		showFullLoading();
		$.ajax({
			url: url_data,
			type: 'POST',
			data: form_data,
			async: false,
			cache: false,
			contentType: false,
			processData: false,
			dataType: 'JSON',
			success: function(data) {
				hideFullLoading();

				if (data.status == true) {
					if (data.model.isSubmit == 1) {
						window.location.href = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/job/view', 'id' => $model->id]); ?>';
					}

					$('#job-mealallowance').prop("disabled", data.model.is_meal);;
					$('#job-outofofficeallowance').prop("disabled", data.model.is_ope);;
					$('#job-taxiallowance').prop("disabled", data.model.is_taxi);;

					$('#TimeCharges').html(data.model.time_charges);
					$('#TimeChargesAct').html(data.model.time_charges_actual);

					$('#MealAllowanceAct').html(data.model.meal_allowance_actual);

					$('#TaxiAllowanceAct').html(data.model.taxi_allowance_actual);

					$('#OutOfOfficeAllowanceAct').html(data.model.ope_allowance_actual);
					// console.log(data.model.administrative_charge.toLocaleString('en-US'));
					$('#administrative_charge_actual').html(data.model.administrative_charge_actual);

					$('#other_expense_allowance_actual').html(data.model.other_expense_allowance_actual);

					$('#job-percentage').html(data.model.percentage);
					$('#job-percentage_actual').html(data.model.percentage_actual);
				} else {
					$('#error').html(data.message);
					$('#error-bottom').html(data.message);
					$('#error').show();
					$('#error-bottom').show();
				}
			},
			error: function(jqXHR, errMsg) {
				hideFullLoading();
				$('#error').html(errMsg);
				$('#error-bottom').html(errMsg);
				$('#error').show();
				$('#error-bottom').show();
			}
		});
		return false;
	}

	function format_number(num) {
	  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
	}

	function estimated_default() {
		var link = (link || paramJs.urlDefault);
		showFullLoading();
		$.ajax({
			url: link,
			data: $('#job-form').serialize(),
			method: "POST",
			dataType: 'html',
			success: function(data) {
				var d = JSON.parse(data);
				// var d.administrative_charge_actual = d.administrative_charge;
				console.log(d);
				$('#job-meal_allowance').val(d.meal_allowance);
				$('#job-taxi_allowance').val(d.taxi_allowance);
				$('#job-ope_allowance').val(d.ope_allowance);
				$('#job-administrative_charge').val(d.administrative_charge);
				$('#administrative_charge_actual').html(format_number(d.administrative_charge));
				$('#job-other_expense_allowance').val(d.other_expense_allowance);
				$('#job-percentage').html(d.percentage);
				$('#job-percentage_actual').html(d.percentage_actual);
				hideFullLoading();
			},
		});
	}

	function BudgetShowModal(link = "") {
		var link = (link || paramJs.urlBudgetShowModal);
		showFullLoading();
		$.ajax({
			url: link,
			data: $('#job-form').serialize(),
			method: "POST",
			dataType: 'html',
			success: function(data) {
				hideFullLoading();
				$('#detail').html(data);
			},
		});
	}

	function BudgetCreate() {
		BudgetShowModal();
		$('#DetailModal').modal('show');
	}

	function BudgetUpdate(link) {
		BudgetShowModal(link);
		$('#DetailModal').modal('show');
	}

	function BudgetDelete(EmployeeID, JobID) {
		if (confirm("Are you sure want to delete this item permanently ?")) {
			$.ajax({
				url: paramJs.urlBudgetDelete,
				data: {
					EmployeeID: EmployeeID,
					JobID: JobID
				},
				method: "POST",
				dataType: 'json',
				success: function() {
					reload();
				},
				complete: function(data) {},
				error: function(data) {}
			});
		}
	}


	window.closeModal = function() {
		reload();
	};

	function reload() {
		$.pjax.defaults.timeout = false;
		$.pjax.reload({
			container: '#job-pjax'
		})
		$('#DetailModal').modal('hide');
		$('#DetailModal').data('modal', null);
	}
</script>
