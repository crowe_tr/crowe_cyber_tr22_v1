<?php

use kartik\widgets\ActiveForm;

$form = ActiveForm::begin([
    'id' => 'form-ganteng',
    'options' => ['enctype' => 'multipart/form-data'],
]);
echo "<div class='row'>";
echo "<div class='col-3'>";
echo $form->field($model, 'code', ['options' => ['id' => 'code']])->textInput();
echo $form->field($model, 'field_name', ['options' => ['id' => 'iline_update']])->textInput();
echo "</div>";
echo "</div>";
ActiveForm::end();
?>

<script type="text/javascript">
    $("#iline_update").change(function() {inline_save();});
    $("#code").change(function() {inline_save();});

    function inline_save() {
        var link = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/job/inline_update']); ?>';
        var $form = new FormData($('#form-ganteng')[0]);
        $.ajax({
            url: link,
            type: 'POST',
            data: $form,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                alert(data);
            },
            error: function(jqXHR, errMsg) {
                alert(errMsg);
            }
        });
        return false;

    }
</script>