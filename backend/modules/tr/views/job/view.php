<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use kartik\widgets\ActiveForm;
use common\components\CommonHelper;
use common\models\tr\JobTemp;
use mdm\admin\components\Helper;
use common\components\JobHelper;


$user = CommonHelper::getUserIndentity();
JobHelper::set_title_txt($this, ['label' => Yii::t('backend', 'JOB LIST'), 'url' => ['all']], $modelJob->job_code . ' (' . $modelView->job_status . ') ');

$data = [];
if ($modelJob->partner_id == $user->user_id or $modelJob->manager_id == $user->user_id or $user->is_admin == 1) {
    if (in_array($modelJob->job_status, [0, 3]) and ($user->user_id == $modelJob->manager_id or $user->is_admin == 1)) {
        if (Helper::checkRoute('/tr/jobcreate/n') && $user->user_id != $modelJob->manager_id && $user->is_admin != 1) {
            $data['button-right1'] = Html::a(Yii::t('app', '<i class="fa fa-edit"></i> UPDATE HEADER'), ['/tr/job/request', 'id' => $modelJob->id], ['class' => 'btn btn-info text-white active ']);;
        } else {
            $data['button-right1'] = Html::a(Yii::t('app', '<i class="fa fa-edit"></i> UPDATE'), ['/tr/job/request', 'id' => $modelJob->id], ['class' => 'btn btn-warning text-white active ']);;
        }
    }
    if ($modelJob->job_status == 0  and ($user->user_id == $modelJob->manager_id or $user->is_admin == 1 or $modelJob->created_by == $user->user_id)) {
        $data['button-right11'] =  Html::a(Yii::t('app', '<i class="fa fa-trash"></i> DELETE '), ['/tr/job/delete', 'id' => $modelJob->id], [
            'class' => 'btn btn-danger text-white active ',  'data-method' => 'post',
            'data-pjax' => 1,
            'data-confirm' => 'ARE YOU SURE TO DELETE THIS JOB?',
        ]);;
    }
    if ($modelJob->job_status == 2 and ($user->user_id == $modelJob->manager_id or $user->is_admin == 1) and $modelJob->flag == 0) {
        $data['button-right2'] = Html::a(Yii::t('app', 'REVISE'), ['/tr/job/revise', 'id' => $modelJob->id], ['class' => 'btn btn-info text-white active ']);
    }
    if ($modelJob->job_status == 2 and ($user->user_id == $modelJob->manager_id or $user->is_admin == 1) and $modelJob->flag == 1) {
        $data['button-right2'] = Html::a(Yii::t('app', 'UPDATe REVISION'), ['/tr/job/revise', 'id' => $modelJob->id], ['class' => 'btn btn-warning text-white active ']);
    }
    if ($modelJob->job_status == 2 and ($user->user_id == $modelJob->partner_id or $user->is_admin == 1) and $modelJob->flag == 2) {
        $data['button-right3'] = Html::a('<i class="text-danger fa fa-exclamation-circle"></i> PENDING REVISE', false, ['onclick' => 'revise_view();', 'class' => 'btn btn-warning text-white active ']);
    }
}

if ($modelJob->partner_id == $user->user_id) {
    $data['button-right-back'] = Html::a(Yii::t('app', 'BACK'), ['/tr/jobapproval/index'], ['class' => 'btn btn-secondary text-white active ']);
} else {
    $data['button-right-back'] = Html::a(Yii::t('app', 'BACK'), ['index'], ['class' => 'btn btn-secondary text-white active ']);
}
$data['button-log'] = Html::a('<i class="fa fa-history"></i> LOGS', ['revise_log', 'id' => $modelJob->id], ['class' => 'btn btn-primary text-white active ']);

$buttons = [];
foreach ($data as $key => $value) {
    array_push($buttons, $value);
}
$btn = implode('&nbsp;', $buttons);
$this->params['breadcrumbs_btn'] =
    $btn . '&nbsp;' .
    Html::a(
        // '<i class="pg-comment"></i> <span class="badge badge-warning">' . $modelView->comment_count . '</span> ',
        '<i class="pg-comment"></i> <span class="badge badge-warning">' . $modelView->comments_count . '</span> ',
        false,
        [
            'class' => 'btn btn-success text-white',
            'onclick' => 'reload_comment();',
            'data-toggle' => 'modal',
            'data-target' => '#ModalComments',
            'title' => '' . $modelView->comments_count . 'Comments',
        ]
    );;

\yii\web\YiiAsset::register($this);

$st = isset($_GET['st']) ? $_GET['st'] : false;
if ($st != false) {
    echo '<div class="alert alert-info" role="alert">
			  REVISE NO CHANGES
			</div>';
}

?>
<?php \yii\widgets\Pjax::begin(['id' => rand() . 'pjax', 'enablePushState' => false, 'id' => 'job-pjax']); ?>

<div class="modal fade slide-right" id="ModalComments" role="dialog" aria-hidden="false">
    <div class="modal-dialog no-border">
        <div class="modal-content no-border">
            <div class="modal-header no-border bg-success">
                <p class="text-white">
                    <b><i class="pg-comment fax2"></i> COMMENTS</b>
                </p>
            </div>
            <div id="detail" class="modal-body no-padding">
                <div class=" notification-toggle" role="menu" aria-labelledby="notification-center">
                    <div class="notification-panel" style="width: 100%; border: none;">
                        <div class="notification-body scrollable scroll-content scroll-scrolly_visible bg-pattern">
                            <div id="comments" style="height: 300px; overflow: auto; box-shadow: none; max-height: 320px; height: 320px; overflow:auto; background: url('/../../themes/pages/assets/img/pattern.jpg') repeat scroll 0% 0% !important;">

                            </div>
                        </div>
                        <div class="notification-footer" id="ModalCommentsForm">
                            <?php
                            $form = ActiveForm::begin([
                                'id' => 'comment-form',
                                'enableClientValidation' => true,
                                'validateOnSubmit' => true,
                                'validateOnChange' => true,
                                'validateOnType' => true,
                            ]);
                            ?>
                            <?= $form->field($modelComments, 'job_id', ['options' => ['class' => '']])->hiddenInput()->label(false); ?>
                            <?= $form->field($modelComments, 'comments')->label(false)->widget(\yii\redactor\widgets\Redactor::className([
                                'clientOptions' => [
                                    'imageManagerJson' => ['/redactor/upload/image-json'],
                                    'imageUpload' => ['/redactor/upload/image'],
                                    'fileUpload' => ['/redactor/upload/file'],
                                    'plugins' => ['clips', 'fontcolor', 'imagemanager'],
                                ],
                            ]), ['options' => ['height' => '300px']]); ?>

                            <div class="row m-t-10">
                                <div class="col-sm-8">
                                    <a class=" btn btn-lg btn-warning btn-block text-white" onclick="addComment()" style="opacity: initial" href="javascript:;">
                                        <b><i class="fa fa-sends"></i> SEND COMMENT</b>
                                    </a>
                                </div>
                                <div class="col-sm-4">
                                    <a class=" btn btn-lg btn-info btn-block text-white" data-dismiss="modal" href="javascript:;">
                                        <b><i class="fa fa-sends"></i> CLOSE</b>
                                    </a>
                                </div>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card card-default m-t-5 m-b-5">
    <div class="card-body">
        <div class="item ">
            <label class="row">
                <div class="col-md-12">
                    <div>
                        <div class="row p-b-5">
                            <?php
                            echo JobHelper::set_label('5', 'left', 'JOB DESCRIPTION', $modelJob->description, 'b');
                            echo JobHelper::set_label('4', 'left', 'CLIENT', $modelView->client_name, 'b');
                            echo JobHelper::set_label('3', 'left', 'TOTAL FEE (IDR)', (($modelJob->job_fee > 0) ? (number_format($modelJob->job_fee)) : "") . ' (' . (($modelJob->include_ope) ? "Include OPE" : "Exclude OPE") . ')', 'b');
                            echo JobHelper::set_line();
                            ?>
                        </div>
                        <div class="row">
                            <?php
                            echo JobHelper::set_label(
                                '5',
                                'left',
                                'PERIODS',
                                (isset($modelJob->start_date) ? date_format(date_create($modelJob->start_date), "M d, Y") : "") . ' - ' .
                                    (isset($modelJob->end_date) ? date_format(date_create($modelJob->end_date), "M d, Y") : ""),
                                'b'
                            );
                            echo JobHelper::set_label('4', 'left', 'ENTITY / DIVISION', $modelView->entity_name . ' / ' . $modelView->div_name, 'b');
                            echo JobHelper::set_label('3', 'left', 'JOB AREA', (isset($modelJob->job_area) ? $modelJob->job_area : "-"), 'b');

                            ?>
                        </div>
                    </div>
                </div>
            </label>
        </div>
    </div>
</div>

<?php
echo JobHelper::set_allowance(
    ($modelJob->is_meal == 1) ? '<i class="fa fa-check"></i>' : '<i class="fa fa-lg fa-times"></i>',
    ($modelJob->is_taxi == 1) ? '<i class="fa fa-check"></i>' : '<i class="fa fa-lg fa-times"></i>',
    ($modelJob->is_ope == 1) ? '<i class="fa fa-check"></i>' : '<i class="fa fa-lg fa-times"></i>'
);
?>

<?php
    // var_dump($dataProviderBudget);
?>

<div class="card card-default m-t-5 m-b-5">
    <div class="card-body">
        <p><b>JOB BUDGETING</b></p>
        <?= GridView::widget([
            'id' => 'job-details',
            'dataProvider' => $dataProviderBudget,
            'emptyText' => JobHelper::set_empty_text_budgeting(),
            'columns' => [
                JobHelper::set_column_no(),
                JobHelper::set_column('employee_id', '3', '', '', 'employee.full_name'),
                JobHelper::set_column('level_id', '2', '', '', 'employee.level.level_name'),
                JobHelper::set_column2($dataProviderBudget, 'Total WH', '2', 'text-right', 'text-right','total_wh'),
                JobHelper::set_column2($dataProviderBudget, 'Planning', '1', 'text-right', 'text-right', 'planning'),
                JobHelper::set_column2($dataProviderBudget, 'Field Work', '1', 'text-right', 'text-right', 'field_work'),
                JobHelper::set_column2($dataProviderBudget, 'Reporting', '1', 'text-right', 'text-right', 'reporting'),
                JobHelper::set_column2($dataProviderBudget, 'Wrap Up', '1', 'text-right', 'text-right', 'wrap_up'),
                JobHelper::set_column2($dataProviderBudget, 'Overtime', '1', 'text-right', 'text-right', 'over_time'),
                // JobHelper::set_column('employee_id', '2', '', '', 'employee.full_name'),
                // JobHelper::set_column('level_id', '2', '', '', 'employee.level.level_name'),
                // JobHelper::set_column('total_wh', '1', '', 'text-right'),
                // JobHelper::set_column('planning', '1', 'text-center', 'text-right'),
                // JobHelper::set_column('field_work', '2', 'text-center', 'text-right'),
                // JobHelper::set_column('reporting', '1', 'text-center', 'text-right'),
                // JobHelper::set_column('wrap_up', '1', 'text-center', 'text-right'),
                // JobHelper::set_column('over_time', '2', 'text-center', 'text-right'),
            ],
            // 'layout' => '{items}',
            'layout' => JobHelper::set_layout('items'),
            'resizableColumns' => false,
            'bordered' => false,
            'striped' => false,
            'condensed' => false,
            'responsive' => false,
            'hover' => false,
            'persistResize' => false,
        ]);
        ?>
    </div>
</div>

<div class="card card-default m-b-10">
    <div class="card-body">
        <p><b>PIC PROJECT</b></p>
        <hr />
        <div class="form-group-attached">
            <div class="row">
                <hr />
                <?php
                echo JobHelper::set_label_footer(
                    '4',
                    'Partner',
                    (empty($modelJob->partner->full_name) ? $modelJob->partner_id : $modelJob->partner->full_name . ' - ' . $modelJob->partner->level->level_name),
                    'b'
                );
                echo JobHelper::set_label_footer(
                    '4',
                    'MANAGER',
                    (empty($modelJob->manager->full_name) ? $modelJob->manager_id : $modelJob->manager->full_name . ' - ' . $modelJob->manager->level->level_name),
                    'b'
                );
                echo JobHelper::set_label_footer(
                    '4',
                    'SUPERVISOR',
                    (empty($modelJob->supervisor->full_name) ? $modelJob->supervisor_id : $modelJob->supervisor->full_name . ' - ' . $modelJob->supervisor->level->level_name),
                    'b'
                );
                ?>
            </div>
        </div>
    </div>
</div>

<div class="card card-default m-t-5 m-b-5">
    <div class="card-body">
        <p><b>JOB REALITATION</b></p>

        <div class="row">
            <div class="col-md-7">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <td class="text-center btn-success text-white"></td>
                            <td class="text-center btn-success text-white">ESTIMATED</td>
                            <td class="text-center btn-success text-white">ACTUAL</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        echo JobHelper::set_table_footer('Time Charges', number_format($modelJob->time_charges), isset($modelJob->time_charges_actual) ? number_format($modelJob->time_charges_actual) : "0");
                        echo JobHelper::set_table_footer('Meals Allowed', number_format($modelJob->meal_allowance), isset($modelJob->meal_allowance_actual) ? number_format($modelJob->meal_allowance_actual) : "0");
                        echo JobHelper::set_table_footer('Taxi Used Allowed', number_format($modelJob->taxi_allowance), isset($modelJob->taxi_allowance_actual) ? number_format($modelJob->taxi_allowance_actual) : "0");
                        echo JobHelper::set_table_footer('Out Of Office Allowance', number_format($modelJob->ope_allowance), isset($modelJob->ope_allowance_actual) ? number_format($modelJob->ope_allowance_actual) : "0");
                        echo JobHelper::set_table_footer('Administrative Charge', number_format($modelJob->administrative_charge), isset($modelJob->administrative_charge) ? number_format($modelJob->administrative_charge) : "0");
                        echo JobHelper::set_table_footer('Other Expense Allowance', number_format($modelJob->other_expense_allowance), isset($modelJob->other_expense_allowance) ? number_format($modelJob->other_expense_allowance) : "0");
                        echo JobHelper::set_table_footer('Recovery Rate (%)', isset($modelJob['percentage']) ? $modelJob['percentage'] : "0", isset($modelJob['percentage_actual']) ? $modelJob['percentage_actual'] : "0");
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col mb-3 text-right">
        <?php
        if ($modelJob->partner_id == $user->user_id or $user->is_admin == 1) {
            if ($modelJob->job_status == 1) {
                echo Html::a(
                    '<i class="fa fa-check"></i>  APPROVE',
                    false,
                    [
                        'class' => 'btn btn-primary btn-lg text-white active ',
                        'onclick' => 'job_approve()',
                    ]
                );
                echo Html::a(
                    '<i class="fa fa-close"></i> REJECT ',
                    false,
                    [
                        'class' => 'btn btn-danger btn-lg text-white active ',  'data-method' => 'post',
                        'onclick' => 'job_reject()',
                    ]
                );
            }
            if ($modelView->job_status == "ONGOING") {
                echo Html::a('<i class="fa fa-close"></i> CLOSED ', false, [
                    'onclick' => 'job_close()',
                    'class' => 'btn btn-secondary btn-lg text-white active ',
                ]);
            }
        }
        ?>

    </div>
</div>
<?php \yii\widgets\Pjax::end(); ?>

<?php
$this->registerJs(
    "
	     var paramJs = (paramJs || {});
            paramJs.job_approve = '" . Yii::$app->urlManager->createAbsoluteUrl(['tr/job/approve', 'id' => $modelJob->id]) . "';
            paramJs.job_reject = '" . Yii::$app->urlManager->createAbsoluteUrl(['tr/job/reject', 'id' => $modelJob->id]) . "';
            paramJs.job_close = '" . Yii::$app->urlManager->createAbsoluteUrl(['tr/job/close', 'id' => $modelJob->id]) . "';
            paramJs.notifurl = '" . Yii::$app->urlManager->createAbsoluteUrl(['tr/job/show_notification']) . "';
            paramJs.comments = '" . Yii::$app->urlManager->createAbsoluteUrl(['tr/job/comments', 'id' => $modelJob->id]) . "';
            paramJs.commentsave = '" . Yii::$app->urlManager->createAbsoluteUrl(['tr/job/commentsave']) . "';
            paramJs.revise_view = '" . Yii::$app->urlManager->createAbsoluteUrl(['/tr/job/revise_view', 'id' => $modelJob->id]) . "';

	    ",
    \yii\web\View::POS_HEAD
);
?>
<div class="modal fade stick-up " id="JobModal" role="dialog" aria-hidden="false" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg ">
        <div class="modal-content no-border">
            <div class="modal-header bg-success text-white">
            </div>
            <div id="JobModalBody" class="modal-body padding-40">
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function job_approve() {
        showFullLoading();
        if (confirm("Are you sure want to approve this Job ?")) {
            approval(paramJs.job_approve);
        } else {
            hideFullLoading();
        }
        return false;
    }

    function job_reject() {
        showFullLoading();
        if (confirm("Are you sure want to reject this Job ?")) {
            approval(paramJs.job_reject);
        } else {
            hideFullLoading();

        }
        return false;
    }

    function job_close() {
        showFullLoading();
        if (confirm("Are you sure want to close this Job ?")) {
            approval(paramJs.job_close);
        } else {
            hideFullLoading();
        }
        hideFullLoading();
        return false;
    }


    //REVISE
    function revise_view() {
        showFullLoading();
        $.ajax({
            url: paramJs.revise_view,
            method: "POST",
            dataType: 'html',
            success: function(data) {
                hideFullLoading();
                $('#JobModalBody').html(data);
            },
        });
        $('#JobModal').modal('show');
    }








    function approval(link) {
        $.ajax({
            url: link,
            type: 'POST',
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'JSON',
            success: function(data) {
                if (data.status == true) {
                    reload();
                } else {
                    alert(data.message);
                }
            },
            error: function(jqXHR, errMsg) {
                alert(errMsg);
            }
        });
    }

    function showNotification(type, msg) {
        $.ajax({
            url: paramJs.notifurl,
            data: {
                type: type,
                msg: msg
            },
            method: "POST",
            dataType: 'text',
            success: function(data) {
                $('#notification-wrapper').html(data);
                reload();
            },
            error: function(data) {
                $('#notification-wrapper').html(data);
            }
        });
    }

    $('.notification-footer').height($(window).height() / 2);

    function addComment() {
        $.ajax({
            url: paramJs.commentsave,
            type: 'POST',
            data: new FormData($('#comment-form')[0]),
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                reload_comment()
            },
            error: function(jqXHR, errMsg) {
                alert(errMsg);
            }
        });
        return false;
    }

    function reload() {
        location.reload();
        /*
        $.pjax.defaults.timeout = false;
        $.pjax.reload({
            container: '#job-pjax'
        });
        */
    }

    function reload_comment() {
        $(".redactor-editor").html("");
        $("#fnpcrreqcomments-comment").val("");
        showFullLoading();
        $.ajax({
            async: true,
            url: paramJs.comments,
            data: $('#comment-form').serialize(),
            method: "POST",
            contentType: "application/json",
            success: function(data) {
                hideFullLoading();
                $("#comments").html(data);
            },
            error: function(data) {
                $("#comments").html(data);
            }
        });
    }
</script>
