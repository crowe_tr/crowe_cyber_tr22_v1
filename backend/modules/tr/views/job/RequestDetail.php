<?php

use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\MaskedInput;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;
use common\models\hr\Employee;

$form = ActiveForm::begin([
  'id' => 'budget-form',
  'enableClientValidation' => true,
  'validateOnSubmit' => true,
  'validateOnChange' => true,
  'validateOnType' => true,
  'fieldConfig' => [
    'template' => '{label}{input}',
    'options' => [
      'class' => 'form-group form-group-default',
    ],
  ],
  'errorSummaryCssClass' => 'alert alert-danger'
]);
echo $form->errorSummary($modelBudget);
?>
<div class="alert alert-danger" id="error" style="display:none">
</div>
<div class="form-group-attached">
  <div class="row">
    <div class="col-md-6 col-6">
      <?= $form->field($modelBudget, 'job_id', ['options' => ['class' => '']])->hiddenInput()->label(false); ?>
      <?php
      echo $form->field($modelBudget, 'Entity', ['options' => ['class' => ' form-group form-group-default form-group-default-select2 client-select']])->widget(
        Select2::classname(),
        [
          'options' => ['placeholder' => 'Select...'],
          'data' => $data['Entity'],
          'pluginOptions' => [
            'allowClear' => true,
            'placeholder' => Yii::t('backend', 'Select..'),
          ],
          'options' => ['id' => 'Entity', 'placeholder' => 'Select ...'],
        ]
      );
      ?>
    </div>
    <div class="col-md-6 col-6">
      <?php
      echo $form->field($modelBudget, 'Division', ['options' => ['class' => ' form-group form-group-default form-group-default-select2 client-select']])->widget(
        Select2::classname(),
        [
          'options' => ['placeholder' => 'Select...'],
          'data' => $data['Division'],
          'pluginOptions' => [
            'allowClear' => true,
            'placeholder' => Yii::t('backend', 'Select..'),
          ],
          'options' => ['id' => 'Division', 'placeholder' => 'Select ...'],

        ]
      );
      ?>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <?php
      echo $form->field($modelBudget, 'Grup', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(DepDrop::classname(), [
        //'data' => $data['VempGroup'],
        //'options' => ['placeholder' => 'Select ...'],
        'type' => DepDrop::TYPE_SELECT2,
        'pluginOptions' => [
          'allowClear' => false,
          'minimumInputLength' => 2,
          'depends' => ['Entity', 'Division'],
          'url' => Url::to(['/hr/helper/load_group']),
          'loadingText' => 'Loading ...',
        ],
        'options' => ['id' => 'Grup'],
      ]);
      ?>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <?php
      $Employee = '';
      if (!$modelBudget->isNewrecord || !empty($modelBudget->employee_id)) {
        $dataEmployee = Employee::findOne($modelBudget->employee_id);
        $Employee = empty($dataEmployee->full_name) ? $modelBudget->employee_id : $dataEmployee->user_id . ' - ' . $dataEmployee->full_name;
      }
      echo $form->field($modelBudget, 'employee_id', ['options' => ['onchange' => 'getLevelId()', 'class' => ' form-group form-group-default form-group-default-select2']])
        ->widget(Select2::classname(), [
          'initValueText' => $Employee,
          'options' => ['placeholder' => 'Search Employee ... '],
          'pluginOptions' => [
            'tags' => false,
            'minimumInputLength' => 2,
            'ajax' => [
              'url' => Url::to(['helper/srcemployeeopt']),
              'dataType' => 'json',
              'data' => new JsExpression('function(params) { return {q:params.term, entity:$("#Entity").val() , division:$("#Division").val() , grup:$("#Grup").val()}; }'),
              'loadingText' => 'Loading ...',
            ],
          ],
        ]);
      ?>
    </div>
  </div>
</div>



<div class="form-group-attached">
  <div class="row">
    <div class="col-12 TimeAvailable EA" id="EA">

    </div>
  </div>
</div>
<p class="m-t-10 work-time"><b>Estimated Work Time : </b></p>
<div class="form-group-attached work-time">
  <div class="row">
    <div class="col-md-12">
      <?= $form->field($modelBudget, 'level_name', ['options' => ['class' => '']])->hiddenInput(['id' => 'getLevel'])->label(false); ?>
      <?= $form->field($modelBudget, 'full_name', ['options' => ['class' => '']])->hiddenInput(['id' => 'getFullName'])->label(false); ?>
      <?php
      echo $form->field($modelBudget, 'total_wh', ['options' => ['onchange' => 'totalWH()']])->textInput(['id' => 'totalWH']);
      ?>
    </div>
    <div class="col-md-2">
      <?php
      echo $form->field($modelBudget, 'planning')->textInput(['id' => 'planning']);
      ?>
    </div>
    <div class="col-md-3">
      <?php
      echo $form->field($modelBudget, 'field_work')->textInput(['id' => 'field_work']);
      ?>
    </div>
    <div class="col-md-2">
      <?php
      echo $form->field($modelBudget, 'reporting')->textInput(['id' => 'reporting']);
      ?>
    </div>
    <div class="col-md-2">
      <?php
      echo $form->field($modelBudget, 'wrap_up')->textInput(['id' => 'wrap_up']);
      ?>
    </div>
    <div class="col-md-3">
      <?=
      $form->field($modelBudget, 'over_time', ['options' => [/*'onkeyup' => 'counttotal()'*/]])->widget(MaskedInput::className(), [
        'clientOptions' => [
          'alias' => 'decimal',
        ],
        'value' => 0
      ]);
      ?>
    </div>
  </div>
</div>

<div class="row m-t-20">
  <div class="col-md-12 text-right">
    <hr />
    <button type="submit" name="add_new_item" class="btn btn-success btn-cons p-t-10 p-b-10" style="font-size: 12px">
      ADD
    </button>

    <button type="button" class="btn btn-info p-t-10 p-b-10" style="font-size: 12px" data-dismiss="modal">CANCEL</button>
  </div>
</div>

<?php ActiveForm::end(); ?>

<script type="text/javascript">
  var paramJs = (paramJs || {});
  paramJs.urlGetLevelID = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/job/budget_getlevelid']); ?>';
  paramJs.urlTotalWH = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/job/budget_totalwh']); ?>';

  $('#budget-form').on('beforeSubmit', function() {
    var url = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/job/budgetsave', 'id' => $id]); ?>';
    var form_data = new FormData($('#budget-form')[0]);
    return HelperSaveAjax(url, form_data);
  });

  function getLevelId() {
    var link = (link || paramJs.urlGetLevelID);
    showFullLoading();
    $.ajax({
      url: link,
      data: $('#budget-form').serialize(),
      method: "POST",
      dataType: 'html',
      success: function(data) {
        var d = JSON.parse(data);
        hideFullLoading();
        $('.work-time').show();
        $('#getLevel').val(d.levelName);
        $('#getFullName').val(d.fullName);
        $('#totalWH').val(0);
        $('#planning').val(0);
        $('#field_work').val(0);
        $('#reporting').val(0);
        $('#wrap_up').val(0);
        $('#EA').html(d.eA);

      },
    });
  }

  function totalWH() {
    var link = (link || paramJs.urlTotalWH);
    $.ajax({
      url: link,
      data: $('#budget-form').serialize(),
      method: "POST",
      dataType: 'html',
      success: function(data) {
        var d = JSON.parse(data);
        $('#planning').val(d.planning);
        $('#field_work').val(d.field_work);
        $('#wrap_up').val(d.wrap_up);
        $('#reporting').val(d.reporting);
      },
    });
  }
</script>
