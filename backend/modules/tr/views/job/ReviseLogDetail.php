<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use kartik\widgets\ActiveForm;
use common\components\CommonHelper;

$user = CommonHelper::getUserIndentity();
?>

<div class="padding-20">
	<div id="error" class="alert alert-danger" style="display:none;"></div>

	<?php
	echo '<table class="table table-bordered">';
	echo '<thead>'
		. '<tr>'
		. '<th class="bg-primary"></th>'
		. '<th class="bg-primary col-md-6" colspan="2">DESCRIPTION CHANGES</th>'
		. '<th class="bg-primary col-md-3">ORIGINAL</th>'
		. '<th class="bg-primary col-md-3">REVISION</th>'
		. '</tr>'
		. '</thead>';
	if (!empty($header)) {

		$header_desc = json_decode('[' . $header->audit_description . ']');
		// $header_desc = json_encode($header->audit_description);
		// $header_desc = json_decode($header_desc);
		// var_dump($header_desc);
		// var_dump(array($header->audit_description));
		// var_dump(json_decode(json_encode($header->audit_description)));
		if (!empty($header_desc)) {
			echo "<tr>
							<td class='bg-info text-white'></td>
							<td class='bg-info text-white' colspan='4'>JOB</td>
						</tr>";

			$no = 0;
			foreach ($header_desc as $val => $h) {
				$no++;
				echo "<tr>";
				echo "<td>" . $no . "</td>";
				echo "<td colspan='2'>" . $h->description . "</td>";

				if (
					$h->description == 'FEE'
					or $h->description == 'MEAL ALLOWANCE'
					or $h->description == 'OPE ALLOWANCE'
					or $h->description == 'TAXI ALLOWANCE'
					or $h->description == 'TIME CHARGES'
					or $h->description == 'ADMINISTRATIVE CHARGE'
					or $h->description == 'OTHER EXPENSE'
				) {
					echo "<td>" . number_format($h->before_value) . "</td>";
					echo "<td>" . number_format($h->after_value) . "</td>";
				} else {
					echo "<td>" . $h->before_value . "</td>";
					echo "<td>" . $h->after_value . "</td>";
				}
				echo "</tr>";
			}
		}
	}
	if (!empty($details)) {
		echo "<tr>
						<td class='bg-info text-white'></td>
						<td class='bg-info text-white' colspan='4'>JOB BUDGETING</td>
					</tr>";

		$no = 0;
		foreach ($details as $detail) {
			$desc = json_decode($detail->audit_description);
			if (!empty($desc)) {
				$no++;
				echo "<tr>";
				echo "<td>" . $no . "</td>";
				echo "<td colspan='2'>" . $desc->description . "</td>";
				echo "<td>" . $desc->before_value . "</td>";
				echo "<td>" . $desc->after_value . "</td>";
				echo "</tr>";
			}
		}
	}
	echo '</tbody>';
	echo '</table>';
	?>
</div>
