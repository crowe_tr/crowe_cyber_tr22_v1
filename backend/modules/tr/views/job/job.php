
<?php

use yii\helpers\Html;

$this->title = 'JOB LIST';
$this->params['breadcrumbs'][] = $this->title;

$this->params['grid']['emptytext'] = '<div class="text-center" style="padding: 2em 0">'
    . '<i class="ion-android-alert fa-5x text-warning"></i>'
    . '<br>'
    . Yii::t('backend', 'You do not have any Pending orders within your Filters. .')
    . '<br>'
    . Yii::t('backend', 'To create new Job click <b><i class="icon-plus"></i> NEW JOB</b> on the right top of corner')
    . '</div>';
?>

<div class="row">
    <div class="col-12">
        <div class="scroll">
            <div class="card card-transparent mt-0 mb-0" style="width: max-content;">
                <div class="card-body pt-0 pb-0 pr-md-0 pl-md-0">
                    <?= $OrderButtons; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
echo $this->render('jobIndex', ['searchModel' => $searchModel, 'dataProvider' => $dataProvider]); 
?>
