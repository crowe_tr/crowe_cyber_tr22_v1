<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use common\models\tr\Job;
use common\models\hr\Employee;
use common\models\cl\Client;

$this->title = 'Job List';
$column = [
  [
    'class' => 'kartik\grid\SerialColumn',
    'header' => 'No',
    'mergeHeader' => false,
    'headerOptions' => ['class' => 'bg-success b-r'],
    'contentOptions' => ['class' => 'text-right'],
    'width' => '36px',
    'filterOptions' => ['class' => 'b-b b-grey'],
  ],
  [
    'attribute' => 'job_code',
    'label' => 'Job Code',
    'headerOptions' => ['class' => 'col-sm-2 bg-success'],
    'filterOptions' => ['class' => 'b-b b-grey'],
    'contentOptions' => ['class' => 'kv-align-middle'],
    'width' => '10%',
  ],
  [
    'attribute' => 'manager_id',
    'headerOptions' => ['class' => 'col-sm-4 bg-success'],
    'filterOptions' => ['class' => 'b-b b-grey'],
    'contentOptions' => ['class' => 'kv-align-middle'],
    'width' => '15%',
    'filterType' => GridView::FILTER_SELECT2,
    'filterWidgetOptions' => [
      'pluginOptions' => ['allowClear' => true],
    ],
    'filterInputOptions' => ['placeholder' => ''],
    'format' => 'raw',
    'filter' => ArrayHelper::map(Employee::find()->distinct()->all(), 'user_id', 'full_name'),
    'value' => 'manager.full_name'
  ],
  [
    'attribute' => 'client_id',
    'headerOptions' => ['class' => 'col-sm-4 bg-success'],
    'filterOptions' => ['class' => 'b-b b-grey'],
    'contentOptions' => ['class' => 'kv-align-middle'],
    'width' => '20%',
    'filterType' => GridView::FILTER_SELECT2,
    'filterWidgetOptions' => [
      'pluginOptions' => ['allowClear' => true],
    ],
    'filterInputOptions' => ['placeholder' => ''],
    'format' => 'raw',
    'filter' => ArrayHelper::map(Client::find()->distinct()->all(), 'id', 'client_name'),
    'value' => 'client.client_name'
  ],

  [
    'attribute' => 'description',
    'label' => 'Description ',
    'headerOptions' => ['class' => 'col-sm-9 bg-success'],
    'filterOptions' => ['class' => 'b-b b-grey'],
    'contentOptions' => ['class' => 'kv-align-middle'],
  ],
  [
    'attribute' => 'job_status',
    'label' => 'Status ',
    'headerOptions' => ['class' => 'bg-success'],
    'filterOptions' => ['class' => 'b-b b-grey'],
    'contentOptions' => ['class' => 'kv-align-middle'],
    'value' => function ($data) {
      return Job::status_label($data->job_status);
    }
  ],

  [
    'header' => '<i class="fa fa-cogs"></i>',
    'headerOptions' => ['class' => 'bg-success text-center'],
    'filterOptions' => ['class' => 'b-b b-grey'],
    'contentOptions' => ['class' => 'no-padding'],
    'format' => 'raw',
    'value' => function ($data) {
      $btn = "";
      $btn_label = "";

      $btn_label = Html::a('<i class="fa fa-ellipsis-h"></i>', false, ['class' => 'text-info', 'data-toggle' => 'tooltip', 'data-original-title' => 'Up here!']);
      if ($data->job_status == 0) {
        $btn .= Html::a(
          'UPDATE <i class="fa fa-edit"></i>',
          false,
          [
            'onclick' => "FormUpdate('" . Yii::$app->urlManager->createAbsoluteUrl(['/tr/jobcreate/n', 'id' => $data->id]) . "');",
            'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
          ]
        );
        $btn .= html::a(
          '<i class="fa fa-trash"></i> DELETE',
          ['delete', 'id' => $data->id],
          [
            'class' => 'btn btn-danger text-white padding-5 p-l-10 p-r-10',
            'data-method' => 'post',
            'data-pjax' => 1,
            'data-confirm' => 'ARE YOU SURE TO DELETE THIS JOB?',
          ]
        );
      }else{
        $btn .= Html::a(
          'UPDATE JOB CODE <i class="fa fa-edit"></i>',
          false,
          [
            'onclick' => "FormUpdate('" . Yii::$app->urlManager->createAbsoluteUrl(['/tr/jobcreate/n_code', 'id' => $data->id]) . "');",
            'class' => 'btn btn-info text-white padding-5 p-l-10 p-r-10',
          ]
        );

      }

      $res = '<div class="tooltip-action">
              <div class="trigger">
                ' . $btn_label . '
              </div>
              <div class="action-mask">
                <div class="action">
                  ' . $btn . '
                </div>
              </div>
            </div>';

      return $res;
    },
  ],

];
?>


<?php \yii\widgets\Pjax::begin(['id' => 'crud-pjax', 'enablePushState' => false]);
?>
<div class="modal fade stick-up " id="crud-modal" role="dialog" aria-hidden="false" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg ">
    <div class="modal-content no-border">
      <div class="modal-header bg-success text-white">
      </div>
      <div id="detail" class="modal-body padding-20">
      </div>
    </div>
  </div>
</div>
<div class="row mb-3">
  <div class="col-auto">
    <h3><?= $this->title; ?></h3>
  </div>
  <div class="col text-right">
    <?= Html::a('<i class="pg-plus"></i> CREATE NEW', false, ['onclick' => 'FormCreate()', 'class' => 'btn btn-warning text-white']); ?>
  </div>
</div>
<div class="scroll">
  <?php
  echo GridView::widget(
    [
      'dataProvider' => $dataProvider,
      'filterModel' => $searchModel,
      'columns' => $column,
      'layout' => '
              <div class="card card-default">
                <div class="row ">
                  <div class="col-md-12">
                    {items}
                  </div>
                </div>
                <div class="row padding-10">
                  <div class="col-md-4">{summary}</div>
                  <div class="col-md-8">{pager}</div>
                </div>
              </div>
            ',
      'emptyText' => '
              <div class="text-center" style="padding: 2em 0">
                <i class="fa fa-exclamation-circle fa-5x text-warning"></i>
                <br>
                <br>
                You do not have any data within your Filters.
                <br>
                To create a new data, click <b><i class="icon-plus-circle"></i> ADD NEW</b> on the right top of corner
              </div>',
      'responsiveWrap' => false,
      'resizableColumns' => true,
      'bordered' => false,
      'striped' => false,
      'condensed' => false,
      'responsive' => false,
      'hover' => true,
      'persistResize' => false,
    ]
  );
  ?>
</div>

<?php \yii\widgets\Pjax::end(); ?>

<script type="text/javascript">
  var paramJs = (paramJs || {});
  paramJs.urlFormShowModal = '<?= Yii::$app->urlManager->createAbsoluteUrl(['/tr/jobcreate/n']); ?>';

  function FormShowModal(link = "") {
    var link = (link || paramJs.urlFormShowModal);
    $.ajax({
      url: link,
      data: $('#crud-form').serialize(),
      method: "POST",
      dataType: 'html',
      success: function(data) {
        $('#detail').html(data);
      },
    });
  }

  function FormCreate() {
    FormShowModal();
    $('#crud-modal').modal('show');

  }

  function FormUpdate(link) {
    FormShowModal(link);
    $('#crud-modal').modal('show');
  }

  window.closeModal = function() {
    reload();
  };

  function reload() {
    $.pjax.defaults.timeout = false;
    $.pjax.reload({
      container: '#crud-pjax'
    })
    $('#crud-form').modal('hide');
    $('#crud-form').data('modal', null);
    $('.modal-backdrop').modal('hide');
  }
</script>
