<table class="table table-bordered" border="1" style="width: 100%">
  <?php
  echo "<tr>";
  echo "<th class='bg-primary'>NO.</th>";
  foreach ($fields['selected'] as $f => $g) {
    echo "<th class='".$f." bg-primary'>" . $fields['data'][$f] . "</th>";
  }
  echo "</tr>";

  $arr_number = array( 'meal_value', 'ope_value', 'taxi_value' );
  $arr_dec = array( 'work_hour', 'over_time' );

  $i = 0;
  foreach ($data as $d) {
    $i++;
    $bg = ($d['calendar_sign'] == 1) ? 'background: #ffcccc' : '';

    echo "<tr>";
    echo "<td style='" . $bg . "'>" . $i . "</td>";
    foreach ($fields['selected'] as $f => $g) {
      if ( in_array($f, $arr_number)) {
        echo "<td class='".$f."' style='" . $bg . "' align='right'>" . number_format ( $d[$f], 0, '.', ',' ) . "</td>";
      } elseif ( in_array($f, $arr_dec)) {
        echo "<td class='".$f."' style='" . $bg . "' align='right'>" . number_format ( $d[$f], 1, '.', ',' ) . "</td>";
      } else {
        echo "<td class='".$f."' style='" . $bg . "'>" . $d[$f] . "</td>";
      }
    }
    echo "</tr>";
  }
  ?>
</table>
