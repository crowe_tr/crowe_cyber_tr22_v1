<?php

function nbsp($icount)
{
  $res_echo = '';
  for ($i = 0; $i < $icount; $i++) {
    $res_echo = $res_echo . "&nbsp;";
  }
  return $res_echo;
}

// type = 1 => YES/NO, 2=>INCLUDE OPE
function rpt_to_text($itype, $ival)
{
  $res_text = '';
  switch ($itype) {
    case '2':
      $res_text = '';
      if ($ival == '1') {
        $res_text = ' ( INCLUDE OPE )';
      }
      break;

    default:
      $res_text = ' NO';
      if ($ival == '1') {
        $res_text = ' YES';
      }
      break;
  }
  return $res_text;
}

function diff_amount($iest, $iact)
{
  $res_amount = intval($iest) - intval($iact);
  return number_format($res_amount, 0, '.', ',');
}


$data_header = Yii::$app->db->createCommand("call rpt_job_by_employee( null, null, null,'" . $model->Custom['JobCode'] . "' );")->queryOne();
// $data_header = Yii::$app->db->createCommand("call rpt_recovery_rate_detail_header( '" . $model->Custom['JobCode'] . "' );")->queryAll();
// var_dump($data_header);
if (empty($data_header)) {
  echo "no results found.";
} else {
  $data_header_progress = Yii::$app->db->createCommand("call rpt_val('job-code-progress', '".$model->Custom['JobCode']."');")->queryOne();
  echo "<span><b>RECOVERY RATE JOB REPORT - DETAIL</b></span>";
  $res_val = $data_header;
  // var_dump($res_val);
  // die();
  echo "
    <table class='table table-bordered' border='1' style='width: 100%'>
      <tr>
        <th width='10%' class='bg-primary'>JOB</th>
        <td width='2px'>:</td>
        <td width='50%'>".$res_val['job_client_description']."</td>
        <th width='10%' class='bg-primary'>STATUS</th>
        <td width='2px'>:</td>
        <td>".$res_val['job_status']."</td>
      </tr>
      <tr>
        <th  class='bg-primary'>JOB PERIOD</th>
        <td width='2px'>:</td>
        <td>". strtoupper(date_format(date_create($res_val['start_date']), 'M d, Y'))
            . " <b>TO</b> " . strtoupper(date_format(date_create($res_val['end_date']), 'M d, Y')) .
        "</td>
        <th  class='bg-primary'>FEE</th>
        <td width='2px'>:</td>
        <td>".number_format($res_val['job_fee'], 0, '.', ',').rpt_to_text('2', $res_val['job_include_ope'])."</td>

      </tr>
      <tr>
        <th  class='bg-primary'>ENTITY / DIVISION</th>
        <td width='2px'>:</td>
        <td>".$res_val['entity_name'] . " - " . $res_val['div_name']."</td>
        <th  class='bg-primary'>AREA</th>
        <td width='2px'>:</td>
        <td>".$res_val['job_area']."</td>
      </tr>

    </table>
    <table class='table table-bordered' border='1' style='width: 100%'>
      <tr>

        <th width='10%' class='bg-primary'>MEALS</th>
        <td width='2px'>:</td>
        <td>".$res_val['is_meal']."</td>

        <th width='10%' class='bg-primary'>TRANSPORT</th>
        <td width='2px'>:</td>
        <td>".$res_val['is_taxi']."</td>

        <th  width='10%' class='bg-primary'>OUT OF OFFICE</th>
        <td width='2px'>:</td>
        <td>".$res_val['is_ope']."</td>

      </tr>
      <tr>
        <th  class='bg-primary'>PARTNER</th>
        <td width='2px'>:</td>
        <td>".$res_val['partner_full_name']."</td>
        <th  class='bg-primary'>MANAGER</th>
        <td width='2px'>:</td>
        <td>".$res_val['manager_full_name']."</td>
        <th  class='bg-primary'>SUPERVISOR</th>
        <td width='2px'>:</td>
        <td>".$res_val['supervisor_full_name']."</td>
      </tr>
    </table>
  ";

?>


  <table class="table table-bordered" border="1" style="width: 100%">
    <?php
    echo "<tr>";
    echo "<th class='bg-primary' colspan=4>EMPLOYEE INFORMATION</th>";
    echo "<th class='bg-primary' colspan=6>BUDGETING</th>";
    echo "<th class='bg-primary' colspan=6>ACTUAL</th>";
    echo "<th class='bg-primary' colspan=2>DIFFERENCE</th>";
    echo "</tr>";
    echo "<tr>";
    echo "<th class='bg-primary'>NO.</th>";
    foreach ($fields['selected'] as $f => $g) {
      echo "<th class='" . $f . " bg-primary'>" . $fields['data'][$f] . "</th>";
    }
    echo "</tr>";


    $arr_number = array(
      'job_planning', 'job_fieldwork', 'job_reporting', 'job_wrapup', 'job_overtime', 'job_total',
      'job_planning_act', 'job_fieldwork_act', 'job_reporting_act', 'job_wrapup_act', 'job_overtime_act', 'job_total_act',
      'job_total_diff', 'job_overtime_diff'
    );

    $arr_ctr = array('employee_id');

    $i = 0;
    foreach ($data as $d) {
      $i++;
      $bg = (false) ? 'background: #ffcccc' : '';
      echo "<tr>";
      echo "<td style='" . $bg . "'>" . $i . "</td>";
      foreach ($fields['selected'] as $f => $g) {
        if (in_array($f, $arr_number)) {
          echo "<td class='" . $f . "' style='" . $bg . "' align='right'>" . number_format($d[$f], 0, '.', ',') . "</td>";
        } elseif (in_array($f, $arr_ctr)) {
          echo "<td class='" . $f . "' style='" . $bg . "' align='center'>" . $d[$f] . "</td>";
        } else {
          echo "<td class='" . $f . "' style='" . $bg . "'>" . $d[$f] . "</td>";
        }
      }
      echo "</tr>";
    }
    ?>
  </table>

  <!-- <br> -->
  <p><b>JOB REALITATION</b></p>
  <table class="table table-bordered" border="1" style="width: 50%">
    <?php
    echo "<tr>";
    echo "<th class='bg-primary'>DESCRIPTION</th>";
    echo "<th class='bg-primary'>ESTIMATED</th>";
    echo "<th class='bg-primary'>ACTUAL</th>";
    echo "<th class='bg-primary'>DIFFERENCE</th>";
    echo "</tr>";
    // var_dump($res_val);
    // die();
    echo "<tr><td>TIME CHARGES</td><td align='right'>"
      . number_format($res_val['time_charges'], 0, '.', ',') . "</td><td align='right'>"
      . number_format($res_val['time_charges_actual'], 0, '.', ',') . "</td><td align='right'>"
      . diff_amount($res_val['time_charges'], $res_val['time_charges_actual']) . "</td></tr>";

    echo "<tr><td>MEALS ALLOWED</td><td align='right'>"
      . number_format($res_val['meal_allowance'], 0, '.', ',') . "</td><td align='right'>"
      . number_format($res_val['meal_allowance_actual'], 0, '.', ',') . "</td><td align='right'>"
      . diff_amount($res_val['meal_allowance'], $res_val['meal_allowance_actual']) . "</td></tr>";

    echo "<tr><td>TAXI USED ALLOWED</td><td align='right'>"
      . number_format($res_val['taxi_allowance'], 0, '.', ',') . "</td><td align='right'>"
      . number_format($res_val['taxi_allowance_actual'], 0, '.', ',') . "</td><td align='right'>"
      . diff_amount($res_val['taxi_allowance'], $res_val['taxi_allowance_actual']) . "</td></tr>";

    echo "<tr><td>OUT OF OFFICE ALLOWANCE</td><td align='right'>"
      . number_format($res_val['ope_allowance'], 0, '.', ',') . "</td><td align='right'>"
      . number_format($res_val['ope_allowance_actual'], 0, '.', ',') . "</td><td align='right'>"
      . diff_amount($res_val['ope_allowance'], $res_val['ope_allowance_actual']) . "</td></tr>";

    echo "<tr><td>ADMINISTRATIVE CHARGE</td><td align='right'>"
      . number_format($res_val['administrative_charge'], 0, '.', ',') . "</td><td align='right'>"
      . number_format($res_val['administrative_charge_actual'], 0, '.', ',') . "</td><td align='right'>"
      . diff_amount($res_val['administrative_charge'], $res_val['administrative_charge_actual']) . "</td></tr>";

    echo "<tr><td>OTHER EXPENSE ALLOWANCE</td><td align='right'>"
      . number_format($res_val['other_expense_allowance'], 0, '.', ',') . "</td><td align='right'>"
      . number_format($res_val['other_expense_allowance_actual'], 0, '.', ',') . "</td><td align='right'>"
      . diff_amount($res_val['other_expense_allowance'], $res_val['other_expense_allowance_actual']) . "</td></tr>";

    echo "<tr><td>PROGRESS TIME</td><td align='right'>"
    . number_format($data_header_progress['job_total_wh'], 0, '.', ',') . "</td><td align='right'>"
    . number_format($data_header_progress['job_total_wh_actual'], 0, '.', ',') . "</td><td align='right'>"
    . $data_header_progress['progress_time'] . "</td></tr>";

    echo "<tr><td>RECOVERY RATE ( % )</td><td align='right'>"
    . $res_val['percentage'] . "</td><td align='right'>"
    . $res_val['percentage_actual'] . "</td><td align='right'></td></tr>";
    ?>
  </table>

<?php
}
?>
