<div class="card card-default padding-20">
    <h2><b>Reports</b></h2>
    <hr />
    <ul>
        <li>
            Recovery Rate
            <ul>
                <li><a href="/tr/report_manager/recovery_rate_summary">Report Summary</a></li>
                <li><a href="/tr/report_manager/recovery_rate_detail">Report Detail</a></li>
            </ul>
        </li>
    </ul>
</div>
