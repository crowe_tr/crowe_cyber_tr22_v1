<table class="table table-bordered" border="1" style="width: 100%">
  <?php
  echo "<tr>";
  echo "<th class='bg-primary'>NO.</th>";
  foreach ($fields['selected'] as $f => $g) {
    echo "<th class='".$f." bg-primary'>" . $fields['data'][$f] . "</th>";
  }
  echo "</tr>";


  // $arr_number = array( 'job_fee',  'total_charges' );
  //
  // $arr_pct = array( 'rate_time_charges', 'rate_total_charges', 'progress_time' );

  $arr_number = array(  'job_fee',  'tc_total', 'tc_other', 'total_charges', 
                        'time_charges', 'meal_allowance', 'ope_allowance', 
                        'taxi_allowance', 'administrative_charge', 'other_expense_allowance', 
                        'tc_partner', 'tc_director', 'tc_manager', 'tc_supervisor', 'tc_senior', 'tc_junior', 'tc_others');

  $arr_pct = array( 'rate_time_charges', 'rate_total_charges', 'progress_time' );

  $i = 0;
  foreach ($data as $d) {
    $i++;
    $bg = (false) ? 'background: #ffcccc' : '';
    echo "<tr>";
    echo "<td style='" . $bg . "'>" . $i . "</td>";
    foreach ($fields['selected'] as $f => $g) {
      if ( in_array($f, $arr_number)) {
        echo "<td class='".$f."' style='" . $bg . "' align='right'>" . number_format ( $d[$f], 0, '.', ',' ) . "</td>";
      } elseif ( in_array($f, $arr_pct) ) {
        echo "<td class='".$f."' style='" . $bg . "' align='right'>" . $d[$f] . "</td>";
      } else {
        echo "<td class='".$f."' style='" . $bg . "'>" . $d[$f] . "</td>";
      }
    }
    echo "</tr>";
  }
  ?>
</table>
