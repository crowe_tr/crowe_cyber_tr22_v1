<table class="table table-bordered" border="1" style="width: 100%">
  <?php
  // include 'form.php';

  $arr_number = array();
  $arr_time = array('total_wh','planning','field_work','reporting','wrap_up','wh_project',
    'wh_back_office','wh_preparation','wh_self_development',
    'wh_leave_wo_doctor','wh_leave_doctor','wh_leave_annual','wh_leave_other','wh_total_leave',
    'wh_other');
  $arr_pct = array();
  $arr_date = array('date_from','date_to');

  $sel_person = array('employee_name','employee_level','partner_initial', 'group_initial');



  
  // var_dump(isset($_POST('value')));
  // var_dump($fields['data']);
//   $value = $_POST['value'];
//   if (isset($_POST['value'])) {
//     // Get the value from the POST request
//     $value = $_POST['value'];

//     // You can process the value here, like saving it to a database
//     // For demonstration, we'll just echo it back
//     echo "Value received: " . htmlspecialchars($value);
// } else {
//     echo "No value received.";
// }
  // foreach ($fields['selected'] as $f => $g) {    
  //   if ( in_array($f, $sel_person) ) {
  //     $xperson++;
  //   }
  // }
  $xperson = 5;
  $xperiod = 3;
  $xproject = 5;
  $xleave = 5;
  $xsingle = array('wh_back_office','wh_preparation','wh_self_development','wh_other');
  echo "<tr>";
  echo "<th rowspan='2' class='bg-primary' style='text-align: center;vertical-align: middle;'>NO.</th>";  
  echo "<th colspan='".$xperson."'  class='bg-primary' style='text-align: center;'>EMPLOYEE DATA</th>";
  echo "<th colspan='".$xperiod."'  class='bg-primary' style='text-align: center;'>PERIOD</th>";
  echo "<th colspan='".$xproject."'  class='bg-primary' style='text-align: center;'>PROJECT</th>";
  echo "<th rowspan='2' class='bg-primary' style='text-align: center;vertical-align: middle;'>BACK OFFICE</th>";  
  echo "<th rowspan='2' class='bg-primary' style='text-align: center;vertical-align: middle;'>SELF DEVELOPMENT</th>";  
  echo "<th rowspan='2' class='bg-primary' style='text-align: center;vertical-align: middle;'>PREPARATION</th>";  
  echo "<th colspan='".$xleave."'  class='bg-primary' style='text-align: center;'>LEAVE</th>";
  echo "<th rowspan='2' class='bg-primary' style='text-align: center;vertical-align: middle;'>OTHER</th>";  

  echo "<tr>";
  foreach ($fields['selected'] as $f => $g) {
    if (!in_array($f, $xsingle)) {
      echo "<th class='".$f." bg-primary' style='text-align: center;'>" . $fields['data'][$f] . "</th>";
    }
  }
  echo "</tr>";



  $i = 0;
  foreach ($data as $d) {
    $i++;
    $bg = (false) ? 'background: #ffcccc' : '';

    echo "<tr>";
    echo "<td style='" . $bg . "'>" . $i . "</td>";
    foreach ($fields['selected'] as $f => $g) {
      if ( in_array($f, $arr_number)) {
        echo "<td class='".$f."' style='" . $bg . "' align='right'>" . number_format ( $d[$f], 0, '.', ',' ) . "</td>";
      } elseif ( in_array($f, $arr_pct) ) {
        echo "<td class='".$f."' style='" . $bg . "' align='right'>" . $d[$f] . "</td>";
      } elseif ( in_array($f, $arr_time) ) {
        echo "<td class='".$f."' style='" . $bg . "' align='right'>" . number_format ( $d[$f], 1, '.', ',' ) . "</td>";
      } elseif ( in_array($f, $arr_date) ) {
        $data_date = new DateTime($d[$f]);
        echo "<td class='".$f."' style='" . $bg . "' align='center'>" . $data_date->format('d-M-Y') . "</td>";
      } else {
          echo "<td class='".$f."' style='" . $bg . "'>" . $d[$f] . "</td>";
      }
    }
    echo "</tr>";
  }
  ?>
</table>
