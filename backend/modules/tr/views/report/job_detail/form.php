<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\daterange\DateRangePicker;
use common\components\CommonHelper; //helper dipanggil disni :O ASIIIKK
use common\models\cl\search\Client;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use kartik\widgets\DepDrop;

use common\models\cm\CmMaster;
use common\models\cm\Entity;
use common\models\cm\Dept;
use common\models\cm\Division;
use common\models\cm\Level;
use common\models\tr\Job;
use common\models\hr\Employee;
use common\models\cm\Taxi as cmTaxi;

$this->title = $options['name'];
?>
<?php
$form = ActiveForm::begin([
  'enableClientValidation' => true,
  'validateOnSubmit' => true,
  'validateOnChange' => true,
  'validateOnType' => true,
  'options' => [
    'enctype' => 'multipart/form-data',
  ],
  'fieldConfig' => [
    'template' => '{label}{input}',
    'options' => [
      'class' => 'form-group form-group-default',
    ],
  ],
]);
?>

<div class="row" style="overflow:scroll; min-height: 132px;">
  <div class="col-8 bg-white padding-10 p-l-30 p-r-30">
    <h2 style="font-size: 18px !important" class="no-margin no-padding"><?= $this->title ?></h2>
  </div>
  <div class="col-4 bg-white padding-10 p-t-20 p-l-30 p-r-30 text-right">
    <?= $form->field($model, 'Custom[fields][]', ['options' => ['class' => '']])->dropDownList(
      $fields['data'],
      [
        'class' => 'ant-multiselect',
        'multiple' => true,
        'options' => $fields['options']
      ]
    )->label(false);
    ?>
  </div>
  <div class="col-12 bg-white padding-10 p-l-30 p-r-30">

    <div class="form-group-attached">
      <div class="row">
        <div class="col-md-2" >
          <?php
          $model->Date = empty($model->Date) ? date('Y-01-01') . ' TO ' . date('Y-m-d') : $model->Date;
          echo $form->field($model, 'Date')->widget(DateRangePicker::classname(), [
            'pluginOptions' => [
              'locale' => [
                'format' => 'Y-MM-DD',
                'separator' => ' TO ',
              ],
            ],

          ])->label('Job Start Date');;
          ?>
        </div>

        <div class="col-md-3">
          <?php
          echo $form->field($model, 'Custom[Entity]', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
            Select2::classname(),
            [
              'options' => ['id' => 'Entity', 'placeholder' => 'SELECT ALL'],
              'data' => ArrayHelper::map(Entity::find()->asArray()->all(), 'entity_name', 'entity_name'),
              'pluginOptions' => [
                'allowClear' => true,
                'placeholder' => Yii::t('backend', 'SELECT ALL'),
              ],
            ]
          )->label('Entity Name');
          ?>
        </div>

        <div class="col-md-3">
          <?php
          $parent = empty($model->Custom['parentID']) ? [] : Employee::find()->where(['user_id' => $model->Custom['parentID']])->one();
          echo $form->field($model, 'Custom[parentID]', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])
            ->widget(Select2::classname(), [
              'initValueText' => !empty($parent->user_id) ? $parent->user_id . ' - ' . $parent->full_name : "",
              'options' => ['placeholder' => 'Search Leader ...'],
              'pluginOptions' => [
                'tags' => false,
                'allowClear' => true,

                //'minimumInputLength' => 1,
                'ajax' => [
                  'url' => Url::to(['/hr/helper/srcinitial']),
                  'dataType' => 'json',
                  'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                ],
              ],
            ])->label('Team');
          ?>
        </div>

        <div class="col-md-4">
          <?php
          echo $form->field($model, 'Custom[job_status]', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])
            ->widget(Select2::classname(), [
              'options' => ['placeholder' => 'Search'],
              'pluginOptions' => [
                'allowClear' => true,
                'tags' => true,
                'multiple' => true,
                'minimumInputLength' => 2,
                'ajax' => [
                  'url' => Url::to(['/cm/helper/srcjob_status']),
                  'delay' => 250,
                  'dataType' => 'json',
                  'data' => new JsExpression('function(params) { return {q:params.term} }'),
                  'loadingText' => 'Loading ...',
                ],
              ],
            ])->label('JOB STATUS');
          ?>
        </div>
      </div>

      <div class ="row"> 
        <div class="col-md-10">
          <?php
          echo $form->field($model, 'Custom[EmployeeList]', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])
            ->widget(Select2::classname(), [
              'options' => ['placeholder' => 'SELECT ALL'],
              'pluginOptions' => [
                'allowClear' => true,
                'tags' => true,
                'multiple' => true,

                'minimumInputLength' => 2,
                'ajax' => [
                  'url' => Url::to(['/cm/helper/srcemployee']),
                  'delay' => 250,
                  'dataType' => 'json',
                  'data' => new JsExpression('function(params) { return {q:params.term} }'),
                  'loadingText' => 'Loading ...',
                ],
                // 'loadingText' => 'Loading ...',
                //'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                //'templateResult' => new JsExpression('formatSelect2ClientID'),
                //'templateSelection' => new JsExpression('formatSelect2ClientIDSelection'),
              ],
            ])->label('EMPLOYEE NAME');
          ?>
        </div>



        <div class="col-md-1">
          <?php
          $model->ReportFormat = 'view';
          echo $form->field($model, 'ReportFormat', ['options' => ['class' => ' form-group form-group-default form-group-default-select2']])->widget(
            Select2::classname(),
            [
              'options' => ['id' => 'ReportFormat', 'placeholder' => 'format'],
              'data' => ['view' => 'VIEW', 'xls' => 'XLS', 'pdf' => 'PDF'],
              'pluginOptions' => [
                'allowClear' => false,
                'placeholder' => 'format ..',
              ],
            ]
          );
          ?>
        </div>
        <div class="col-md-1">
          <?= Html::submitButton('SEARCH', ['class' => 'btn btn-success btn-block active padding-15 btn-lg']) ?>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row" style="height: 360px;overflow:scroll !important;  ">

  <div class="p-l-10 p-r-10" style="background: #dedede; min-height: 1020px; min-width: 2020px">
    <?= $data; ?>
  </div>
</div>
<?php ActiveForm::end(); ?>
<div id="response"></div>
<script>
  $(document).ready(function() {
    $('input[type="checkbox"]').click(function() {
      var inputValue = $(this).attr("value");
      $("." + inputValue).toggle();
      // const selectedText = document.querySelector('.multiselect-selected-text').innerHTML;
      // console.log($(this).attr("value") + " " + $(this).is(":checked"));
      // console.log($('input[type="checkbox"]'));
      
      // var value = $('input[type="checkbox"]:checked').map(function() {
      //   return $(this).val();
      // }).get();
      // console.log(value);
      // $.ajax({
      //               type: 'POST',
      //               url: './report.php', // URL of your PHP script
      //               data: { value: value },
      //               success: function(response) {
      //                   // Display the response from the PHP script
      //                   $('#response').html(response);
      //               },
      //               error: function() {
      //                   $('#response').html('Error occurred.');
      //               }
      //           });
      
      
      // if($(this).is(":checked")) {
      //   console.log(inputValue);
        
      // } else {
      //   console.log('unchecked');
        
      // }
      
    });
  });
</script>
