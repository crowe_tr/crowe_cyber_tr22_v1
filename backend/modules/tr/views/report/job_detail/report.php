<table class="table table-bordered" border="1" style="width: 100%">
  <?php
  // include 'form.php';

  $arr_number = array(  'job_fee');
  $arr_time = array('job_planning','job_fieldwork','job_reporting','job_wrapup',
    'job_total','job_overtime',
    'job_planning_act','job_fieldwork_act','job_reporting_act','job_wrapup_act',
    'job_total_act','job_overtime_act',
    'job_total_diff','job_overtime_diff');
  $arr_pct = array();
  $arr_date = array('start_date','end_date');

  $sel_person = array('employee_name','employee_level','partner_initial', 'group_initial');



  
  // var_dump(isset($_POST('value')));
  // var_dump($fields['data']);
//   $value = $_POST['value'];
//   if (isset($_POST['value'])) {
//     // Get the value from the POST request
//     $value = $_POST['value'];

//     // You can process the value here, like saving it to a database
//     // For demonstration, we'll just echo it back
//     echo "Value received: " . htmlspecialchars($value);
// } else {
//     echo "No value received.";
// }
  // foreach ($fields['selected'] as $f => $g) {    
  //   if ( in_array($f, $sel_person) ) {
  //     $xperson++;
  //   }
  // }
  $xperson = 4;
  $xjob = 6;
  $xbudget = 6;
  $xactual = 6;
  $xdiff = 2;
  echo "<tr>";
  echo "<th rowspan='2' class='bg-primary' style='text-align: center;vertical-align: middle;'>NO.</th>";  
  echo "<th colspan='".$xperson."'  class='bg-primary' style='text-align: center;'>EMPLOYEE DATA</th>";
  echo "<th colspan='".$xjob."'  class='bg-primary' style='text-align: center;'>JOB INFORMATION</th>";
  echo "<th colspan='".$xbudget."'  class='bg-primary' style='text-align: center;'>BUDGET</th>";
  echo "<th colspan='".$xactual."'  class='bg-primary' style='text-align: center;'>ACTUAL</th>";
  echo "<th colspan='".$xdiff."'  class='bg-primary' style='text-align: center;'>DIFFERENCE</th>";
  echo "<th rowspan='2' class='bg-primary' style='text-align: center;vertical-align: middle;'>STATUS</th>";  

  echo "<tr>";
  foreach ($fields['selected'] as $f => $g) {
    if ($f != 'job_status') {
      echo "<th class='".$f." bg-primary' style='text-align: center;'>" . $fields['data'][$f] . "</th>";
    }
  }
  echo "</tr>";



  $i = 0;
  foreach ($data as $d) {
    $i++;
    $bg = (false) ? 'background: #ffcccc' : '';

    echo "<tr>";
    echo "<td style='" . $bg . "'>" . $i . "</td>";
    foreach ($fields['selected'] as $f => $g) {
      if ( in_array($f, $arr_number)) {
        echo "<td class='".$f."' style='" . $bg . "' align='right'>" . number_format ( $d[$f], 0, '.', ',' ) . "</td>";
      } elseif ( in_array($f, $arr_pct) ) {
        echo "<td class='".$f."' style='" . $bg . "' align='right'>" . $d[$f] . "</td>";
      } elseif ( in_array($f, $arr_time) ) {
        echo "<td class='".$f."' style='" . $bg . "' align='right'>" . number_format ( $d[$f], 1, '.', ',' ) . "</td>";
      } elseif ( in_array($f, $arr_date) ) {
        $data_date = new DateTime($d[$f]);
        echo "<td class='".$f."' style='" . $bg . "' align='center'>" . $data_date->format('d-M-Y') . "</td>";
      } else {
          echo "<td class='".$f."' style='" . $bg . "'>" . $d[$f] . "</td>";
      }
    }
    echo "</tr>";
  }
  ?>
</table>
