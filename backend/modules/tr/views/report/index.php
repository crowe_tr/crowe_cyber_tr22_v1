<div class="card card-default padding-20">
    <h2><b>Reports</b></h2>
    <hr />
    <ul>
        <li>
            Time Report
            <ul>
                <li><a href="daily">Time Report Daily</a></li>
                <li><a href="detail">Time Report Detail</a></li>
                <li><a href="detail2">Time Report Detail v2</a></li>
                <li><a href="recap_perform">Recap for Performance Apprisal</a></li>
            </ul>
        </li>
        <li>
            Annual Leave
            <ul>
                <li><a href="annual_leave_summary">Report Summary</a></li>
                <li><a href="annual_leave_detail">Report Detail</a></li>
                <li><a href="sick_list">Report Sick List</a></li>
            </ul>
        </li>
        <li>
            Recovery Rate
            <ul>
                <li><a href="recovery_rate_summary">Report Summary</a></li>
                <li><a href="recovery_rate_detail">Report Detail</a></li>
                <li><a href="job_detail">Job Detail</a></li>
                <li><a href="job_detail_v2">Job Detail - With Time Report Period</a></li>
            </ul>
        </li>
        <li>
            Taxi Report
            <ul>
                <li><a href="taxi_summary">Report Summary</a></li>
                <li><a href="taxi_detail">Report Detail</a></li>
            </ul>
        </li>
        <li>
            Email
            <ul>
                <li><a href="javascript:;" onclick="email_monthly_supervisor()">Supervisor Monthly Notification</a></li>
                <li><a href="javascript:;" onclick="email_monthly_manager()">Manager Monthly Notification</a></li>
            </ul>
        </li>
    </ul>
</div>

<script type="text/javascript">
    var paramJs = (paramJs || {});
    paramJs.urlemail_monthly_supervisor = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/report/email_monthly_supervisor']); ?>';
    paramJs.urlemail_monthly_manager = '<?= Yii::$app->urlManager->createAbsoluteUrl(['tr/report/email_monthly_manager']); ?>';

    function email_monthly_supervisor(link = "") {
        if (confirm("Are you sure want to send email to All Supervisor ?")) {
            showFullLoading();
            $.ajax({
                url: paramJs.urlemail_monthly_supervisor,
                data: {},
                method: "POST",
                dataType: 'html',
                success: function(data) {
                    hideFullLoading();
                    alert(data);
                },
            });

        }
    }

    function email_monthly_manager(link = "") {
        if (confirm("Are you sure want to send email to All Manager ?")) {
            showFullLoading();
            $.ajax({
                url: paramJs.urlemail_monthly_manager,
                data: {'adad':'asdasdsd'},
                method: "POST",
                dataType: 'html',
                success: function(data) {
                    hideFullLoading();
                    alert(data);
                },
            });
        }
    }
</script>