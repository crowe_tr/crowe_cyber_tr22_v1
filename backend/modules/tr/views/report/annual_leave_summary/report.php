<span><b>ANNUAL LEAVE REPORT - SUMMARY</b></span>
</br>
<span>FILTER BY :
  <?php 
    // year":"2019","Entity":"KNMTR","Division":"","levelID":"","parentID":"","ManagerID":"","Employee":""
    $res_val = '';
    foreach ( $model->Custom as $ckey => $cval ){
      if ($cval != '') {
        switch ($ckey) {
          case "year" : $res_val = $res_val . 'YEAR = ' . $cval . ', '; break;
          case "Entity" : $res_val = $res_val . 'ENTITY = ' . $cval . ', '; break;
          case "Division" : $res_val = $res_val . 'DIVISION = ' . $cval . ', '; break;
          case "levelID" : $res_val = $res_val . 'LEVEL = ' . $cval . ', '; break;
          case "parentID" : $res_val = $res_val . 'GROUP = ' . $cval . ', '; break;
          case "ManagerID" : $res_val = $res_val . 'MANAGER = ' . $cval . ', '; break;
          case "Employee" : $res_val = $res_val . 'EMPLOYEE = ' . $cval . ', '; break;
        }
      }
    }
    $res_val = substr($res_val,0,strlen($res_val)-2);
    echo $res_val;
  ?>
</span>
<table class="table table-bordered" border="1" style="width: 100%">
  <?php
  echo "<tr>";
  echo "<th class='bg-primary'>NO.</th>";
  foreach ($fields['selected'] as $f ) {
    echo "<th class='".$f." bg-primary'>" . $fields['data'][$f] . "</th>";
  }
  echo "</tr>";

  $arr_dec = array( 'annual_days_ob', 'annual_days_used', 'annual_days_balance' );

  $i = 0;
  foreach ($data as $d) {
    $i++;
    // $bg = ($d['calendar_sign'] == 1) ? 'background: #ffcccc' : '';

    echo "<tr>";
    echo "<td style=''>" . $i . "</td>";
    foreach ($fields['selected'] as $f) {
      if ( in_array($f, $arr_dec)) {
        echo "<td class='".$f."' style='' align='right'>" . number_format ( $d[$f], 1, '.', ',' ) . "</td>";
      } else {
        echo "<td class='".$f."' style=''>" . $d[$f] . "</td>";
      }    
    }
    echo "</tr>";
  }
  ?>
</table>