<?php

function nbsp($icount)
{
  $res_echo = '';
  for ($i = 0; $i < $icount; $i++) {
    $res_echo = $res_echo . "&nbsp;";
  }
  return $res_echo;
}

// type = 1 => YES/NO, 2=>INCLUDE OPE
function rpt_to_text($itype, $ival)
{
  $res_text = '';
  switch ($itype) {
    case '2':
      $res_text = '';
      if ($ival == '1') {
        $res_text = ' ( INCLUDE OPE )';
      }
      break;

    default:
      $res_text = ' NO';
      if ($ival == '1') {
        $res_text = ' YES';
      }
      break;
  }
  return $res_text;
}

function diff_amount($iest, $iact)
{
  $res_amount = intval($iest) - intval($iact);
  return number_format($res_amount, 0, '.', ',');
}


$data_header = Yii::$app->db->createCommand("call rpt_recovery_rate_detail_header( '" . $model->Custom['JobCode'] . "' );")->queryAll();

if (empty($data_header[0])) {
  echo "no results found.";
} else {
  echo "<span><b>RECOVERY RATE JOB REPORT - DETAIL</b></span>";
  $res_val = $data_header[0];
  echo "
    <table class='table table-bordered' border='1' style='width: 100%'>
      <tr>
        <th width='10%' class='bg-primary'>JOB</th>
        <td width='2px'>:</td>
        <td width='50%'>".$res_val['job_code'] . " - " . $res_val['client_name'] . " - " . $res_val['job_desc']."</td>
        <th width='10%' class='bg-primary'>STATUS</th>
        <td width='2px'>:</td>
        <td>".$res_val['job_status']."</td>
      </tr>
      <tr>
        <th  class='bg-primary'>JOB PERIOD</th>
        <td width='2px'>:</td>
        <td>". strtoupper(date_format(date_create($res_val['job_start']), 'M d, Y'))
            . " <b>TO</b> " . strtoupper(date_format(date_create($res_val['job_end']), 'M d, Y')) . 
        "</td>
        <th  class='bg-primary'>FEE</th>
        <td width='2px'>:</td>
        <td>".number_format($res_val['job_fee'], 0, '.', ',').rpt_to_text('2', $res_val['job_include_ope'])."</td>

      </tr>
      <tr>
        <th  class='bg-primary'>ENTITY / DIVISION</th>
        <td width='2px'>:</td>
        <td>".$res_val['entity_name'] . " - " . $res_val['division_name']."</td>
        <th  class='bg-primary'>AREA</th>
        <td width='2px'>:</td>
        <td>".$res_val['job_area']."</td>
      </tr>

    </table>
    <table class='table table-bordered' border='1' style='width: 100%'>
      <tr>
      
        <th width='10%' class='bg-primary'>MEALS</th>
        <td width='2px'>:</td>
        <td>".rpt_to_text('1', $res_val['job_meal'])."</td>

        <th width='10%' class='bg-primary'>TRANSPORT</th>
        <td width='2px'>:</td>
        <td>".rpt_to_text('1', $res_val['job_taxi'])."</td>

        <th  width='10%' class='bg-primary'>OUT OF OFFICE</th>
        <td width='2px'>:</td>
        <td>".rpt_to_text('1', $res_val['job_outofoffice'])."</td>

      </tr>
      <tr>
        <th  class='bg-primary'>PARTNER</th>
        <td width='2px'>:</td>
        <td>".$res_val['partner_name']."</td>
        <th  class='bg-primary'>MANAGER</th>
        <td width='2px'>:</td>
        <td>".$res_val['manager_name']."</td>
        <th  class='bg-primary'>SUPERVISOR</th>
        <td width='2px'>:</td>
        <td>".$res_val['supervisor_name']."</td>
      </tr>
    </table>
  ";
    
?>


  <table class="table table-bordered" border="1" style="width: 100%">
    <?php
    echo "<tr>";
    echo "<th class='bg-primary' colspan=4>EMPLOYEE INFORMATION</th>";
    echo "<th class='bg-primary' colspan=6>BUDGETING</th>";
    echo "<th class='bg-primary' colspan=6>ACTUAL</th>";
    echo "<th class='bg-primary' colspan=2>DIFFERENCE</th>";
    echo "</tr>";
    echo "<tr>";
    echo "<th class='bg-primary'>NO.</th>";
    foreach ($fields['selected'] as $f) {
      echo "<th class='" . $f . " bg-primary'>" . $fields['data'][$f] . "</th>";
    }
    echo "</tr>";


    $arr_number = array(
      'job_planning', 'job_fieldwork', 'job_reporting', 'job_wrapup', 'job_overtime', 'job_total',
      'job_planning_act', 'job_fieldwork_act', 'job_reporting_act', 'job_wrapup_act', 'job_overtime_act', 'job_total_act',
      'job_total_diff', 'job_overtime_diff'
    );

    $arr_ctr = array('employee_id');

    $i = 0;
    foreach ($data as $d) {
      $i++;
      $bg = (false) ? 'background: #ffcccc' : '';
      echo "<tr>";
      echo "<td style='" . $bg . "'>" . $i . "</td>";
      foreach ($fields['selected'] as $f) {
        if (in_array($f, $arr_number)) {
          echo "<td class='" . $f . "' style='" . $bg . "' align='right'>" . number_format($d[$f], 0, '.', ',') . "</td>";
        } elseif (in_array($f, $arr_ctr)) {
          echo "<td class='" . $f . "' style='" . $bg . "' align='center'>" . $d[$f] . "</td>";
        } else {
          echo "<td class='" . $f . "' style='" . $bg . "'>" . $d[$f] . "</td>";
        }
      }
      echo "</tr>";
    }
    ?>
  </table>

  <!-- <br> -->
  <p><b>JOB REALITATION</b></p>
  <table class="table table-bordered" border="1" style="width: 50%">
    <?php
    echo "<tr>";
    echo "<th class='bg-primary'>DESCRIPTION</th>";
    echo "<th class='bg-primary'>ESTIMATED</th>";
    echo "<th class='bg-primary'>ACTUAL</th>";
    echo "<th class='bg-primary'>DIFFERENCE</th>";
    echo "</tr>";

    echo "<tr><td>TIME CHARGES</td><td align='right'>"
      . number_format($res_val['job_time_charges'], 0, '.', ',') . "</td><td align='right'>"
      . number_format($res_val['job_time_charges_act'], 0, '.', ',') . "</td><td align='right'>"
      . diff_amount($res_val['job_time_charges'], $res_val['job_time_charges_act']) . "</td></tr>";

    echo "<tr><td>MEALS ALLOWED</td><td align='right'>"
      . number_format($res_val['job_meal_allowance'], 0, '.', ',') . "</td><td align='right'>"
      . number_format($res_val['job_meal_allowance_act'], 0, '.', ',') . "</td><td align='right'>"
      . diff_amount($res_val['job_meal_allowance'], $res_val['job_meal_allowance_act']) . "</td></tr>";

    echo "<tr><td>TAXI USED ALLOWED</td><td align='right'>"
      . number_format($res_val['job_taxi_allowance'], 0, '.', ',') . "</td><td align='right'>"
      . number_format($res_val['job_taxi_allowance_act'], 0, '.', ',') . "</td><td align='right'>"
      . diff_amount($res_val['job_taxi_allowance'], $res_val['job_taxi_allowance_act']) . "</td></tr>";

    echo "<tr><td>OUT OF OFFICE ALLOWANCE</td><td align='right'>"
      . number_format($res_val['job_outofoffice_allowance'], 0, '.', ',') . "</td><td align='right'>"
      . number_format($res_val['job_outofoffice_allowance_act'], 0, '.', ',') . "</td><td align='right'>"
      . diff_amount($res_val['job_outofoffice_allowance'], $res_val['job_outofoffice_allowance_act']) . "</td></tr>";

    echo "<tr><td>ADMINISTRATIVE CHARGE</td><td align='right'>"
      . number_format($res_val['job_adm_charge'], 0, '.', ',') . "</td><td align='right'>"
      . number_format($res_val['job_adm_charge'], 0, '.', ',') . "</td><td align='right'>"
      . diff_amount($res_val['job_adm_charge'], $res_val['job_adm_charge']) . "</td></tr>";

    echo "<tr><td>OTHER EXPENSE ALLOWANCE</td><td align='right'>"
      . number_format($res_val['job_other_expense'], 0, '.', ',') . "</td><td align='right'>"
      . number_format($res_val['job_other_expense'], 0, '.', ',') . "</td><td align='right'>"
      . diff_amount($res_val['job_other_expense'], $res_val['job_other_expense']) . "</td></tr>";

    echo "<tr><td>PROGRESS TIME</td><td align='right'>"
      . number_format($res_val['job_total_hours'], 0, '.', ',') . "</td><td align='right'>"
      . number_format($res_val['job_total_hours_act'], 0, '.', ',') . "</td><td align='right'>"
      . $res_val['job_progress'] . "</td></tr>";

    echo "<tr><td>RECOVERY RATE ( % )</td><td align='right'>"
      . $res_val['recovery_rate'] . "</td><td align='right'>"
      . $res_val['recovery_rate_act'] . "</td><td align='right'></td></tr>";
    ?>
  </table>

<?php
}
?>