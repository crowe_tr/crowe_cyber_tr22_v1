<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\Url;
use common\models\cm\CmDept;
use common\models\cm\CmCompanyBranch;
use yii\web\View;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
?>
<div class="modal fade stick-up " id="TrModal" role="dialog" aria-hidden="false" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg ">
        <div class="modal-content no-border">
            <div class="modal-header bg-success text-white">
            </div>
            <div id="detail" class="modal-body padding-20">
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-sm-10">
                <h3 class="no-margin no-padding bold"><?= $param['page_label']; ?></h3>
            </div>
            <div class="col-sm-2">
                <?php
                $form = ActiveForm::begin([
                    'id' => 'tr-form',
                    'enableClientValidation' => true,
                    'validateOnSubmit' => true,
                    'validateOnChange' => true,
                    'validateOnType' => true,
                    'fieldConfig' => [
                        'template' => '{input}',
                        'options' => [
                            'class' => '',

                        ],
                    ],
                    'errorSummaryCssClass' => 'alert alert-danger'
                ]);
                ?>
                <?php
                $_month = [];
                $now = strtotime(date("Y-m-01"));
                for ($i = 0; $i <= 6; $i++) {
                    $_month[date('Y-m-t', strtotime("-{$i} months", $now))] = strtoupper(date('F Y', strtotime("-{$i} months", $now)));
                }
                $model->YearDate = !empty($param['YearDate']) ? $param['YearDate'] : date('Y-m-t');
                echo $form->field($model, 'YearDate', ['options' => ['class' => 'select-withoutlabel']])->widget(
                    Select2::classname(),
                    [
                        'data' =>  $_month,
                        'options' => ['id' => 'YearDate', 'placeholder' => 'Select ...'],
                    ]
                )->label(false);
                ?>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <div id="tr-calendar-approval" class="table-responsive m-t-20">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <td class="bg-primary text-center align-middle" rowspan="2">ID</td>
                        <td class="bg-primary text-center align-middle" rowspan="3">EMPLOYEE NAME</td>
                        <td class="bg-primary text-center align-middle" colspan="2">WORK HOUR</td>
                        <td class="bg-primary text-center align-middle" colspan="3">ALLOWANCE</td>
                        <td class="bg-primary text-center align-middle" rowspan="2">STATUS</td>
                        <td class="bg-info" rowspan="2"></td>
                    </tr>
                    <tr>
                        <td class="bg-primary text-center">WH</td>
                        <td class="bg-primary text-center">OT</td>
                        <td class="bg-primary text-center">MEAL</td>
                        <td class="bg-primary text-center">OUT OF OFFICE</td>
                        <td class="bg-primary text-center">TAXI</td>
                    </tr>

                </thead>
                <tbody>
                    <?php
                    if (!empty($data['list'])) {
                        foreach ($data['list'] as $list) {
                            if ($list['tr_status'] == $param['page_completed_status']) {
                                $list['tr_status_label'] = "Completed";
                            }

                            echo "<tr>";
                            echo "<td>" . $list['employee_id'] . "</td>";
                            echo "<td>" . $list['employee_name'] . "</td>";
                            if (empty($list['work_hour']) and empty($list['work_hour'])) {
                                echo "<td colspan='7' class='text-center align-middle' style='color: #dedede;'>There are no requests for approval on this date.</td>";
                            } else {
                                echo "
                                <td class='text-right align-middle'>" . $list['work_hour'] . "</td>
                                <td class='text-right align-middle'>" . $list['over_time'] . "</td>
                                <td class='text-right align-middle'>Rp. " . number_format($list['meal_amount'], 2) . "</td>
                                <td class='text-right align-middle'>Rp. " . number_format($list['ope_amount'], 2) . "</td>
                                <td class='text-right align-middle'>Rp. " . number_format($list['taxi_amount'], 2) . "</td>
                                <td class='text-right align-middle'><span class='badge " . $list['tr_statusCSS'] . "'>" . $list['tr_status_label'] . "</span></td>
                                <td class='text-center  align-middle'>"
                                    . Html::a(
                                        '<i class="fa fa-edit"></i>',
                                        empty($list['work_hour']) ? false : ['detail', 'id' => $list['employee_id']],
                                        [
                                            'class' => 'btn btn-success text-white',
                                            'style' => 'border-radius: 5px !important',
                                        ]
                                    )
                                    . "</td>";
                            }
                            echo "</tr>";
                        }
                    } else {
                        echo "<tr><td colspan='10'>There are no requests for approval on this periode.</td></tr>";
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>




<script type="text/javascript">
    $(document).ready(function() {
        $('#YearDate').on('change', function() {
            $('#tr-form').submit();
        });
    });
</script>
