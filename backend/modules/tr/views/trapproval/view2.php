<?php
    use yii\helpers\Html;
    if(!empty($data['Detail'])){
        echo "<p><b>TASK</b></p>";
        foreach($data['Detail'] as $task){
            $button = "";
            if(!empty($task['attachment'])) {
                $button .="<tr>"
                . "<td>Attachment</td>"
                . "<td>:</td>"
                . "<td>"
                . Html::a('Download file', Yii::getAlias('@public_baseurl/timereport_detail/') . $task['attachment'], ['class'=>'btn btn-info padding-5 p-l-10 p-r-10', 'download' => 'true'])
                . "</td>"
                . "</tr>";
            }
            if($task['tr_status'] == "APPROVAL2-PENDING"){
                $button .= "<tr>
                    <td colspan='3' class='text-right'>";
                $button .= " ".Html::a(
                    '<i class="fa fa-check-square"></i> Approve',
                    false,
                    [
                        'onclick' => "approval('".Yii::$app->urlManager->createAbsoluteUrl([$param['page_url'].'/detailapproval', 'id' => $task['time_report_id'] , 'TrDetID' => $task['tr_det_id'], 'set'=>1])."')",
                        'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
                    ]);
                $button .= " ".Html::a(
                    '<i class="fa fa-edit"></i> Revise',
                    false,
                    [
                        'onclick' => "
                            if(confirm('Are you sure want to reject this item ?')) {
                                approval('".Yii::$app->urlManager->createAbsoluteUrl([$param['page_url'].'/detailapproval', 'id' => $task['time_report_id'] , 'TrDetID' => $task['tr_det_id'], 'set'=>3])."')
                            }else{
                                return false;
                            }
                        ",
                        'class' => 'btn btn-warning text-white padding-5 p-l-10 p-r-10',
                    ]);
                $button .= "</tr>";
            }
            echo "
            <div class='m-t-5 m-b-0'>
                <div class=''>
                    <table class='table table-bordered'>
                        <tr>
                            <td width='100px'>WorkHour</td>
                            <td width='10px'>:</td>
                            <td>{$task['work_hour']} Hours</td>
                        </tr>
                        <tr>
                            <td width='100px'>Overtime</td>
                            <td width='10px'>:</td>
                            <td>{$task['over_time']} Hours</td>
                        </tr>
                        <tr>
                            <td width='100px'>Description</td>
                            <td width='10px'>:</td>
                            <td>{$task['description']}</td>
                        </tr>
                        <tr>
                            <td width='100px'>Status</td>
                            <td width='10px'>:</td>
                            <td><span class='badge {$task['tr_status_label']}'>{$task['tr_status_label']}</span></td>
                        </tr>
                        ".$button."
                    </table>
                </div>
            </div>
            ";
        }
    }
    if(!empty($data['Meal'])){
        echo "<br/><p><b>MEALS</b></p>";
        foreach($data['Meal'] as $meal){
            $button = "";
            if($meal['tr_status'] == "APPROVAL2-PENDING" && $task['tr_status'] == "COMPLETED"){
                $button .= "<tr>
                <td colspan='3' class='text-right'>";

                $button .=  Html::a(
                        '<i class="fa fa-check-square"></i> Approve',
                        false,
                        [
                            'onclick' => "approval('".Yii::$app->urlManager->createAbsoluteUrl([$param['page_url'].'/mealapproval', 'id' => $meal['time_report_id'] , 'TrDetID' => $meal['tr_det_id'], 'Seq' => $meal['seq'], 'set'=>1])."')",
                            'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
                            'style' => 'bjob-radius: 5px !important',
                        ])
                        ." ".
                        Html::a(
                            '<i class="fa fa-close"></i> Reject',
                            false,
                            [
                                'onclick' => "
                                    if(confirm('Are you sure want to reject this item ?')) {
                                        approval('".Yii::$app->urlManager->createAbsoluteUrl([$param['page_url'].'/mealapproval', 'id' => $meal['time_report_id'] , 'TrDetID' => $meal['tr_det_id'], 'Seq' => $meal['seq'], 'set'=>2 ])."')
                                    }else{
                                        return false;
                                    }
                                ",
                                'class' => 'btn btn-danger text-white padding-5 p-l-10 p-r-10',
                                'style' => 'bjob-radius: 5px !important',
                            ]
                        )
                        ." ".
                        Html::a(
                            '<i class="fa fa-edit"></i> Revise',
                            false,
                            [
                                'onclick' => "
                                if(confirm('Are you sure want to denie this item and ask user to revise ?')) {
                                    approval('".Yii::$app->urlManager->createAbsoluteUrl([$param['page_url'].'/mealapproval', 'id' => $meal['time_report_id'] , 'TrDetID' => $meal['tr_det_id'], 'Seq' => $meal['seq'], 'set'=>3 ])."')
                                }else{
                                    return false;
                                }
                                ",
                                'class' => 'btn btn-warning text-white padding-5 p-l-10 p-r-10',
                                'style' => 'bjob-radius: 5px !important',
                            ]
                        );
                $button .= "</tr>";
            }
            echo "
            <div class='m-t-5 m-b-0'>
                <div class=''>
                    <table class='table table-bordered'>
                        <tr>
                            <td width='100px'>Description</td>
                            <td width='10px'>:</td>
                            <td>{$meal['description']}</td>
                        </tr>
                        <tr>
                            <td width='100px'>Amount</td>
                            <td width='10px'>:</td>
                            <td>Rp. ".number_format($meal['meal_value'], 2)."</td>
                        </tr>
                        <tr>
                            <td width='100px'>Status</td>
                            <td width='10px'>:</td>
                            <td><span class='badge {$meal['tr_status_label']}'>{$meal['tr_status_label']}</span></td>
                        </tr>

                        ".$button."

                    </table>
                </div>
            </div>
            ";
        }
    }
    if(!empty($data['Ope'])){
        echo "<br/><p><b>OUT OF OFFICE</b></p>";
        foreach($data['Ope'] as $outofoffice) {
            $button = "";
            if($outofoffice['tr_status'] == "APPROVAL2-PENDING" && $task['tr_status'] == "COMPLETED"){
                    $button .= "<tr>
                    <td colspan='3' class='text-right'>";
                    $button .=  Html::a(
                        '<i class="fa fa-check-square"></i> Approve',
                        false,
                        [

                            'onclick' => "approval('".Yii::$app->urlManager->createAbsoluteUrl([$param['page_url'].'/outofficeapproval', 'id' => $outofoffice['time_report_id'], 'TrDetID' => $outofoffice['tr_det_id'], 'Seq' => $outofoffice['seq'], 'set'=>1])."')",
                            'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
                            'style' => 'bjob-radius: 5px !important',
                        ])
                        ." ".
                        Html::a(
                            '<i class="fa fa-close"></i> Reject',
                            false,
                            [
                                'onclick' => "
                                    if(confirm('Are you sure want to reject this item ?')) {
                                        approval('".Yii::$app->urlManager->createAbsoluteUrl([$param['page_url'].'/outofficeapproval', 'id' => $outofoffice['time_report_id'], 'TrDetID' => $outofoffice['tr_det_id'], 'Seq' => $outofoffice['seq'], 'set'=>2 ])."')                                    }else{
                                        return false;
                                    }
                                ",
                                'class' => 'btn btn-danger text-white padding-5 p-l-10 p-r-10',
                                'style' => 'bjob-radius: 5px !important',
                            ]
                        )
                        ." ".
                        Html::a(
                            '<i class="fa fa-edit"></i> Revise',
                            false,
                            [
                                'onclick' => "
                                if(confirm('Are you sure want to denie this item and ask user to revise ?')) {
                                    approval('".Yii::$app->urlManager->createAbsoluteUrl([$param['page_url'].'/outofficeapproval', 'id' => $outofoffice['time_report_id'] , 'TrDetID' => $outofoffice['tr_det_id'], 'Seq' => $outofoffice['seq'], 'set'=>3 ])."')
                                }else{
                                    return false;
                                }
                                ",
                                'class' => 'btn btn-warning text-white padding-5 p-l-10 p-r-10',
                                'style' => 'bjob-radius: 5px !important',
                            ]
                        );
                $button .= "</tr>";
            }
            echo "
            <div class='m-t-5 m-b-0'>
                <div class=''>
                    <table class='table table-bordered'>
                        <tr>
                            <td width='100px'>Description</td>
                            <td width='10px'>:</td>
                            <td>{$outofoffice['description']}</td>
                        </tr>
                        <tr>
                            <td width='100px'>Zone</td>
                            <td width='10px'>:</td>
                            <td>{$outofoffice['zone_id']} </td>
                        </tr>

                        <tr>
                            <td width='100px'>Amount</td>
                            <td width='10px'>:</td>
                            <td>Rp. ".number_format($outofoffice['ope_value'], 2)."</td>
                        </tr>
                        <tr>
                            <td width='100px'>Status</td>
                            <td width='10px'>:</td>
                            <td><span class='badge {$outofoffice['tr_status_label']}'>{$outofoffice['tr_status_label']}</span></td>
                        </tr>

                        ".$button."

                    </table>
                </div>
            </div>

            ";
        }
    }
    if(!empty($data['Taxi'])){


        echo "<br/><p><b>TAXI</b></p>";
        foreach($data['Taxi'] as $taxi){
            $button = "";

            if($taxi['tr_status'] == "APPROVAL2-PENDING" && $task['tr_status'] == "COMPLETED"){
                $button .= "<tr>
                <td colspan='3' class='text-right'>";

                $button .=  Html::a(
                        '<i class="fa fa-check-square"></i> Approve',
                        false,
                        [
                            'onclick' => "approval('".Yii::$app->urlManager->createAbsoluteUrl([$param['page_url'].'/taxiapproval', 'id' => $taxi['time_report_id'] , 'TrDetID' => $taxi['tr_det_id'], 'Seq' => $taxi['seq'], 'set'=>1])."')",
                            'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
                            'style' => 'bjob-radius: 5px !important',
                        ])
                        ." ".
                        Html::a(
                            '<i class="fa fa-close"></i> Reject',
                            false,
                            [
                                'onclick' => "
                                    if(confirm('Are you sure want to reject this item ?')) {
                                        approval('".Yii::$app->urlManager->createAbsoluteUrl([$param['page_url'].'/taxiapproval', 'id' => $taxi['time_report_id'] , 'TrDetID' => $taxi['tr_det_id'], 'Seq' => $taxi['seq'],  'set'=>2 ])."')
                                    }else{
                                        return false;
                                    }
                                ",
                                'class' => 'btn btn-danger text-white padding-5 p-l-10 p-r-10',
                                'style' => 'bjob-radius: 5px !important',
                            ]
                        )
                        ." ".
                        Html::a(
                            '<i class="fa fa-edit"></i> Revise',
                            false,
                            [
                                'onclick' => "
                                    if(confirm('Are you sure want to denie this item and ask user to revise ?')) {
                                        approval('".Yii::$app->urlManager->createAbsoluteUrl([$param['page_url'].'/taxiapproval', 'id' => $taxi['time_report_id'] , 'TrDetID' => $taxi['tr_det_id'], 'Seq' => $taxi['seq'], 'set'=>3 ])."')
                                    }else{
                                        return false;
                                    }
                                ",
                                'class' => 'btn btn-warning text-white padding-5 p-l-10 p-r-10',
                                'style' => 'bjob-radius: 5px !important',
                            ]
                        );
                $button .= "</tr>";
            }

            echo "
            <div class='m-t-5 m-b-0'>
            <div class=''>
                <table class='table table-bordered'>
                    <tr>
                        <td width='100px'>Type</td>
                        <td width='10px'>:</td>
                        <td>{$taxi['taxi_name']}</td>
                    </tr>
                    <tr>
                        <td>Amount</td>
                        <td>:</td>
                        <td>Rp. ".number_format($taxi['amount'], 0)."</td>
                    </tr>
                    <tr>
                        <td>Destination</td>
                        <td>:</td>
                        <td>{$taxi['destination']} </td>
                    </tr>
                    <tr>
                        <td>Description</td>
                        <td>:</td>
                        <td>{$taxi['description']} </td>
                    </tr>
                    <tr>
                        <td width='100px'>Status</td>
                        <td width='10px'>:</td>
                        <td><span class='badge {$taxi['tr_status_label']}'>{$taxi['tr_status_label']}</span></td>
                    </tr>

                    ".$button."

                </table>
            </div>
        </div>            ";
        }
    }

?>
<div class="row">
    <div class="col-md-12 text-right">
        <hr class="m-b-5"/>
        <button type="button" class="btn btn-info p-t-10 p-b-10" style="font-size: 12px" onclick="CloseModal()">BACK</button>
    </div>
</div>

<script type="text/javascript">
	var paramJs = (paramJs || {});
	paramJs.showModal = '<?= Yii::$app->urlManager->createAbsoluteUrl([$param['page_url'].'/view', 'id'=>$param['TrID'], 'TrDetID'=>$param['TrDetID']]); ?>';

	function approval(link) {
        showFullLoading();

		$.ajax({
			url: link,
			data:  {},
			method: "POST",
			dataType: 'html',
			success: function(data) {
                hideFullLoading();
                showModal();
			},
			error: function(data) {
                hideFullLoading();
			},
		});
	}
    function showModal() {
        showFullLoading();

        $('#ChildModal').modal('show');
        $('#ChildModaldetail').html("Loading ...");

		$.ajax({
			url: paramJs.showModal,
			data:  {},
			method: "POST",
			dataType: 'html',
			success: function(data) {
                hideFullLoading();
				$('#ChildModaldetail').html(data);
			},
			error: function(data) {
                hideFullLoading();
			},
		});
	}

    function CloseModal(){
        $('#ChildModal').modal('hide');
        reload();
    }

	window.closeModal = function(){
		reload();
	};
	function reload(){
		$.pjax.defaults.timeout = false;
		$.pjax.reload({
			container: '#trapproval-pjax'
		})
		$('#ChildModal').modal('hide');
		$('#ChildModal').data('modal', null);
	}

</script>
