<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\Url;
use common\models\cm\CmDept;
use common\models\cm\CmCompanyBranch;
use yii\web\View;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
\yii\widgets\Pjax::begin(['id' => 'trapproval-pjax', 'enablePushState' => true]);
?>
<?php

$body = "";
$subtitle = "";
if (!empty($data['list'])) {
    foreach ($data['list'] as $list) {
        $subtitle = empty($list["employee_id"]) ? $subtitle : $list["employee_id"] . " - " . $list["employee_name"];

        $holidayType = "bg-warning text-black";
        if ($list['calendar_sign'] != 0) {
            $holidayType = "bg-danger text-white";
        }

        $body .= "<tr>";
        $body .= "<td class='" . $holidayType . " align-middle'>"
            . " " . $list['calendar_day'] . "<br/>"
            . "<b>" . date('F d, Y', strtotime($list['calendar_date'])) . "</b>"
            . "</td>";
        if (empty($list['work_hour']) and empty($list['work_hour'])) {
            $body .= "<td colspan='10' class='text-center align-middle' style='color: #dedede;'>There are no requests for approval on this date.</td>";
        } else {
            if ($list['tr_status'] == $param['page_completed_status']) {
                $list['tr_status_label'] = "Completed";
            }

            $body .= "
            <td>"
                . "<input type='checkbox' style='display:none' name='data[]' value='" . $list['tr_date'] . ';' . $list['task_description'] . ';' . $list['tr_id'] . ';' . $list['tr_det_id'] . "' />"
                . $list['task_description']
                . "
            </td>
            <td class='text-right'>" . $list['task_name'] . "</td>
            <td class='text-right'>" . $list['work_hour'] . "</td>
            <td class='text-right'>" . $list['over_time'] . "</td>
            <td class='text-right'>Rp. " . number_format($list['meal_amount'], 2) . "</td>
            <td class='text-right'>Rp. " . number_format($list['ope_amount'], 2) . "</td>
            <td class='text-right'>Rp. " . number_format($list['taxi_amount'], 2) . "</td>
            <td class='text-right'><span class='badge " . $list['tr_statusCSS'] . "'>" . $list['tr_status_label'] . "</span></td>
            ";

            $button = "";
            $button .= Html::a(
                '<i class="fa fa-edit"></i> ',
                false,
                [
                    'onclick' => "approvedetail('" . Yii::$app->urlManager->createAbsoluteUrl([$param['page_url'].'/view', 'id' => $list['tr_id'], 'TrDetID' => $list['tr_det_id']]) . "')",
                    'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
                    'style' => 'bjob-radius: 5px !important',
                ]
            );
            $button .= Html::a(
                '<i class="fa fa-comment"></i> Comment',
                false,
                [
                    'onclick' => "Comments('" . Yii::$app->urlManager->createAbsoluteUrl(['tr/timereport/comments', 'tr' => $list['tr_id'], 'id' => $list['tr_det_id'], 'type' => 1]) . "')",
                    'class' => 'btn btn-info text-white padding-5 p-l-10 p-r-10',
                    'style' => 'bjob-radius: 5px !important',
                ]
            );


            $body .= "<td class='text-center'>"
                . "<div class='tooltip-action'>
                    <div class='trigger'>
                        " . Html::a('<i class="fa fa-ellipsis-h"></i>', false, ['class' => 'text-info', 'data-toggle' => 'tooltip', 'data-original-title' => 'Up here!']) . "
                    </div>
                    <div class='action-mask'>
                        <div class='action'>
                            '.$button.'
                        </div>
                    </div>
                </div>"
                . "</td>";
        }
        $body .= "</tr>";
    }
}

?>

<form id="approval-detail">
    <div class="row">
        <div class="col-md-6">
            <h3 class="no-margin no-padding bold">
                <?= $param['page_label']; ?>
            </h3>
            <h6 class="no-margin no-padding ">
                <?= $subtitle; ?>
            </h6>
        </div>
        <div class="col-md-6 p-t-20 text-right">
            <?php

            echo html::a("BACK", ['all'], ['id' => 'approve', 'class' => 'btn btn-info text-white m-l-5']);
            echo html::a("APPROVE", false, ['id' => 'approve', 'onclick' => 'approve()', 'class' => 'btn btn-primary text-white m-l-5']);
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 m-t-20">
            <div class="alert alert-danger custom-error" id="custom-error" style="display:none"></div>

            <div id="tr-calendar-approval" class="table-responsive">
                <table class="table table-bordered" style="cursor:pointer">
                    <thead>
                        <tr>
                            <td class="bg-info text-center align-middle" rowspan="2" width="15%">
                                <a href="javascript:;" id="selectall" class="btn btn-primary">Select all</a>
                                <a href="javascript:;" id="unselectall" style="display: none;" class="btn btn-primary">Unselect all</a>
                            </td>
                            <td class="bg-primary text-center align-middle" rowspan="3">JOB DESCRIPTION</td>
                            <td class="bg-primary text-center align-middle" rowspan="2">TASK</td>
                            <td class="bg-primary text-center align-middle" colspan="2">WORK HOUR</td>
                            <td class="bg-primary text-center align-middle" colspan="3">ALLOWANCE</td>
                            <td class="bg-primary text-center align-middle" rowspan="2">STATUS</td>
                            <td class="bg-info text-center align-middle" rowspan="2"></td>
                        </tr>
                        <tr>
                            <td class="bg-primary text-center">WH</td>
                            <td class="bg-primary text-center">OT</td>
                            <td class="bg-primary text-center">MEAL</td>
                            <td class="bg-primary text-center">OUT OF OFFICE</td>
                            <td class="bg-primary text-center">TAXI</td>
                        </tr>

                    </thead>
                    <tbody>
                        <?php
                        echo $body;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</form>
<div class="modal fade slide-right" id="ModalComments" role="dialog" aria-hidden="false">
	<div class="modal-dialog no-border">
		<div class="modal-content no-border">
			<div class="modal-header no-border bg-success">
				<p class="text-white">
					<b><i class="pg-comment fax2"></i> COMMENTS</b>
				</p>
			</div>
			<div id="ModalCommentsBody" class="modal-body no-padding">

			</div>
		</div>
	</div>
</div>
<div class="modal slide-up" id="ChildModal" role="dialog" aria-hidden="false" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-md">
		<div class="modal-content no-border">
			<div class="modal-header bg-success text-white">
			</div>
			<div id="ChildModaldetail" class="modal-body padding-20">
			</div>
		</div>
	</div>
</div>

<?php \yii\widgets\Pjax::end(); ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#selectall').click(function() {
            $("input[type='checkbox']").prop('checked', true);
            $("tbody td").css("background", "#FEF8DD");
            $('#selectall').hide();
            $('#unselectall').show();
        });
        $('#unselectall').click(function() {
            $("input[type='checkbox']").prop('checked', false);
            $("tbody td").css("background", "#fff");
            $('#selectall').show();
            $('#unselectall').hide();
        });

        $("tr").slice(1).on('click', function(event) {
            var chks = $("input[type='checkbox']", this);
            if (typeof chks.val() !== 'undefined') {
                if (chks.prop("checked")) {
                    chks.prop("checked", false);
                    $(this).children("td").css("background", "#fff");
                } else {
                    chks.prop("checked", true);
                    $(this).children("td").css("background", "#FEF8DD");
                }
            }
        });
    });

    var paramJs = (paramJs || {});
    paramJs.urlApprove = '<?= Yii::$app->urlManager->createAbsoluteUrl([$param['page_url'].'/approve']); ?>';
    paramJs.urlRevise = '<?= Yii::$app->urlManager->createAbsoluteUrl([$param['page_url'].'/revise']); ?>';
    paramJs.urlComments = '<?= Yii::$app->urlManager->createAbsoluteUrl([$param['page_url'].'/comments']); ?>';


    function approve() {
        $.ajax({
            url: paramJs.urlApprove,
            type: 'POST',
            data: $('#approval-detail').serialize(),
            dataType: 'JSON',
            success: function(data) {
                if (data.status == true) {
                    reload();
                } else {
                    alert(data.message);
                }
                hideFullLoading();
            },
            error: function(jqXHR, errMsg) {
                $('.custom-error').show();
                $('.custom-error').html(errMsg);

                $('html, body').animate({
                    scrollTop: 0
                }, 0);
                hideFullLoading();
            }
        });
        return false;
    }
	function approvedetail(link) {
        $('#ChildModal').modal('show');
        $('#ChildModaldetail').html("Loading ...");

		$.ajax({
			url: link,
			data:  {},
			method: "POST",
			dataType: 'html',
			success: function(data) {
				$('#ChildModaldetail').html(data);
			},
		});
	}

    function Comments(link) {
        $('#ModalComments').modal('show');
        $('#ModalCommentsBody').html("Loading ...");

        $.ajax({
            url: link,
            data: {},
            method: "POST",
            dataType: 'html',
            success: function(data) {
                $('#ModalCommentsBody').html(data);
                hideFullLoading();

            },
        });
    }

    function CommentsSave(id) {
        $('#ModalComments').modal('show');
        $('#ModalCommentsBody').html("Loading ...");
        var link = (link || paramJs.urlComments);

        $.ajax({
            url: link,
            data: {
                id: id
            },
            method: "POST",
            dataType: 'html',
            success: function(data) {
                $('#ModalCommentsBody').html(data);
                hideFullLoading();

            },
        });
    }

    function Modal(link = "") {
        showFullLoading();
        var link = (link || paramJs.urlBudgetShowModal);
        $.ajax({
            url: link,
            data: $('#tr-form').serialize(),
            method: "POST",
            dataType: 'html',
            success: function(data) {
                hideFullLoading();
                $('#ChildModal').modal('show');
                $('#ChildModaldetail').html("Loading ...");

                $('#ChildModaldetail').html(data);
            },
        });
    }

    window.closeModal = function() {
        reload();
        hideFullLoading();
    };

    function reload() {
        $.pjax.defaults.timeout = false;
        $.pjax.reload({
            container: '#trapproval-pjax'
        })
        $('#ChildModal').modal('hide');
        $('#ChildModal').data('modal', null);
    }
</script>
