<?php

namespace backend\modules\tr\controllers;

use Yii;
use yii\base\Exception;
use common\models\tr\Job;
use common\models\tr\Taxi;
use common\models\tr\TimeReport;
use common\models\tr\TimeReportNote;
use common\models\tr\TimeReportDetail;
use common\models\tr\TimeReportMeals;
use common\models\tr\TimeReportOutOfOffice;
use common\models\tr\TimeReportOutOfTown;
use common\models\tr\TimeReportTaxi;
use common\models\tr\TrTimeReportLogs;

use common\models\st\TaskType;
use common\models\st\Task;
use common\models\cm\Taxi as cmTaxi;

use common\models\tr\search\TimeReportSearch;
use common\models\tr\search\TimeReportDetailSearch;
use common\models\tr\search\TimeReportNoteSearch;
use common\models\tr\search\TimeReportMealsSearch;
use common\models\tr\search\TimeReportOutOfOfficeSearch;
use common\models\tr\search\TimeReportTaxiSearch;
use common\models\tr\search\TrTimeReportLogsSearch;

use yii\base\ErrorException;

use common\components\CommonHelper;
use common\components\Helper;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\MethodNotAllowedHttpException;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class TimereportController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    public function actionIndex()
    {
        return $this->index(0);
    }
    public function actionAll()
    {
        return $this->index(0);
    }
    public function IndexFilter()
    {
        $session = Yii::$app->session;
        $session->open();

        $post = Yii::$app->request->post();
        $model = new TimeReport();
        if ($model->load($post)) {
            $data = [];
            $date = explode("-", $model->YearDate);

            $data['Year'] = $date[0];
            $data['Month'] = $date[1];
            $data['YearDate'] = $model->YearDate;
            $session->set('__trtimreport_index', $data);
        }
    }

    public function index($Status = null)
    {
        $user = CommonHelper::getUserIndentity();
        $session = Yii::$app->session;
        $session->remove('__timereport_index');

        $data = array();

        $this->IndexFilter();

        $model = new TimeReport();
        $param = isset($_SESSION['__trtimreport_index']) ? $_SESSION['__trtimreport_index'] : [];
        $param['employee_id'] = isset($param['employee_id']) ? $param['employee_id'] : $user->user_id;
        $param['Year'] = isset($param['Year']) ? $param['Year'] : date('Y');
        $param['Month'] = isset($param['Month']) ? $param['Month'] : date('m');
        $param['Status'] = isset($Status) ?  $Status : 0;
        $param['YearDate'] = isset($param['YearDate']) ? $param['YearDate'] : date('Y-m-t');

        // $sql_list = "call tr_list('" . $param['employee_id'] . "', " . $param['Year'] . ", " . $param['Month'] . ")";
        $sql_calendar = "call calendar_list('" . $param['employee_id'] ."'," . $param['Year'] . ", " . $param['Month'] . ")";
        $dt_calendar = Yii::$app->db->createCommand($sql_calendar)->queryAll();
        foreach ($dt_calendar as $kc => $vc) {
          $sql_tr_list = "call tr_list('tr-data','" . $param['employee_id'] .";" . $vc['calendar_date'] . "');";
          // $sql_tr_list = "                                                ;";
          // $sql_tr_list = "select * from calendar_list where calendar_date = '2022-05-04'";
          $dt_tr_list = Yii::$app->db->createCommand($sql_tr_list)->queryOne();
          if (!($dt_tr_list)) {
            $dt_calendar[$kc]['tr_id'] = null;
            $dt_calendar[$kc]['tr_status_only'] = null;
            $dt_calendar[$kc]['employee_id'] = null;
            $dt_calendar[$kc]['employee_name'] = null;
            $dt_calendar[$kc]['work_hour'] = null;
            $dt_calendar[$kc]['over_time'] = null;
            $dt_calendar[$kc]['meal_value'] = null;
            $dt_calendar[$kc]['ope_value'] = null;
            $dt_calendar[$kc]['taxi_value'] = null;
            $dt_calendar[$kc]['tr_status'] = null;
            $dt_calendar[$kc]['tr_status_label'] = null;
            $dt_calendar[$kc]['tr_statusCSS'] = null;
          } else {
            $dt_calendar[$kc]['tr_id'] = $dt_tr_list['tr_id'];
            $dt_calendar[$kc]['tr_status_only'] = $dt_tr_list['tr_status_only'];
            $dt_calendar[$kc]['employee_id'] = $dt_tr_list['employee_id'];
            $dt_calendar[$kc]['employee_name'] = $dt_tr_list['employee_name'];
            $dt_calendar[$kc]['work_hour'] = $dt_tr_list['work_hour'];
            $dt_calendar[$kc]['over_time'] = $dt_tr_list['over_time'];
            $dt_calendar[$kc]['meal_value'] = $dt_tr_list['meal_value'];
            $dt_calendar[$kc]['ope_value'] = $dt_tr_list['ope_value'];
            $dt_calendar[$kc]['taxi_value'] = $dt_tr_list['taxi_value'];
            $dt_calendar[$kc]['tr_status'] = $dt_tr_list['tr_status'];
            $dt_calendar[$kc]['tr_status_label'] = $dt_tr_list['tr_status_label'];
            $dt_calendar[$kc]['tr_statusCSS'] = $dt_tr_list['tr_statusCSS'];
          }
        }

        $data['list'] = $dt_calendar;
        // $data['list'] = Yii::$app->db->createCommand($sql_list)->queryAll();
        return $this->render('index', [
            'param' => $param,
            'data' => $data,
            'model' => $model
        ]);
    }
    public function actionIndexdetail()
    {
        $session = Yii::$app->session;
        $session->open();

        $post = Yii::$app->request->post();
        if (!empty($post['id'])) {
            $id = $post['id'];

            $stored_session = isset($session['__timereport_index']) ? $session['__timereport_index'] : [];
            if (!empty($stored_session['Indexdetail'][$id])) {
                $details = $stored_session['Indexdetail'][$id];
            } else {
                $sql_job = "call tr_hint({$id})";
                $details = Yii::$app->db->createCommand($sql_job)->queryAll();

                $stored_session['Indexdetail'][$id] = $details;
                $session->set('__timereport_index', $stored_session);
            }

            if (!empty($details)) {
                echo '
                    <b>Summary :</b>
                    <table class="table table-bordered ">
                        <tr>
                            <th class="bg-primary">Description</th>
                            <th class="bg-primary">WH</th>
                            <th class="bg-primary">OT</th>
                            <th class="bg-primary">Meals</th>
                            <th class="bg-primary">Out Of Office</th>
                            <th class="bg-primary">Taxi</th>
                            <th class="bg-primary">Status</th>
                        </tr>';
                foreach ($details as $d) {
                    echo '<tr>
                                <th>' . $d['description'] . '</th>
                                <th>' . $d['work_hour'] . '</th>
                                <th>' . $d['over_time'] . '</th>
                                <th>Rp.' . number_format($d['meal_amount']) . '</th>
                                <th>Rp.' . number_format($d['ope_amount']) . '</th>
                                <th>Rp.' . number_format($d['taxi_amount']) . '</th>
                                <th>' . $d['tr_status'] . '</th>
                            </tr>';
                }
                echo '</table>';
            } else {
                echo "No work has been inputted on this date";
            }
        } else {
            echo "No work has been inputted on this date";
        }
    }
    public function actionView($id = null, $EmployeeId = null, $action = null)
    {
        $post = Yii::$app->request->post();
        $user = CommonHelper::getUserIndentity();

        $EmployeeId = empty($EmployeeId) ? $user->Id : $EmployeeId;
        $Date = $id;

        $model = TimeReport::find()->with('employee')->where(['EmployeeId' => $EmployeeId, 'Date' => $Date])->one();
        if ($model == null) {
            $model = new TimeReport();
            $model->EmployeeId = $EmployeeId;
            $model->tr_date = $Date;
            $model->isStayed = 0;
            $model->Status = 0;
            $model->save();
        }
        $searchModel = new TimeReportDetailSearch();
        $searchModel->TimeReportID = $model->ID;
        $dataProvider = $searchModel->search([]);

        $searchModel_Meals = new TimeReportMealsSearch();
        $searchModel_Meals->TimeReportID = $model->ID;
        $dataProvider_Meals = $searchModel_Meals->search([]);

        $searchModel_OPE = new TimeReportOutOfOfficeSearch();
        $searchModel_OPE->TimeReportID = $model->ID;
        $dataProvider_OPE = $searchModel_OPE->search([]);

        $searchModel_TAXI = new TimeReportTaxiSearch();
        $searchModel_TAXI->TimeReportID = $model->ID;
        $dataProvider_TAXI = $searchModel_TAXI->search([]);

        if ($model->load(Yii::$app->request->post())) {
            if (!isset($post['submit'])) {
                $model->Status = 0;
                if ($model->save()) {
                    return $this->redirect(['index']);
                }
            } else {
                $sql = "call getTrValidate('" . $model->ID . "');";
                $data = Yii::$app->db->createCommand($sql)->queryone();

                if ($data['vSummaryValidate']) {
                    $model->Status = 1;
                    if ($model->save()) {
                        return $this->redirect(['index']);
                    }
                } else {
                    if ($data['vDetailValidate'] == 0) {
                        $model->addError('ID', $data['vDetailDescription']);
                    }
                    if ($data['vMealValidate'] == 0) {
                        $model->addError('ID', $data['vMealDescription']);
                    }
                    if ($data['vOutOfficeValidate'] == 0) {
                        $model->addError('ID', $data['vOutOfficeDescription']);
                    }
                }
            }
        }

        return $this->renderAjax('view', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'dataProvider_Meals' => $dataProvider_Meals,
            'dataProvider_OPE' => $dataProvider_OPE,
            'dataProvider_TAXI' => $dataProvider_TAXI,
        ]);
    }
    public function actionUpdate($id = null, $EmployeeId = null, $action = null)
    {
        $post = Yii::$app->request->post();
        $user = CommonHelper::getUserIndentity();
        $EmployeeId = empty($EmployeeId) ? $user->user_id : $EmployeeId;
        $Date = $id;

        $model = TimeReport::find()->with('employee')->where(['employee_id' => $EmployeeId, 'tr_date' => $Date])->one();
        if ($model == null) {
            $model = new TimeReport();
            $model->employee_id = $EmployeeId;
            $model->tr_date = $Date;
            $model->is_stayed = 0;
            $model->tr_status = 0;
            $sql_emp_rate = "select get_job_val('tr', 'head-employee-rate', '" . $EmployeeId . ";" . $Date . "') as employee_rate;";
            $data_emp_rate = Yii::$app->db->createCommand($sql_emp_rate)->queryone();
            $model->employee_rate = $data_emp_rate['employee_rate'];
            $model->save();
        }

        $data = array();
        $data['Detail'] = Yii::$app->db->createCommand("call tr_list('tr-detail','" . $model->id . ";detail')")->queryAll();
        $data['Meal'] = Yii::$app->db->createCommand("call tr_list('tr-detail','" . $model->id . ";meal')")->queryAll();
        $data['OutOffice'] = Yii::$app->db->createCommand("call tr_list('tr-detail','" . $model->id . ";ope')")->queryAll();
        $data['Taxi'] = Yii::$app->db->createCommand("call tr_list('tr-detail','" . $model->id . ";taxi')")->queryAll();
        // $data['Detail'] = Yii::$app->db->createCommand("call tr_update_list(" . $model->ID . ",'Details')")->queryAll();
        // $data['Meal'] = Yii::$app->db->createCommand("call tr_update_list(" . $model->ID . ",'Meals')")->queryAll();
        // $data['OutOffice'] = Yii::$app->db->createCommand("call tr_update_list(" . $model->ID . ",'OutOfOffice')")->queryAll();
        // $data['Taxi'] = Yii::$app->db->createCommand("call tr_update_list(" . $model->ID . ",'Taxi')")->queryAll();
        // $data['OutOfTown'] = Yii::$app->db->createCommand("call tr_update_list(" . $model->ID . ",'OutOfTown')")->queryAll();

        return $this->render('form', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    public function actionTime_report_logs($id)
    {
        $modelView = $this->findModelView($id);
        
        $searchModel = new TrTimeReportLogsSearch();
        $dataProvider = $searchModel->search(['tr_id'=>$id]);
        // var_dump($id,$dataProvider);
        return $this->render('TimeReportLogs', [
        'modelView' => $modelView,
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTime_report_logs_detail()
    {
      $post = Yii::$app->request->post();
  
      $id = $post['expandRowKey'];
      $header = TrTimeReportLogs::find()->where(['id' => $id])->one();
    //   $details = AuditTrailDet::find()->where(['audit_trail_id' => $header->id])->all();
      // $details = AuditTrailDet::find()->where(['audit_trail_id' => $header->id])->andWhere("AuditDescription  != '' and AuditDescription is not null")->all();
  
      return $this->renderPartial('TimeReportLogsDetail', [
        'header' => $header,
      ]);
    }   

    public function actionGetjobtimeavailable()
    {
        $html = '';
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $user = CommonHelper::getUserIndentity();

            if (!empty($post['id'])) {
                $id = intval($post['id']);
                $html = $this->Getjobtimeavailable($id);
            }
        }
        return $html;
    }
    public function Getjobtimeavailable($id)
    {
        $user = CommonHelper::getUserIndentity();

        $EmployeeId = $user->user_id; //empty($EmployeeId) ? $user->Id : $EmployeeId;
        $sql = "call common_list('trdet-job-available', '{$EmployeeId};{$id}')";
        // $sql = "call tr_job_list_available({$id}, '{$EmployeeId}')";
        // $sql = "call TrJobTimeAvailable({$id}, '{$EmployeeId}')";
        $data = Yii::$app->db->createCommand($sql)->queryAll();
        $html = "";
        if (!empty($data)) {
            $html .= "<div class='well' style='margin: 10px 0 30px; overflow-x: scroll'>";
            $html .= "<table class='table table-primary'>";
            $html .= "<tr>
                <td class='bg-primary'>Planning</td>
                <td class='bg-primary'>Field Work</td>
                <td class='bg-primary'>Reporting</td>
                <td class='bg-primary'>Wrap Up</td>
                <td class='bg-primary'>Over Time</td>
            </tr>";

            foreach ($data as $d) {
                $html .= "<tr>";
                $html .= "<td>" . $d['available_planning'] . "</td>";
                $html .= "<td>" . $d['available_field_work'] . "</td>";
                $html .= "<td>" . $d['available_reporting'] . "</td>";
                $html .= "<td>" . $d['available_wrap_up'] . "</td>";
                $html .= "<td>" . $d['available_over_time'] . "</td>";
                $html .= "<tr>";
            }

            $html .= "</table>";
            $html .= "</div>";
        } else {
            $html .= "";
        }
        return $html;
    }

    // public function AutoApproveItem($sql, $type)
    public function AutoApproveItem($type, $iid)
    {
        $sql = "call tr_auto_approve_list('" . $type . "'," . $iid . ");";
        $DetailList = Yii::$app->db->createCommand($sql)->queryAll();

        $flag = 1;

        if (!empty($DetailList)) {
            foreach ($DetailList as $d) {
                if ($type == 'detail') {
                    $modelDetail = TimeReportDetail::find()->where(['time_report_id' => $d['tr_id'], 'id' => $d['tr_det_id']])->one();
                } else if ($type == 'meal') {
                    $modelDetail = TimeReportMeals::find()->where(['time_report_id' => $d['tr_id'], 'tr_det_id' => $d['tr_det_id'], 'seq' => $d['seq']])->one();
                } else if ($type == 'ope') {
                    $modelDetail = TimeReportOutOfOffice::find()->where(['time_report_id' => $d['tr_id'], 'tr_det_id' => $d['tr_det_id'], 'seq' => $d['seq']])->one();
                } else if ($type == 'taxi') {
                    $modelDetail = TimeReportTaxi::find()->where(['time_report_id' => $d['tr_id'], 'tr_det_id' => $d['tr_det_id'], 'seq' => $d['seq']])->one();
                }

                if (!empty($modelDetail)) {
                    $modelDetail->approval1 = $d['approval1'];
                    $modelDetail->approval2 = $d['approval2'];

                    if (in_array($d['list_approval1'], [3]) or in_array($d['list_approval2'], [3])) {
                        $modelDetail->approval1 = 0;
                        $modelDetail->approval2 = 0;
                    }
                    if (in_array($d['det_approval1'], [3]) or in_array($d['det_approval2'], [3])) {
                        $modelDetail->approval1 = 0;
                        $modelDetail->approval2 = 0;
                    }
                    $modelDetail->save(false);



                    if (!($flag = $modelDetail->save(false))) {
                        $flag = 0;
                        break;
                    }
                }
            }
            return $flag;
        } else {
            return $flag;
        }
    }

    public function actionUpdatesave($id = null, $EmployeeId = null, $submit = false)
    {
        $post = Yii::$app->request->post();
        $user = CommonHelper::getUserIndentity();
        // var_dump($post['TimeReport']['tr_date']);

        $return = true;
        $EmployeeId = empty($EmployeeId) ? $user->user_id : $EmployeeId;
        $model = TimeReport::find()->with('employee')->where(['employee_id' => $EmployeeId, 'tr_date' => $id])->one();
        if ($submit) {
            $model->tr_status = 1;
        } else {
            $model->tr_status = 0;
        }
        if ($model->load($post)) {
            if ($model->validate()) {
                $transaction = \Yii::$app->db->beginTransaction();
                // try {
                //   $model->save();
                //   if ($model->Status == 1) {
                //     // $msg = $this->AutoApproveItem('detail', $model->ID);
                //     // if ($msg != "") {
                //     //   $return = $msg;
                //     //   $transaction->rollBack();
                //     // }
                //     // $msg = $this->AutoApproveItem('meal', $model->ID);
                //     // if ($msg != "") {
                //     //   $return = $msg;
                //     //   $transaction->rollBack();
                //     // }
                //     // $msg = $this->AutoApproveItem('ope', $model->ID);
                //     // if ($msg != "") {
                //     //   $return = $msg;
                //     //   $transaction->rollBack();
                //     // }
                //     // $msg = $this->AutoApproveItem('taxi', $model->ID);
                //     // if ($msg != "") {
                //     //   $return = $msg;
                //     //   $transaction->rollBack();
                //     // }
                //     $this->AutoApproveItem('detail', $model->ID);
                //     $this->AutoApproveItem('meal', $model->ID);
                //     $this->AutoApproveItem('ope', $model->ID);
                //     $this->AutoApproveItem('taxi', $model->ID);
                //     $transaction->commit();
                //     $return = true;
                //   }
                // } catch (\Exception $e) {
                //     $return = CommonHelper::db_trigger_error($e->getMessage());
                //     // $return = $msg;
                //     $transaction->rollBack();
                //    // $return = \yii\helpers\Html::errorSummary($model, ['encode' => true]);
                //    // $return = CommonHelper::db_trigger_error($e->getMessage());
                // }


                try {
                    if (!($flag = $model->save(false))) {
                        $transaction->rollBack();
                        $return = \yii\helpers\Html::errorSummary($model, ['encode' => true]);
                    } else {
                        if ($model->tr_status == 1) {
                            // $sqlDetailList = "call TrAutoApproveDetailList('" . $model->ID . "')";
                            // if (!($flag = $this->AutoApproveItem($sqlDetailList, 'detail'))) {
                            if (!($flag = $this->AutoApproveItem('detail', $model->id))) {
                                $transaction->rollBack();
                                $return = \yii\helpers\Html::errorSummary($model, ['encode' => true]);
                            }

                            // $sqlMealsList = "call TrAutoApproveMealsList('" . $model->ID . "')";
                            // if (!($flag = $this->AutoApproveItem($sqlMealsList, 'meals'))) {
                            if (!($flag = $this->AutoApproveItem('meal', $model->id))) {
                                $transaction->rollBack();
                                $return = \yii\helpers\Html::errorSummary($model, ['encode' => true]);
                            }

                            // $sqlOutOfOfficeList = "call TrAutoApproveOutOfOfficeList('" . $model->ID . "')";
                            // if (!($flag = $this->AutoApproveItem($sqlOutOfOfficeList, 'outofoffice'))) {
                            if (!($flag = $this->AutoApproveItem('ope', $model->id))) {
                                $transaction->rollBack();
                                $return = \yii\helpers\Html::errorSummary($model, ['encode' => true]);
                            }

                            // $sqlTaxiList = "call TrAutoApproveTaxiList('" . $model->ID . "')";
                            // if (!($flag = $this->AutoApproveItem($sqlTaxiList, 'taxi'))) {
                            if (!($flag = $this->AutoApproveItem('taxi', $model->id))) {
                                $transaction->rollBack();
                                $return = \yii\helpers\Html::errorSummary($model, ['encode' => true]);
                            }
                        }
                    }
                    
                    if ($flag) {
                        if ($submit) {
                            $value_logs = [];
                            $sql_tr_logs = "call tr_list('tr-data','" . $model->employee->user_id .";" . $model->tr_date . "');";
                            $dt_tr_logs = Yii::$app->db->createCommand($sql_tr_logs)->queryOne();
                            if (!($dt_tr_logs)) {
                              $value_logs['tr_status'] = null;
                            } else {
                              $value_logs['tr_status'] = $dt_tr_logs['tr_status'];
                            }

                            $sql_tr_det_logs = "call tr_hint({$model->id})";
                            $dt_tr_det_logs = Yii::$app->db->createCommand($sql_tr_det_logs)->queryAll();
                            // var_dump(json_encode($dt_tr_det_logs));

                            // var_dump($model->tr_date);
                            $model_logs = new TrTimeReportLogs;
                            $model_logs->tr_id = $model->id;
                            $model_logs->tr_date = $model->tr_date;
                            $model_logs->employee_name = $model->employee->user_id . ' - ' . $model->employee->full_name;
                            $model_logs->tr_status = $value_logs['tr_status'];
                            $model_logs->tr_memo = json_encode($dt_tr_det_logs);
                            $model_logs->tr_type = 'ALL TIME REPORT';
                            $model_logs->user_action = 'SUBMIT';
                            $model_logs->created_by = $user->user_id . ' - ' . $user->full_name;
                            $model_logs->save(false);
                        }
                        try {
                            $transaction->commit();
                            $return = true;
                        } catch (\Throwable $th) {
                            $transaction->rollBack();
                            $return = "Error : " . $e;
                        }
                        
                    }
                } catch (ErrorException $e) {
                    $transaction->rollBack();
                    // $return = 'abc';
                    $return = "Error : " . $e;
                }
            } else {
                $return = \yii\helpers\Html::errorSummary($model, ['encode' => true]);
            }
        } else {
            $return = "Can't load post";
        }
        return $return;
    }
    public function GetFormData($model)
    {
        $user = CommonHelper::getUserIndentity();

        $data = [];
        $data['job_id'] = [];

        if ($model->task_type_id == 1) { //if PROJECTS
            $job_id = ($model->isNewRecord) ? 0 : $model->id;
            // $sql_list = "call tr_job_list(" . $model->TimeReportID . ");"; //"call getTrJob('".$user->Id."')";
            $sql_list = "call job_list('tr-job-list','" . $model->employee_id . ";" . $model->tr_date . "');"; //"call getTrJob('".$user->Id."')";
            // $sql_list = "call TrJobList('" . $model->TimeReportID . "', " . $job_id . ")"; //"call getTrJob('".$user->Id."')";
            $list = Yii::$app->db->createCommand($sql_list)->queryAll();
            // var_dump($list);
            // die();

            if (!empty($list)) {
                foreach ($list as $i => $ls) {
                    $data['job_id'][$ls['job_id']] = $ls['job_client_description'];
                }
            }

        } else {
            $data['job_id'][] = "";
        }
        // var_dump($data['job_id']);
        $data['zones'] = empty($model->job_id) ? [] : ArrayHelper::map(
            Yii::$app->db->createCommand("call common_list('tr-zone-list','" . $model->job_id . "')")->queryAll(),
            // Yii::$app->db->createCommand("call common_list('zone','" . $model->JobId . "')")->queryAll(),
            // Yii::$app->db->createCommand("call getJobZone('" . $model->JobId . "')")->queryAll(),
            'id',
            'term_zone_name'
        );

        $data['task_type_id'] = ArrayHelper::map(
            Yii::$app->db->createCommand("call common_list('tr-task-type','" . $user->user_id . "')")->queryAll(),
            // Yii::$app->db->createCommand("call common_list('task-type','" . $user->Id . "')")->queryAll(),
            // Yii::$app->db->createCommand("call getTaskType('" . $user->Id . "')")->queryAll(),
            'id',
            'task_type_name'
        );


        $data['task_id'] = "";
        $sql_tasks = "call common_list('tr-task','" . $user->user_id . ";" . $model->task_type_id . "')";
        if (!empty($model->task_type_id)) {
            $data['task_id'] = ArrayHelper::map(
                Yii::$app->db->createCommand($sql_tasks)->queryAll(),
                'id',
                'task_name'
            );
        }


        ArrayHelper::map(TaskType::find()->where(['flag' => 1])->asArray()->all(), 'id', 'task_type_name');;
        $data['tasks'] = empty($model->task_type_id) ? [] : ArrayHelper::map(Task::find()->where(['task_type_id' => $model->task_type_id])->asArray()->all(), 'task_id', 'task_name');
        return $data;
    }

    //------------------------------------------------------------
    public function GetrulesDetails($modelDetail)
    {
        $JobId = empty($modelDetail->job_id) ? 0 : $modelDetail->job_id;
        $sql = "call common_list('tr-option','" . $modelDetail->tr_date . "')";
        // $sql = "call tr_options('" . $modelDetail->Date . "')";
        // $sql = "call CmOptionTr('" . $modelDetail->Date . "')";
        $data = Yii::$app->db->createCommand($sql)->queryOne();

        $id = empty($modelDetail->id) ? 0 : $modelDetail->id;
        $sql_allocation = "call tr_list('tr-allocation','" . $modelDetail->time_report_id . ";" . $id . "')";
        // $sql_allocation = "call getTimeReportAllocation('" . $modelDetail->TimeReportID . "', " . $id . ")";
        $data_allocation = Yii::$app->db->createCommand($sql_allocation)->queryOne();

        $modelDetail->work_hour = empty($modelDetail->work_hour) ? 0 : CommonHelper::ResetDecimal($modelDetail->work_hour);
        $modelDetail->over_time = empty($modelDetail->over_time) ? 0 : CommonHelper::ResetDecimal($modelDetail->over_time);

        $modelDetail->max_over_time = $data['max_over_time'];
        $modelDetail->max_work_hour = $data['max_work_hour'];
        $modelDetail->min_work_hour = $data['min_work_hour'];

        $data_allocation['over_time'] = empty($data_allocation['over_time']) ? 0 : $data_allocation['over_time'];
        $data_allocation['work_hour'] = empty($data_allocation['work_hour']) ? 0 : $data_allocation['work_hour'];

        $modelDetail->over_time_allocation = $modelDetail->over_time + $data_allocation['over_time'];
        $modelDetail->work_hour_allocation = $modelDetail->work_hour + $data_allocation['work_hour'];

        return $modelDetail;
    }

    public function actionDetail($id = null)
    {
        Yii::$app->cache->flush();
        Yii::$app->db->schema->refresh();

        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $user = CommonHelper::getUserIndentity();
            $data = [];
            $TimeAvailable = "";

            $model = new TimeReport();
            $modelDetail = new TimeReportDetail();
            if ($model->load($post)) {
                if (!empty($id)) {
                    $modelDetail = TimeReportDetail::find()->where(['id' => $id])->one();
                    if ($modelDetail->job_id != null) {
                        $TimeAvailable = $this->Getjobtimeavailable($modelDetail->job_id);
                    }
                }

                $work_hour = 0;
                if (!empty($model->work_hour)) {
                  $work_hour = $model->work_hour;
                }

                // var_dump($work_hour, $model->work_hour);
                // die();

                $modelDetail->time_report_id = $model->id;
                $modelDetail->employee_id = $model->employee_id;
                $modelDetail->tr_date = $model->tr_date;
                $modelDetail->is_stayed = $model->is_stayed;

                $rule = Yii::$app->db->createCommand("select is_true('task-add','{$modelDetail->tr_date};{$work_hour}') as 'valid'")->queryOne();
                // $rule = Yii::$app->db->createCommand("select is_true('task-add','{$modelDetail->TimeReportID}', null) as 'Valid'")->queryOne();
                // $rule = Yii::$app->db->createCommand("select TrRuleTaskAdd('{$modelDetail->TimeReportID}') as 'Valid'")->queryOne();

                $valid = $rule['valid'];
                $valid = ($modelDetail->isNewRecord) ? $valid : 1;
                if ($valid) {
                    $data = $this->GetFormData($modelDetail);
                    return $this->renderAjax('formDetail', [
                        'modelDetail' => $modelDetail,
                        'data' => $data,
                        'TimeAvailable' => $TimeAvailable
                    ]);
                } else {
                    $return = "<div class='alert alert-danger'>Cannot add a new Task because it has reached its maximum limit</div>";
                    $return .= '<button type="button" class="btn btn-info p-t-10 p-b-10 pull-right" style="font-size: 12px" onclick="CloseModal()">CLOSE</button>';
                    return $return;
                }
            } else {
                return "Can't load data ...";
            }
        } else {
            throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
        }
    }
    public function actionOvertime($id = null)
    {
        Yii::$app->cache->flush();
        Yii::$app->db->schema->refresh();

        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $user = CommonHelper::getUserIndentity();
            $data = [];

            $model = new TimeReport();
            $modelDetail = new TimeReportDetail();
            if ($model->load($post)) {
                if (!empty($id)) {
                    $modelDetail = TimeReportDetail::find()->where(['id' => $id])->one();
                }

                $work_hour = 0;
                if (!empty($model->work_hour)) {
                  $work_hour = $model->work_hour;
                }

                $modelDetail->time_report_id = $model->id;
                $modelDetail->employee_id = $model->employee_id;
                $modelDetail->tr_date = $model->tr_date;
                $modelDetail->is_stayed = $model->is_stayed;

                $rule = Yii::$app->db->createCommand("select is_true('task-add','{$modelDetail->tr_date};{$work_hour}') as 'valid'")->queryOne();
                // $rule = Yii::$app->db->createCommand("select is_true('task-add','{$modelDetail->TimeReportID}', null) as 'Valid'")->queryOne();
                // $rule = Yii::$app->db->createCommand("select TrRuleTaskAdd('{$modelDetail->TimeReportID}') as 'Valid'")->queryOne();
                $valid = $rule['valid'];
                $valid = ($modelDetail->isNewRecord) ? $valid : 1;
                if ($valid) {
                    $data = $this->GetFormData($modelDetail);
                    return $this->renderAjax('formOvertime', [
                        'modelDetail' => $modelDetail,
                        'data' => $data,
                    ]);
                } else {
                    $return = "<div class='alert alert-danger'>Cannot add a new Task because it has reached its maximum limit</div>";
                    $return .= '<button type="button" class="btn btn-info p-t-10 p-b-10 pull-right" style="font-size: 12px" onclick="CloseModal()">CLOSE</button>';
                    return $return;
                }
            } else {
                return "Can't load data ...";
            }
        } else {
            throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
        }
    }
    public function actionDetailsave()
    {
        $post = Yii::$app->request->post();
        $user = CommonHelper::getUserIndentity();

        $model = new TimeReport();
        $modelDetail = new TimeReportDetail();
        $model_old = $modelDetail;

        if ($modelDetail->load($post)) {
            if (!empty($modelDetail->id)) {
                $modelDetail = TimeReportDetail::findOne(['id' => $modelDetail->id]);
                $model_old = TimeReportDetail::findOne(['id' => $modelDetail->id]);
                $modelDetail->updated_by = $user->user_id;
            } else {
              $modelDetail->created_by = $user->user_id;
            }

            $modelDetail->load($post);

            $filename = 'trd_' . $model->id;
            $modelDetail->attachment = Helper::upload_model($modelDetail, 'attachment', $model_old->attachment, 'timereport_detail/', $filename . '_attachment_');

            $model = TimeReport::findOne(['id' => $modelDetail->time_report_id]);
            $modelDetail->tr_date = $model->tr_date;
            $modelDetail->employee_id = $model->employee_id;
            $modelDetail->tr_detail_value = $model->employee_rate * $modelDetail->work_hour;
            $modelDetail = $this->GetrulesDetails($modelDetail);

            if ($modelDetail->validate()) {
                $modelDetail->job_id = empty($modelDetail->job_id) ? null : $modelDetail->job_id;
                try {
                    $modelDetail->save();
                    $return = true;
                } catch (\Exception $e) {
                    $message = explode('___', $e->getMessage());
                    $return = $message[1];
                }
            } else {
                $return = \yii\helpers\Html::errorSummary($modelDetail, ['encode' => true]);
            }
        } else {
            $return = "Error : Can't load model";
        }
        return $return;
    }
    public function actionDetaildelete($id)
    {
        $post = Yii::$app->request->post();

        $return = false;
        if (!empty($id)) {
            $model = TimeReportDetail::find()->where(['id' => $id])->one();
            if (!empty($model)) {
                try {
                    $model->delete();
                    return true;
                } catch (\Exception $e) {
                    $message = explode('___', $e->getMessage());
                    return $message[1];
                }
            }
        }
        echo $return;
    }



    public function actionMeals($id = null, $seq = null)
    {
        Yii::$app->cache->flush();
        Yii::$app->db->schema->refresh();

        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $user = CommonHelper::getUserIndentity();
            $data = [];
            $model = new TimeReport();
            $modelMeals = new TimeReportMeals();
            if ($model->load($post)) {
                if (!empty($id) && !empty($seq)) {
                    $modelMeals = TimeReportMeals::find()->where(['time_report_id' => $id, 'seq' => $seq])->one();
                }
                $modelMeals->time_report_id = $model->id;
                $modelMeals->tr_det_id = ($modelMeals->isNewRecord) ? '' : $modelMeals->tr_det_id;
                $modelMeals->employee_id = $model->employee_id;
                $modelMeals->tr_date = $model->tr_date;

                // $sql_rule = "select is_true('meal-add','{$modelMeals->tr_date};{$modelMeals->employee_id}') as valid";

                $rule = Yii::$app->db->createCommand("select is_true('meal-add','{$modelMeals->tr_date};{$modelMeals->employee_id}') as 'valid'")->queryOne();
                // $rule = Yii::$app->db->createCommand("select is_true('meal-add','{$modelMeals->TimeReportID}', null) as 'Valid'")->queryOne();
                // $rule = Yii::$app->db->createCommand("select TrRuleMealAdd('{$modelMeals->TimeReportID}') as 'Valid'")->queryOne();

                $valid = $rule['valid'];
                $valid = ($modelMeals->isNewRecord) ? $valid : 1;
                if ($valid) {
                    $TrDetID_lookups = empty($modelMeals->tr_det_id) ? 0 : $modelMeals->tr_det_id;
                    $sql_lookups = "call job_list('tr-job-meal','{$modelMeals->time_report_id};{$TrDetID_lookups}')";
                    // $sql_lookups = "call trdet_update_list('meal','{$modelMeals->TimeReportID}', {$TrDetID_lookups}, null)";
                    // $sql_lookups = "call TrJobMealList('{$modelMeals->TimeReportID}', {$TrDetID_lookups})";

                    $lookups = Yii::$app->db->createCommand($sql_lookups)->queryAll();
                    // var_dump($lookups);
                    // die();

                    $data['lookup'] = [];
                    if (!empty($lookups)) {
                        foreach ($lookups as $lookup) {
                            $data['lookup'][$lookup['tr_det_id']] = $lookup['description'];
                        }
                    }

                    return $this->renderAjax('formMeals', [
                        'modelMeals' => $modelMeals,
                        'data' => $data,
                    ]);
                } else {
                    $return = "<div class='alert alert-danger'>Cannot add a new Meals because it has reached its maximum limit</div>";
                    $return .= '<button type="button" class="btn btn-info p-t-10 p-b-10 pull-right" style="font-size: 12px" onclick="CloseModal()">CLOSE</button>';
                    return $return;
                }
            } else {
                return "Can't load data ...";
            }
        } else {
            throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
        }
    }
    public function actionMealssave()
    {
        $post = Yii::$app->request->post();
        $user = CommonHelper::getUserIndentity();
        Yii::$app->cache->flush();
        $modelMeals = new TimeReportMeals();
        if ($modelMeals->load($post)) {
            if (!empty($modelMeals->seq)) {
                $modelMeals = TimeReportMeals::find()->where(['time_report_id' => $modelMeals->time_report_id, 'seq' => $modelMeals->seq])->one();
            }
            $modelMeals->load($post);
            $modelDetail = TimeReportDetail::find()->where(['time_report_id' => $modelMeals->time_report_id, 'id' => $modelMeals->tr_det_id])->one();
            $modelMeals->approval1 = 0;
            $modelMeals->tr_date = $modelDetail->tr_date;
            $modelMeals->employee_id = $modelDetail->employee_id;
            $modelMeals->task_type_id = $modelDetail->task_type_id;
            $modelMeals->task_id = $modelDetail->task_id;
            $modelMeals->job_id = $modelDetail->job_id;
            $modelMeals->approval2 = 0;

            if ($modelMeals->validate()) {

                try {
                    $modelMeals->save();
                    return true;
                } catch (\Exception $e) {
                    $message = explode('___', $e->getMessage());
                    $return = $message[1];
                }
            } else {
                $return = \yii\helpers\Html::errorSummary($modelMeals, ['encode' => true]);
            }
        } else {
            $return = "Error : Can't load model";
        }
        return $return;
    }
    public function actionMealsdelete($id, $seq)
    {
        $post = Yii::$app->request->post();

        $return = false;
        if (!empty($id) && !empty($seq)) {
            $model = TimeReportMeals::find()->where(['time_report_id' => $id, 'seq' => $seq])->one();
            if (!empty($model)) {
                $return = $model->delete();
            }
        }
        return $return;
    }


    public function actionOpe($id = null, $seq = null)
    {
        Yii::$app->cache->flush();
        Yii::$app->db->schema->refresh();

        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $user = CommonHelper::getUserIndentity();
            $data = [];

            $model = new TimeReport();
            $modelOutOfOffice = new TimeReportOutOfOffice();
            if ($model->load($post)) {
                if (!empty($id) && !empty($seq)) {
                    $modelOutOfOffice = TimeReportOutOfOffice::find()->where(['time_report_id' => $id, 'seq' => $seq])->one();
                }
                $modelOutOfOffice->time_report_id = $model->id;
                $modelOutOfOffice->tr_date = $model->tr_date;
                $modelOutOfOffice->employee_id = $model->employee_id;
                $modelOutOfOffice->tr_det_id = ($modelOutOfOffice->isNewRecord) ? 0 : $modelOutOfOffice->tr_det_id;

                $rule = Yii::$app->db->createCommand("select is_true('ope-add','{$modelOutOfOffice->tr_date};{$modelOutOfOffice->employee_id}') as 'valid'")->queryOne();
                // $rule = Yii::$app->db->createCommand("select is_true('ope-add','{$modelOutOfOffice->TimeReportID}', null) as 'Valid'")->queryOne();
                // $rule = Yii::$app->db->createCommand("select TrRuleOutOffAdd('{$modelOutOfOffice->TimeReportID}') as 'Valid'")->queryOne();

                // $valid = 1;
                $valid = $rule['valid'];
                $valid = ($modelOutOfOffice->isNewRecord) ? $valid : 1;

                if ($valid) {
                    $sql_lookups = "call job_list('tr-job-ope','{$modelOutOfOffice->time_report_id};{$modelOutOfOffice->tr_det_id}')";

                    // $sql_lookups = "call trdet_update_list('ope','{$modelOutOfOffice->TimeReportID}', {$modelOutOfOffice->TrDetID}, null)";
                    // var_dump('a');
                    // die();
                    $lookups = Yii::$app->db->createCommand($sql_lookups)->queryAll();
                    // $lookups = Yii::$app->db->createCommand("call TrJobOutOffList('{$modelOutOfOffice->TimeReportID}', '{$modelOutOfOffice->TrDetID}')")->queryAll();
                    $data['lookup'] = [];
                    $data['Zone'] = [];
                    foreach ($lookups as $lookup) {
                        if ($modelOutOfOffice->tr_det_id == $lookup['tr_det_id']) {
                            $JobArea = explode(',', $lookup['job_area']);
                            foreach ($JobArea as $Area) {
                                $data['Zone'][$Area] = $Area;
                            }
                            $data['Zone']['OUT OF TOWN'] = 'OUT OF TOWN';

                        }
                        $data['lookup'][$lookup['tr_det_id']] = $lookup['description'];
                    }

                    return $this->renderAjax('formOPE', [
                        'modelOutOfOffice' => $modelOutOfOffice,
                        'data' => $data,
                    ]);
                } else {
                    $return = "<div class='alert alert-danger'>Cannot add a new Out Of Office because it has reached its maximum limit</div>";
                    $return .= '<button type="button" class="btn btn-info p-t-10 p-b-10 pull-right" style="font-size: 12px" onclick="CloseModal()">CLOSE</button>';
                    return $return;
                }
            } else {
                return "Can't load data ...";
            }
        } else {
            throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
        }
    }
    public function actionOpesave()
    {

      $post = Yii::$app->request->post();
      $user = CommonHelper::getUserIndentity();
      Yii::$app->cache->flush();
      $modelOutOfOffice = new TimeReportOutOfOffice();
      if ($modelOutOfOffice->load($post)) {
          if (!empty($modelOutOfOffice->seq)) {
              $modelOutOfOffice = TimeReportOutOfOffice::find()->where(['time_report_id' => $modelOutOfOffice->time_report_id, 'seq' => $modelOutOfOffice->seq])->one();
          }
          $modelOutOfOffice->load($post);
          $modelDetail = TimeReportDetail::find()->where(['time_report_id' => $modelOutOfOffice->time_report_id, 'id' => $modelOutOfOffice->tr_det_id])->one();
          // $modelMeals->approval1 = 0;
          // $modelMeals->tr_date = $modelDetail->tr_date;
          // $modelMeals->employee_id = $modelDetail->employee_id;
          // $modelMeals->task_type_id = $modelDetail->task_type_id;
          // $modelMeals->task_id = $modelDetail->task_id;
          // $modelMeals->job_id = $modelDetail->job_id;
          // $modelMeals->approval2 = 0;
          $modelOutOfOffice->approval1 = 0;
          $modelOutOfOffice->tr_date = $modelDetail->tr_date;
          $modelOutOfOffice->employee_id = $modelDetail->employee_id;
          $modelOutOfOffice->task_type_id = $modelDetail->task_type_id;
          $modelOutOfOffice->task_id = $modelDetail->task_id;
          $modelOutOfOffice->job_id = $modelDetail->job_id;
          $modelOutOfOffice->inter_city = 0;
          if ($modelOutOfOffice->zone_id == 'OUT OF TOWN') {
              $modelOutOfOffice->zone_id = 'OUT OF TOWN';
              $modelOutOfOffice->inter_city = 1;
          }
          $modelOutOfOffice->approval2 = 0;


          if ($modelOutOfOffice->validate()) {

              try {
                  $modelOutOfOffice->save();
                  return true;
              } catch (\Exception $e) {
                  $message = explode('___', $e->getMessage());
                  $return = $message[1];
              }
          } else {
              $return = \yii\helpers\Html::errorSummary($modelOutOfOffice, ['encode' => true]);
          }
      } else {
          $return = "Error : Can't load model";
      }
      return $return;


        // $post = Yii::$app->request->post();
        // $user = CommonHelper::getUserIndentity();
        //
        // $modelOutOfOffice = new TimeReportOutOfOffice();
        // if ($modelOutOfOffice->load($post)) {
        //     if (!empty($modelOutOfOffice->Seq)) {
        //         $modelOutOfOffice = TimeReportOutOfOffice::find()->where(['time_report_id' => $modelOutOfOffice->time_report_id, 'seq' => $modelOutOfOffice->seq])->one();
        //     }
        //     $modelOutOfOffice->load($post);
        //     // var_dump($modelOutOfOffice);
        //     if ($modelOutOfOffice->validate()) {
        //
        //         $transaction = \Yii::$app->db->beginTransaction();
        //         $modelDetail = TimeReportDetail::find()->where(['time_report_id' => $modelOutOfOffice->time_report_id, 'id' => $modelOutOfOffice->tr_det_id])->one();
        //         $modelOutOfOffice->approval1 = 0;
        //         $modelOutOfOffice->tr_date = $modelDetail->tr_date;
        //         $modelOutOfOffice->employee_id = $modelDetail->employee_id;
        //         $modelOutOfOffice->task_type_id = $modelDetail->task_type_id;
        //         $modelOutOfOffice->task_id = $modelDetail->task_id;
        //         $modelOutOfOffice->job_id = $modelDetail->job_id;
        //         $modelOutOfOffice->inter_city = 0;
        //         if ($modelOutOfOffice->zone_id == 'OUT OF TOWN') {
        //             $modelOutOfOffice->zone_id = 'OUT OF TOWN';
        //             $modelOutOfOffice->inter_city = 1;
        //             // $modelOutOfOffice->OutOfOfficeValue = 50000;
        //         }
        //
        //         /*
        //         if(!empty($modelOutOfOffice->TimeReportID) && !empty($modelOutOfOffice->TrDetID)){
        //             $cekBoss = Yii::$app->db->createCommand("select ifnull(antHrIsNotBoss(".$modelOutOfOffice->TimeReportID.", ".$modelOutOfOffice->TrDetID."), 0) as IsNotBoss")->queryOne();
        //             if($cekBoss['IsNotBoss']==0){
        //                 $modelOutOfOffice->Approval1 = 1;
        //             }
        //         }
        //         */
        //         $modelOutOfOffice->approval2 = 0;
        //
        //         try {
        //             if (!($flag = $modelOutOfOffice->save(false))) {
        //                 $transaction->rollBack();
        //                 $return = \yii\helpers\Html::errorSummary($modelOutOfOffice, ['encode' => true]);
        //             }
        //
        //             if ($flag) {
        //                 $transaction->commit();
        //                 $return = true;
        //             }
        //         } catch (Exception $e) {
        //             $transaction->rollBack();
        //             $return = "Error : " . $e;
        //         }
        //     } else {
        //         $return = \yii\helpers\Html::errorSummary($modelOutOfOffice, ['encode' => true]);
        //     }
        // } else {
        //     $return = "Error : Can't load model";
        // }
        // return $return;
    }

    public function actionOpedelete($id, $seq)
    {
        $post = Yii::$app->request->post();

        $return = false;
        if (!empty($id) && !empty($seq)) {
            $model = TimeReportOutOfOffice::find()->where(['time_report_id' => $id, 'seq' => $seq])->one();
            if (!empty($model)) {
                $return = $model->delete();
            }
        }


        return $return;
    }

    public function actionTaxi($id = null, $seq = null)
    {
        Yii::$app->cache->flush();
        Yii::$app->db->schema->refresh();

        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $user = CommonHelper::getUserIndentity();
            $data = [];

            $model = new TimeReport();
            $modelTaxi = new TimeReportTaxi();
            if ($model->load($post)) {

                if (!empty($id) && !empty($seq)) {
                    $modelTaxi = TimeReportTaxi::find()->where(['time_report_id' => $id, 'seq' => $seq])->one();
                }

                $modelTaxi->time_report_id = $model->id;
                $modelTaxi->employee_id = $model->employee_id;
                $modelTaxi->tr_date = $model->tr_date;
                // $modelTaxi->isStayed = $model->isStayed;

                $data['tasks'] = [];
                if (!empty($modelTaxi->taxi_id)) {
                    $sql_class = "call job_list('tr-job-taxi-class','{$model->id};{$modelTaxi->taxi_id}')";
                    $class = Yii::$app->db->createCommand($sql_class)->queryAll();
                    // $class = Yii::$app->db->createCommand("call common_list('taxi-class',$modelTaxi->cmTaxiID)")->queryAll();
                    // $class = Yii::$app->db->createCommand("call getTrTaxiClassList($modelTaxi->cmTaxiID)")->queryAll();
                    foreach ($class as $c) {
                        $data['tasks'][$c['id']] = $c['task_type_name'];
                    }
                }

                $data['lookup'] = [];
                if (!empty($modelTaxi->taxi_id)) {
                    $tr_det_id = empty($modelTaxi->tr_det_id) ? 0 : $modelTaxi->tr_det_id;
                    $sql = "call job_list('tr-job-taxi-list','{$model->id};$modelTaxi->tr_det_id;{$modelTaxi->taxi_id}')";
                    // var_dump($sql);
                    // $sql = "call trdet_update_list('taxi',{$modelTaxi->TimeReportID}, {$TrDetID}, {$modelTaxi->cmTaxiID})";
                    // $sql = "call TrJobTaxiList({$modelTaxi->TimeReportID}, {$modelTaxi->cmTaxiID}, {$TrDetID})";
                    $lookups = Yii::$app->db->createCommand($sql)->queryAll();
                    foreach ($lookups as $lookup) {
                        $data['lookup'][$lookup['tr_det_id']] = $lookup['description'];
                    }
                }

                $sql_boss = "select fis_not_boss('{$modelTaxi->employee_id}') as can_claim";
                // $sql_boss = "select is_true('is-not-boss-emp',null,'{$modelTaxi->EmployeeId}') as CanClaim";
                // $sql_boss = "select antTrCanClaimTaxi('{$modelTaxi->EmployeeId}') as CanClaim";
                $boss = Yii::$app->db->createCommand($sql_boss)->queryOne();
                $CanClaim = empty($boss['can_claim']) ? 0 : 1;
                if (intval($CanClaim) != 1) {
                    $data['taxi_id'] = ArrayHelper::map(cmTaxi::find()->where('is_over_time!=1')->asArray()->all(), 'id', 'taxi_name');
                } else {
                    $data['taxi_id'] = ArrayHelper::map(cmTaxi::find()->where('id!=7')->asArray()->all(), 'id', 'taxi_name');
                }

                return $this->renderAjax('formTaxi', [
                    'modelTaxi' => $modelTaxi,
                    'data' => $data,
                ]);
            } else {
                return "Can't load data ...";
            }
        } else {
            throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
        }
    }
    public function actionTaxisave()
    {

      $post = Yii::$app->request->post();
      $user = CommonHelper::getUserIndentity();
      Yii::$app->cache->flush();
      $modelTaxi = new TimeReportTaxi();
      if ($modelTaxi->load($post)) {
          if (!empty($modelTaxi->seq)) {
              $modelTaxi = TimeReportTaxi::find()->where(['time_report_id' => $modelTaxi->time_report_id, 'seq' => $modelTaxi->seq])->one();
          }

          $modelTaxi->load($post);
          $modelTaxi->amount = CommonHelper::ResetDecimal($modelTaxi->amount);
          $modelDetail = TimeReportDetail::find()->where(['time_report_id' => $modelTaxi->time_report_id, 'id' => $modelTaxi->tr_det_id])->one();
          $modelTaxi->approval1 = 0;
          $modelTaxi->tr_date = $modelDetail->tr_date;
          $modelTaxi->employee_id = $modelDetail->employee_id;
          $modelTaxi->task_type_id = $modelDetail->task_type_id;
          $modelTaxi->task_id = $modelDetail->task_id;
          $modelTaxi->job_id = $modelDetail->job_id;
          $modelTaxi->approval2 = 0;

          // $modelOutOfOffice->approval1 = 0;
          // $modelOutOfOffice->tr_date = $modelDetail->tr_date;
          // $modelOutOfOffice->employee_id = $modelDetail->employee_id;
          // $modelOutOfOffice->task_type_id = $modelDetail->task_type_id;
          // $modelOutOfOffice->task_id = $modelDetail->task_id;
          // $modelOutOfOffice->job_id = $modelDetail->job_id;
          // $modelOutOfOffice->inter_city = 0;
          // if ($modelOutOfOffice->zone_id == 'OUT OF TOWN') {
          //     $modelOutOfOffice->zone_id = 'OUT OF TOWN';
          //     $modelOutOfOffice->inter_city = 1;
          // }
          // $modelOutOfOffice->approval2 = 0;


          if ($modelTaxi->validate()) {
              try {
                  $modelTaxi->save();
                  return true;
              } catch (\Exception $e) {
                  $message = explode('___', $e->getMessage());
                  $return = $message[1];
              }
          } else {
              $return = \yii\helpers\Html::errorSummary($modelTaxi, ['encode' => true]);
          }
      } else {
          $return = "Error : Can't load model";
      }
      return $return;

        // $post = Yii::$app->request->post();
        // $user = CommonHelper::getUserIndentity();
        //
        // $modelTaxi = new TimeReportTaxi();
        //
        // if ($modelTaxi->load($post)) {
        //     if (!empty($modelTaxi->seq)) {
        //         $modelTaxi = TimeReportTaxi::find()->where(['time_report_id' => $modelTaxi->time_report_id, 'seq' => $modelTaxi->seq])->one();
        //     }
        //
        //     $modelTaxi->load($post);
        //     $modelTaxi->amount = CommonHelper::ResetDecimal($modelTaxi->amount);
        //     if ($modelTaxi->validate()) {
        //         $modelDetail = TimeReportDetail::find()->where(['time_report_id' => $modelTaxi->time_report_id, 'id' => $modelTaxi->tr_det_id])->one();
        //         $modelTaxi->approval1 = 0;
        //         $modelTaxi->tr_date = $modelDetail->tr_date;
        //         $modelTaxi->employee_id = $modelDetail->employee_id;
        //         $modelTaxi->task_type_id = $modelDetail->task_type_id;
        //         $modelTaxi->task_id = $modelDetail->task_id;
        //         $modelTaxi->job_id = $modelDetail->job_id;
        //         /*
        //         if(!empty($modelTaxi->TimeReportID) && !empty($modelTaxi->TrDetID)){
        //             $cekBoss = Yii::$app->db->createCommand("select ifnull(antHrIsNotBoss(".$modelTaxi->TimeReportID.", ".$modelTaxi->TrDetID."), 0) as IsNotBoss")->queryOne();
        //             if($cekBoss['IsNotBoss']==0){
        //                 $modelTaxi->Approval1 = 1;
        //             }
        //         }
        //         */
        //         $modelTaxi->approval2 = 0;
        //         try {
        //             $modelTaxi->save();
        //             return true;
        //         } catch (\Exception $e) {
        //             $message = explode('___', $e->getMessage());
        //             $return = $message[1];
        //         }
        //     } else {
        //         $return = \yii\helpers\Html::errorSummary($modelTaxi, ['encode' => true]);
        //     }
        // } else {
        //     $return = "Error : Can't load model";
        // }
        // return $return;
    }

    public function actionTaxidelete($id, $seq)
    {
        $post = Yii::$app->request->post();

        $return = false;
        if (!empty($id) && !empty($seq)) {
            $model = TimeReportTaxi::find()->where(['time_report_id' => $id, 'seq' => $seq])->one();
            if (!empty($model)) {
                $return = $model->delete();
            }
        }
        return $return;
    }



    public function actionComments($tr, $id, $type)
    {
        Yii::$app->cache->flush();
        Yii::$app->db->schema->refresh();
        $user = CommonHelper::getUserIndentity();

        $modelComments = new TimeReportNote();
        $modelComments->time_report_id = $tr;
        $modelComments->parent_id = $id;
        $modelComments->comment_type = $type;
        $for = Yii::$app->db->createCommand("select ifnull(comment_for(" . $modelComments->time_report_id . ", " . $modelComments->parent_id . "), 1) as f")->queryOne();
        $modelComments->comment_for = empty($for['f']) ? 1 : $for['f'];

        $forLookUps = Yii::$app->db->createCommand("select ifnull(comment_for_lookup(" . $modelComments->time_report_id . ", " . $modelComments->parent_id . ", '" . $user->user_id . "'), 1) as fl")->queryOne();
        $forLookUps = empty($forLookUps['fl']) ? 0 : $forLookUps['fl'];

        if ($forLookUps == 2) {
            $forLookUp = [2 => 'Junior'];
        } elseif ($forLookUps == 1) {
            $forLookUp = [1 => 'Junior'];
        } else {
            $forLookUp = [1 => 'Supervisor', 2 => 'Manager'];
        }

        $searchmodelComments = new TimeReportNoteSearch([
            'time_report_id' => $modelComments->time_report_id,
            'parent_id' => $modelComments->parent_id,
            'comment_type' => $modelComments->comment_type,
            'comment_for' => $modelComments->comment_for,
        ]);
        $dataProviderComments = $searchmodelComments->search([]);

        $list = $this->renderPartial('formCommentsDetail', [
            'dataProviderComments' => $dataProviderComments,
        ]);
        return $this->renderAjax('formComments', [
            'list' => $list,
            'modelComments' => $modelComments,
            'dataProviderComments' => $dataProviderComments,
            'for' => $for,
            'forLookUp' => $forLookUp,
        ]);
    }

    public function actionCommentssave()
    {
        $user = CommonHelper::getUserIndentity();
        $post = Yii::$app->request->post();
        $modelComments = new TimeReportNote();
        if ($modelComments->load($post)) {
            $modelComments->employee_id = $user->user_id;
            $modelComments->comment_date = date('Y-m-d H:i:s');

            if ($modelComments->save()) {
                $this->CommentsEmail($modelComments);
                $searchmodelComments = new TimeReportNoteSearch([
                    'time_report_id' => $modelComments->time_report_id,
                    'parent_id' => $modelComments->parent_id,
                    'comment_type' => $modelComments->comment_type,
                    'comment_for' => $modelComments->comment_for,

                ]);
                $dataProviderComments = $searchmodelComments->search([]);

                $return = $this->renderAjax('formCommentsDetail', [
                    'dataProviderComments' => $dataProviderComments,
                ]);
            } else {
                $return = "error : cant save data";
            }
        } else {
            $return = "error : cant load post";
        }

        return $return;
    }
    public function actionCommentsrefresh()
    {
        $user = CommonHelper::getUserIndentity();
        $post = Yii::$app->request->post();
        $modelComments = new TimeReportNote();
        if ($modelComments->load($post)) {
            $modelComments->employee_id = $user->user_id;
            $modelComments->comment_date = date('Y-m-d H:i:s');

            $searchmodelComments = new TimeReportNoteSearch([
                'time_report_id' => $modelComments->time_report_id,
                'parent_id' => $modelComments->parent_id,
                'comment_type' => $modelComments->comment_type,
                'comment_for' => $modelComments->comment_for,

            ]);
            $dataProviderComments = $searchmodelComments->search([]);

            $return = $this->renderAjax('formCommentsDetail', [
                'dataProviderComments' => $dataProviderComments,
            ]);
        } else {
            $return = "error : cant load post";
        }

        return $return;
    }
    public function CommentsEmail($model)
    {
        if (!empty($model->Id)) {
            $modelComments = TimeReportNote::find()->where(['Id' => $model->Id])->one();

            if (!empty($modelComments)) {
                $sql = "CALL tr_comment_email({$modelComments->TimeReportID}, {$modelComments->ParentID})";
                $lookups = Yii::$app->db->createCommand($sql)->queryAll();

                $email = [];
                if (!empty($lookups)) {
                    foreach ($lookups as $r) {
                        if (!empty($r['EmployeeEmail'])) {
                            if (filter_var($r['EmployeeEmail'], FILTER_VALIDATE_EMAIL)) {
                                $email[$r['EmployeeEmail']] = $r['EmployeeEmail'];
                            }
                        }

                        if (!empty($r['SupervisorEmail'])) {
                            if (filter_var($r['SupervisorEmail'], FILTER_VALIDATE_EMAIL)) {
                                $email[$r['SupervisorEmail']] = $r['SupervisorEmail'];
                            }
                        }

                        if (!empty($r['ManagerEmail'])) {
                            if (filter_var($r['ManagerEmail'], FILTER_VALIDATE_EMAIL)) {
                                $email[$r['ManagerEmail']] = $r['ManagerEmail'];
                            }
                        }
                    }
                }
                return $this->Email(
                    $email,
                    'Crowe TimeReport - New Comment in your TimeReport #' . $modelComments->TimeReportID,
                    ['model' => $modelComments]
                );
            }
        }
    }

    public function actionGetattachment()
    {
        $post = Yii::$app->request->post();
        $return = 0;
        if (!empty($post['id'])) {
            $id = $post['id'];
            $task = Task::find()->where(['id' => $id])->one();
            if (!empty($task->id)) {
                $return = $task->is_attachment;
            }
        }

        return $return;
    }
    public function Email($sendto, $subject, $param, $layouts = 'layouts/newcomments')
    {
        Yii::$app->cache->flush();

        $from = Yii::$app->params['appNotificationEmail'];
        $cc = Yii::$app->params['appNotificationEmailCC'];

        $to = $sendto;

        if (!empty($to)) {
            $html = ['html' => $layouts];

            $compose = Yii::$app->mailer->compose($html, $param);
            $compose->setFrom($from);
            $compose->setTo($to);
            if (!empty($cc)) {
                $compose->setCc($cc);
            }
            $compose->setSubject($subject);

            $compose->send();
        }
    }





    public function actionOot($id = null, $job_id = null)
    {
        Yii::$app->cache->flush();
        Yii::$app->db->schema->refresh();

        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $user = CommonHelper::getUserIndentity();
            $data = [];

            $model = new TimeReport();
            $modelOutOfTown = new TimeReportOutOfTown();
            if ($model->load($post)) {
                if (!empty($id) && !empty($job_id)) {
                    $modelOutOfTown = TimeReportOutOfTown::find()->where(['tr_id' => $id, 'job_id' => $job_id])->one();
                } else {
                    $modelOutOfTown = new TimeReportOutOfTown();
                }

                $modelOutOfTown->tr_id = $model->ID;
                $modelOutOfTown->tr_date = $model->tr_date;
                $modelOutOfTown->employee_id = $model->EmployeeId;

                $sql_job = "call tr_job_list_out_of_town('{$modelOutOfTown->tr_id}')";
                $jobs = Yii::$app->db->createCommand($sql_job)->queryAll();
                $data['jobs'] = [];
                foreach ($jobs as $job) {
                    $data['jobs'][$job['job_id']] = $job['job_description'];
                }

                return $this->renderAjax('formOOT', [
                    'modelOutOfTown' => $modelOutOfTown,
                    'data' => $data,
                ]);
            } else {
                return "Can't load data ...";
            }
        } else {
            throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
        }
    }
    public function actionOotsave()
    {
        $post = Yii::$app->request->post();
        $modelOutOfTown = new TimeReportOutOfTown();
        if ($modelOutOfTown->load($post)) {
            $modelOutOfTown = TimeReportOutOfTown::find()->where(['tr_id' => $modelOutOfTown->tr_id, 'job_id' => $modelOutOfTown->job_id])->one();
            if (empty($modelOutOfTown->job_id)) {
                $modelOutOfTown = new TimeReportOutOfTown();
            }

            $modelOutOfTown->load($post);
            if ($modelOutOfTown->validate()) {
                $transaction = \Yii::$app->db->beginTransaction();
                $model = TimeReport::find()->where(['ID' => $modelOutOfTown->tr_id])->one();
                $modelOutOfTown->tr_id = $model->ID;
                $modelOutOfTown->tr_date = $model->tr_date;
                $modelOutOfTown->employee_id = $model->EmployeeId;
                $modelOutOfTown->approval1 = 0;
                $modelOutOfTown->approval2 = 0;

                try {
                    if (!($flag = $modelOutOfTown->save(false))) {
                        $transaction->rollBack();
                        $return = \yii\helpers\Html::errorSummary($modelOutOfTown, ['encode' => true]);
                    }

                    if ($flag) {
                        $transaction->commit();
                        $return = true;
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                    $return = "Error : " . $e;
                }
            } else {
                $return = \yii\helpers\Html::errorSummary($modelOutOfTown, ['encode' => true]);
            }
        } else {
            $return = "Error : Can't load model";
        }
        return $return;
    }
    public function actionOotdelete($id, $job_id)
    {
        $return = false;
        if (!empty($id) && !empty($job_id)) {
            $model = TimeReportOutOfTown::find()->where(['tr_id' => $id, 'job_id' => $job_id])->one();
            if (!empty($model)) {
                $return = $model->delete();
            }
        }
        return $return;
    }






    protected function findModelView($id)
    {
      if (($model = TimeReport::findOne($id)) !== null) {
        return $model;
      }
      throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findModelDetail($id)
    {
        if (($model = TimeReportDetail::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    protected function findModel($EmployeeId, $Date)
    {
        if (($model = TimeReport::findOne(['employee_id' => $EmployeeId, 'tr_date' => $Date])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
