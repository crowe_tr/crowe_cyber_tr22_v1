<?php

namespace backend\modules\tr\controllers;

use Yii;
use common\models\tr\Job;
use common\models\tr\Taxi;
use common\models\tr\TimeReport;
use common\models\tr\TimeReportDetail;
use common\models\tr\TimeReportMeals;
use common\models\tr\TimeReportOutOfOffice;
use common\models\tr\TimeReportTaxi;
use common\models\tr\TrTimeReportLogs;

use common\models\hr\Employee;

use common\models\st\TaskType;
use common\models\st\Task;
use common\models\cm\Taxi as cmTaxi;

use common\models\tr\search\TimeReportSearch;
use common\models\tr\search\TimeReportDetailSearch;
use common\components\CommonHelper;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\MethodNotAllowedHttpException;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class Trapproval2Controller extends Controller
{
    public $page_label = "TIMEREPORT / APPROVAL-2";
    public $page_url = "/tr/trapproval2";
    public $page_id =  2;
    public $page_approval =  "Approval2";
    public $page_completed_status =  "COMPLETED";
    public $page_view =  "view2";

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    //========================================================
    public function actionAll()
    {
        return $this->actionIndex();
    }
    public function IndexFilter()
    {
        $session = Yii::$app->session;
        $session->open();

        $post = Yii::$app->request->post();
        $model = new TimeReport();
        if ($model->load($post)) {
            $data = [];
            $date = explode("-", $model->YearDate);

            $data['Year'] = $date[0];
            $data['Month'] = $date[1];
            $data['YearDate'] = $model->YearDate;
            $session->set('__trapproval_new', $data);
        }
    }

    public function actionIndex()
    {
        $user = CommonHelper::getUserIndentity();
        $session = Yii::$app->session;
        $data = [];


        $this->IndexFilter();

        $model = new TimeReport();
        $param = isset($_SESSION['__trapproval_new']) ? $_SESSION['__trapproval_new'] : [];
        $param['page_label'] = $this->page_label;
        $param['page_url'] = $this->page_url;
        $param['page_id'] = $this->page_id;
        $param['page_completed_status'] = $this->page_completed_status;

        $param['EmployeeId'] = isset($param['EmployeeId']) ? $param['EmployeeId'] : $user->Id;
        $param['Year'] = isset($param['Year']) ? $param['Year'] : date('Y');
        $param['Month'] = isset($param['Month']) ? $param['Month'] : date('m');
        $param['YearDate'] = isset($param['YearDate']) ? $param['YearDate'] : date('Y-m-t');
        $param['Date'] = date('Y-m-d');

        $sql_list = "call tr_approval_by_month(" . $this->page_id . ",'" . $param['EmployeeId'] . "', '" . $param['Year'] . "', '" . $param['Month'] . "')";
        // $sql_list = "call tr_approval_employee_list(" . $this->page_id . ",'" . $param['EmployeeId'] . "', '" . $param['Year'] . "', '" . $param['Month'] . "')";
        $data['list'] = Yii::$app->db->createCommand($sql_list)->queryAll();

        return $this->render('../trapproval/index', [
            'param' => $param,
            'data' => $data,
            'model' => $model
        ]);
    }

    public function actionDetail($id)
    {
        $user = CommonHelper::getUserIndentity();
        $session = Yii::$app->session;
        $data = array();

        $model = new TimeReport();
        $param = isset($_SESSION['__trapproval_new']) ? $_SESSION['__trapproval_new'] : [];
        $param['page_label'] = $this->page_label;
        $param['page_url'] = $this->page_url;
        $param['page_id'] = $this->page_id;
        $param['page_completed_status'] = $this->page_completed_status;

        $param['EmployeeId'] = isset($param['EmployeeId']) ? $param['EmployeeId'] : $user->Id;
        $param['Year'] = isset($param['Year']) ? $param['Year'] : date('Y');
        $param['Month'] = isset($param['Month']) ? $param['Month'] : date('m');
        $param['YearDate'] = isset($param['YearDate']) ? $param['YearDate'] : date('Y-m-t');

        $list_det = [];
        $sql_calendar = "call calendar_list('" . $param['EmployeeId'] . "', '" . $param['Year'] . "', '" . $param['Month'] . "')";
        $list = Yii::$app->db->createCommand($sql_calendar)->queryAll();
        foreach ($list as $kl => $vl) {
          $sql_list_data = "call tr_approval_detail_employee('" . $vl['calendar_date'] ."',". $this->page_id .",'" . $param['EmployeeId'] . "','" . $id . "')";
          $list_data = Yii::$app->db->createCommand($sql_list_data)->queryAll();
          if ($list_data == true) {
            foreach ($list_data as $kld => $vld) {
              $list_det[] = array_merge($vl, $vld);
            }
          } else {
            $list_det[] = $vl;
          }
        }

        $data['list'] = $list_det;

        // $sql_list = "call tr_approval_employee_detail(" . $this->page_id . ",'" . $param['EmployeeId'] . "','" . $id . "', '" . $param['Year'] . "', '" . $param['Month'] . "')";

        // $data['list'] = Yii::$app->db->createCommand($sql_list)->queryAll();
        return $this->render('../trapproval/detail', [
            'param' => $param,
            'data' => $data,
            'model' => $model
        ]);
    }

    public function actionView($id, $TrDetID)
    {
        $user = CommonHelper::getUserIndentity();
        $session = Yii::$app->session;
        $data = array();
        $param = isset($_SESSION['__trapproval_new']) ? $_SESSION['__trapproval_new'] : [];
        $param['page_label'] = $this->page_label;
        $param['page_url'] = $this->page_url;
        $param['page_id'] = $this->page_id;

        $data['Detail'] = Yii::$app->db->createCommand("call tr_approval_item('detail'," . $id . ", " . $TrDetID . ")")->queryAll();
        $data['Meal'] = Yii::$app->db->createCommand("call tr_approval_item('meal'," . $id . ", " . $TrDetID . ")")->queryAll();
        $data['Ope'] = Yii::$app->db->createCommand("call tr_approval_item('ope'," . $id . ", " . $TrDetID . ")")->queryAll();
        $data['Taxi'] = Yii::$app->db->createCommand("call tr_approval_item('taxi'," . $id . ", " . $TrDetID . ")")->queryAll();

        $param['TrID'] = $id;
        $param['TrDetID'] = $TrDetID;

        return $this->renderPartial('../trapproval/'.$this->page_view, [
            'param' => $param,
            'data' => $data,
        ]);
    }

    public function actionApprove()
    {
        $post = Yii::$app->request->post();
        $user = CommonHelper::getUserIndentity();

        $return = [];
        $return['status'] = false;
        $return['message'] = "";

        if (!empty($post['data'])) {
            foreach ($post['data'] as $data) {
                $d = explode(";", $data);
                $approval = isset($param['EmployeeId']) ? $param['EmployeeId'] : $user->Id;
                $date = $d[0];
                $job_description = $d[1];
                $trid = $d[2];
                $trdetid = $d[3];

                $sql = "call tr_approve_detail(2, " . $trid . ", " . $trdetid . ")";
                $update = Yii::$app->db->createCommand($sql)->queryOne();

                if ($update['valid'] == 1) {
                    $tr_data = TimeReport::findOne($trid);
                    $employee_data = Employee::findOne($tr_data->employee_id);
                    // $approval_data = Employee::findOne($approval);

                    $value_logs = null;
                    $sql_tr_logs = "call tr_list('tr-data','" . $employee_data->user_id .";" . $date . "');";
                    $dt_tr_logs = Yii::$app->db->createCommand($sql_tr_logs)->queryOne();
                    if ($dt_tr_logs) {
                      $value_logs = $dt_tr_logs['tr_status'];
                    }

                    $sql_tr_det_logs = "call tr_hint2({$trid},{$trdetid})";
                    $dt_tr_det_logs = Yii::$app->db->createCommand($sql_tr_det_logs)->queryAll();
                    $model_logs = new TrTimeReportLogs;
                    $model_logs->tr_id = $trid;
                    $model_logs->tr_det_id = $trdetid;
                    $model_logs->tr_date = $date;
                    $model_logs->employee_name = $employee_data->user_id . ' - ' . $employee_data->full_name;
                    $model_logs->tr_status = $value_logs;
                    $model_logs->tr_memo = json_encode($dt_tr_det_logs);
                    $model_logs->tr_type = 'ALL DETAIL';
                    $model_logs->user_action = 'APPROVE-2';
                    $model_logs->created_by = $user->user_id . ' - ' . $user->full_name;
                    $model_logs->save(false);

                    $return['status'] = true;
                    $return['message'] = "Success";
                } else {
                    $return['status'] = false;
                    $return['message'] = $update['Valid'];
                }
            }
        } else {
            $return['message'] = "Please select at least one item";
        }
        return json_encode($return);
    }


    public function detail_approve($models, $id, $TrDetID, $set)
    {
        if (!empty($models)) {
            $table_name = $models->getTableSchema()->fullName;
            $models->approval2 = $set;

            if ($models->validate()) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $models->save();
                    $user = CommonHelper::getUserIndentity();
                    $tr_data = TimeReport::findOne($id);
                    $employee_data = Employee::findOne($tr_data->employee_id);
                    // $approval_data = Employee::findOne($user->user_id);

                    $value_logs = null;
                    $sql_tr_logs = "call tr_list('tr-data','" . $employee_data->user_id .";" . $tr_data->tr_date . "');";
                    $dt_tr_logs = Yii::$app->db->createCommand($sql_tr_logs)->queryOne();
                    if ($dt_tr_logs) {
                      $value_logs = $dt_tr_logs['tr_status'];
                    }

                    $sql_tr_det_logs = "call tr_hint2({$id},{$TrDetID})";
                    $dt_tr_det_logs = Yii::$app->db->createCommand($sql_tr_det_logs)->queryAll();
                    $model_logs = new TrTimeReportLogs;
                    $model_logs->tr_id = $id;
                    $model_logs->tr_det_id = $TrDetID;
                    $model_logs->tr_date = $tr_data->tr_date;
                    $model_logs->employee_name = $employee_data->user_id . ' - ' . $employee_data->full_name;
                    $model_logs->tr_status = $value_logs;
                    $model_logs->tr_memo = json_encode($dt_tr_det_logs);
                    $model_logs->created_by = $user->user_id . ' - ' . $user->full_name;

                    $model_logs->tr_type = '';
                    $model_logs->user_action = '';

                    if ($table_name == 'tr_time_report_detail') {
                        $model_logs->tr_type = 'TIME REPORT - DETAIL';
                    } elseif ($table_name == 'tr_time_report_meals') {
                        $model_logs->tr_type = 'TIME REPORT - MEAL';
                    } elseif ($table_name == 'tr_time_report_ope') {
                        $model_logs->tr_type = 'TIME REPORT - OUT OF OFFICE';
                    } elseif ($table_name == 'tr_time_report_taxi') {
                        $model_logs->tr_type = 'TIME REPORT - TAXI';
                    }

                    if ($set == 1) {
                        $model_logs->user_action = 'APPROVE-2';
                    } elseif ($set == 2) {
                        $this->ApprovalsEmail($id, $TrDetID, $models, "REJECTED");
                        $model_logs->user_action = 'REJECT-2';
                    } elseif ($set == 3) {
                        $this->ApprovalsEmail($id, $TrDetID, $models, "REVISED");
                        $model_logs->user_action = 'REVISE-2';
                    }
                    
                    $model_logs->save(false);

                    $transaction->commit();
                    $return = true;
                } catch (Exception $e) {
                    $transaction->rollBack();
                    $return = "Error : " . $e;
                }
            } else {
                $return = \yii\helpers\Html::errorSummary($models, ['encode' => true]);
            }
        }
        return $return;
    }

    public function actionDetailapproval($id, $TrDetID, $set)
    {
        $modelDetail = TimeReportDetail::find()->where(['time_report_id' => $id, 'id' => $TrDetID])->one();
        $return = self::detail_approve($modelDetail, $id, $TrDetID, $set);
    }
    public function actionMealapproval($id, $TrDetID, $Seq, $set)
    {
        $modelMeals = TimeReportMeals::find()->where(['time_report_id' => $id, 'tr_det_id' => $TrDetID, 'seq' => $Seq])->one();
        $return = self::detail_approve($modelMeals, $id, $TrDetID, $set);
    }
    public function actionOutofficeapproval($id, $TrDetID, $Seq, $set)
    {
        $modelOutoffice = TimeReportOutOfOffice::find()->where(['time_report_id' => $id, 'tr_det_id' => $TrDetID, 'seq' => $Seq])->one();
        $return = self::detail_approve($modelOutoffice, $id, $TrDetID, $set);
    }
    public function actionTaxiapproval($id, $TrDetID, $Seq, $set)
    {
        $modelTaxi = TimeReportTaxi::find()->where(['time_report_id' => $id, 'tr_det_id' => $TrDetID, 'seq' => $Seq])->one();
        $return = self::detail_approve($modelTaxi, $id, $TrDetID, $set);
    }


    public function ApprovalsEmail($TimeReportID, $TrDetID, $model, $label)
    {
        $sql = "CALL tr_comment_email({$TimeReportID}, {$TrDetID})";
        $lookups = Yii::$app->db->createCommand($sql)->queryAll();

        $email = [];
        if (!empty($lookups)) {
            foreach ($lookups as $r) {
                if (!empty($r['EmployeeEmail'])) {
                    if (filter_var($r['EmployeeEmail'], FILTER_VALIDATE_EMAIL)) {
                        $email[$r['EmployeeEmail']] = $r['EmployeeEmail'];
                    }
                }
            }
        }
        $date = !empty($model->tr_date) ? $model->tr_date : $model->timeReport->tr_date;
        $date = date('l, d F Y', strtotime($date));
        return $this->Email(
            $email,
            'Crowe TimeReport - Your timereport submission on ' . $date . ' was ' . $label . ' #' . $TimeReportID,
            [
                'model' => $model,
                'label' => $label
            ]
        );
    }
    public function Email($sendto, $subject, $param, $layouts = 'layouts/newapprovals')
    {
        Yii::$app->cache->flush();

        $from = Yii::$app->params['appNotificationEmail'];
        $cc = Yii::$app->params['appNotificationEmailCC'];

        $to = $sendto;

        if (!empty($to)) {
            $html = ['html' => $layouts];

            $compose = Yii::$app->mailer->compose($html, $param);
            $compose->setFrom($from);
            $compose->setTo($to);
            if (!empty($cc)) {
                $compose->setCc($cc);
            }
            $compose->setSubject($subject);

            $compose->send();
        }
    }
}
