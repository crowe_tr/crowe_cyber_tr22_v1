<?php

namespace backend\modules\tr\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\MethodNotAllowedHttpException;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\base\ErrorException;
use common\models\cm\CmReport;
use common\models\tr\views\ReportTimeReport;
use common\models\tr\views\ReportTimeReportSearch;
use common\components\CommonHelper;
use common\components\TimeReportHelper;
use kartik\mpdf\Pdf;
use common\models\st\TaskType;
use common\models\st\Task;

class Report_managerController extends Controller
{
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    //=============== START REPORT GENERATOR ===============
    public function generate_results($view, $model, $fields, $data)
    {
        // $data = Yii::$app->db->createCommand($sql)->queryAll();
        if ($model->ReportFormat == "xls") {
            $results = $this->renderPartial($view, [
                'data' => $data,
                'fields' => $fields,
                'format' => $model->ReportFormat,
                'model' => $model,
            ]);
        } else {
            $results = $this->renderPartial($view, [
                'data' => $data,
                'fields' => $fields,
                'format' => $model->ReportFormat,
                'model' => $model,
            ]);
        }

        return $results;
    }
    public function generate_report($data, $filename, $format, $param = '')
    {
        if ($format == 'xls') {
            $filename = $filename . '.xls';
            $fopen = fopen($filename, 'w');
            file_put_contents($filename, $data);
            fclose($fopen);

            Yii::$app->response->SendFile($filename);
            unlink($filename);
        } else if ($format == 'pdf') {
            $pdf = new Pdf([
                'mode' => Pdf::MODE_CORE,
                'format' => Pdf::FORMAT_A4,
                'orientation' => Pdf::ORIENT_LANDSCAPE,
                'destination' => Pdf::DEST_DOWNLOAD,
                'marginTop' => 8,
                'marginBottom' => 8,
                'marginRight' => 8,
                'marginLeft' => 8,
                'filename' => 'REPORT # ' . rand() . '.pdf',

                'cssInline' => '
                *, p, span, b {
                    font-family: arial;
                    font-size: 13px !important;
                    padding:0;
                    margin:0;
                    text-size-adjust: none;
                    text-transform: uppercase;
                }
                .table {
                    border-collapse: collapse;
                    display: block;
                    text-transform: uppercase;
                    text-size-adjust: none;
                }
                .table th{
                    padding: 5px;
                    vertical-align: top;
                    font-size: 12px !important;
                    background: #007be8 !important;
                }
                .table td {
                    padding: 5px;
                    vertical-align: top;
                    font-size: 12px !important;
                }
                .table .bg-primary {
                    background: #007be8 !important;
                }
                ',
                'content' => $data,
            ]);
            return $pdf->render();
        } else {
            return $data;
        }
        return $data;
    }
    public function get_fields_options($fields)
    {
        $options = [];
        $fields = array_keys($fields);
        foreach ($fields as $f) {
            $options[$f] = ['selected' => 'selected'];
        }
        return $options;
    }
    public function report($_view, $_sql, $_report)
    {
        $this->layout = "../../../../views/layouts/report";

        $posts = Yii::$app->request->post();
        $model = new CmReport();

        $data = "";
        $fields = [];
        $fields['data'] = $_sql['field'];
        // var_dump($fields['data']);
        // die();
        $fields['selected'] = $fields['data'];
        $fields['options'] = $this->get_fields_options($fields['selected']);
        if ($model->load($posts)) {
            // if (!empty($model->Date)) {
                $param = $this->parameter($model);
                // var_dump($param);
                $fields['user_id'] = $param['user_id'];

                $fields['selected'] = empty($param['fields']) ? $fields['selected'] : $param['fields'];
                $fields['options'] = $this->get_fields_options($fields['selected']);

                // $sql = $this->sql($_sql['name'], $param);
                $sql = $this->call_sql($_sql['name'], $param);

                $data_job = Yii::$app->db->createCommand($sql)->queryAll();

                if ( $_sql['name'] == 'rpt_recovery_rate_summary') {
                  foreach ($data_job as $key => $val) {
                    $tc_val = Yii::$app->db->createCommand("call rpt_val('tc-value-group',". $val['job_id'] .")")->queryOne();
                    $data_job[$key]['tc_partner'] = ($tc_val['tc_partner'] != 0) ? $tc_val['tc_partner'] : 0 ;
                    $data_job[$key]['tc_director'] = ($tc_val['tc_director'] != 0) ? $tc_val['tc_director'] : 0 ;
                    $data_job[$key]['tc_manager'] = ($tc_val['tc_manager'] != 0) ? $tc_val['tc_manager'] : 0 ;
                    $data_job[$key]['tc_supervisor'] = ($tc_val['tc_supervisor'] != 0) ? $tc_val['tc_supervisor'] : 0 ;
                    $data_job[$key]['tc_senior'] = ($tc_val['tc_senior'] != 0) ? $tc_val['tc_senior'] : 0 ;
                    $data_job[$key]['tc_junior'] = ($tc_val['tc_junior'] != 0) ? $tc_val['tc_junior'] : 0 ;
                    $data_job[$key]['tc_others'] = ($tc_val['tc_others'] != 0) ? $tc_val['tc_others'] : 0 ;

                    $job_progress = Yii::$app->db->createCommand("call rpt_val('job-progress',". $val['job_id'] .")")->queryOne();
                    $data_job[$key]['progress_time'] = ($job_progress['progress_time'] != "") ? $job_progress['progress_time'] : "" ;

                    $data_job[$key]['total_charges'] = $val['tc_total'] + $val['tc_other'];
                  }
                }
                $results = $this->generate_results($_view['results'], $model, $fields, $data_job);
                $data = $this->generate_report($results, $_report['name'] . date('ymdhis'), $model->ReportFormat);
            // }
        }
        // var_dump($param);
        $init = $this->get_init($model);
        return $this->render($_view['form'], [
            'model' => $model,
            'init' => $init,
            'data' => $data,
            'fields' => $fields,
            'options' => $_report,
        ]);
    }
    public function get_init($model)
    {
        $param = [];
        $param['TaskTypeID'] = ArrayHelper::map(TaskType::find()->where(['flag' => 1])->asArray()->all(), 'id', 'taskTypeName');;
        $param['TaskID'] = empty($model->Custom['TaskTypeID']) ? [] : ArrayHelper::map(Task::find()->where(['taskTypeID' => $model->Custom['TaskTypeID']])->asArray()->all(), 'id', 'taskName');;
        return $param;
    }
    public function parameter($model)
    {
        $user = CommonHelper::getUserIndentity();
        $param = [];
        $Custom = $model->Custom;
        // var_dump($Custom);
        // die();
        $param['user_id'] = $user->user_id;
        // $param['date'] = explode(" TO ", $model->Date);
        // $param['date1'] = date('Y-m-d', strtotime($param['date'][0]));
        // $param['date2'] = date('Y-m-d', strtotime($param['date'][1]));
        $param['client_code'] = empty($Custom['client_code']) ? "null" : "'" . $Custom['client_code'] . "'";
        $param['job_code'] = empty($Custom['job_code']) ? "null" : "'" . $Custom['job_code'] . "'";
        // $param['job_id'] = empty($Custom['job_id']) ? "null" : "'" . $Custom['job_id'] . "'";
        // $param['entity_name'] = empty($Custom['entity_name']) ? "null" : "'" . $Custom['entity_name'] . "'";
        // $param['division'] = empty($Custom['Division']) ? "null" : "'" . $Custom['Division'] . "'";
        // $param['dept'] = empty($Custom['deptID']) ? "null" : "'" . $Custom['deptID'] . "'";
        // $param['group'] = empty($Custom['parentID']) ? "null" : "'" . $Custom['parentID'] . "'";
        // $param['manager'] = empty($Custom['ManagerID']) ? "'".$user->initial."'" : "'" . $Custom['ManagerID'] . "'";
        // $param['supervisor'] = empty($Custom['SupervisorID']) ? "null" : "'" . $Custom['SupervisorID'] . "'";
        // $param['level'] = empty($Custom['levelID']) ? "null" : "'" . $Custom['levelID'] . "'";
        // $param['employee'] = empty($Custom['Employee']) ? "null" : "'" . $Custom['Employee'] . "'";
        // $param['EmployeeList'] = empty($Custom['EmployeeList']) ? "null" : "'" . implode(",", $Custom['EmployeeList']) . "'";
        //
        // $param['TypeName'] = empty($Custom['TypeName']) ? "null" : "'" . $Custom['TypeName'] . "'";
        // $param['TaskName'] = empty($Custom['TaskName']) ? "null" : "'" . $Custom['TaskName'] . "'";
        // $param['ClientCode'] = empty($Custom['ClientCode']) ? "null" : "'" . $Custom['ClientCode'] . "'";
        // $param['JobCode'] = empty($Custom['JobCode']) ? "null" : "'" . $Custom['JobCode'] . "'";
        // $param['status'] = empty($Custom['status']) ? "null" : "'" . $Custom['status'] . "'";
        // $param['partner'] = empty($Custom['PartnerID']) ? "null" : "'" . $Custom['PartnerID'] . "'";
        // $param['TaxiType'] = empty($Custom['TaxiType']) ? "null" : "'" . $Custom['TaxiType'] . "'";
        //
        // $param['year'] = empty($Custom['year']) ? "null" : "'" . $Custom['year'] . "'";
        //
        // $param['fields'] = empty($Custom['fields']) ? [] : $Custom['fields'];
        return $param;
    }

    public function call_sql($_sql, $param)
    {
      // var_dump($param);
      switch ($_sql) {
        case 'rpt_recovery_rate_summary':
          $sql =  "
                    call rpt_job_by_employee(
                        'manager',
                        '" . $param['user_id'] . "',
                        " . $param['client_code'] . ",
                        " . $param['job_code'] . "
                    );
                  ";
          break;

        case 'rpt_recovery_rate_detail':
          $sql =  "
                  call rpt_val(
                      'job-detail',
                      " . $param['job_code'] . "
                  );
                ";
          break;

        default:
          $sql = '';
          break;
      }
      return $sql;
    }

    public function sql($_sql, $param)
    {
        switch ($_sql) {
            case "rpt_time_report_daily":
                $sql = "call rpt_time_report_daily(
                    '" . $param['date1'] . "',
                    '" . $param['date2'] . "',
                    " . $param['entity'] . ",
                    " . $param['division'] . ",
                    " . $param['dept'] . ",
                    " . $param['group'] . ",
                    " . $param['manager'] . ",
                    " . $param['level'] . ",
                    " . $param['employee'] . "
                );";
                break;
            case "rpt_time_report_detail":
                $sql = "
                    call rpt_time_report_detail(
                        '" . $param['date1'] . "',
                        '" . $param['date2'] . "',
                        " . $param['entity'] . ",
                        " . $param['division'] . ",
                        " . $param['TypeName'] . ",
                        " . $param['TaskName'] . ",
                        " . $param['ClientCode'] . ",
                        " . $param['JobCode'] . ",
                        " . $param['manager'] . ",
                        " . $param['group'] . ",
                        " . $param['employee'] . ",
                        " . $param['status'] . "
                    );
                    ";
                break;
            case "rpt_recovery_rate_summary":
                $sql = "
                        call rpt_recovery_rate_summary(
                            " . $param['entity'] . ",
                            " . $param['division'] . ",
                            " . $param['partner'] . ",
                            " . $param['manager'] . ",
                            " . $param['ClientCode'] . ",
                            " . $param['JobCode'] . ",
                            " . $param['status'] . "
                        );
                    ";
                break;
            case "rpt_taxi_detail":
                $sql = "
                    call rpt_taxi_detail(
                        '" . $param['date1'] . "',
                        '" . $param['date2'] . "',
                        " . $param['entity'] . ",
                        " . $param['division'] . ",
                        " . $param['dept'] . ",
                        " . $param['TaxiType'] . ",
                        " . $param['group'] . ",
                        " . $param['manager'] . ",
                        " . $param['TypeName'] . ",
                        " . $param['TaskName'] . ",
                        " . $param['ClientCode'] . ",
                        " . $param['JobCode'] . ",
                        " . $param['level'] . ",
                        " . $param['employee'] . ",
                        " . $param['status'] . "
                    );
                ";
                break;
            case "rpt_annual_report_summary":
                $sql = "call rpt_annual_report_summary2(
                    " . $param['year'] . ",
                    " . $param['entity'] . ",
                    " . $param['division'] . ",
                    " . $param['dept'] . ",
                    " . $param['group'] . ",
                    " . $param['manager'] . ",
                    " . $param['level'] . ",
                    " . $param['employee'] . "
                );";
                break;
            case "rpt_annual_report_detail":
                $sql = "call rpt_annual_report_detail(
                    " . $param['employee'] . ",
                    " . $param['year'] . "
                );";
                break;
            case "rpt_sick_list":
                $sql = "call rpt_sick_list(
                    '" . $param['date1'] . "',
                    '" . $param['date2'] . "',
                    " . $param['entity'] . ",
                    " . $param['division'] . ",
                    " . $param['group'] . ",
                    " . $param['manager'] . ",
                    " . $param['supervisor'] . ",
                    " . $param['employee'] . "
                );";
                break;
            case "rpt_recovery_rate_detail":
                $sql = "call rpt_recovery_rate_detail(
                        " . $param['JobCode'] . "
                    );";
                break;
            case "rpt_tr_det_office88":
                $sql = "call rpt_tr_det_office88(
                    '" . $param['date1'] . "',
                    '" . $param['date2'] . "',
                    " . $param['EmployeeList'] . "
                );";
                break;
            default:
                $sql = "";
        }
        return $sql;
    }
    //=============== END REPORT GENERATOR ===============

    public function actionIndex()
    {

        return $this->render('../report/manager', []);
    }

    public function actionRecovery_rate_summary()
    {
        $_view['form'] = 'recovery_rate_summary/form';
        $_view['results'] = 'recovery_rate_summary/report';

        $_sql['name'] = "rpt_recovery_rate_summary";

        $_sql['field'] = [
            'entity_name' => 'ENTITY', //
            'div_name' => 'DIVISION', //
            'client_code' => 'CUSTOMER', //
            'client_name' => 'CLIENT', //
            'client_industry' => 'INDUSTRY', //
            'job_code' => 'JOB CODE', //
            'job_description' => 'JOB DESCRIPTION', //
            'start_date' => 'START DATE', //
            'end_date' => 'FINISH DATE', //
            'job_status' => 'STATUS', //
            'manager_id' => 'MANAGER ID', //
            'manager_full_name' => 'MANAGER NAME', //
            'manager_initial' => 'MANAGER INITIAL', //
            'partner_id' => 'PARTNER ID', //
            'partner_full_name' => 'PARTNER NAME', //
            'partner_initial' => 'PARTNER INITIAL', //
            'job_fee' => 'JOB FEE', //
            // 'tc_partner' => 'PARTNER', //
            // 'tc_senior_manager_3' => 'SENIOR MANAGER 3', //
            // 'tc_senior_manager_2' => 'SENIOR MANAGER 2', //
            // 'tc_senior_manager_1' => 'SENIOR MANAGER 1', //
            // 'tc_manager_3' => 'MANAGER 3', //
            // 'tc_manager_2' => 'MANAGER 2', //
            // 'tc_manager_1' => 'MANAGER 1', //
            // 'tc_associate_manager' => 'ASSOCIATE MANAGER', //
            // 'tc_supervisor_2' => 'SUPERVISOR 2', //
            // 'tc_supervisor_1' => 'SUPERVISOR 1', //
            // 'tc_senior_3' => 'SENIOR 3', //
            // 'tc_senior_2' => 'SENIOR 2', //
            // 'tc_senior_1' => 'SENIOR 1', //
            // 'tc_associate_2' => 'ASSOCIATE 2', //
            // 'tc_associate_1' => 'ASSOCIATE 1', //
            // 'tc_junior_associate' => 'JUNIOR ASSOCIATE', //
            // 'tc_internship' => 'INTERNSHIP', //

            'tc_partner' => 'PARTNER', // PARTNER,MANAGING PARTNER,DEPUTY MANAGING PARTNER
            'tc_director' => 'DIRECTOR', // DIRECTOR,ASSOCIATE DIRECTOR
            'tc_manager' => 'MANAGER', // SENIOR MANAGER,MANAGER,ASSOCIATE MANAGER,MANAGER 3
            'tc_supervisor' => 'SUPERVISOR', // SUPERVISOR,SUPERVISOR 1,SUPERVISOR 2,SUPERVISOR 3
            'tc_senior' => 'SENIOR', // SENIOR,SENIOR 1,SENIOR 2,SENIOR 3
            'tc_junior' => 'JUNIOR', // JUNIOR
            'tc_others' => 'OTHER', // SECRETARY,CHAIRMAN,PRINCIPAL,INTERNSHIP,ADMIN STAFF,ADMINISTRASI,MANAGEMENT TRAINEE

            'tc_total' => 'TOTAL TIME CHARGE', //
            'tc_other' => 'OTHER EXPENSE', //
            'total_charges' => 'TOTAL CHARGES', //
            'rate_time_charges' => 'RECOVERY TIME CHARGES', //
            'rate_total_charges' => 'RECOVERY TOTAL CHARGES', //
            'progress_time' => 'PROGRESS TIME',  //

        ];
        $_report['name'] = 'Recovery Rate - Summary';
        return $this->report($_view, $_sql, $_report);
    }
    public function actionRecovery_rate_detail()
    {
        $_view['form'] = 'recovery_rate_detail/form';
        $_view['results'] = 'recovery_rate_detail/report';

        $_sql['name'] = "rpt_recovery_rate_detail";

        $_sql['field'] = [
            'employee_id' => 'EMPLOYEE ID', //
            'employee_name' => 'EMPLOYEE NAME', //
            'employee_level' => 'LEVEL', //
            'job_planning' => 'PLANNING', //
            'job_fieldwork' => 'FIELD WORK', //
            'job_reporting' => 'REPORTING', //
            'job_wrapup' => 'WRAP UP', //
            'job_total' => 'TOTAL', //
            'job_overtime' => 'OVERTIME', //
            'job_planning_act' => 'PLANNING', //
            'job_fieldwork_act' => 'FIELD WORK', //
            'job_reporting_act' => 'REPORTING', //
            'job_wrapup_act' => 'WRAP UP', //
            'job_total_act' => 'TOTAL', //
            'job_overtime_act' => 'OVERTIME', //
            'job_total_diff' => 'DIFF WH',
            'job_overtime_diff' => 'DIFF OT',
        ];
        $_report['name'] = 'Recovery Rate - Detail';
        return $this->report($_view, $_sql, $_report);
    }


}
