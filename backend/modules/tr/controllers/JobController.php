<?php

namespace backend\modules\tr\controllers;

use Yii;
use common\components\CommonHelper;
use common\components\TimeReportHelper;

use common\models\tr\JobList;
use common\models\tr\Job;
use common\models\tr\JobTemp;
use common\models\tr\JobBudgeting;
use common\models\tr\JobBudgetingTemp;
use common\models\tr\JobComment;
use common\models\tr\TrPreviousChangesJob;
use common\models\tr\AuditTrail;
use common\models\tr\AuditTrailDet;


use common\models\tr\search\JobviewSearch;
use common\models\tr\search\JobBudgetingSearch;
use common\models\tr\search\JobBudgetingTempSearch;
use common\models\tr\search\JobCommentSearch;
use common\models\tr\search\AuditTrailSearch;

use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\MethodNotAllowedHttpException;

use common\components\HelperDB;

use common\models\hr\EmployeeList;


class JobController extends Controller
{

  protected function findModelView($id)
  {
    if (($model = JobList::findOne($id)) !== null) {
      return $model;
    }
    throw new NotFoundHttpException('The requested page does not exist.');
  }

  protected function findModel($id)
  {
    if (($model = Job::findOne($id)) !== null) {
      return $model;
    }
    throw new NotFoundHttpException('The requested page does not exist.');
  }

  protected function findModelTemp($id)
  {
    if (($model = JobTemp::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException('The requested page does not exist.');
  }

  protected function findModelBudget($EmployeeID, $JobID, $param = [])
  {
    if (($modelBudget = JobBudgeting::find()->where(['employee_id' => $EmployeeID, 'job_id' => $JobID])->one()) !== null) {
      return $modelBudget;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
  protected function findModelBudgetTemp($EmployeeID, $JobID, $param = [])
  {
    if (($modelBudget = JobBudgetingTemp::find()->where(['employee_id' => $EmployeeID, 'job_id' => $JobID])->one()) !== null) {
      return $modelBudget;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  protected function findModelHistoryJob($JobID)
  {
    if ($model = TrPreviousChangesJob::find()->where(['JobID' => $JobID])->all()) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }


  public function behaviors()
  {
    return HelperDB::set_behaviors();
  }


  public function actionAll()
  {
    return $this->index("");
  }
  public function actionIndex()
  {
    return $this->redirect(['draft']);
  }
  public function actionDraft()
  {
    return $this->index('DRAFT');
  }
  public function actionSubmitted()
  {
    return $this->index("SUBMITTED");
  }
  public function actionApproved()
  {
    return $this->index("APPROVED");
  }
  public function actionRejected()
  {
    return $this->index("REJECTED");
  }
  public function actionOngoing()
  {
    return $this->index("ONGOING");
  }
  public function actionExpired()
  {
    return $this->index("EXPIRED");
  }
  public function actionClosed()
  {
    return $this->index("CLOSED");
  }



  public function IndexFilter()
  {
    $session = Yii::$app->session;
    $session->open();

    $post = Yii::$app->request->post();
    $model = new JobviewSearch();
    if ($model->load($post)) {
      $data = [];

      $data['client_id'] = $model->client_id;
      $data['job_code'] = $model->job_code;
      $data['job_description'] = $model->job_description;
      $data['periode'] = $model->periode;
      $session->set('__job_index', $data);
    }
  }

  public function index($status_job)
  {
    $this->IndexFilter();

    $user = CommonHelper::getUserIndentity();
    $param = isset($_SESSION['__job_index']) ? $_SESSION['__job_index'] : [];
    $param['job_code'] = isset($param['job_code']) ? $param['job_code'] : '';
    $param['client_id'] = isset($param['client_id']) ? $param['client_id'] : '';
    $param['job_description'] = isset($param['job_description']) ? $param['job_description'] : '';
    $param['periode'] = isset($param['periode']) ? $param['periode'] : '';

    $searchModel = new JobviewSearch();
    $searchModel->client_id = $param['client_id'];
    $searchModel->job_code = $param['job_code'];
    $searchModel->job_description = $param['job_description'];
    $searchModel->periode = $param['periode'];

    if ($user->is_admin != 1) {
      $searchModel->manager_id = $user->user_id;
    }

    if (!empty($status_job) and $status_job != "all") {
      $searchModel->job_status = $status_job;
    }

    $dataProvider = $searchModel->search(Yii::$app->request->post());

    return $this->render('job', [
      'OrderButtons' => JobList::set_button_header($status_job),
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  public function getDataForForm($model)
  {
    $data = array();
    $data['Division'] = TimeReportHelper::getDivision();
    $data['Entity'] = TimeReportHelper::getEntity();
    $data['Client'] = TimeReportHelper::getClient();
    $data['Employee'] = TimeReportHelper::getEmployee();
    return $data;
  }


  // REQUEST :

  public function actionRequest($id)
  {
    $modelView = $this->findModelView($id);
    $model = $this->findModel($id);
    // var_dump($model);
    // die();
    $model->scenario = 'update';

    $model->job_area = explode(',', $model->job_area);

    if ($model->job_status != 0 and $model->job_status != 3) {
      return $this->redirect(['view', 'id' => $model->id]);
    }

    $data = $this->getDataForForm($model);

    $search = new JobBudgetingSearch();
    $search->job_id = $model->id;
    $modelBudgeting = $search->search($model->id);

    return $this->render('Request', [
      'data' => $data,
      'modelView' => $modelView,
      'model' => $model,
      'modelBudgeting' => $modelBudgeting,
    ]);
  }

  public function actionRequest_save($submit = 0)
  {
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

    $post = Yii::$app->request->post();
    $user = CommonHelper::getUserIndentity();

    $result = HelperDB::result_default();
    // var_dump('a');
    // die();
    $model = new Job();
    $model->scenario = 'update';

    if ($model->load($post)) {
      $model = $this->findModel($model->id);

      if ($model->load($post)) {
        $model->job_fee = empty($model->job_fee) ? 0 : str_replace(',', '', $model->job_fee);
        $model->meal_allowance = empty($model->meal_allowance) ? 0 : str_replace(',', '', $model->meal_allowance);
        $model->taxi_allowance = empty($model->taxi_allowance) ? 0 : str_replace(',', '', $model->taxi_allowance);
        $model->ope_allowance = empty($model->ope_allowance) ? 0 : str_replace(',', '', $model->ope_allowance);
        $model->administrative_charge = empty($model->administrative_charge) ? 0 : str_replace(',', '', $model->administrative_charge);
        //$model->AdministrativeChargeAct = empty($model->AdministrativeCharge) ? 0 : str_replace(',', '', $model->AdministrativeCharge);
        $model->other_expense_allowance = empty($model->other_expense_allowance) ? 0 : str_replace(',', '', $model->other_expense_allowance);
        //$model->OtherExpenseAllowanceAct = empty($model->OtherExpenseAllowance) ? 0 : str_replace(',', '', $model->OtherExpenseAllowance);


        $model->job_area = empty($model->job_area) ? null : implode(',', $model->job_area);

        $model->partner_id = empty($model->partner_id) ? null : $model->partner_id;
        $model->manager_id = empty($model->manager_id) ? null : $model->manager_id;
        $model->supervisor_id = empty($model->supervisor_id) ? null : $model->supervisor_id;
        $model->updated_by = $user->user_id;

        if ($submit == 1) {
          $model->job_status = 1;
        }

        $result = HelperDB::save($model, false);
        $model = $this->findModel($model->id);
        $result['state']['model']['isSubmit'] = false;

        $result['state']['model']['is_meal'] = $model->is_meal;
        $result['state']['model']['is_ope'] = $model->is_ope;
        $result['state']['model']['is_taxi'] = $model->is_taxi;

        $result['state']['model']['time_charges'] = empty($model->time_charges) ? 0 : $model->time_charges;
        $result['state']['model']['time_charges_actual'] = empty($model->time_charges_actual) ? 0 : number_format($model->time_charges_actual);

        $result['state']['model']['meal_allowance'] = empty($model->meal_allowance) ? 0 : $model->meal_allowance;
        $result['state']['model']['meal_allowance_actual'] = empty($model->meal_allowance_actual) ? 0 : number_format($model->meal_allowance_actual);

        $result['state']['model']['taxi_allowance'] = empty($model->taxi_allowance) ? 0 : $model->taxi_allowance;
        $result['state']['model']['taxi_allowance_actual'] = empty($model->taxi_allowance_actual) ? 0 : number_format($model->taxi_allowance_actual);

        $result['state']['model']['ope_allowance'] = empty($model->ope_allowance) ? 0 : $model->ope_allowance;
        $result['state']['model']['ope_allowance_actual'] = empty($model->ope_allowance_actual) ? 0 : number_format($model->ope_allowance_actual);

        $result['state']['model']['administrative_charge'] = empty($model->administrative_charge) ? 0 : $model->administrative_charge;
        $result['state']['model']['administrative_charge_actual'] = empty($model->administrative_charge) ? 0 : number_format($model->administrative_charge);

        $result['state']['model']['other_expense_allowance'] = empty($model->other_expense_allowance) ? 0 : $model->other_expense_allowance;
        $result['state']['model']['other_expense_allowance_actual'] = empty($model->other_expense_allowance) ? 0 : number_format($model->other_expense_allowance);

        $result['state']['model']['percentage'] = empty($model->percentage) ? 0 : $model->percentage;
        $result['state']['model']['percentage_actual'] = empty($model->percentage_actual) ? 0 : $model->percentage_actual;

        if ($submit == 1 && $result['state']['status'] == true) {
          $result['state']['model']['isSubmit'] = true;
          if (!empty($model->partner->user_email)) {
            $send_email = $this->Email($model->partner->user_email, 'THERE IS A NEW JOB NEEDS YOUR APPROVAL', $model);
            // $send_email = true;
            if ($send_email) {
              return $this->redirect(['view', 'id' => $model->id]);
            } else {
              $result['state']['status'] = false;
              $result['state']['message'] = "Can't Send Email to Partner, please cek email address";
            }
          } else {
            $result['state']['status'] = false;
            $result['state']['message'] = "Partner's email not found";
          }
        }
      } else {
        $result = HelperDB::result_noloaded($model);
      }
    } else {
      $result = HelperDB::result_noloaded($model);
    }

    $result['model'] = [];

    return $result['state'];
  }







  public function actionEstimated_default()
  {
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    $post = Yii::$app->request->post();
    $modelJob = new Job();
    if ($modelJob->load($post)) {
      $sql = "call job_list('job-estimated', '" . $modelJob->id . ";0');";
      Yii::$app->db->createCommand($sql)->queryOne();

      $results = $this->findModel($modelJob->id);
    } else {
      $results = "Cannot load module";
    }

    return $results;
  }

  public function actionEstimated_default_temp()
  {

    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    $post = Yii::$app->request->post();
    $modelJob = new JobTemp();
    if ($modelJob->load($post)) {
      $sql = "call job_list('job-estimated', '" . $modelJob->id . ";1');";
      Yii::$app->db->createCommand($sql)->queryOne();

      $results = $this->findModel($modelJob->id);
    } else {
      $results = "Cannot load module";
    }

    return $results;

    // \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    // $post = Yii::$app->request->post();
    // $modelJob = new JobTemp();
    // if ($modelJob->load($post)) {
    //   $sql = "call job_estimated('" . $modelJob->job_id . "', 0)";
    //   Yii::$app->db->createCommand($sql)->queryOne();
    //
    //   $results = $this->findModel($modelJob->job_id);
    // } else {
    //   $results = "Cannot load module";
    // }
    //
    // return $results;
  }
  public function actionApprove($id)
  {
    $user = CommonHelper::getUserIndentity();
    $modelJob = $this->findModel($id);
    $modelJob->job_status = 2;
    $haveRight = ($modelJob->partner_id == $user->user_id or $user->is_admin == 1) ? 1 : 0;

    $email = [];
    $JobBudgeting = JobBudgeting::find()->where(['job_id' => $modelJob->id])->all();
    foreach ($JobBudgeting as $jb) {
      $email[] = $jb->employee->user_email;
    }
    return $this->approval($haveRight, $modelJob, 'YOU HAVE A NEW JOB', $email);
  }
  public function actionReject($id)
  {
    $user = CommonHelper::getUserIndentity();

    $modelJob = $this->findModel($id);
    $modelJob->job_status = 3;
    $haveRight = ($modelJob->partner_id == $user->Id or $user->IsAdmin == 1) ? 1 : 0;

    $email = [];
    $email[] = $modelJob->manager->user_email;
    return $this->approval($haveRight, $modelJob, 'YOUR JOB REQUEST HAS REJECTED', $email);
  }
  public function actionClose($id)
  {
    $user = CommonHelper::getUserIndentity();
    $modelJob = $this->findModel($id);
    $modelJob->Status = 4;
    $haveRight = ($modelJob->Partner == $user->Id or $user->IsAdmin == 1) ? 1 : 0;

    $email = [];
    $JobBudgeting = JobBudgeting::find()->where(['JobID' => $modelJob->JobID])->all();
    foreach ($JobBudgeting as $jb) {
      $email[] = $jb->employee->email;
    }
    return $this->approval($haveRight, $modelJob, 'A JOB HAS BEEN CLOSED', $email);
  }

  public function approval($haveRight, $model, $subject, $email)
  {
    $result = HelperDB::result_default();
    if ($haveRight) {
      if ($model->save(false)) {
        if ($model->client->entity->is_job_email_notify) {
          if (!empty($email)) {
            foreach ($email as $mail) {
              $send_email = $this->Email($mail, $subject, $model);

              if ($send_email) {
                $result['state']['status'] = true;
                $result['state']['message'] = "Success";
              } else {
                $result['state']['status'] = false;
                $result['state']['message'] = "Can't Send Email, please cek email address " . $mail;
                break;
              }
            }
          } else {
            $result['state']['status'] = false;
            $result['state']['message'] = "Email not found";
          }
        } else {
          $result['state']['status'] = true;
          $result['state']['message'] = "This departement is not notified by email when new job updated";
        }
      } else {
        $result['state']['status'] = false;
        $result['state']['message'] = "Can not approve the Job";
      }
    } else {
      $result['state']['status'] = false;
      $result['state']['message'] = "You do not have right to make approval for this Job";
    }
    return json_encode($result['state']);
  }


  // BUDGET

  public function actionBudget($id = null, $action = null)
  {
    $session = Yii::$app->session;
    $session->open();
    $data = [];
    $post = Yii::$app->request->post();
    $model = new Job();
    $model->load($post);

    $modelJob = Job::find()->where(['id' => $model->id])->one();
    if ($modelJob->load($post)) {
      $session->set('_trjob_form' . $modelJob->id, $modelJob);
      if (Yii::$app->request->isAjax) {
        if (!empty($id)) {
          $modelBudget = $this->findModelBudget($id, $modelJob->id);
        } else {
          $modelBudget = new JobBudgeting();
          $modelBudget->job_id = $modelJob->id;
          if (empty($action)) {
            $modelBudget->isNewRecord;
          }
        }
        $data = $this->getDataForForm($modelJob);


        return $this->renderAjax('RequestDetail', [
          'id' => empty($action) ? $id : "",
          'modelJob' => $modelJob,
          'modelBudget' => $modelBudget,
          'data' => $data
        ]);

      } else {
        throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
      }
    }
  }
  public function actionBudgetdelete()
  {
    $return = false;
    $post = Yii::$app->request->post();

    $EmployeeID = $post['EmployeeID'];
    $JobID = $post['JobID'];
    $modelBudget = $this->findModelBudget($EmployeeID, $JobID);

    $transaction = \Yii::$app->db->beginTransaction();
    try {
      if ($modelBudget->delete()) {
        $transaction->commit();
        $return = true;
      } else {
        $transaction->rollback();
      }
    } catch (Exception $e) {
      $transaction->rollBack();
    }

    return $return;
  }

  public function actionBudgetsave($id = null)
  {
    $result = HelperDB::result_default();
    $user = CommonHelper::getUserIndentity();
    $post = Yii::$app->request->post();

    $modelBudget = new JobBudgeting();
    if ($modelBudget->load($post)) {
      if (!empty($id)) {
        $modelBudget = $this->findModelBudget($id, $modelBudget->job_id);
        $modelBudget->load($post);
      }

      $transaction = \Yii::$app->db->beginTransaction();
      try {
        $valid = $modelBudget->validate();
        $modelBudget->created_by = $user->user_id;
        if ($modelBudget->over_time == null) {
          $modelBudget->over_time = 0;
        }
        if ($modelBudget->save()) {
          $transaction->commit();
          $result = HelperDB::result_success();
        } else {
          $result['state']['status'] = false;
          $result['state']['message'] = HelperDB::db_field_error(Html::errorSummary($modelBudget));
        }
      } catch (\yii\db\Exception $e) {
        $result['state']['status'] = false;
        $result['state']['message'] = HelperDB::db_trigger_error($e->getMessage());
      }
    } else {
      $result = HelperDB::result_noloaded($modelBudget);
    }

    return json_encode($result['state']);
  }














  // REVISE :

  public function actionRevise($id)
  {
    $model = $this->findModel($id);
    if ($model->flag == 0) {
      $model->flag = 1;
      $model->save();
    }

    $modelView = $this->findModelView($id);
    $model = $this->findModelTemp($model->id);
    $model->job_area = explode(',', $model->job_area);

    $data = $this->getDataForForm($model);

    $search = new JobBudgetingTempSearch();
    $search->job_id = $model->id;
    $modelBudgeting = $search->search($model->id);

    return $this->render('Revise', [
      'data' => $data,
      'modelView' => $modelView,
      'model' => $model,
      'modelBudgeting' => $modelBudgeting,
    ]);
  }

  public function actionRevise_save($submit = 0)
  {
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

    $post = Yii::$app->request->post();
    $user = CommonHelper::getUserIndentity();

    $result = HelperDB::result_default();

    $model = new JobTemp();
    $model->scenario = 'update';



    if ($model->load($post)) {
      $model = $this->findModelTemp($model->id);
      // var_dump($model);

      if ($model->load($post)) {
        // var_dump($model);
        $model->job_fee = empty($model->job_fee) ? 0 : str_replace(',', '', $model->job_fee);
        $model->meal_allowance = empty($model->meal_allowance) ? 0 : str_replace(',', '', $model->meal_allowance);
        $model->taxi_allowance = empty($model->taxi_allowance) ? 0 : str_replace(',', '', $model->taxi_allowance);
        $model->ope_allowance = empty($model->ope_allowance) ? 0 : str_replace(',', '', $model->ope_allowance);
        $model->administrative_charge = empty($model->administrative_charge) ? 0 : str_replace(',', '', $model->administrative_charge);
        //$model->AdministrativeChargeAct = empty($model->AdministrativeCharge) ? 0 : str_replace(',', '', $model->AdministrativeCharge);
        $model->other_expense_allowance = empty($model->other_expense_allowance) ? 0 : str_replace(',', '', $model->other_expense_allowance);
        //$model->OtherExpenseAllowanceAct = empty($model->OtherExpenseAllowance) ? 0 : str_replace(',', '', $model->OtherExpenseAllowance);


        $model->job_area = empty($model->job_area) ? null : implode(',', $model->job_area);

        $model->partner_id = empty($model->partner_id) ? null : $model->partner_id;
        $model->manager_id = empty($model->manager_id) ? null : $model->manager_id;
        $model->supervisor_id = empty($model->supervisor_id) ? null : $model->supervisor_id;
        $model->updated_by = $user->user_id;
        // $model->job_status = 2;

        if ($submit == 1) {
          $model->flag = 2;
        }

        $result = HelperDB::save($model, false);

        $model = $this->findModelTemp($model->id);
        $result['state']['model']['isSubmit'] = false;

        $result['state']['model']['is_meal'] = $model->is_meal;
        $result['state']['model']['is_ope'] = $model->is_ope;
        $result['state']['model']['is_taxi'] = $model->is_taxi;

        $result['state']['model']['partner_id'] = $model->partner_id;
        $result['state']['model']['manager_id'] = $model->manager_id;
        $result['state']['model']['supervisor_id'] = $model->supervisor_id;

        $result['state']['model']['time_charges'] = empty($model->time_charges) ? 0 : $model->time_charges;
        $result['state']['model']['time_charges_actual'] = empty($model->time_charges_actual) ? 0 : number_format($model->time_charges_actual);

        $result['state']['model']['meal_allowance'] = empty($model->meal_allowance) ? 0 : $model->meal_allowance;
        $result['state']['model']['meal_allowance_actual'] = empty($model->meal_allowance_actual) ? 0 : number_format($model->meal_allowance_actual);

        $result['state']['model']['taxi_allowance'] = empty($model->taxi_allowance) ? 0 : $model->taxi_allowance;
        $result['state']['model']['taxi_allowance_actual'] = empty($model->taxi_allowance_actual) ? 0 : number_format($model->taxi_allowance_actual);

        $result['state']['model']['ope_allowance'] = empty($model->ope_allowance) ? 0 : $model->ope_allowance;
        $result['state']['model']['ope_allowance_actual'] = empty($model->ope_allowance_actual) ? 0 : number_format($model->ope_allowance_actual);

        $result['state']['model']['administrative_charge'] = empty($model->administrative_charge) ? 0 : $model->administrative_charge;
        $result['state']['model']['administrative_charge_actual'] = empty($model->administrative_charge) ? 0 : number_format($model->administrative_charge);

        $result['state']['model']['other_expense_allowance'] = empty($model->other_expense_allowance) ? 0 : $model->other_expense_allowance;
        $result['state']['model']['other_expense_allowance_actual'] = empty($model->other_expense_allowance) ? 0 : number_format($model->other_expense_allowance);

        $result['state']['model']['percentage'] = empty($model->percentage) ? 0 : $model->percentage;
        $result['state']['model']['percentage_actual'] = empty($model->percentage_actual) ? 0 : $model->percentage_actual;

        if ($result['state']['status'] == true) {
          if ($submit == 1) {
            $result['state']['model']['isSubmit'] = true;

            $modelReal = $this->findModel($model->id);
            // $modelReal->save();
            // $modelReal->partner_id = empty($model->partner_id) ? null : $model->partner_id;


            $modelReal->flag = 2;

            $resultReal = HelperDB::save($modelReal, false);
            // var_dump($modelReal);

            $result['state']['status'] = $resultReal['state']['status'];
            $result['state']['message'] = $resultReal['state']['message'];
            if ($result['state']['status'] == true) {
              if (!empty($model->partner->user_email)) {
                $send_email = $this->Email($model->partner->user_email, 'THERE IS A NEW REVISED JOB NEEDS YOUR APPROVAL', $model);
                if ($send_email) {
                  //return $this->redirect(['view', 'id' => $model->JobID]);
                } else {
                  $result['state']['status'] = false;
                  $result['state']['message'] = "Can't Send Email to Partner, please cek email address";
                }
              } else {
                $result['state']['status'] = false;
                $result['state']['message'] = "Partner's email not found";
              }
            }
          }
        }
      } else {
        $result = HelperDB::result_noloaded($model);
      }
    } else {
      $result = HelperDB::result_noloaded($model);
    }

    $result['model'] = [];

    return $result['state'];
  }

  public function actionRevise_view($id)
  {
    $model = $this->findModel($id);
    $header = AuditTrail::find()->where(['trans_id' => $id, 'source'=>'TR', 'trans_type'=>'JOB'])->orderBy('created_at DESC')->one();
    $details = AuditTrailDet::find()->where(['audit_trail_id' => $header->id])->all();
    // $details = AuditTrailDet::find()->where(['audit_trail_id' => $header->id])->andWhere("AuditDescription  != '' and AuditDescription is not null")->all();

    return $this->renderAjax('ReviseView', [
      'model' => $model,
      'header' => $header,
      'details' => $details,
    ]);
  }

  public function actionRevise_log($id)
  {
    $modelView = $this->findModelView($id);
    $searchModel = new AuditTrailSearch();
    // $searchModel->trans_id = $id;
    // $searchModel->source = 'TR';
    // $searchModel->trans_type = 'JOB';
    // Yii::$app->request->queryParams['source'] = 'TR';
    // var_dump(Yii::$app->request->queryParams);
    // die();
    // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    $dataProvider = $searchModel->search(['trans_id'=>$id, 'source'=>'TR', 'trans_type'=>'JOB']);
    // var_dump($dataProvider);
    return $this->render('ReviseLog', [
      'modelView' => $modelView,
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }
  public function actionRevise_log_detail()
  {
    $post = Yii::$app->request->post();

    $id = $post['expandRowKey'];
    $header = AuditTrail::find()->where(['id' => $id])->one();
    $details = AuditTrailDet::find()->where(['audit_trail_id' => $header->id])->all();
    // $details = AuditTrailDet::find()->where(['audit_trail_id' => $header->id])->andWhere("AuditDescription  != '' and AuditDescription is not null")->all();

    return $this->renderPartial('ReviseLogDetail', [
      'header' => $header,
      'details' => $details,
    ]);
  }

  public function actionRevise_approve($id = "")
  {
    $modelTemp = $this->findModelTemp($id);
    $model = $this->findModel($modelTemp->id);

    $transaction = \Yii::$app->db->beginTransaction();
    try {
      $model->description = $modelTemp->description;
      $model->job_area = $modelTemp->job_area;
      $model->start_date = $modelTemp->start_date;
      $model->end_date = $modelTemp->end_date;
      $model->partner_id = $modelTemp->partner_id;
      $model->manager_id = $modelTemp->manager_id;
      $model->supervisor_id = $modelTemp->supervisor_id;
      $model->include_ope = $modelTemp->include_ope;
      $model->is_meal = $modelTemp->is_meal;
      $model->is_ope = $modelTemp->is_ope;
      $model->is_taxi = $modelTemp->is_taxi;
      $model->meal_allowance = $modelTemp->meal_allowance;
      $model->ope_allowance = $modelTemp->ope_allowance;
      $model->administrative_charge = $modelTemp->administrative_charge;
      $model->other_expense_allowance = $modelTemp->other_expense_allowance;
      $model->taxi_allowance = $modelTemp->taxi_allowance;
      $model->job_fee = $modelTemp->job_fee;
      $model->flag = 0;


      $result = HelperDB::save($model, false);
      if ($result['state']['status'] == true) {

        $temps = JobBudgetingTemp::find()->where(['job_id' => $id])->all();
        if (!empty($temps)) {

          //Update all Job Budgeting from Temporary to Real
          $tmps_list = '';
          foreach ($temps as $tmp) {
            $JobBudgeting = JobBudgeting::find()->where(['job_id' => $tmp->job_id, 'employee_id' => $tmp->employee_id])->one();
            if (empty($JobBudgeting)) {
              $JobBudgeting = new JobBudgeting();
            }

            if (empty($tmps_list)) {
              $tmps_list .= "'" . $tmp->employee_id . "'";
            } else {
              $tmps_list .= ", '" . $tmp->employee_id . "'";
            }

            $JobBudgeting->job_id = $tmp->job_id;
            $JobBudgeting->employee_id = $tmp->employee_id;
            $JobBudgeting->planning = empty($tmp->planning) ? 0 : $tmp->planning;
            $JobBudgeting->field_work = empty($tmp->field_work) ? 0 : $tmp->field_work;
            $JobBudgeting->reporting = empty($tmp->reporting) ? 0 : $tmp->reporting;
            $JobBudgeting->wrap_up = empty($tmp->wrap_up) ? 0 : $tmp->wrap_up;
            $JobBudgeting->over_time = empty($tmp->over_time) ? 0 : $tmp->over_time;
            $JobBudgeting->total_wh = empty($tmp->total_wh) ? 0 : $tmp->total_wh;
            $JobBudgeting->update_by = $tmp->updated_by;
            $JobBudgeting->update_at = $tmp->updated_at;

            $employee_name = EmployeeList::find()->where(['employee_id' => $tmp->employee_id])->one();

            $result = HelperDB::save($JobBudgeting, false);
            if ($result['state']['status'] == true) {
              //Delete Temporary Job Budgeting :
              $JobBudgetingTemp = JobBudgetingTemp::find()->where(['job_id' => $tmp->job_id, 'employee_id' => $tmp->employee_id])->one();
              if (!$JobBudgetingTemp->delete()) {
                $result['state']['status'] = false;
                $result['state']['message'] = $JobBudgetingTemp->employee_id . " : " . HelperDB::db_field_error(Html::errorSummary($JobBudgetingTemp));
                break;
              }
            } else {
              $result['state']['status'] = false;
              $result['state']['message'] =  $employee_name->employee_desc . " : " . $result['state']['message'];
              // $result['state']['message'] = $JobBudgeting->employee_id . " : " . HelperDB::db_field_error(Html::errorSummary($JobBudgeting));
              break;
            }


          }
        }
      } else {
        $result['state']['status'] = false;
        $result['state']['message'] = "Can not save Job Temp : " . HelperDB::db_field_error(Html::errorSummary($model));
      }

      if ($result['state']['status'] == true) {
        //Delete Job Budgeting which not ini Job Budgeting Temp
        if (!empty($tmps_list)) {
          JobBudgeting::deleteAll('job_id = ' . $id . ' and (employee_id NOT IN (' . $tmps_list . ')) ');
        }

        //Delete Job Temp
        $modelTemp->delete();
        $transaction->commit();
      } else {
        $transaction->rollBack();
      }
    } catch (\yii\db\Exception $e) {
      $result['state']['status'] = false;
      $result['state']['message'] = HelperDB::db_trigger_error($e->getMessage());
    }

    return json_encode($result['state']);
  }

  public function actionRevise_reject($id = "")
  {
    $model = $this->findModel($id);
    $model->flag = 1;

    $result = HelperDB::save($model, false);
    if ($result['state']['status'] == false) {
      $result['state']['status'] = false;
      $result['state']['message'] = HelperDB::db_field_error(Html::errorSummary($model));
    }
    return json_encode($result['state']);
  }



  // BUDGET TEMPORARY

  public function actionBudget_temp($id = null, $action = null)
  {
    $session = Yii::$app->session;
    $session->open();

    $post = Yii::$app->request->post();
    $model = new JobTemp();
    $model->load($post);

    $modelJob = JobTemp::find()->where(['id' => $model->id])->one();
    if ($modelJob->load($post)) {
      $session->set('_trjob_form' . $modelJob->id, $modelJob);
      if (Yii::$app->request->isAjax) {
        if (!empty($id)) {
          $modelBudget = $this->findModelBudgetTemp($id, $modelJob->id);
        } else {
          $modelBudget = new JobBudgetingTemp();
          $modelBudget->job_id = $modelJob->id;
          if (empty($action)) {
            $modelBudget->isNewRecord;
          }
        }
        $data = $this->getDataForForm($modelJob);
        return $this->renderAjax('ReviseDetail', [
          'id' => empty($action) ? $id : "",
          'modelJob' => $modelJob,
          'modelBudget' => $modelBudget,
          'data' => $data
        ]);
      } else {
        throw new MethodNotAllowedHttpException('Please do not repeat this request again..');
      }
    }
  }
  public function actionBudgetsave_temp($id = null)
  {
    $result = HelperDB::result_default();
    $user = CommonHelper::getUserIndentity();
    $post = Yii::$app->request->post();

    $modelBudget = new JobBudgetingTemp();
    if ($modelBudget->load($post)) {
      if (!empty($id)) {
        $modelBudget = $this->findModelBudgetTemp($id, $modelBudget->job_id);
        $modelBudget->load($post);
      }

      $transaction = \Yii::$app->db->beginTransaction();
      try {
        $modelBudget->planning = empty($modelBudget->planning) ? 0 : $modelBudget->planning;
        $modelBudget->field_work = empty($modelBudget->field_work) ? 0 : $modelBudget->field_work;
        $modelBudget->reporting = empty($modelBudget->reporting) ? 0 : $modelBudget->reporting;
        $modelBudget->wrap_up = empty($modelBudget->wrap_up) ? 0 : $modelBudget->wrap_up;
        $modelBudget->over_time = empty($modelBudget->over_time) ? 0 : $modelBudget->over_time;
        $modelBudget->total_wh = empty($modelBudget->total_wh) ? 0 : $modelBudget->total_wh;
        $valid = $modelBudget->validate();
        $modelBudget->created_by = $user->user_id;
        if ($modelBudget->over_time == null) {
          $modelBudget->over_time = 0;
        }
        if ($modelBudget->save()) {
          $transaction->commit();
          $result = HelperDB::result_success();
        } else {
          $result['state']['status'] = false;
          $result['state']['message'] = HelperDB::db_field_error(Html::errorSummary($modelBudget));
        }
      } catch (\yii\db\Exception $e) {
        $result['state']['status'] = false;
        $result['state']['message'] = HelperDB::db_trigger_error($e->getMessage());
      }
    } else {
      $result = HelperDB::result_noloaded($modelBudget);
    }

    return json_encode($result['state']);
  }

  public function actionBudgetdelete_temp()
  {
    $return = false;
    $post = Yii::$app->request->post();

    $EmployeeID = $post['EmployeeID'];
    $JobID = $post['JobID'];
    $modelBudget = $this->findModelBudgetTemp($EmployeeID, $JobID);

    $transaction = \Yii::$app->db->beginTransaction();
    try {
      if ($modelBudget->delete()) {
        $transaction->commit();
        $result = HelperDB::result_success();
      } else {
        $result['state']['status'] = false;
        $result['state']['message'] = HelperDB::db_field_error(Html::errorSummary($modelBudget));
        $transaction->rollBack();
      }
    } catch (Exception $e) {
      $result['state']['status'] = false;
      $result['state']['message'] = HelperDB::db_trigger_error($e->getMessage());
      $transaction->rollBack();
    }

    // $transaction = \Yii::$app->db->beginTransaction();
    // try {
    //   if ($modelBudget->delete()) {
    //     $transaction->commit();
    //     $return = true;
    //   } else {
    //     $transaction->rollback();
    //   }
    // } catch (Exception $e) {
    //   $transaction->rollBack();
    // }

    return $return;
  }


















  //UTIITES


  public function actionBudget_totalwh()
  {
    $post = Yii::$app->request->post();
    $modelJob = new JobBudgeting();
    if (!$modelJob->load($post)) {
      $modelJob = new JobBudgetingTemp();
      $modelJob->load($post);
    }

    $sql = "call common_list('job-task-ae','" . $modelJob->employee_id . ";" . $modelJob->total_wh . "')";
    $task = Yii::$app->db->createCommand($sql)->queryOne();

    return json_encode($task);
  }

  public function actionBudget_latesjob()
  {
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    $post = Yii::$app->request->post();

    $modelJob = new Job();
    $modelJob->load($post);

    $result = \Yii::$app->db->createCommand("select common_val('new-job-no',:paramName1);")
      ->bindValue(':paramName1', $modelJob->client_id);
    $data['job'] = $result->queryScalar();
    $data['client_id'] = $modelJob->client_id;

    // $result = \Yii::$app->db->createCommand("select name_val('new-job-no',:paramName1,null)")
    //   ->bindValue(':paramName1', $modelJob->ClientID);
    // $data['job'] = $result->queryScalar();
    // $data['ClientID'] = $modelJob->ClientID;

    return $data;
  }

  public function actionBudget_getlevelid()
  {
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    $post = Yii::$app->request->post();
    $modelJob = new JobBudgeting();
    $modelJob->load($post);

    $data['fullName'] = empty($modelJob->employee->full_name) ? "" : $modelJob->employee->full_name;
    $data['levelName'] = empty($modelJob->employee->level->level_name) ? "" : $modelJob->employee->level->level_name;
    $data['eA'] = $this->GetEmployeeAvailable($modelJob->employee_id);

    return $data;
  }

  public function GetEmployeeAvailable($id)
  {
    $sql = "call common_list('job-wh-available','{$id}')";
    $data = Yii::$app->db->createCommand($sql)->queryAll();
    $html = "";
    if (!empty($data)) {
      $html .= "<div class='well table-responsive' style='margin: 10px 0 30px; max-height : 130px;'>";
      $html .= "<table class='table table-primary'>";
      $html .= "<tr>
            <td class='bg-primary'>No</td>
            <td class='bg-primary'>Client</td>
            <td class='bg-primary'>Description</td>
            <td class='bg-primary'>WH</td>
        </tr>";
      $no = 1;
      foreach ($data as $d) {
        $html .= "<tr>";
        $html .= "<td>" . $no++ . "</td>";
        $html .= "<td>" . $d['client_desc'] . "</td>";
        $html .= "<td>" . $d['job_description'] . "</td>";
        $html .= "<td>" . $d['wh_available'] . "</td>";
        $html .= "<tr>";
      }

      $html .= "</table>";
      $html .= "</div>";
    } else {
      $html .= "";
    }
    return $html;
  }




  public function actionView($id, $comment = '')
  {
    Yii::$app->cache->flush();

    $modelJob = $this->findModel($id);
    $modelView = $this->findModelView($id);
    $modelComments = new JobComment();
    $modelComments->job_id = $id;
    $searchModelBudget = new JobBudgetingSearch([
      'job_id' => $id,
    ]);
    $dataProviderBudget = $searchModelBudget->search(Yii::$app->request->queryParams);

    return $this->render('view', [
      'modelJob' => $modelJob,
      'modelView' => $modelView,
      'modelComments' => $modelComments,
      'dataProviderBudget' => $dataProviderBudget,
    ]);
  }


  public function actionComments($id = '')
  {
    // var_dump($id);
    $user = CommonHelper::getUserIndentity();
    $modelComments = new JobComment();
    $post = Yii::$app->request->post();
    $modelComments->load($post);

    //Start read comment
    $count = JobComment::find()
      ->select(['COUNT(*) AS jml'])
      ->where(['job_id' => $id])
      ->andWhere(['like', 'list_employee', $user->user_id])
      ->count();

    if ($count != 0) {
      $modelCommentsRead = JobComment::find()
        ->where(['JobID' => $id])
        ->andWhere(['like', 'list_employee', $user->user_id])->one();
      foreach (explode(',', $modelCommentsRead->list_employee) as $key => $value) {
        if ($value != $user->user_id) {
          $data[] = $value;
        }
      }
      $readcomment = implode(', ', $data);

      $modelCommentsRead->list_employee = $readcomment;
      Yii::$app->db->createCommand()
        ->update('tr_job_comment', ['list_employee' => $readcomment], 'job_id = ' . $id . '')
        ->execute();
    }
    //End read comment

    $searchmodelComments = new JobCommentSearch([
      'job_id' => !empty($id) ? $id : $modelComments->job_id,
    ]);
    $dataProviderComments = $searchmodelComments->search(Yii::$app->request->queryParams);

    return $this->renderAjax('viewComments', [
      'dataProviderComments' => $dataProviderComments,
      'job_id' => $modelComments->job_id,
    ]);
  }
  public function actionCommentsave()
  {
    $post = Yii::$app->request->post();
    $html = '';
    $modelComments = new JobComment();
    if ($modelComments->load($post)) {
      $user = CommonHelper::getUserIndentity();

      $modelComments->created_by = $user->user_id;
      if ($modelComments->save()) {
        $html = '';
      }
    }
    return $html;
  }




  protected function Email($sendto, $subject, $modelJob, $status = "", $layouts = 'layouts/newjobsapproval')
  {
    // var_dump('a');
    // die();
    Yii::$app->cache->flush();
    $from = Yii::$app->params['appNotificationEmail'];
    $cc = Yii::$app->params['appNotificationEmailCC'];

    $to = array();
    $to[$sendto] = $sendto;

    if (!empty($to)) {
      $to = $to;
      $html = ['html' => $layouts];
      $html_bind = ['title' => $subject, 'modelJob' => $modelJob, 'status' => $status];

      $compose = Yii::$app->mailer->compose($html, $html_bind);
      $compose->setFrom($from);
      $compose->setTo($to);
      if (!empty($cc)) {
        $compose->setCc($cc);
      }
      $compose->setSubject($subject);

      if ($compose->send()) {
        return "true";
      } else {
        return "false";
      }
    }
  }

  /* END RAPIH */
}
