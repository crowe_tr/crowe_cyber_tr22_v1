<?php

namespace backend\modules\tr\controllers;

use Yii;
use yii\helpers\Html;

use common\components\CommonHelper;
// use common\components\TimeReportHelper;

use common\models\tr\VJob;
// use common\models\tr\JobBudgeting;
// use common\models\tr\JobTemp;
// use common\models\tr\JobBudgetingTemp;
// use common\models\tr\JobComment;
// use common\models\tr\tr_previous_changes_job;
// use common\models\tr\ms\Entity;
// use common\models\tr\search\JobSearch;
// use common\models\tr\search\JobviewSearch;
// use common\models\tr\search\JobBudgetingSearch;
// use common\models\tr\search\JobBudgetingTempSearch;
// use common\models\tr\search\JobCommentSearch;
// use common\models\cl\Client;
// use common\models\hr\VempGroup;
// use common\models\hr\EmployeeGroup;
// use common\models\hr\Employee;
use  yii\data\ArrayDataProvider;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


class VJobController extends Controller
{
    protected function findModel($id)
    {
        if (($model = VJob::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionView($id, $comment = '', $frommail = 0)
    {
        if ($frommail == 1) {
            return $this->frommail($id, $comment = '');
        } else {
            return $this->view($id, $comment = '');
        }
    }

    public function view($id, $comment = '')
    {
        $post = Yii::$app->request->post();
        $user = CommonHelper::getUserIndentity();

        $modelJob = $this->findModel($id);
        $modelComments = new JobComment();
        $modelComments->JobID = $id;

        $searchModelBudget = new JobBudgetingSearch([
                            'JobID' => $id,
                        ]);
        $dataProviderBudget = $searchModelBudget->search(Yii::$app->request->queryParams);

        $count = JobComment::find()
          ->select(['COUNT(*) AS jml'])
          ->where(['JobID' => $id])
          ->andWhere(['like','ListEmployee' , $user->Id])
          ->count();
            $commentCount['count'] = $count;
            // var_dump($commentCount['count']);

        if (!empty($id)) {
          // code...
          // $sql = "call JobEstimated('".$id."')";
          // $task = Yii::$app->db->createCommand($sql)->queryOne();
          // $modelTask['TimeCharges'] =  $task['TimeCharges'];
          // $modelTask['RecoveryRate'] =  $task['RecoveryRate'];
          // $modelTask['MealAllowance'] =  $task['MealAllowance'];
          // $modelTask['TaxiAllowance'] =  $task['TaxiAllowance'];
          // $modelTask['OutOfOfficeAllowance'] =  $task['OutOfOfficeAllowance'];
          // $modelTask['TimeCharges'] =  0;
          $modelTask['RecoveryRate'] =  0;
          $modelTask['MealAllowance'] =  0;
          $modelTask['TaxiAllowance'] =  0;
          $modelTask['OutOfOfficeAllowance'] =  0;
        }
        else{
          // $modelTask['TimeCharges'] =  0;
          $modelTask['RecoveryRate'] =  0;
          // $modelTask['OutOfOfficeAllowance'] =  0;
          // $modelTask['TaxiAllowance'] =  0;
          // $modelTask['OutOfOfficeAllowance'] =  0;

        }

        return $this->render('view', [
            'modelJob' => $modelJob,
            'modelComments' => $modelComments,
            'searchModelBudget' => $searchModelBudget,
            'dataProviderBudget' => $dataProviderBudget,
            'modelTask' => $modelTask,
            'commentCount' => $commentCount,
            // 'param' => $param,
        ]);
    }





}
