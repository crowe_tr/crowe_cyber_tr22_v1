<?php

namespace backend\modules\tr\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\MethodNotAllowedHttpException;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\base\ErrorException;
use common\models\cm\CmReport;
use common\models\tr\views\ReportTimeReport;
use common\models\tr\views\ReportTimeReportSearch;
use common\components\CommonHelper;
use common\components\TimeReportHelper;
use kartik\mpdf\Pdf;
use common\models\st\TaskType;
use common\models\st\Task;

class ReportController extends Controller
{
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    //=============== START REPORT GENERATOR ===============
    public function generate_results($view, $model, $fields, $data)
    {
      // var_dump($fields);
      // die();
        // $data = Yii::$app->db->createCommand($sql)->queryAll();
        if ($model->ReportFormat == "xls") {
            $results = $this->renderPartial($view, [
                'data' => $data,
                'fields' => $fields,
                'format' => $model->ReportFormat,
                'model' => $model,
            ]);
        } else {
            $results = $this->renderPartial($view, [
                'data' => $data,
                'fields' => $fields,
                'format' => $model->ReportFormat,
                'model' => $model,
            ]);
        }

        return $results;
    }
    public function generate_report($data, $filename, $format, $param = '')
    {
        if ($format == 'xls') {
            $filename = $filename . '.xls';
            $fopen = fopen($filename, 'w');
            file_put_contents($filename, $data);
            fclose($fopen);

            Yii::$app->response->SendFile($filename);
            unlink($filename);
        } else if ($format == 'pdf') {
            $pdf = new Pdf([
                'mode' => Pdf::MODE_CORE,
                'format' => Pdf::FORMAT_A4,
                'orientation' => Pdf::ORIENT_LANDSCAPE,
                'destination' => Pdf::DEST_DOWNLOAD,
                'marginTop' => 8,
                'marginBottom' => 8,
                'marginRight' => 8,
                'marginLeft' => 8,
                'filename' => 'REPORT # ' . rand() . '.pdf',

                'cssInline' => '
                *, p, span, b {
                    font-family: arial;
                    font-size: 13px !important;
                    padding:0;
                    margin:0;
                    text-size-adjust: none;
                    text-transform: uppercase;
                }
                .table {
                    border-collapse: collapse;
                    display: block;
                    text-transform: uppercase;
                    text-size-adjust: none;
                }
                .table th{
                    padding: 5px;
                    vertical-align: top;
                    font-size: 12px !important;
                    background: #007be8 !important;
                }
                .table td {
                    padding: 5px;
                    vertical-align: top;
                    font-size: 12px !important;
                }
                .table .bg-primary {
                    background: #007be8 !important;
                }
                ',
                'content' => $data,
            ]);
            return $pdf->render();
        } else {
            return $data;
        }
        return $data;
    }
    public function get_fields_options($fields)
    {
        $options = [];
        $fields = array_keys($fields);
        foreach ($fields as $f) {
            $options[$f] = ['selected' => 'selected'];
        }
        return $options;
    }
    public function report($_view, $_sql, $_report)
    {
        $this->layout = "../../../../views/layouts/report";

        $posts = Yii::$app->request->post();
        $model = new CmReport();

        $data = "";
        $fields = [];
        $fields['data'] = $_sql['field'];
        $fields['selected'] = $fields['data'];
        $fields['options'] = $this->get_fields_options($fields['selected']);

        if ($model->load($posts)) {
            if (!empty($model->Date)) {
                $param = $this->parameter($model);
                $fields['selected'] = empty($param['fields']) ? $fields['selected'] : $param['fields'];
                $fields['options'] = $this->get_fields_options($fields['selected']);

                $sql = $this->call_sql($_sql['name'], $param);
                // var_dump($param);
                // die();
                // $sql = $this->sql($_sql['name'], $param);

                $data_job = Yii::$app->db->createCommand($sql)->queryAll();
                if ( $_sql['name'] == 'rpt_recovery_rate_summary') {
                  foreach ($data_job as $key => $val) {
                    $tc_val = Yii::$app->db->createCommand("call rpt_val('tc-value-group',". $val['job_id'] .")")->queryOne();
                    $data_job[$key]['tc_partner'] = ($tc_val['tc_partner'] != 0) ? $tc_val['tc_partner'] : 0 ;
                    $data_job[$key]['tc_director'] = ($tc_val['tc_director'] != 0) ? $tc_val['tc_director'] : 0 ;
                    $data_job[$key]['tc_manager'] = ($tc_val['tc_manager'] != 0) ? $tc_val['tc_manager'] : 0 ;
                    $data_job[$key]['tc_supervisor'] = ($tc_val['tc_supervisor'] != 0) ? $tc_val['tc_supervisor'] : 0 ;
                    $data_job[$key]['tc_senior'] = ($tc_val['tc_senior'] != 0) ? $tc_val['tc_senior'] : 0 ;
                    $data_job[$key]['tc_junior'] = ($tc_val['tc_junior'] != 0) ? $tc_val['tc_junior'] : 0 ;
                    $data_job[$key]['tc_others'] = ($tc_val['tc_others'] != 0) ? $tc_val['tc_others'] : 0 ;

                    $job_progress = Yii::$app->db->createCommand("call rpt_val('job-progress',". $val['job_id'] .")")->queryOne();
                    $data_job[$key]['progress_time'] = ($job_progress['progress_time'] != "") ? $job_progress['progress_time'] : "" ;

                    $data_job[$key]['total_charges'] = $val['tc_total'] + $val['tc_other'];
                  }
                }

                $results = $this->generate_results($_view['results'], $model, $fields, $data_job);
                $data = $this->generate_report($results, $_report['name'] . date('ymdhis'), $model->ReportFormat);
            }
        }

        $init = $this->get_init($model);
        return $this->render($_view['form'], [
            'model' => $model,
            'init' => $init,
            'data' => $data,
            'fields' => $fields,
            'options' => $_report,
        ]);
    }
    public function get_init($model)
    {
        $param = [];
        $param['TaskTypeID'] = ArrayHelper::map(TaskType::find()->where(['flag' => 1])->asArray()->all(), 'id', 'task_type_name');;
        $param['TaskID'] = empty($model->Custom['TaskTypeID']) ? [] : ArrayHelper::map(Task::find()->where(['task_type_id' => $model->Custom['TaskTypeID']])->asArray()->all(), 'id', 'task_name');;
        return $param;
    }
    public function parameter($model)
    {
        $param = [];
        $Custom = $model->Custom;
        // var_dump($Custom);
        // die();
        $param['date'] = explode(" TO ", $model->Date);
        $param['date1'] = date('Y-m-d', strtotime($param['date'][0]));
        $param['date2'] = date('Y-m-d', strtotime($param['date'][1]));
        $param['entity'] = empty($Custom['Entity']) ? "null" : "'" . $Custom['Entity'] . "'";
        $param['division'] = empty($Custom['Division']) ? "null" : "'" . $Custom['Division'] . "'";
        $param['dept'] = empty($Custom['deptID']) ? "null" : "'" . $Custom['deptID'] . "'";
        $param['group'] = empty($Custom['parentID']) ? "null" : "'" . $Custom['parentID'] . "'";
        $param['manager'] = empty($Custom['ManagerID']) ? "null" : "'" . $Custom['ManagerID'] . "'";
        $param['supervisor'] = empty($Custom['SupervisorID']) ? "null" : "'" . $Custom['SupervisorID'] . "'";
        $param['level'] = empty($Custom['levelID']) ? "null" : "'" . $Custom['levelID'] . "'";
        $param['employee'] = empty($Custom['Employee']) ? "null" : "'" . $Custom['Employee'] . "'";
        $param['EmployeeList'] = empty($Custom['EmployeeList']) ? "null" : "'" . implode(",", $Custom['EmployeeList']) . "'";
        //
        $param['TypeName'] = empty($Custom['TaskTypeID']) ? "null" : "'" . $Custom['TaskTypeID'] . "'";
        $param['TaskName'] = empty($Custom['TaskID']) ? "null" : "'" . $Custom['TaskID'] . "'";
        $param['client_code'] = empty($Custom['ClientCode']) ? "null" : "'" . $Custom['ClientCode'] . "'";
        $param['job_code'] = empty($Custom['JobCode']) ? "null" : "'" . $Custom['JobCode'] . "'";
        $param['status'] = empty($Custom['status']) ? "null" : "'" . $Custom['status'] . "'";
        // $param['partner'] = empty($Custom['PartnerID']) ? "null" : "'" . $Custom['PartnerID'] . "'";
        $param['TaxiType'] = empty($Custom['TaxiType']) ? "null" : "'" . $Custom['TaxiType'] . "'";
        $param['job_status'] = empty($Custom['job_status']) ? "null" : "'" . implode(",", $Custom['job_status']) . "'";
        //
        // $param['year'] = empty($Custom['year']) ? "null" : "'" . $Custom['year'] . "'";
        //
        // $param['fields'] = empty($Custom['fields']) ? [] : $Custom['fields'];
        return $param;
    }

    public function call_sql($_sql, $param)
    {
      switch ($_sql) {
        case 'rpt_recovery_rate_summary':
          $sql =  "
                    call rpt_job(
                        " . $param['entity'] . ",
                        " . $param['division'] . ",
                        " . $param['group'] . ",
                        " . $param['manager'] . ",
                        " . $param['client_code'] . ",
                        " . $param['job_code'] . "
                    );
                  ";
          break;

        case 'rpt_recovery_rate_detail':
          $sql =  "
                  call rpt_val(
                      'job-detail',
                      " . $param['job_code'] . "
                  );
                ";
          break;

          case 'rpt_job_detail':
            $sql =  "
                    call rpt_job_detail(
                        " . $param['EmployeeList'] . ",
                        " . $param['entity'] . ",
                        " . $param['group'] . ",
                        " . $param['job_status'] . ",
                        '" . $param['date1'] . "',
                        '" . $param['date2'] . "'
                    );
                  ";
            // var_dump($sql);
            // die();
            break;

            case 'rpt_job_detail_v2':
                $sql =  "
                        call rpt_job_detail_v2(
                            " . $param['EmployeeList'] . ",
                            " . $param['entity'] . ",
                            " . $param['group'] . ",
                            " . $param['job_status'] . ",
                            '" . $param['date1'] . "',
                            '" . $param['date2'] . "'
                        );
                      ";
                // var_dump($sql);
                // die();
                break;

            case 'rpt_recap_perform':
                $sql =  "
                        call rpt_recap_perform(
                            " . $param['EmployeeList'] . ",
                            " . $param['entity'] . ",
                            " . $param['group'] . ",
                            " . $param['status'] . ",
                            '" . $param['date1'] . "',
                            '" . $param['date2'] . "'
                        );
                      ";
                // var_dump($sql);
                // die();
                break;

        case "rpt_time_report_daily":
            // var_dump($param['level']);
            $sql = "call rpt_time_report_daily(
                '" . $param['date1'] . "',
                '" . $param['date2'] . "',
                " . $param['entity'] . ",
                " . $param['division'] . ",
                " . $param['dept'] . ",
                " . $param['group'] . ",
                " . $param['manager'] . ",
                " . $param['level'] . ",
                " . $param['employee'] . "
            );";
            break;

        case "rpt_time_report_detail":
            $sql = "
                call rpt_time_report_detail(
                    '" . $param['date1'] . "',
                    '" . $param['date2'] . "',
                    " . $param['entity'] . ",
                    " . $param['division'] . ",
                    " . $param['TypeName'] . ",
                    " . $param['TaskName'] . ",
                    " . $param['client_code'] . ",
                    " . $param['job_code'] . ",
                    " . $param['manager'] . ",
                    " . $param['group'] . ",
                    " . $param['employee'] . ",
                    " . $param['status'] . "
                );
                ";
            break;

        case "rpt_tr_det_office88":
            $sql = "call rpt_tr_det_office88(
                '" . $param['date1'] . "',
                '" . $param['date2'] . "',
                " . $param['EmployeeList'] . "
            );";
            break;

        case "rpt_taxi_detail":
            $sql = "
                call rpt_taxi_detail(
                    '" . $param['date1'] . "',
                    '" . $param['date2'] . "',
                    " . $param['entity'] . ",
                    " . $param['division'] . ",
                    " . $param['dept'] . ",
                    " . $param['TaxiType'] . ",
                    " . $param['group'] . ",
                    " . $param['manager'] . ",
                    " . $param['TypeName'] . ",
                    " . $param['TaskName'] . ",
                    " . $param['client_code'] . ",
                    " . $param['job_code'] . ",
                    " . $param['level'] . ",
                    " . $param['employee'] . ",
                    " . $param['status'] . "
                );
            ";
            break;

        case "rpt_annual_report_detail":
            $sql = "call rpt_annual_report_detail(
                " . $param['employee'] . ",
                " . $param['year'] . "
            );";
            break;

        case "rpt_sick_list":
            $sql = "call rpt_sick_list(
                '" . $param['date1'] . "',
                '" . $param['date2'] . "',
                " . $param['entity'] . ",
                " . $param['division'] . ",
                " . $param['group'] . ",
                " . $param['manager'] . ",
                " . $param['supervisor'] . ",
                " . $param['employee'] . "
            );";
            break;

        default:
          $sql = '';
          break;
      }

      return $sql;
    }

    public function sql($_sql, $param)
    {
        switch ($_sql) {
            case "rpt_time_report_daily":
                $sql = "call rpt_time_report_daily(
                    '" . $param['date1'] . "',
                    '" . $param['date2'] . "',
                    " . $param['entity'] . ",
                    " . $param['division'] . ",
                    " . $param['dept'] . ",
                    " . $param['group'] . ",
                    " . $param['manager'] . ",
                    " . $param['level'] . ",
                    " . $param['employee'] . "
                );";
                break;
            case "rpt_time_report_detail":
                $sql = "
                    call rpt_time_report_detail(
                        '" . $param['date1'] . "',
                        '" . $param['date2'] . "',
                        " . $param['entity'] . ",
                        " . $param['division'] . ",
                        " . $param['TypeName'] . ",
                        " . $param['TaskName'] . ",
                        " . $param['ClientCode'] . ",
                        " . $param['JobCode'] . ",
                        " . $param['manager'] . ",
                        " . $param['group'] . ",
                        " . $param['employee'] . ",
                        " . $param['status'] . "
                    );
                    ";
                break;
            case "rpt_recovery_rate_summary":
                $sql = "
                        call rpt_recovery_rate_summary(
                            " . $param['entity'] . ",
                            " . $param['division'] . ",
                            " . $param['partner'] . ",
                            " . $param['manager'] . ",
                            " . $param['ClientCode'] . ",
                            " . $param['JobCode'] . ",
                            " . $param['status'] . "
                        );
                    ";
                break;
            case "rpt_taxi_detail":
                $sql = "
                    call rpt_taxi_detail(
                        '" . $param['date1'] . "',
                        '" . $param['date2'] . "',
                        " . $param['entity'] . ",
                        " . $param['division'] . ",
                        " . $param['dept'] . ",
                        " . $param['TaxiType'] . ",
                        " . $param['group'] . ",
                        " . $param['manager'] . ",
                        " . $param['TypeName'] . ",
                        " . $param['TaskName'] . ",
                        " . $param['ClientCode'] . ",
                        " . $param['JobCode'] . ",
                        " . $param['level'] . ",
                        " . $param['employee'] . ",
                        " . $param['status'] . "
                    );
                ";
                break;
            case "rpt_annual_report_summary":
                $sql = "call rpt_annual_report_summary2(
                    " . $param['year'] . ",
                    " . $param['entity'] . ",
                    " . $param['division'] . ",
                    " . $param['dept'] . ",
                    " . $param['group'] . ",
                    " . $param['manager'] . ",
                    " . $param['level'] . ",
                    " . $param['employee'] . "
                );";
                break;
            case "rpt_annual_report_detail":
                $sql = "call rpt_annual_report_detail(
                    " . $param['employee'] . ",
                    " . $param['year'] . "
                );";
                break;
            case "rpt_sick_list":
                $sql = "call rpt_sick_list(
                    '" . $param['date1'] . "',
                    '" . $param['date2'] . "',
                    " . $param['entity'] . ",
                    " . $param['division'] . ",
                    " . $param['group'] . ",
                    " . $param['manager'] . ",
                    " . $param['supervisor'] . ",
                    " . $param['employee'] . "
                );";
                break;
            case "rpt_recovery_rate_detail":
                $sql = "call rpt_recovery_rate_detail(
                        " . $param['JobCode'] . "
                    );";
                break;
            case "rpt_tr_det_office88":
                $sql = "call rpt_tr_det_office88(
                    '" . $param['date1'] . "',
                    '" . $param['date2'] . "',
                    " . $param['EmployeeList'] . "
                );";
                break;
            default:
                $sql = "";
        }
        return $sql;
    }
    //=============== END REPORT GENERATOR ===============

    public function actionIndex()
    {

        return $this->render('index', []);
    }

    public function actionDaily()
    {
        $_view['form'] = 'daily/form';
        $_view['results'] = 'daily/report';

        $_sql['name'] = "rpt_time_report_daily";
        $_sql['field'] = [
            'employee_id' => 'EMPLOYEE ID', //
            'employee_name' => 'EMPLOYEE NAME', //
            'employee_level' => 'EMPLOYEE LEVEL', //
            'entity_name' => 'ENTITY', //
            'div_name' => 'DIVISION', //
            'dept_name' => 'DEPARTMENT', //
            'group_id' => 'GROUP ID', //
            'group_name' => 'GROUP NAME', //
            'group_initial' => 'GROUP INITIAL', //
            'manager_id' => 'MANAGER ID', //
            'manager_name' => 'MANAGER NAME', //
            'manager_initial' => 'MANAGER INITIAL', //
            'calendar_day' => 'DAY', //
            'calendar_date' => 'DATE', //
            'description' => 'CALENDAR DESCRIPTION', //
            'work_hour' => 'WH', //
            'over_time' => 'OT', //
            'meal_value' => 'MEALS', //
            'ope_value' => 'OPE', //
            'taxi_value' => 'TAXI', //
            'tr_status_text' => 'Status',
        ];

        $_report['name'] = 'Time Report - Daily';
        return $this->report($_view, $_sql, $_report);
    }
    public function actionDetail()
    {
        $_view['form'] = 'detail/form';
        $_view['results'] = 'detail/report';

        $_sql['name'] = "rpt_time_report_detail";

        $_sql['field'] = [
            'employee_id' => 'EMPLOYEE ID', //
            'employee_name' => 'NAME', //
            'level_name' => 'POSITION', //
            'entity_name' => 'ENTITY', //
            'div_name' => 'DIVISION', //
            'group_id' => 'GROUP ID',  //
            'group_name' => 'GROUP NAME',  //
            'group_initial' => 'GROUP INITIAL',  //
            'approval2_id' => 'MANAGER ID',  //
            'approval2_name' => 'MANAGER NAME',  //
            'approval2_initial' => 'MANAGER INITIAL',  //
            'approval1_id' => 'SUPERVISOR ID',  //
            'approval1_name' => 'SUPERVISOR NAME',  //
            'approval1_initial' => 'SUPERVISOR INITIAL',  //
            'calendar_day' => 'DAY', //
            'tr_date' => 'DATE', //
            'task_type_name' => 'TYPE NAME', //
            'task_name' => 'TASK NAME', //
            'job_code' => 'JOB CODE', //
            'client_code' => 'CLIENT CODE', //
            'client_name' => 'CLIENT NAME', //
            // 'job_description' => 'JOB DESCRIPTION', //
            'job_client_description' => 'JOB DESCRIPTION', //
            'job_area' => 'ZONE', //
            'tr_detail_value' => 'MAN HOURS', //
            'work_hour' => 'WH', //
            'over_time' => 'OT', //
            'meal_amount' => 'MEALS', //
            'ope_amount' => 'OPE', //
            'meals_ope' => 'TOTAL',
            'taxi_desc' => 'TAXI TYPE', //
            'taxi_amount' => 'TAXI AMOUNT', //
            'tr_description' => 'TR DESCRIPTION', //
            'tr_status_description' => 'Status', //
        ];

        $_report['name'] = 'Time Report - Detail';
        return $this->report($_view, $_sql, $_report);
    }
    public function actionRecovery_rate_summary()
    {
        $_view['form'] = 'recovery_rate_summary/form';
        $_view['results'] = 'recovery_rate_summary/report';

        $_sql['name'] = "rpt_recovery_rate_summary";

        $_sql['field'] = [
            'entity_name' => 'ENTITY', //
            'div_name' => 'DIVISION', //
            'client_code' => 'CUSTOMER', //
            'client_name' => 'CLIENT', //
            'client_industry' => 'INDUSTRY', //
            'job_code' => 'JOB CODE', //
            'job_description' => 'JOB DESCRIPTION', //
            'start_date' => 'START DATE', //
            'end_date' => 'FINISH DATE', //
            'job_status' => 'STATUS', //
            'manager_id' => 'MANAGER ID', //
            'manager_full_name' => 'MANAGER NAME', //
            'manager_initial' => 'MANAGER INITIAL', //
            'partner_id' => 'PARTNER ID', //
            'partner_full_name' => 'PARTNER NAME', //
            'partner_initial' => 'PARTNER INITIAL', //
            'job_fee' => 'JOB FEE', //

            'time_charges' => 'EST TIME CHARGE',
            // 'est_expense' => 'EST EXPENSE',
            'meal_allowance' => 'EST MEAL',
            'ope_allowance' => 'EST OPE',
            'taxi_allowance' => 'EST TAXI',
            'administrative_charge' => 'EST ADM CHARGE',
            'other_expense_allowance' => 'EST OTHER EXP',

            'tc_partner' => 'PARTNER', // PARTNER,MANAGING PARTNER,DEPUTY MANAGING PARTNER
            'tc_director' => 'DIRECTOR', // DIRECTOR,ASSOCIATE DIRECTOR
            'tc_manager' => 'MANAGER', // SENIOR MANAGER,MANAGER,ASSOCIATE MANAGER,MANAGER 3
            'tc_supervisor' => 'SUPERVISOR', // SUPERVISOR,SUPERVISOR 1,SUPERVISOR 2,SUPERVISOR 3
            'tc_senior' => 'SENIOR', // SENIOR,SENIOR 1,SENIOR 2,SENIOR 3
            'tc_junior' => 'JUNIOR', // JUNIOR
            'tc_others' => 'OTHER', // SECRETARY,CHAIRMAN,PRINCIPAL,INTERNSHIP,ADMIN STAFF,ADMINISTRASI,MANAGEMENT TRAINEE

            'tc_total' => 'TOTAL TIME CHARGE', //
            'tc_other' => 'OTHER EXPENSE', //
            'total_charges' => 'TOTAL CHARGES', //
            'rate_time_charges' => 'RECOVERY TIME CHARGES', //
            'rate_total_charges' => 'RECOVERY TOTAL CHARGES', //
            'progress_time' => 'PROGRESS TIME',  //

        ];

        // $_sql['field'] = [
        //     'entity_name' => 'ENTITY', //
        //     'div_name' => 'DIVISION', //
        //     'cust_code' => 'CUSTOMER', //
        //     'client_name' => 'CLIENT', //
        //     'industry_name' => 'INDUSTRY', //
        //     'job_code' => 'JOB CODE', //
        //     'job_description' => 'JOB DESCRIPTION', //
        //     'job_start_date' => 'START DATE', //
        //     'job_finish_date' => 'FINISH DATE', //
        //     'job_status' => 'STATUS', //
        //     'manager_id' => 'MANAGER ID', //
        //     'manager_name' => 'MANAGER NAME', //
        //     'manager_initial' => 'MANAGER INITIAL', //
        //     'partner_id' => 'PARTNER ID', //
        //     'partner_name' => 'PARTNER NAME', //
        //     'partner_initial' => 'PARTNER INITIAL', //
        //     'job_fee' => 'JOB FEE', //
        //     'tc_partner' => 'PARTNER', //
        //     'tc_senior_manager_3' => 'SENIOR MANAGER 3', //
        //     'tc_senior_manager_2' => 'SENIOR MANAGER 2', //
        //     'tc_senior_manager_1' => 'SENIOR MANAGER 1', //
        //     'tc_manager_3' => 'MANAGER 3', //
        //     'tc_manager_2' => 'MANAGER 2', //
        //     'tc_manager_1' => 'MANAGER 1', //
        //     'tc_associate_manager' => 'ASSOCIATE MANAGER', //
        //     'tc_supervisor_2' => 'SUPERVISOR 2', //
        //     'tc_supervisor_1' => 'SUPERVISOR 1', //
        //     'tc_senior_3' => 'SENIOR 3', //
        //     'tc_senior_2' => 'SENIOR 2', //
        //     'tc_senior_1' => 'SENIOR 1', //
        //     'tc_associate_2' => 'ASSOCIATE 2', //
        //     'tc_associate_1' => 'ASSOCIATE 1', //
        //     'tc_junior_associate' => 'JUNIOR ASSOCIATE', //
        //     'tc_internship' => 'INTERNSHIP', //
        //     'tc_total' => 'TOTAL TIME CHARGE', //
        //     'tc_other' => 'OTHER EXPENSE', //
        //     'total_charges' => 'TOTAL CHARGES', //
        //     'rate_time_charges' => 'RECOVERY TIME CHARGES', //
        //     'rate_total_charges' => 'RECOVERY TOTAL CHARGES', //
        //     'progress_time' => 'PROGRESS TIME',  //
        //
        // ];
        $_report['name'] = 'Recovery Rate - Summary';
        return $this->report($_view, $_sql, $_report);
    }
    public function actionTaxi_detail()
    {
        $_view['form'] = 'taxi_detail/form';
        $_view['results'] = 'taxi_detail/report';

        $_sql['name'] = "rpt_taxi_detail";

        $_sql['field'] = [
            'employee_id' => 'EMPLOYEE ID', //
            'employee_name' => 'NAME', //
            'timereport_date' => 'DATE', //
            'timereport_day' => 'DAY', //
            'level_name' => 'LEVEL', //
            'entity_name' => 'ENTITY', //
            'div_name' => 'DIVISION', //
            'dept_name' => 'DEPARTEMENT', //
            'group_id' => 'GROUP ID', //
            'group_name' => 'GROUP NAME', //
            'group_initial' => 'GROUP INITIAL', //
            'approval2_id' => 'MANAGER ID', //
            'approval2_name' => 'MANAGER NAME', //
            'approval2_initial' => 'MANAGER INITIAL', //
            'approval1_id' => 'SUPERVISOR ID', //
            'approval1_name' => 'SUPERVISOR NAME', //
            'approval1_initial' => 'SUPERVISOR INITIAL', //
            'job_description' => 'JOB DESCRIPTION', //
            'job_memo' => 'MEMO', //
            'taxi_type' => 'TAXI', //
            'taxi_voucher_no' => 'VOUCHER NO', //
            'taxi_start' => 'START', //
            'taxi_finish' => 'FINISH', //
            'taxi_destination' => 'DESTINATION', //
            'taxi_amount' => 'AMOUNT', //
            'taxi_description' => 'DESCRIPTION', //
            'approval_status' => 'STATUS', //
        ];
        $_report['name'] = 'Time Report - Taxi';
        return $this->report($_view, $_sql, $_report);
    }

    public function actionAnnual_leave_summary()
    {
        $_view['form'] = 'annual_leave_summary/form';
        $_view['results'] = 'annual_leave_summary/report';

        $_sql['name'] = "rpt_annual_report_summary";

        $_sql['field'] = [
            'employee_id' => 'EMPLOYEE ID', //
            'employee_name' => 'NAME', //
            'employee_level' => 'LEVEL', //
            'annual_days_ob' => 'OPEN', //
            'annual_days_used' => 'USED', //
            'annual_days_balance' => 'BALANCE', //
            'entity_name' => 'ENTITY', //
            'division_name' => 'DIVISION', //
            'dept_name' => 'DEPARTEMENT', //
            'group_id' => 'GROUP ID', //
            'group_name' => 'GROUP NAME', //
            'group_initial' => 'GROUP INITIAL', //
            'manager_id' => 'MANAGER ID', //
            'manager_name' => 'MANAGER NAME', //
            'manager_initial' => 'MANAGER INITIAL', //
            'supervisor_id' => 'SUPERVISOR ID', //
            'supervisor_name' => 'SUPERVISOR NAME', //
            'supervisor_initial' => 'SUPERVISOR INITIAL', //
        ];
        $_report['name'] = 'Annual Leave - Summary';
        return $this->report($_view, $_sql, $_report);
    }

    public function actionAnnual_leave_detail()
    {
        $_view['form'] = 'annual_leave_detail/form';
        $_view['results'] = 'annual_leave_detail/report';

        $_sql['name'] = "rpt_annual_report_detail";

        $_sql['field'] = [
            'employee_id' => 'EMPLOYEE ID', //
            'annual_year' => 'YEAR', //
            'trans_date' => 'DATE', //
            'description' => 'DESCRIPTION', //
            'annual_used' => 'USED', //
            'res_balance' => 'BALANCE', //
            'annual_status' => 'STATUS', //
        ];
        $_report['name'] = 'Annual Leave - Detail';
        return $this->report($_view, $_sql, $_report);
    }

    public function actionSick_list()
    {
        $_view['form'] = 'sick_list/form'; //view form
        $_view['results'] = 'sick_list/report'; //view results

        $_sql['name'] = "rpt_sick_list"; //nama function sql di controller ini

        $_sql['field'] = [
            'employee_id' => 'EMPLOYEE ID', //
            'employee_name' => 'EMPLOYEE NAME', //
            'entity_name' => 'ENTITY', //
            'div_name' => 'DIVISION', //
            'group_id' => 'GROUP ID', //
            'group_name' => 'GROUP NAME', //
            'group_initial' => 'GROUP INITIAL', //
            'manager_id' => 'MANAGER ID', //
            'manager_name' => 'MANAGER NAME', //
            'manager_initial' => 'MANAGER INITIAL', //
            'supervisor_id' => 'SUPERVISOR ID', //
            'supervisor_name' => 'SUPERVISOR', //
            'supervisor_initial' => 'SUPERVISOR INITIAL', //
            'supervisor_name' => 'SUPERVISOR NAME', //
            'timereport_day' => 'DAY', //
            'timereport_date' => 'DATE', //
            'attachment' => 'attachment', //
        ];
        $_report['name'] = 'Annual Leave - Sick List';
        return $this->report($_view, $_sql, $_report);
    }


    public function actionDetail2()
    {
        $_view['form'] = 'detail2/form';
        $_view['results'] = 'detail2/report';

        $_sql['name'] = "rpt_tr_det_office88";

        $_sql['field'] = [
            'employee_id' => 'EMPLOYEE ID', //
            'employee_name' => 'EMPLOYEE NAME', //
            'level_name' => 'LEVEL', //
            'tr_date' => 'DATE', //
            'day_date' => 'DAY DATE', //
            'sign_date' => 'HOLIDAY', //
            //'tr_id' => 'Timereport ID', //
            //'tr_det_id' => 'Timereport Detail', //
            //'tr_status' => 'Status', //

            'job_code' => 'JOB CODE', //
            'job_description' => 'JOB DESCRIPTION', //
            'partner_name' => 'PARTNER NAME', //
            'partner_initial' => 'PARTNER INITIAL', //
            'manager_id' => 'MANAGER ID', //
            'manager_name' => 'MANAGER NAME', //
            'manager_initial' => 'MANAGER INITIAL', //

            'spv_id' => 'SUPERVISOR',
            'spv_name' => 'SUPERVISOR NAME',
            'spv_initial' => 'SUPERVISOR INITIAL',
            'ot_weekday' => 'OT WEEKDAY',
            'ot_holiday' => 'OT HOLIDAY',
            'regular_project' => 'REGULAR PROJECT',
            'regular_back_office' => 'REGULAR BACK OFFICE',
            'regular_leave' => 'REGULAR LEAVE',
            'regular_other' => 'REGULAR OTHER',
            'meal_value' => 'MEAL',
            'ope_value' => 'OPE',
            'taxi_value' => 'TAXI',
            'Status' => 'STATUS',
            //'StatusCSS' => 'StatusCSS',
            //'StatusLabel' => 'StatusLabel',
        ];
        $_report['name'] = 'TimeReport Detail';
        return $this->report($_view, $_sql, $_report);
    }


    public function actionRecovery_rate_detail()
    {
        $_view['form'] = 'recovery_rate_detail/form';
        $_view['results'] = 'recovery_rate_detail/report';

        $_sql['name'] = "rpt_recovery_rate_detail";

        $_sql['field'] = [
            'employee_id' => 'EMPLOYEE ID', //
            'employee_name' => 'EMPLOYEE NAME', //
            'employee_level' => 'LEVEL', //
            'job_planning' => 'PLANNING', //
            'job_fieldwork' => 'FIELD WORK', //
            'job_reporting' => 'REPORTING', //
            'job_wrapup' => 'WRAP UP', //
            'job_total' => 'TOTAL', //
            'job_overtime' => 'OVERTIME', //
            'job_planning_act' => 'PLANNING', //
            'job_fieldwork_act' => 'FIELD WORK', //
            'job_reporting_act' => 'REPORTING', //
            'job_wrapup_act' => 'WRAP UP', //
            'job_total_act' => 'TOTAL', //
            'job_overtime_act' => 'OVERTIME', //
            'job_total_diff' => 'DIFF WH',
            'job_overtime_diff' => 'DIFF OT',
        ];
        $_report['name'] = 'Recovery Rate - Detail';
        return $this->report($_view, $_sql, $_report);
    }

    public function actionJob_detail()
    {
        $_view['form'] = 'job_detail/form';
        $_view['results'] = 'job_detail/report';

        $_sql['name'] = "rpt_job_detail";

        $_sql['field'] = [
            // 'employee_id' => 'EMPLOYEE ID', //
            'employee_name' => 'EMPLOYEE NAME', //
            'employee_level' => 'LEVEL', //
            'partner_initial' => 'JOB TEAM', //
            'group_initial' => 'EMPLOYEE TEAM', //
            // 'job_period' => 'JOB PERIOD', //
            'job_code' => 'JOB CODE', //
            'client_code' => 'CLIENT CODE', //
            'client_name' => 'CLIENT NAME', //
            'start_date' => 'START DATE', //
            'end_date' => 'END DATE', //
            'job_fee' => 'JOB FEE', //
            'job_planning' => 'PLANNING', //
            'job_fieldwork' => 'FIELD WORK', //
            'job_reporting' => 'REPORTING', //
            'job_wrapup' => 'WRAP UP', //
            'job_total' => 'TOTAL', //
            'job_overtime' => 'OVERTIME', //
            'job_planning_act' => 'PLANNING', //
            'job_fieldwork_act' => 'FIELD WORK', //
            'job_reporting_act' => 'REPORTING', //
            'job_wrapup_act' => 'WRAP UP', //
            'job_total_act' => 'TOTAL', //
            'job_overtime_act' => 'OVERTIME', //
            'job_total_diff' => 'DIFF WH',
            'job_overtime_diff' => 'DIFF OT',
            'job_status' => 'STATUS',
        ];
        $_report['name'] = 'Job Detail';
        return $this->report($_view, $_sql, $_report);
    }

    public function actionJob_detail_v2()
    {
        $_view['form'] = 'job_detail_v2/form';
        $_view['results'] = 'job_detail_v2/report';

        $_sql['name'] = "rpt_job_detail_v2";

        $_sql['field'] = [
            // 'employee_id' => 'EMPLOYEE ID', //
            'employee_name' => 'EMPLOYEE NAME', //
            'employee_level' => 'LEVEL', //
            'partner_initial' => 'JOB TEAM', //
            'group_initial' => 'EMPLOYEE TEAM', //
            // 'job_period' => 'JOB PERIOD', //
            'job_code' => 'JOB CODE', //
            'client_code' => 'CLIENT CODE', //
            'client_name' => 'CLIENT NAME', //
            'start_date' => 'START DATE', //
            'end_date' => 'END DATE', //
            'job_fee' => 'JOB FEE', //
            'job_planning' => 'PLANNING', //
            'job_fieldwork' => 'FIELD WORK', //
            'job_reporting' => 'REPORTING', //
            'job_wrapup' => 'WRAP UP', //
            'job_total' => 'TOTAL', //
            'job_overtime' => 'OVERTIME', //
            'job_planning_act' => 'PLANNING', //
            'job_fieldwork_act' => 'FIELD WORK', //
            'job_reporting_act' => 'REPORTING', //
            'job_wrapup_act' => 'WRAP UP', //
            'job_total_act' => 'TOTAL', //
            'job_overtime_act' => 'OVERTIME', //
            'job_total_diff' => 'DIFF WH',
            'job_overtime_diff' => 'DIFF OT',
            'planning_current' => 'PLANNING',
            'field_work_current' => 'FIELD WORK',
            'reporting_current' => 'REPORTING',
            'wrap_up_current' => 'WRAP UP',
            'total_wh_current' => 'TOTAL',
            'over_time_current' => 'OVERTIME',
            'job_status' => 'STATUS',
        ];
        $_report['name'] = 'Job Detail - With Time Report Period';
        return $this->report($_view, $_sql, $_report);
    }

    public function actionRecap_perform()
    {
        $_view['form'] = 'recap_perform/form';
        $_view['results'] = 'recap_perform/report';

        $_sql['name'] = "rpt_recap_perform";

        $_sql['field'] = [
            // 'employee_id' => 'EMPLOYEE ID', 
            'employee_name' => 'EMPLOYEE NAME', 
            'employee_level' => 'LEVEL', //
            'group_initial' => 'EMPLOYEE TEAM', 
            'entity_name' => 'ENTITY', 
            'employee_status' => 'STATUS', 
            'date_from' => 'DATE FROM', 
            'date_to' => 'DATE TO', 
            'total_wh' => 'TOTAL WH', 
            'planning' => 'PLANNING', 
            'field_work' => 'FIELD WORK', 
            'reporting' => 'REPORTING', 
            'wrap_up' => 'WRAP UP', 
            'wh_project' => 'TOTAL PROJECT', 
            'wh_back_office' => 'BACK OFFICE',
            'wh_preparation' => 'PREPARATION',
            'wh_self_development' => 'SELF DEVELOPMENT',
            'wh_leave_wo_doctor' => 'LEAVE WITHOUT DOCTOR NOTES',
            'wh_leave_doctor' => 'LEAVE WITH DOCTOR NOTES',
            'wh_leave_annual' => 'ANNUAL LEAVE',
            'wh_leave_other' => 'OTHER LEAVE',
            'wh_total_leave' => 'TOTAL LEAVE',
            'wh_other' => 'OTHER',
        ];
        $_report['name'] = 'Recap for Performance Apprisal';
        return $this->report($_view, $_sql, $_report);
    }

    public function actionEmail_monthly_supervisor()
    {
        echo TimeReportHelper::emailMonthlySupervisor();
    }
    public function actionEmail_monthly_manager()
    {
        echo TimeReportHelper::emailMonthlyManager();
    }
}
