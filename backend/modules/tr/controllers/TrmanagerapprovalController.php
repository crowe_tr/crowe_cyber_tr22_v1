<?php

namespace backend\modules\tr\controllers;

use Yii;
use common\models\tr\Job;
use common\models\tr\Taxi;
use common\models\tr\TimeReport;
use common\models\tr\TimeReportDetail;
use common\models\tr\TimeReportMeals;
use common\models\tr\TimeReportOutOfOffice;
use common\models\tr\TimeReportTaxi;
use common\models\tr\TrTimeReportLogs;

use common\models\hr\Employee;

use common\models\st\TaskType;
use common\models\st\Task;
use common\models\cm\Taxi as cmTaxi;

use common\models\tr\search\TimeReportSearch;
use common\models\tr\search\TimeReportDetailSearch;
use common\components\CommonHelper;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\MethodNotAllowedHttpException;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class TrmanagerapprovalController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    //========================================================
    public function actionAll(){
        return $this->actionIndex();
    }
    public function IndexFilter(){
		$session = Yii::$app->session;
        $session->open();

		$post = Yii::$app->request->post();
		$model = new TimeReport();
		if($model->load($post)){
            $data = [];
            $date = explode("-",$model->YearDate);

			$data['Year'] = $date[0];
			$data['Month'] = $date[1];
			$data['YearDate'] = $model->YearDate;
			$session->set('__trapproval_index', $data);
		}
	}

    public function actionIndex($Status=null)
    {
        $user = CommonHelper::getUserIndentity();
		$session = Yii::$app->session;
        $data = array();

        $this->IndexFilter();

        $model = new TimeReport();
		$param = isset($_SESSION['__trapproval_index']) ? $_SESSION['__trapproval_index'] : [];
		$param['EmployeeId'] = isset($param['EmployeeId']) ? $param['EmployeeId'] : $user->Id;
		$param['Year'] = isset($param['Year']) ? $param['Year'] : date('Y');
		$param['Month'] = isset($param['Month']) ? $param['Month'] : date('m');
		$param['Status'] = isset($Status) ?  $Status : 0;
        $param['YearDate'] = isset($param['YearDate']) ? $param['YearDate'] : date('Y-m-t');
        $param['Date'] = date('Y-m-d');

        // $sql_list = "call antTrListApproval2('".$param['EmployeeId']."', '".$param['Year']."', '".$param['Month']."')";
        // $sql_list = "call tr_approval_list(2,'".$param['EmployeeId']."', '".$param['Year']."', '".$param['Month']."')";
        // $data['list'] = Yii::$app->db->createCommand($sql_list)->queryAll();

        $sql_calendar = "call calendar_list('" . $param['EmployeeId'] . "', '" . $param['Year'] . "', '" . $param['Month'] . "')";
        $list = Yii::$app->db->createCommand($sql_calendar)->queryAll();
        foreach ($list as $kl => $vl) {;
          $sql_list_data = "call tr_approval_by_date('" . $vl['calendar_date'] ."',2,'" . $param['EmployeeId'] . "')";
          $list_data = Yii::$app->db->createCommand($sql_list_data)->queryOne();
          if ($list_data) {
            $list[$kl]['work_hour'] = $list_data['work_hour'];
            $list[$kl]['over_time'] = $list_data['over_time'];
            $list[$kl]['meal_amount'] = $list_data['meal_amount'];
            $list[$kl]['ope_amount'] = $list_data['ope_amount'];
            $list[$kl]['taxi_amount'] = $list_data['taxi_amount'];
            $list[$kl]['tr_status'] = $list_data['tr_status'];
            $list[$kl]['tr_status_label'] = $list_data['tr_status_label'];
            $list[$kl]['tr_statusCSS'] = $list_data['tr_statusCSS'];
          }
        }

        $data['list'] = $list;

        return $this->render('index', [
            'param' => $param,
            'data' => $data,
            'model'=>$model
        ]);
    }

    public function actionDetail($id, $status=null)
    {
        $user = CommonHelper::getUserIndentity();
		$session = Yii::$app->session;
        $data = array();

        $model = new TimeReport();
		$param = isset($_SESSION['__trapproval_details']) ? $_SESSION['__trapproval_details'] : [];
		$param['EmployeeId'] = isset($param['EmployeeId']) ? $param['EmployeeId'] : $user->Id;
		$param['Status'] = isset($status) ?  $status : 0;
		$param['Date'] = isset($id) ? $id : date('Y-m-d');

        // $sql = "call `antTrListApproval2Date`('".$param['EmployeeId']."', '".$param['Date']."')";
        // $sql = "call `tr_approval_date`(2,'".$param['EmployeeId']."', '".$param['Date']."')";
        $sql = "call tr_approval_by_date('" . $param['Date'] ."',2,'" . $param['EmployeeId'] . "')";
        $data['header'] = Yii::$app->db->createCommand($sql)->queryOne();

        $sql_list = "call tr_approval_detail('" . $param['Date'] . "',2,'" . $param['EmployeeId'] . "')";
        $data['list'] = Yii::$app->db->createCommand($sql_list)->queryAll();
        return $this->render('Detail', [
            'param' => $param,
            'data' => $data,
            'model'=>$model
        ]);

    }
    public function actionView($id, $TrDetID)
    {
        $user = CommonHelper::getUserIndentity();
		$session = Yii::$app->session;
        $data = array();
        $param = [];

        $data['Detail'] = Yii::$app->db->createCommand("call tr_approval_item('detail',".$id.", ".$TrDetID.")")->queryAll();
        $data['Meal'] = Yii::$app->db->createCommand("call tr_approval_item('meal',".$id.", ".$TrDetID.")")->queryAll();
        $data['Ope'] = Yii::$app->db->createCommand("call tr_approval_item('ope',".$id.", ".$TrDetID.")")->queryAll();
        $data['Taxi'] = Yii::$app->db->createCommand("call tr_approval_item('taxi',".$id.", ".$TrDetID.")")->queryAll();

        $param['TrID'] = $id;
        $param['TrDetID'] = $TrDetID;

        return $this->renderPartial('DetailListView', [
            'param' => $param,
            'data' => $data,
        ]);
    }

    //========================================================

    public function detail_approve($models, $id, $TrDetID, $set){
      if(!empty($models)){
          $table_name = $models->getTableSchema()->fullName;
          $models->approval2 = $set;

          if($models->validate()){
              $transaction = \Yii::$app->db->beginTransaction();
              try {
                    $models->save();

                    $user = CommonHelper::getUserIndentity();
                    $tr_data = TimeReport::findOne($id);
                    $employee_data = Employee::findOne($tr_data->employee_id);

                    $value_logs = null;
                    $sql_tr_logs = "call tr_list('tr-data','" . $employee_data->user_id .";" . $tr_data->tr_date . "');";
                    $dt_tr_logs = Yii::$app->db->createCommand($sql_tr_logs)->queryOne();
                    if ($dt_tr_logs) {
                        $value_logs = $dt_tr_logs['tr_status'];
                    }

                    $sql_tr_det_logs = "call tr_hint2({$id},{$TrDetID})";
                    $dt_tr_det_logs = Yii::$app->db->createCommand($sql_tr_det_logs)->queryAll();
                    $model_logs = new TrTimeReportLogs;
                    $model_logs->tr_id = $id;
                    $model_logs->tr_det_id = $TrDetID;
                    $model_logs->tr_date = $tr_data->tr_date;
                    $model_logs->employee_name = $employee_data->user_id . ' - ' . $employee_data->full_name;
                    $model_logs->tr_status = $value_logs;
                    $model_logs->tr_memo = json_encode($dt_tr_det_logs);
                    $model_logs->created_by = $user->user_id . ' - ' . $user->full_name;

                    $model_logs->tr_type = '';
                    $model_logs->user_action = '';
                    
                    if ($table_name == 'tr_time_report_detail') {
                        $model_logs->tr_type = 'TIME REPORT - DETAIL';
                    } elseif ($table_name == 'tr_time_report_meals') {
                        $model_logs->tr_type = 'TIME REPORT - MEAL';
                    } elseif ($table_name == 'tr_time_report_ope') {
                        $model_logs->tr_type = 'TIME REPORT - OUT OF OFFICE';
                    } elseif ($table_name == 'tr_time_report_taxi') {
                        $model_logs->tr_type = 'TIME REPORT - TAXI';
                    }

                    if ($set == 1) {
                        $model_logs->user_action = 'APPROVE-2';
                    } elseif($set == 2){
                        $this->ApprovalsEmail($id, $TrDetID, $models, "REJECTED");
                        $model_logs->user_action = 'REJECT-2';
                    } elseif ($set == 3) {
                        $this->ApprovalsEmail($id, $TrDetID, $models, "REVISED");
                        $model_logs->user_action = 'REVISE-2';
                    }

                    $model_logs->save(false); 

                    $transaction->commit();
                    $return = true;
              } catch (Exception $e) {
                  $transaction->rollBack();
                  $return = "Error : ".$e;
              }
          }else{
              $return = \yii\helpers\Html::errorSummary($models, ['encode' => true]);
          }
      }
      return $return;
    }

    public function actionDetailapproval($id, $TrDetID, $set){
        $modelDetail = TimeReportDetail::find()->where(['time_report_id' => $id, 'id' => $TrDetID])->one();
        $return = self::detail_approve($modelDetail, $id, $TrDetID, $set);
    }

    public function actionMealapproval($id, $TrDetID, $Seq, $set){
        $modelMeals = TimeReportMeals::find()->where(['time_report_id' => $id, 'tr_det_id' => $TrDetID, 'seq' => $Seq])->one();
        $return = self::detail_approve($modelMeals, $id, $TrDetID, $set);
    }

    public function actionOutofficeapproval($id, $TrDetID, $Seq, $set){
        $modelOutoffice = TimeReportOutOfOffice::find()->where(['time_report_id' => $id, 'tr_det_id' => $TrDetID, 'seq' => $Seq])->one();
        $return = self::detail_approve($modelOutoffice, $id, $TrDetID, $set);
    }

    public function actionTaxiapproval($id, $TrDetID, $Seq, $set){
        $modelTaxi = TimeReportTaxi::find()->where(['time_report_id' => $id, 'tr_det_id' => $TrDetID, 'seq' => $Seq])->one();
        $return = self::detail_approve($modelTaxi, $id, $TrDetID, $set);
    }

    protected function findModel($EmployeeId, $Date)
    {
        if (($model = TimeReport::findOne(['EmployeeId' => $EmployeeId, 'Date' => $Date])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    //========================================================
    public function actionApprovalall($id){
        // var_dump($id);
        $user = CommonHelper::getUserIndentity();
        // var_dump($user);
		$session = Yii::$app->session;
        $param = array();

        $param['EmployeeId'] = isset($param['EmployeeId']) ? $param['EmployeeId'] : $user->Id;

        $sql_list = "call tr_approval_detail('".$id."',2,'".$param['EmployeeId']."')";
        $data['list'] = Yii::$app->db->createCommand($sql_list)->queryAll();
        foreach($data['list'] as $d){
            $sql = "call tr_approve2_all('".$param['EmployeeId']."', '".$d['tr_date']."',  '".$d['task_description']."')";
            $update = Yii::$app->db->createCommand($sql)->queryOne();

            if($update['valid']==1){
                $return = true;

                $sql_data_detail = "call tr_approval_detail_by_task('".$d['tr_date']."',2,'".$param['EmployeeId']."','".$d['task_description']."');";
                $data_detail = Yii::$app->db->createCommand($sql_data_detail)->queryAll();
                foreach ($data_detail as $kdet => $vdet) {
                    $sql_tr_logs = "call tr_list('tr-data','" . $vdet['employee_id'] .";" . $d['tr_date'] . "');";
                    $dt_tr_logs = Yii::$app->db->createCommand($sql_tr_logs)->queryOne();
                    
                    $sql_tr_det_logs = "call tr_hint2({$vdet['tr_id']},{$vdet['tr_det_id']})";
                    $dt_tr_det_logs = Yii::$app->db->createCommand($sql_tr_det_logs)->queryAll();
                    $model_logs = new TrTimeReportLogs;
                    $model_logs->tr_id = $vdet['tr_id'];
                    $model_logs->tr_det_id = $vdet['tr_det_id'];
                    $model_logs->tr_date = $vdet['tr_date'];
                    $model_logs->employee_name = $vdet['employee_id'] . ' - ' . $vdet['employee_name'];
                    $model_logs->tr_status = $dt_tr_logs['tr_status'];
                    $model_logs->tr_memo = json_encode($dt_tr_det_logs);
                    $model_logs->tr_type = 'ALL DETAIL';
                    $model_logs->user_action = 'APPROVE-2';
                    $model_logs->created_by = $user->user_id . ' - ' . $user->full_name;
                    $model_logs->save(false);
                }
            }else{
                $return = false;
            }
        }
        return $return;
    }

    public function actionDetailapprovalall($id, $Description){
        $user = CommonHelper::getUserIndentity();
		$session = Yii::$app->session;
        $param = array();

        $param['EmployeeId'] = isset($param['EmployeeId']) ? $param['EmployeeId'] : $user->Id;

        $sql = "call tr_approve2_all('" . $param['EmployeeId'] . "', '" . $id . "',  '" . $Description . "')";
        // $sql = "call tr_approve2_all('".$param['EmployeeId']."', '".$id."',  '".$Description."')";
        $update = Yii::$app->db->createCommand($sql)->queryOne();
        if($update['valid']==1){
            $return = true;

            $sql_data_detail = "call tr_approval_detail_by_task('".$id."',2,'".$param['EmployeeId']."','".$Description."');";
            $data_detail = Yii::$app->db->createCommand($sql_data_detail)->queryAll();
            foreach ($data_detail as $kdet => $vdet) {
                $sql_tr_logs = "call tr_list('tr-data','" . $vdet['employee_id'] .";" . $vdet['tr_date'] . "');";
                $dt_tr_logs = Yii::$app->db->createCommand($sql_tr_logs)->queryOne();
                
                $sql_tr_det_logs = "call tr_hint2({$vdet['tr_id']},{$vdet['tr_det_id']})";
                $dt_tr_det_logs = Yii::$app->db->createCommand($sql_tr_det_logs)->queryAll();
                $model_logs = new TrTimeReportLogs;
                $model_logs->tr_id = $vdet['tr_id'];
                $model_logs->tr_det_id = $vdet['tr_det_id'];
                $model_logs->tr_date = $vdet['tr_date'];
                $model_logs->employee_name = $vdet['employee_id'] . ' - ' . $vdet['employee_name'];
                $model_logs->tr_status = $dt_tr_logs['tr_status'];
                $model_logs->tr_memo = json_encode($dt_tr_det_logs);
                $model_logs->tr_type = 'ALL DETAIL';
                $model_logs->user_action = 'APPROVE-2';
                $model_logs->created_by = $user->user_id . ' - ' . $user->full_name;
                $model_logs->save(false);
            }
        }else{
            $return = false;
        }
        return $return;
    }

    public function actionDetaillistapproveall($id, $Description, $TimeReportID, $TrDetID){
        $user = CommonHelper::getUserIndentity();
		    $session = Yii::$app->session;
        $param = array();

        $param['EmployeeId'] = isset($param['EmployeeId']) ? $param['EmployeeId'] : $user->Id;
        // var_dump($param['EmployeeId'],$id, $Description, $TimeReportID, $TrDetID);

        $sql = "call tr_approve_detail(2, " . $TimeReportID . ", " . $TrDetID . ")";
        // $sql = "call tr_approve2_list_all('".$param['EmployeeId']."', '".$id."',  '".$Description."', '".$TimeReportID."', '".$TrDetID."')";
        $update = Yii::$app->db->createCommand($sql)->queryOne();
        if($update['valid']==1){
            $return = true;
            $sql_tr_logs = "call tr_list3({$TimeReportID});";
            $dt_tr_logs = Yii::$app->db->createCommand($sql_tr_logs)->queryOne();
            
            $sql_tr_det_logs = "call tr_hint2({$TimeReportID},{$TrDetID})";
            $dt_tr_det_logs = Yii::$app->db->createCommand($sql_tr_det_logs)->queryAll();
            $model_logs = new TrTimeReportLogs;
            $model_logs->tr_id = $TimeReportID;
            $model_logs->tr_det_id = $TrDetID;
            $model_logs->tr_date = $dt_tr_logs['tr_date'];
            $model_logs->employee_name = $dt_tr_logs['employee_id'] . ' - ' . $dt_tr_logs['employee_name'];
            $model_logs->tr_status = $dt_tr_logs['tr_status'];
            $model_logs->tr_memo = json_encode($dt_tr_det_logs);
            $model_logs->tr_type = 'ALL DETAIL';
            $model_logs->user_action = 'APPROVE-2';
            $model_logs->created_by = $user->user_id . ' - ' . $user->full_name;
            $model_logs->save(false);
            
        }else{
            $return = false;
        }
        return $return;
    }
    //========================================================

    public function ApprovalsEmail($TimeReportID, $TrDetID, $model, $label){
        // $sql = "CALL antTrCommentsEmailList({$TimeReportID}, {$TrDetID})";
        $sql = "CALL tr_comment_email({$TimeReportID}, {$TrDetID})";
        $lookups = Yii::$app->db->createCommand($sql)->queryAll();

        $email = [];
        if(!empty($lookups)){
            foreach($lookups as $r) {
                if(!empty($r['EmployeeEmail'])){
                    if(filter_var($r['EmployeeEmail'], FILTER_VALIDATE_EMAIL)) {
                        $email[$r['EmployeeEmail']] = $r['EmployeeEmail'];
                    }
                }
            }
        }
        $date = !empty($model->tr_date) ? $model->tr_date : $model->timeReport->tr_date;
        $date = date('l, d F Y', strtotime($date));

        return $this->Email (
            $email,
            'Crowe TimeReport - Your timereport submission on '.$date.' was '.$label.' #'.$TimeReportID,
            [
                'model'=>$model,
                'label'=>$label
            ]
        );
    }

    //============
    public function Email($sendto, $subject, $param, $layouts='layouts/newapprovals'){
        Yii::$app->cache->flush();

        $from = Yii::$app->params['appNotificationEmail'];
        $cc = Yii::$app->params['appNotificationEmailCC'];

        $to = $sendto;

        if (!empty($to)) {
            $html = ['html' => $layouts];

            $compose = Yii::$app->mailer->compose($html, $param);
            $compose->setFrom($from);
            $compose->setTo($to);
            if (!empty($cc)) {
                $compose->setCc($cc);
            }
            $compose->setSubject($subject);

            $compose->send();
        }

    }


}
