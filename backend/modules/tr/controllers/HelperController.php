<?php

namespace backend\modules\tr\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;

use common\components\CommonHelper;
use common\components\TimeReportHelper;

use common\models\cl\Client;
use common\models\hr\Employee;
use common\models\tr\JobBudgeting;
use common\models\tr\JobBudgetingTemp;

class HelperController extends Controller
{
  public $layout;
  public function actions()
  {
    return [
      'error' => [
        'class' => 'yii\web\ErrorAction',
      ],
    ];
  }
  /*start rapih*/

  public function actionSrcemployejob($q = null, $id = null)
  {
    \Yii::$app->response->format = Response::FORMAT_JSON;
    $out = ['results' => ['id' => '', 'text' => '']];

    $data = JobBudgeting::find()->where("job_id = '{$id}' AND employee_id LIKE '%{$q}%'")->all();
    if (!empty($data)) {
      $out['results'] = array();
      foreach ($data as $key => $d) {
        $arr = array(
          'id' => $d['employee_id'],
          'text' => $d['employee_id'] . ' - ' . $d['employee']['full_name'] . ' - ' . $d['employee']['level']['level_name'],
        );
        array_push($out['results'], $arr);
      }
    }
    return $out;
  }

  public function actionSrcemployejobtemp2($q = null, $id = null)
  {
    \Yii::$app->response->format = Response::FORMAT_JSON;
    $out = ['results' => ['id' => '', 'text' => '']];

    $data = JobBudgetingTemp::find()->where("job_id = '{$id}' AND employee_id LIKE '%{$q}%'")->all();
    if (!empty($data)) {
      $out['results'] = array();
      foreach ($data as $key => $d) {
        $arr = array(
          'id' => $d['employee_id'],
          'text' => $d['employee_id'] . ' - ' . $d['employee']['full_name'] . ' - ' . $d['employee']['level']['level_name'],
        );
        array_push($out['results'], $arr);
      }
    }
    return $out;
  }

  public function actionSrcclient($q = null, $id = null)
  {
    \Yii::$app->response->format = Response::FORMAT_JSON;
    $out = ['results' => ['id' => '', 'text' => '']];

    $data = Client::find()->where("id = '{$q}' or client_name LIKE '%{$q}%'")->asArray()->all();
    if (!empty($data)) {
      $out['results'] = array();
      foreach ($data as $key => $d) {
        $arr = array(
          'id' => $d['id'],
          'text' => $d['client_code'] . ' - ' . $d['client_name'],
        );
        array_push($out['results'], $arr);
      }
    }
    return $out;
  }


  /*end rapih*/




  public function actionSrcclientopt($q = null, $id = null)
  {
    //SrcSupplierOpt
    \Yii::$app->response->format = Response::FORMAT_JSON;
    $out = ['results' => ['id' => '', 'text' => '']];
    if (!is_null($q)) {
      $model = Client::find()
        ->select('Id as id, Name as text, OfficeCP, OfficePhone, OfficeFax, OfficeAddress')
        ->where(
          "OfficeCP LIKE '%{$q}%' OR Name LIKE '%{$q}%' OR Id = '{$q}' "
        )
        ->limit(10)
        ->asArray()
        ->all();
      $out['results'] = array();
      foreach ($model as $data) {
        $name = $data['text'];
        $cp = $data['OfficeCP'];
        $phone = $data['OfficePhone'];
        $fax = $data['OfficeFax'];
        $address = $data['OfficeAddress'];

        $array = array(
          'id' => $data['id'],
          'text' => $data['id'] . ' - ' . $name,
        );
        array_push($out['results'], $array);
      }
    } elseif ($id > 0) {
      $out['results'] = ['id' => $id, 'text' => Client::findOne($id)->Name];
    }

    return $out;
  }

  // kamal 21 Mar 2021
  public function actionSearchGroup($entity_name = null, $division_name = null)
  {
    \Yii::$app->response->format = Response::FORMAT_JSON;
  }

  public function actionSrcemployeeopt($q = null, $id = null, $entity = null, $division = null, $grup = null)
  {
    \Yii::$app->response->format = Response::FORMAT_JSON;
    $out = ['results' => ['id' => '', 'text' => '']];
    if (!is_null($q)) {
      if ($entity != null or $division != null) {
        if ($grup != null) {
          $model = Employee::find()

            ->where(
              "(parent_id = '{$grup}') AND (full_name LIKE '%{$q}%' OR user_id LIKE '%{$q}%')"
            )
            ->all();
        } else {
          $model = Employee::find()
            ->where(
              "(entity_id ='{$entity}' OR div_id = '{$division}') AND (full_name LIKE '%{$q}%' OR user_id LIKE '%{$q}%')"
            )
            ->all();
        }
      } elseif ($grup != null) {
        $model = Employee::find()
          ->joinwith('level')
          ->where(
            "(parent_id = '{$grup}') AND (hr_employee.full_name LIKE '%{$q}%' OR hr_employee.user_id LIKE '%{$q}%' OR cm_level.level_name LIKE '{$q}')"
          )
          ->all();
      } elseif ($entity == null and $division == null and $grup == null) {
        $model = Employee::find()
          ->joinwith('level')
          ->where(
            "hr_employee.full_name LIKE '%{$q}%' OR hr_employee.user_id LIKE '%{$q}%' OR cm_level.level_name LIKE '{$q}'"
          )
          ->all();
      }
      // $model = Employee::find()
      //     ->select('Id as id, fullName as text, entityId')
      //     ->where(
      //       "entityId ='{$entity}' OR divisionID = '{$division}' OR parentID ='{$grup}' AND fullName LIKE '%{$q}%' OR Id = '{$q}'"
      //     )
      //     ->limit(10)
      //     ->asArray()
      //     ->all();
      $out['results'] = array();
      foreach ($model as $key => $data) {
        // $fullName = $data['text'];
        $array = array(
          'id' => $data['user_id'],
          'text' => $data['user_id'] . ' - ' . $data['full_name'] . ' - ' . $data['level']['level_name'],
        );
        array_push($out['results'], $array);
      }
    } elseif ($grup != null) {
      if ($grup != null) {
        $model = Employee::find()
          ->where(
            "parent_id = '{$grup}'  AND full_name LIKE '%{$q}%' OR user_id = '{$q}'"
          )

          ->all();
      }

      // $model = Employee::find()
      //     ->select('Id as id, fullName as text, entityId')
      //     ->where(
      //       "entityId ='{$entity}' OR divisionID = '{$division}' OR parentID ='{$grup}' AND fullName LIKE '%{$q}%' OR Id = '{$q}'"
      //     )
      //     ->limit(10)
      //     ->asArray()
      //     ->all();
      $out['results'] = array();
      foreach ($model as $key => $data) {
        // $fullName = $data['text'];
        $array = array(
          'id' => $data['user_id'],
          'text' => $data['user_id'] . ' - ' . $data['full_name'] . ' - ' . $data['level']['level_name'],
        );
        array_push($out['results'], $array);
      }
    } elseif ($id > 0) {
      $data = Employee::findOne($id);
      $out['results'] = ['id' => $id, 'text' => $data->user_id . ' - ' . $data->full_name];
    }
    return $out;
  }


  public function actionSrcemployemanager($q = null, $JobID = null)
  {
    \Yii::$app->response->format = Response::FORMAT_JSON;
    $out = ['results' => ['id' => '', 'text' => '']];
    if (!is_null($q)) {
      // $model = JobBudgeting::find()
      // ->where(
      //   "JobID = '{$JobID}' AND EmployeeID LIKE '%{$q}%'"
      //   )
      //   // ->limit(10)
      //   // ->asArray()
      //   ->all();

      $model = Employee::find()
        ->where(
          "full_name LIKE '%{$q}%'"
        )
        ->all();

      $out['results'] = array();
      foreach ($model as $key => $data) {
        // $out['results'] = ['id' => $data->EmployeeID, 'text' => $data->EmployeeID.' - '.$data->employee->fullName];
        // $fullName = $data['text'];
        // print_r($data['employee']['fullName']);
        $array = array(
          'id' => $data['user_id'],
          'text' => $data['user_id'] . ' - ' . $data['full_name'] . ' - ' . $data['level']['level_name'],
        );
        array_push($out['results'], $array);
      }
    } elseif (is_null($q)) {
      // $model = JobBudgeting::find()
      // ->where(
      //   "JobID = '{$JobID}' AND EmployeeID LIKE '%{$q}%'"
      //   )
      //   // ->limit(10)
      //   // ->asArray()
      //   ->all();

      $model = Employee::find()
        ->where(
          "full_name LIKE '%{$q}%'"
        )
        ->all();

      $out['results'] = array();
      foreach ($model as $key => $data) {
        // $out['results'] = ['id' => $data->EmployeeID, 'text' => $data->EmployeeID.' - '.$data->employee->fullName];
        // $fullName = $data['text'];
        // print_r($data['employee']['fullName']);
        $array = array(
          'id' => $data['user_id'],
          'text' => $data['user_id'] . ' - ' . $data['full_name'] . ' - ' . $data['level']['level_name'],
        );
        array_push($out['results'], $array);
      }
    } elseif ($id > 0) {
      $data = JobBudgeting::findOne($id);
      $out['results'] = ['id' => $id, 'text' => $data->job_id . ' - ' . $data->employee->full_name . ' - ' . $data->employee->level->level_name];
    }
    return $out;
  }

  public function actionSrcemployejobtemp($q = null, $JobID = null)
  {
    \Yii::$app->response->format = Response::FORMAT_JSON;
    $out = ['results' => ['id' => '', 'text' => '']];
    if (!is_null($q)) {
      // $model = JobBudgeting::find()
      // ->where(
      //   "JobID = '{$JobID}' AND EmployeeID LIKE '%{$q}%'"
      //   )
      //   // ->limit(10)
      //   // ->asArray()
      //   ->all();

      $model = JobBudgetingTemp::find()
        ->where(
          "job_id = '{$JobID}' AND employee_id LIKE '%{$q}%'"
        )
        ->all();

      $out['results'] = array();
      foreach ($model as $key => $data) {
        // $out['results'] = ['id' => $data->EmployeeID, 'text' => $data->EmployeeID.' - '.$data->employee->fullName];
        // $fullName = $data['text'];
        // print_r($data['employee']['fullName']);
        $array = array(
          'id' => $data['employee_id'],
          'text' => $data['employee_id'] . ' - ' . $data['employee']['full_name'] . ' - ' . $data['employee']['level']['level_name'],
        );
        array_push($out['results'], $array);
      }
    } elseif (is_null($q)) {
      // $model = JobBudgeting::find()
      // ->where(
      //   "JobID = '{$JobID}' AND EmployeeID LIKE '%{$q}%'"
      //   )
      //   // ->limit(10)
      //   // ->asArray()
      //   ->all();

      $model = JobBudgetingTemp::find()
        ->where(
          "job_id = '{$JobID}' AND employee_id LIKE '%{$q}%'"
        )
        ->all();

      $out['results'] = array();
      foreach ($model as $key => $data) {
        // $out['results'] = ['id' => $data->EmployeeID, 'text' => $data->EmployeeID.' - '.$data->employee->fullName];
        // $fullName = $data['text'];
        // print_r($data['employee']['fullName']);
        $array = array(
          'id' => $data['employee_id'],
          'text' => $data['employee_id'] . ' - ' . $data['employee']['full_name'] . ' - ' . $data['employee']['level']['level_name'],
        );
        array_push($out['results'], $array);
      }
    } elseif ($id > 0) {
      $data = JobBudgetingTemp::findOne($id);
      $out['results'] = ['id' => $id, 'text' => $data->job_id . ' - ' . $data->employee->full_name . ' - ' . $data->employee->level->level_name];
    }
    return $out;
  }

  public function searchJobTeam($q, $where = null)
  {
    /*
        $model = JobBudgeting::find()
        ->JoinWith('employee')
        ->select('hr_employee.Id as id, hr_employee.Name as text')
        ->where(
          "
            (hr_employee.Name LIKE '%{$q}%' OR hr_employee.Id = '{$q}')
          "
        )
        ->limit(10)
        ->asArray()
        ->all();
        */
    $model = Employee::find()
      ->select('Id as id, Name as text')
      ->where(
        "Name LIKE '%{$q}%' OR Id = '{$q}' "
      )
      ->limit(10)
      ->asArray()
      ->all();

    return $model;
  }
  public function actionSrcPartneropt($q = null, $id = null, $where = null)
  {
    \Yii::$app->response->format = Response::FORMAT_JSON;
    $out = ['results' => ['id' => '', 'text' => '']];
    if (!is_null($q)) {
      $model = $this->searchJobTeam($q);
      $out['results'] = array();
      foreach ($model as $data) {
        $name = $data['text'];
        $array = array(
          'id' => $data['id'],
          'text' => $data['id'] . ' - ' . $name,
        );
        array_push($out['results'], $array);
      }
    } elseif ($id > 0) {
      $data = Employee::findOne($id);
      $out['results'] = ['id' => $id, 'text' => $data->Id . ' - ' . $data->Name];
    }
    return $out;
  }
  public function actionSrcmanageropt($q = null, $id = null, $where = null)
  {
    \Yii::$app->response->format = Response::FORMAT_JSON;
    $out = ['results' => ['id' => '', 'text' => '']];
    if (!is_null($q)) {
      $model = $this->searchJobTeam($q);
      $out['results'] = array();
      foreach ($model as $data) {
        $name = $data['text'];
        $array = array(
          'id' => $data['id'],
          'text' => $data['id'] . ' - ' . $name,
        );
        array_push($out['results'], $array);
      }
    } elseif ($id > 0) {
      $data = Employee::findOne($id);
      $out['results'] = ['id' => $id, 'text' => $data->Id . ' - ' . $data->Name];
    }
    return $out;
  }
  public function actionSrcsupervisoropt($q = null, $id = null, $where = null)
  {
    \Yii::$app->response->format = Response::FORMAT_JSON;
    $out = ['results' => ['id' => '', 'text' => '']];
    if (!is_null($q)) {
      $model = $this->searchJobTeam($q);
      $out['results'] = array();
      foreach ($model as $data) {
        $name = $data['text'];
        $array = array(
          'id' => $data['id'],
          'text' => $data['id'] . ' - ' . $name,
        );
        array_push($out['results'], $array);
      }
    } elseif ($id > 0) {
      $data = Employee::findOne($id);
      $out['results'] = ['id' => $id, 'text' => $data->Id . ' - ' . $data->Name];
    }
    return $out;
  }

  public function actionLoaddepartement()
  {
    $out = [];
    if (isset($_POST['depdrop_parents'])) {
      $id = end($_POST['depdrop_parents']);
      $list = CmDept::find()->andWhere(['CmMsCpOffice_Id' => $id])->asArray()->all();

      $selected = null;
      if ($id != null && count($list) > 0) {
        $selected = '';
        foreach ($list as $i => $ls) {
          $out[] = ['id' => $ls['Id'], 'name' => $ls['Departement']];
        }
        echo json_encode(['output' => $out, 'selected' => $selected]);

        return;
      }
    }
    echo json_encode(['output' => array(), 'selected' => '']);
  }
}
