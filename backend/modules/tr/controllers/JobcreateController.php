<?php

namespace backend\modules\tr\controllers;

use Yii;
use yii\base\Exception;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use common\components\CommonHelper;
use common\components\HelperDB;
use common\components\TimeReportHelper;

use common\models\tr\Job;
use common\models\tr\search\JobSearch;


class JobcreateController extends Controller
{
  protected function findModel($id)
  {
    if (($model = Job::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException('The requested page does not exist.');
  }

  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }
  public function actionIndex()
  {
    $searchModel = new JobSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  public function getDataForForm($model)
  {
    $data = array();

    $data['Division'] = TimeReportHelper::getDivision();
    $data['Entity'] = TimeReportHelper::getEntity();
    $data['Client'] = TimeReportHelper::getClient();
    $data['Employee'] = TimeReportHelper::getEmployee();
    $data['VempGroup'] = TimeReportHelper::getVempGroup();
    return $data;
  }

  public function actionN($id = '')
  {

    $modelJob = empty($id) ? new Job() : $this->findModel($id);
    $modelJob->scenario = 'create';

    // $abc = $this->getDataForForm($modelJob);
    // var_dump('coba');
    // die();
    return $this->renderAjax('n', [
      'id' => $id,
      'data' => $this->getDataForForm($modelJob),
      'modelJob' => $modelJob,
    ]);
  }
  public function actionN_code($id = '')
  {
    // var_dump('coba');
    // die();

    $modelJob = empty($id) ? new Job() : $this->findModel($id);
    $modelJob->scenario = 'update_code';

    return $this->renderAjax('n_code', [
      'id' => $id,
      'data' => $this->getDataForForm($modelJob),
      'modelJob' => $modelJob,
    ]);
  }

  public function actionSave($id = null)
  {
    $user = CommonHelper::getUserIndentity();
    $post = Yii::$app->request->post();
    $modelJob = empty($id) ? new Job() : $this->findModel($id);
    $modelJob->scenario = 'create';

    $result = [
      'model' => [],
      'state' => [
        'status' => false,
        'message' => 'Error : default'
      ]
    ];

    $job_id = null;
    $transaction = \Yii::$app->db->beginTransaction();
    if ($modelJob->load($post)) {
      if ($modelJob->validate()) {
        try {
          if ($modelJob->isNewRecord) {
            $modelJob->job_status = 0;
            $modelJob->flag = 0;
            $modelJob->created_by = $user->user_id;

            // $sql = "call command_sql('new-job-manager','" . $modelJob->id . ";" . $modelJob->manager_id . ";" . $user->user_id . "');";
            // $sql = "call cud_data('insert-job-manager','" . $modelJob->JobID . ";" . $modelJob->Manager . ";" . $user->Id . "')";
          } else {
            $modelJob_old = $this->findModel($id);

            $modelJob->updated_by = $user->user_id;

            // $sql = "call command_sql('new-job-manager','" . $modelJob->id . ";" . $modelJob_old->manager_id . ";" . $modelJob->manager_id . ";" . $user->user_id . "')";
            // $sql = "call cud_data('insert-job-manager','" . $modelJob->JobID . ";" . $modelJob_old->Manager . ";" . $modelJob->Manager . ";" . $user->Id . "')";
            $job_id = $modelJob->id;
          }
          // var_dump('$sql');
          // die();
          $modelJob->job_fee = CommonHelper::ResetDecimal($modelJob->job_fee);
          // $modelJob->Fee = CommonHelper::ResetDecimal($modelJob->Fee);

          // Yii::$app->db->createCommand($sql)->queryOne();
          $result = HelperDB::save($modelJob);
          if (is_null($job_id)) {
            $sql = "call command_sql('new-job-manager','" . $modelJob->id . ";;" . $modelJob->manager_id . ";" . $user->user_id . "')";
          } else {
            $sql = "call command_sql('new-job-manager','" . $modelJob->id . ";" . $modelJob_old->manager_id . ";" . $modelJob->manager_id . ";" . $user->user_id . "')";
          }
          Yii::$app->db->createCommand($sql)->queryOne();


          if ($result['state']['status'] == true) {
            // $mail = $modelJob->manager->email;
            $mail = $modelJob->manager->user_email;
            if (!empty($mail)) {
              $send_email = $this->Email($mail, 'THERE IS A NEW DRAFT JOB', $modelJob);
              // $send_email = true;
              if ($send_email) {
                $result['state']['status'] = true;
                $result['state']['message'] = "Success";
                $transaction->commit();
              } else {
                $result['state']['status'] = false;
                $result['state']['message'] = "Can't Send Email, please cek email address " . $mail;
                $transaction->rollback();
              }
            } else {
              $result['state']['status'] = false;
              $result['state']['message'] = "Can't Send Email, Manager's email is empty";
            }
          }
        } catch (Exception $e) {
          $result['state']['status'] = false;
          $result['state']['message'] = HelperDB::db_trigger_error($e->getMessage());
          $transaction->rollBack();
        }
      }
    }

    return json_encode($result['state']);
  }

  public function actionDelete($id)
  {
    try {
      $sql = "call command_sql('delete-all-job-detail'," . $id . ");";
      Yii::$app->db->createCommand($sql)->queryOne();

      $modelJob = $this->findModel($id);
      $modelJob->delete();
      return $this->redirect(['index']);
    } catch (\Exception $e) {
      return CommonHelper::db_trigger_error($e->getMessage());
    }
  }

  protected function Email($sendto, $subject, $modelJob, $status = "", $layouts = 'layouts/JobCreate')
  {
    Yii::$app->cache->flush();

    $from = Yii::$app->params['appNotificationEmail'];
    $cc = Yii::$app->params['appNotificationEmailCC'];

    $to = array();
    $to[$sendto] = $sendto;

    if (!empty($to)) {
      $to = $to;
      $html = ['html' => $layouts];
      $html_bind = ['title' => $subject, 'modelJob' => $modelJob, 'status' => $status];

      $compose = Yii::$app->mailer->compose($html, $html_bind);
      $compose->setFrom($from);
      $compose->setTo($to);
      if (!empty($cc)) {
        $compose->setCc($cc);
      }
      $compose->setSubject($subject);

      if ($compose->send()) {
        return "true";
      } else {
        return "false";
      }
    }
  }
}
