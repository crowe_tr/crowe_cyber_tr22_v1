<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
                <?php $form = ActiveForm::begin();?>
                    <div class="row">
                        <div class="col-md-6">
                            <?= 
                        $form->field($model, 'USERNAME', 
                            [
                             'template' => '
                                <div class="form-group form-md-line-input has-info">
                                    {input}{label}{error}{hint}
                                </div>
                             ',
                            ]
                        )->textInput(['maxlength' => 255, 'class' => 'form-control input-sm']) 
                    ?>
                                <?= 
                        $form->field($model, 'EMAIL', 
                            [
                             'template' => '
                                <div class="form-group form-md-line-input has-info">
                                    {input}{label}{error}{hint}
                                </div>
                             ',
                            ]
                        )->textInput(['maxlength' => 255, 'class' => 'form-control input-sm']) 
                    ?>
                        </div>
                        <div class="col-md-6">
                            <?= 
                        $form->field($model, 'PASSWORD_HASH', 
                            [
                             'template' => '
                                <div class="form-group form-md-line-input has-info">
                                    {input}{label}{error}{hint}
                                </div>
                             ',
                            ]
                        )->passwordInput(['maxlength' => 255, 'class' => 'form-control input-sm']) 
                    ?>
                                <?= 
                        $form->field($model, 'CAPTCHA',[
                             'template' => '
                                <div class="form-group form-md-line-input has-info">
                                    {input}{label}{error}{hint}
                                </div>
                             ',
                            ]

                        )->widget(Captcha::className(
                        )) 
                    ?>
                                    <?=                   
                        Html::submitButton(
                            'Submit register form', 
                            ['class' => 'btn btn-success pull-right']
                        ) 
                        ?>

                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
