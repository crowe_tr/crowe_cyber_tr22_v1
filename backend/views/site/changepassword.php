<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$rand = rand();
$form = ActiveForm::begin([
    'id' => $rand.'-form',
    'action' => ['site/changepassword', 'save' => true],
    'enableClientValidation' => true,
    'enableAjaxValidation' => false,
    'validateOnSubmit' => true,
    'validateOnChange' => true,
    'validateOnType' => true,
    'fieldConfig' => [
        'template' => '{label}{input}',
        'options' => [
            'class' => 'form-group form-group-default',
        ],
    ],
]);
echo $form->errorSummary($_userModel);
?>
<div class="form-group-attached">
	<div class="row">
		<div class="col-md-6">
			<?= $form->field($_userModel, 'password_hash')->passwordInput(['maxlength' => true]) ?>
		</div>
		<div class="col-md-6">
			<?= $form->field($_userModel, 'passwordRepeat')->passwordInput(['maxlength' => true]) ?>
		</div>
	</div>

</div>

<div class="row">
  <div class="col-md-12 m-t-10 text-right">
    <?= Html::submitButton('SAVE CHANGE', ['class' => 'btn btn-success  btn-lg']); ?>
    <button type="button" class="btn btn-info btn-cons p-t-10 p-b-10" style="font-size: 12px" data-dismiss="modal">CANCEL</button>
  </div>
</div>

<?php ActiveForm::end(); ?>
