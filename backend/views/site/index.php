<?php
	use yii\helpers\Html;
	use common\components\CommonHelper;
	use miloschuman\highcharts\Highcharts;
	use yii\web\JsExpression;

?>
<div class="row">
	<div class="col-md-6 text-md-left">
		<h3>DASHBOARD</h3>
	</div>
	<div class="col-md-6 text-md-right">
		<h3><?=date('l, d F Y')?></h3>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
			<img src="<?php echo $this->theme->baseUrl; ?>/assets/img/crowe_indonesia.png" class="bg-img-cs-al" >
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="card card-default">
			<div class="card-body">
				<span class="bold">ON GOING</span>
				<h3>JOB </h3>
				<table class="table table-striped">
					<?php
						if(!empty($job_list)){
							echo "
								<tr>
									<th class='text-right' width='5%'>No.</th>
									<th width='50%'>Job Description</th>
									<th class='text-right' width='10%'>Budget</th>
									<th class='text-right' width='10%'>Allocated</th>
									<th class='text-right' width='10%'>Difference</th>
									<th class='text-center' width='15%'>Comments</th>
								</tr>
							";
							$i = 0;
							foreach($job_list as $jl){
								$i++;
								echo "
									<tr>
										<td class='text-right'>".$i.".</td>
										<td>".$jl['job_description']."</td>
										<td class='text-right'>".$jl['budget']."</td>
										<td class='text-right'>".$jl['allocated']."</td>
										<td class='text-right'>".$jl['diff']."</td>
										<td class=' text-center'>
											<a href='".Yii::$app->urlManager->createAbsoluteUrl(['tr/job/request', 'id'=>$jl['job_id']])."' class='badge badge-success'>".$jl['comment_count']." Comments</a>
										</td>
									</tr>
								";
							}
						}else{
							echo "<tr><td colspan='4'>There are no jobs assigned to you</td></tr>";
						}

					?>
				</table>
			</div>
		</div>
	</div>
</div>
<br/>
