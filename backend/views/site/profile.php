<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\MaskedInput;
use kartik\widgets\ActiveForm;
use kartik\widgets\DepDrop;
use kartik\widgets\FileInput;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use yii\web\JsExpression;

use common\models\cm\CmMaster;
use common\models\cm\Entity;
use common\models\cm\Dept;
use common\models\cm\Division;
use common\models\cm\Level;
use common\models\hr\Employee;


$this->title = 'INFORMATION';
$this->params['breadcrumbs'][] = "PROFILE";
$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs_btn'] = Html::a(
    'BACK',
    ['index'],
    [
      //'onclick' => 'FormCreate()',
      'class' => 'btn btn-info text-white ',
    ]
  );


?>
                <?php
                $form = ActiveForm::begin([
                    'id' => 'crud-form',
                    //'action'=>['save'],
                    'enableClientValidation' => true,
                    'validateOnSubmit' => true,
                    'validateOnChange' => true,
                    'validateOnType' => true,
                    'fieldConfig' => [
                        'template' => '{label}{input}',
                        'options' => [
                            'class' => 'form-group form-group-default',
                        ],
                ],
                'errorSummaryCssClass'=> 'alert alert-danger'
                ]);
                echo $form->errorSummary($model);;

                ?>
                    <div id="error" class="alert alert-danger" style="display:none"></div>
                    <div class="">
                        <div class="modal-content no-border" style="widht: 100% !important">
                            <div class="modal-header clearfix text-left bg-success p-t-20 p-b-10 p-l-20 p-r-20">
                            PROFILE INFORMATION
                            </div>
                            <div class="modal-body">

                                <div class="row m-b-30">
                                    <table class='table table-bordered'>
                                        <tr>
                                            <th class='bg-primary' width='120px'>NAMA</th>
                                            <th><?= $model->full_name; ?></th>
                                        </tr>
                                        <tr>
                                            <th class='bg-primary'>POSITION</th>
                                            <th><?= empty($model->level->level_name) ? "" : $model->level->level_name; ?></th>

                                        </tr>
                                        <tr>
                                            <th class='bg-primary'>DIVISION</th>
                                            <th><?= empty($model->division->div_name) ? "" : $model->division->div_name; ?></th>
                                        </tr>
                                        <tr>
                                            <th class='bg-primary'>NIK</th>
                                            <th><?= $model->Id; ?></th>
                                        </tr>
                                        <tr>
                                            <th class='bg-primary'>GROUP</th>
                                            <th><?= empty($model->leader->full_name) ? "" : $model->leader->full_name; ?></th>

                                        </tr>
                                        <tr>
                                            <th class='bg-primary'>SUPERVISOR</th>
                                            <th><?= empty($model->supervisor->full_name) ? "" : $model->supervisor->full_name; ?></th>
                                        </tr>
                                        <tr>
                                            <th class='bg-primary'>MANAGER</th>
                                            <th><?= empty($model->manager->full_name) ? "" : $model->manager->full_name; ?></th>
                                        </tr>
                                        <tr>
                                            <th class='bg-primary'>EMAIL</th>
                                            <th><?= $model->user_email; ?></th>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php ActiveForm::end(); ?>
