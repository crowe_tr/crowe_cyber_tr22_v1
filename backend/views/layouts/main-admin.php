<?php

	use common\components\CommonHelper; //helper dipanggil disni :O ASIIIKK
	use yii\helpers\Html;
	use yii\widgets\Breadcrumbs;

	$controller = $this->context;
	$menus = $controller->module->menus;
	$route = $controller->route;
	foreach ($menus as $i => $menu) {
		$menus[$i]['active'] = strpos($route, trim($menu['url'][0], '/')) === 0;
	}
	$this->params['nav-items'] = $menus;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
	<head>
		<meta charset="<?= Yii::$app->charset ?>" />
		<?= Html::csrfMetaTags() ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
		<title>
			Crowe
			<?= !empty($this->title) ? ' | '.Html::encode(ucfirst($this->title)) : ''; ?>
		</title>
		<?php $this->head() ?>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
		<link rel="apple-touch-icon" href="<?php echo $this->theme->baseUrl; ?>/pages/ico/60.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo $this->theme->baseUrl; ?>/pages/ico/76.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo $this->theme->baseUrl; ?>/pages/ico/120.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo $this->theme->baseUrl; ?>/pages/ico/152.png">
		<link rel="icon" type="image/x-icon" href="<?php echo $this->theme->baseUrl; ?>/favicon.ico" />
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-touch-fullscreen" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="default">
		<meta content="antariksa - built with love to make life easier" name="description" />
		<meta content="Andre Antariksa" name="author" />

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link href="<?php echo $this->theme->baseUrl; ?>/assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo $this->theme->baseUrl; ?>/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo $this->theme->baseUrl; ?>/assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo $this->theme->baseUrl; ?>/pages/css/pages-icons.css" rel="stylesheet" type="text/css">
		<link class="main-stylesheet" href="<?php echo $this->theme->baseUrl; ?>/pages/css/themes/light.css" rel="stylesheet" type="text/css" />
		<link class="main-stylesheet" href="<?php echo $this->theme->baseUrl; ?>/assets/css/style.css" rel="stylesheet" type="text/css" />

	<script src="<?php echo $this->theme->baseUrl; ?>/<?php echo $this->theme->baseUrl; ?>/assets/plugins/jquery/jquery-3.2.1.min.js" type="text/javascript"></script>
</head>
	<body>
		<?php $this->beginBody(); ?>

			<?php echo $this->render('navbar'); ?>
			<?php echo $this->render('headersub'); ?>
			<div class="page-container ">
				<div class="page-content-wrapper ">
					<div class="content p-t-10">
						<div class="container-fluid container-fixed-lg">
							<div class="row">
								<div class="col-md-8">
									<?= Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [], 'homeLink' => false]); ?>
								</div>
								<div class="col-md-4 text-right">
									<?= isset($this->params['breadcrumbs_btn']) ? $this->params['breadcrumbs_btn'] : ""; ?>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-3">
									<div id="manager-menu" class="list-group">
										<?php
										foreach ($menus as $menu) {
											$label = Html::tag('i', '', ['class' => 'fa fa-chevron-right pull-right']).
												Html::tag('span', Html::encode($menu['label']), []);
											$active = $menu['active'] ? ' active' : '';
											echo Html::a($label, $menu['url'], [
												'class' => 'list-group-item'.$active,
											]);
										}
										?>
									</div>
								</div>
								<div class="col-md-9">
									<?= $content ?>
								</div>
							</div>

						</div>
					</div>
					<?php
					// echo $this->render('footersub'); 
					?>
				</div>
				<?php if(!empty($this->params['alerts'])) : ?>
					<div class="alt-alert pgn-wrapper" data-position="top-right">
						<div class="pgn pgn-flip">
							<div class="alert <?=$this->params['alerts']['class']?>" role="alert">
								<p class="pull-left alt-alert-header"><?=$this->params['alerts']['header']?></p>
								<button class="close" data-dismiss="alert"></button>
								<div class="clearfix"></div>
								<div class="alt-alert-body">
									<?=$this->params['alerts']['body']?>
								</div>
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
			<script src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/feather-icons/feather.min.js" type="text/javascript"></script>
			<script src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/pace/pace.min.js" type="text/javascript"></script>
			<script src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/jquery/jquery-3.2.1.min.js" type="text/javascript"></script>
			<script src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/modernizr.custom.js" type="text/javascript"></script>
			<script src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
			<script src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/popper/umd/popper.min.js" type="text/javascript"></script>
			<script src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
			<script src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
			<script src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
			<script src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
			<script src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/jquery-actual/jquery.actual.min.js"></script>
			<script src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
			<script type="text/javascript" src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/select2/js/select2.full.min.js"></script>
			<script type="text/javascript" src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/classie/classie.js"></script>
			<script src="<?php echo $this->theme->baseUrl; ?>/assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
			<script src="<?php echo $this->theme->baseUrl; ?>/pages/js/pages.js"></script>
			<script src="<?php echo $this->theme->baseUrl; ?>/assets/js/scripts.js" type="text/javascript"></script>
			<script src="<?php echo $this->theme->baseUrl; ?>/assets/js/custom.js" type="text/javascript"></script>
		<?php $this->endBody() ?>
	</body>
</html>
<?php $this->endPage() ?>
