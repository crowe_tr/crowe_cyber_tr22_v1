<div class=" container-fluid  container-fixed-lg footer">
    <div class="copyright sm-text-center">
        <div class="row">
            <div class="col">

                <p class="small no-margin pull-left sm-pull-reset">
                    <span class="hint-text">Copyright &copy; 2019 </span>
                    <span class="font-montserrat"> Crowe </span>.
                    <span class="hint-text">All rights reserved. </span>
                </p>
                <div class="clearfix"></div>
            </div>
            
        </div>
        
    </div>
</div>