<?php

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'admin', 'CustomLogs'],
    'controllerNamespace' => 'backend\controllers',
    
    'aliases' => [
        '@npm' => '@vendor/npm-asset',
        '@bower'=> '@vendor/bower-asset'
    ],
    
    'modules' => [
        'gii' => [
            'class' => 'yii\gii\Module',
            'allowedIPs' => ['127.0.0.1', '::1', '*', 'XXX.XXX.XXX.XXX'], // adjust this to your needs
            'generators' => [ //here
                'crud' => [ // generator name
                    'class' => 'yii\gii\generators\crud\Generator', // generator class
                    'templates' => [ //setting for out templates
                        'antariksa' => '@backend/views/layouts/gii/default', // template name => path to template
                    ]
                ]
            ],
        ],
        'cl' => [
            'class' => 'backend\modules\cl\Module',
        ],
        'cm' => [
            'class' => 'backend\modules\cm\Module',
        ],
        'rg' => [
            'class' => 'app\modules\rg\Module',
        ],
        'hr' => [
            'class' => 'app\modules\hr\Module',
        ],
        'tr' => [ 
            'class' => 'backend\modules\tr\Module',
        ],
        'trms' => [
            'class' => 'backend\modules\trms\Module',
        ],
        'st' => [
            'class' => 'backend\modules\st\Module',
        ],
        'redactor' => [
            'class' => 'yii\redactor\RedactorModule',
            'uploadDir' => '@webroot/public/timereport/',
            'uploadUrl' => '@web/public/timereport/',
            'imageAllowExtensions' => ['jpg', 'jpeg', 'png', 'gif'],
            'fileAllowExtensions' => ['pdf', 'pptx', 'docx', 'doc', 'xls', 'xlsx', 'jpg', 'png', 'jpeg', 'gif'],
        ],
        'markdown' => [
            'class' => 'kartik\markdown\Module',
        ],
        'admin' => [
            'class' => 'mdm\admin\Module',
            'controllerMap' => [
                'assignment' => [
                    'class' => 'mdm\admin\controllers\AssignmentController',
                    'userClassName' => 'common\models\hr\Employee',
                    'idField' => 'Id',
                    'usernameField' => 'Id',
                ],
            ],
            'layout' => '@backend/views/layouts/main-admin',
        ],
        'gridview' => [
            'class' => '\kartik\grid\Module',
        ],
    ],
    'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            'common/*',
            'helper/*',
            'debug/*',
            'cm/helper/*',
            'hr/helper/*',
            'tr/helper/*',
            'site/mt',

        ],
    ],
    'components' => [
        'CustomLogs' => [
            'class' => 'common\components\CustomLogs'
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                ['class' => 'common\components\UrlRule', 'connectionID' => 'db'],
                '<controller:\w+>/<id:\d+>' => '<controller>',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
        ],
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'common\models\hr\Employee',
            'enableAutoLogin' => false,
            'loginUrl' => ['/site/login', 'ref' => rand()],
            'identityCookie' => [ // <---- here!
                'name' => '__webant.antariksa.local',
                'httpOnly' => false,
                //'domain' => 'webant.antariksa.local',
            ],
        ],
        'request' => [
            'enableCsrfValidation' => true,
            'cookieValidationKey' => '1100101110010111001011100101'
        ],
        'session' => [
            'cookieParams' => [
                'httpOnly' => true,
                //'domain' => 'webant.antariksa.local',
            ],
        ],
        'log' => [
            'traceLevel' => 3, //,YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\DbTarget',
                    'levels' => ['error', 'warning', 'trace', 'info'],
                    'prefix' => function ($message) {
                        $user = Yii::$app->has('user', true) ? Yii::$app->get('user') :
                            'undefined user';
                        $userID = $user ? $user->getId(false) : 'anonym';
                        return "[$userID]";
                    }
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'view' => [
            'theme' => [
                'basePath' => '@app/themes/pages/',
                'baseUrl' => '@web/../../themes/pages/',
                'pathMap' => [
                    '@app/views' => '@app/themes/pages/',
                ],
            ],
        ],
        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js' => [],
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js' => [], //ini dimatiin karena bentrok jeung tempalte kk
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [], //ini dimatiin karena bentrok jeung tempalte kk
                ],

            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],

    ],
    'params' => $params,
];
