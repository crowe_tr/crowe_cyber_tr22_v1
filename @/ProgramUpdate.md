Tgl. 4 Nov 2024

1.  pak kamal, untuk syarat Overtime min mengisi 8 jam project sudah OK ya
    Mohon bisa dijalankan ya pak
    8 jam (bisa mix project ya), hanya project saja tidak bisa job development
    kecuali back office (shared services), berlaku aturan yang lama
    Solusi :
    Yang ini harusnya sudah selesai

Tgl. 29 Okt 2024

1.  Recovery per Divisi
    Solusi :
    a. Tambahkan Function get_min_rec_rate
    b. Tambahkan Field recovery_rate(int) di table cm_division
    c. Update Trigger bu_tr_job dan bu_tr_job_temp, cari ELSEIF percent_val_only( New.job_fee, expenses ) <, update jadi
    ELSEIF percent_val_only( New.job_fee, expenses ) < get_min_rec_rate(New.client_id) THEN
    CALL catch_error( CONCAT('Can`t Submit Job : Recovery Rate < ',get_min_rec_rate(New.client_id),'%') );

Tgl. 14 Okt 2024

3.  Kalau dibuat batas saat masukkan budget dan submit job,
    jika estimated recovery rate nya dibawah 60% tidak bs di submit
    a. Tambahkan Function percent_val_only => done
    b. Update Trigger bu_tr_job
    c. Update Function is_true => done
    d. Update Trigger bu_tr_job_temp
4.  Ganti Email

Tgl. 10 Okt 2024

1.  Report Job Detail dengan Parameter Tanggal Time Report
    Solusi :
    a. Tambahkan Procedure rpt_job_detail_v2

Tgl. 25 Sept 2024

1.  Report Job Detail
    a. mohon bisa ditambah entity employee
    b. untuk job periode apakah bs dipisah saja tanggal mulai dan berakhirnya => di pisah kolom supaya bisa di sort
    c. untuk team ini berarti based on job ya pak?
    apakah bisa ada 2 kolom team
    partner dari job
    partner dari grup nama employee tsb
    d. Header Untuk Budget dan Actual
    Solusi : Update procedure rpt_job_detail

2.  Report Recap for Performance Apprisal
    Terdiri dari :
    - Nama
    - Team (based on employee, group partner)
    - Periode
    - Total Workhours
    - Total WH yang ada pada project (berdasarkan Type Name)
    - Total WH atas back office, others, self development, dan preparation (berdasarkan Type Name)
    - Total WH atas Sick Leave without doctor note
    - Total WH atas Sick Leave with doctor note
    - Total WH annual leave
      Solusi : Tambahkan Procedure rpt_recap_perform

Tgl. 24 Sept 2024

1.  Report Job Detail
    Solusi :
    a. Tambahkan Procedure rpt_job_detail

Tgl. 20 Sept 2024

1.  Report Recovery Rate Summary tambahkan Estimasi
    Solusi DB :
    a. Update procedure rpt_job
    Tambahkan baris 36
    , j.meal_allowance + j.ope_allowance + j.taxi_allowance +
    j.administrative_charge + j.other_expense_allowance est_expense

Tgl. 29 Nov 2023

1.  pak kamal, untuk syarat Overtime min mengisi 8 jam project sudah OK ya
    Mohon bisa dijalankan ya pak 8 jam (bisa mix project ya),
    hanya project saja tidak bisa job development
    kecuali back office (shared services), berlaku aturan yang lama
    Solusi di DB :
    a. Update procedure set_tr_val
    Baris 11 Update jadi
    IF ( itask_name = 'tr-detail' ) THEN
    SELECT CONCAT_WS(';', COUNT(d.id), SUM(d.work_hour), SUM(d.over_time)
    , SUM(d.tr_detail_value)
    , IFNULL(
    SUM(
    CASE
    WHEN ( common_val( 'task-type-name', d.task_type_id ) = 'BACK OFFICE' ) THEN d.work_hour
    ELSE IF(fis_project(d.task_type_id),d.work_hour,0)
    END
    ),0)
    , IFNULL(
    SUM(
    CASE
    WHEN ( common_val( 'task-type-name', d.task_type_id ) = 'BACK OFFICE' ) THEN d.over_time
    ELSE IF(fis_project(d.task_type_id),d.over_time,0)
    END
    ),0)
    )

            INTO vdata
            FROM tr_time_report_detail d
            WHERE d.time_report_id=iparam;

            UPDATE tr_time_report
            SET job_count=tokens_param(vdata,1), work_hour=tokens_param(vdata,2), over_time=tokens_param(vdata,3)
            , time_charges=tokens_param(vdata,4)
            , wh_project=tokens_param(vdata,5)
            , ot_project=tokens_param(vdata,6)
            WHERE id=iparam;

    b. update Trigger bu_tr_time_report
    Baris 18 tambahkan
    ELSEIF ( New.over_time > 0 ) THEN
    IF ( ( New.wh_project - New.ot_project ) < min_wh ) THEN
    CALL catch_error( 'Overtime Not Allowed' );
    END IF;
    END IF;

Tgl. 3 Jan 2024

1.  Log History Time Report per User
    Solusi :
    a. Tambahkan table tr_time_report_logs
    b. Tambahkan procedure tr_list2
    c. Tambahkan procedure tr_list3
    d. Tambahkan procedure tr_hint2
2.  Log History Time Report oleh Admin

Tgl. 12 Feb 2024

1.  Apabila Employee dinaikan jadi Supervisor Job, maka yang sudah tersubmit tidak merubah jadi Approve1
    Solusi :
    a. Update procedure "tr_approval_by_month"
    beri komen pada baris 132 s/d 135
    b. Update procedure "tr_approval_detail_employee"
    beri komen pada baris 140 s/d 143
