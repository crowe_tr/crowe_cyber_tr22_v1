<?php

return [
    'siteName' => 'TimeReport',

    //basic configuration for application
    'appUserDefaultPhotoBaseurl' => 'themes/sources/male.png',
    'appAdminEmail' => 'antariksa.org@gmail.com',
    'appNotificationEmail' => 'noreply@knn.co.id',
    'appNotificationEmailCC' => 'andre.antariksa@icloud.com',

    //image folder for some of master table
    'hrEmployeeBasepath' => '/web/pub/common/employee/',
    'hrEmployeeBaseurl' => 'pub/common/employee/',


];
