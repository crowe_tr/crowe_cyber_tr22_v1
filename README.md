CROWE for Office 88
--------------------

Yii2 Version : 2.0.43
PHP Version : 8.0
MySQL : 8.0
MsSQLServer : 2008 R2


CROWE for Cyber 2022

OS : Ubuntu 18.04
Apache : 2.4
PHP Version : 7.3.33
MySQL : 5.7

Untuk Pull server menggunakan
- sudo git pull origin master

Untuk Bersihkan buff/cache
- sudo sh -c 'echo 1 > /proc/sys/vm/drop_caches'

Untuk Restart
- service mysql restart
- service apache2 restart

Tiap bulan hapus file backup MySQL
- sudo rm -f crowe_tr22_cyber_202301*


Memulai Yii2 :
- git clone
- php init
- pilih development
- extract vendor.zip
- ubah database di common/config/main-local.php
