<?php

return [
    'components' => [
        'cache' => [
          'class' => 'yii\caching\FileCache',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost:3306;dbname=tr_o88_dummy',
            'username' => 'root',
            'password' => 'TimeReport@2021!', //T1m3R3p0rt@semut2019!
            'enableSchemaCache' => true,
            'schemaCacheDuration' => 3600,
            'schemaCache' => 'cache',
        ],

        'mailer' => [
		    'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'noreply.crowe.o88@gmail.com', //'crowe.office88@gmail.com',
                'password' => 'trwunszdwzssawuk', //'semutsemuthitam',
                'port' => '587',
                'encryption' => 'tls',
                'streamOptions' => [
                    'ssl' => [
                        'allow_self_signed' => true,
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                    ],
                ],
            ],
        ],

    ],
];
