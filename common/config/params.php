<?php

return [
    'siteName' => 'TimeReport',

    //basic configuration for application
    'appUserDefaultPhotoBaseurl' => 'themes/sources/male.png',
    'appAdminEmail' => 'noreply.timereport@crowe.id',
    'appNotificationEmail' => 'noreply.timereport@crowe.id',
    // 'appNotificationEmail' => 'noreply@knn.co.id',
    // 'appNotificationEmailCC' => 'antariksa.org@gmail.com',

    //image folder for some of master table
    'hrEmployeeBasepath' => '/web/pub/common/employee/',
    'hrEmployeeBaseurl' => 'pub/common/employee/',


];
