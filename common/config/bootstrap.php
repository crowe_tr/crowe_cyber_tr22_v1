<?php
Yii::setAlias('common', dirname(__DIR__));
Yii::setAlias('frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@themes', dirname(dirname(__DIR__)) . '/themes');
Yii::setAlias('console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@public_basepath', dirname(dirname(__DIR__)) . '/backend/web/public');
Yii::setAlias('@public_baseurl', '/public');

