<?php

namespace common\components;

use Yii;
use yii\helpers\Html;
use common\components\CommonHelper;

/****** TimeReport HELPER :
 * fungsi-fungsi yang biasa dipakai / dibutuhkan di semua module TimeReport buatan si AA
 * "buat hidup lebih mudah"
 *
 * ada beberapa initial, yaitu :
 * - get* => return berupa value (berbagai type),
 * - set* => return berupa value juga tapi dikhususkan untuk ganti value,contoh: $a = setValue(123);
 * - is* => return berupa boolean (1/0), contoh: if(isEmpty($a)) ? ...
 * - js => return berupa function js
 * *mau buat baru ? ikutin initial ini biar jadi standar !
 *
 * PENTING !!!!
 * Jangan sebarang hapus/ganti fungsi dibawah jika tidak tau apa yang dilakukan !!!!
 * banyak module yang menggunakan helper ini jika mau ubah sesuatu disarankan buat fungsi baru !
 ***********************/

class TimeReportHelper
{
  public static function getEntity()
  {
    $results = [];
    // $user = CommonHelper::getUserIndentity();
    // Yii::$app->cache->flush();

    // if (!empty($user->CompanyId)) {
    //     if(isset($param['withAll'])){
    //         $sales[0] = 'SELECT All';
    //     }
    //     $data = \common\models\cm\Entity::find()->all();
    //     foreach ($data as $child) {
    //         $results[$child->id] = $child->entityCode.' - '.$child->entityName;
    //     }
    // }

    $data = \common\models\cm\Entity::find()->all();
    foreach ($data as $child) {
      $results[$child->id] = $child->entity_code . ' - ' . $child->entity_name;
    }

    return $results;
  }

  public static function getDivision()
  {
    // $results = [];
    // $user = CommonHelper::getUserIndentity();
    // Yii::$app->cache->flush();
    //
    // if (!empty($user->CompanyId)) {
    //     if(isset($param['withAll'])){
    //         $sales[0] = 'SELECT All';
    //     }
    //     $data = \common\models\cm\Division::find()->all();
    //     // print_r($data);
    //     foreach ($data as $child) {
    //         $results[$child->id] = $child->divCode.' - '.$child->divName;
    //     }
    // }
    //
    // return $results;

    Yii::$app->cache->flush();

    $data = \common\models\cm\Division::find()->all();
    // print_r($data);
    foreach ($data as $child) {
      $results[$child->id] = $child->div_code . ' - ' . $child->div_name;
    }
    return $results;
  }

  public static function getClient()
  {
    // $results[];
    Yii::$app->cache->flush();

    $data = \common\models\cl\Client::find()->all();
    // print_r($data);
    foreach ($data as $child) {
      $results[$child->id] = $child->client_code . ' - ' . $child->client_name;
    }
    return $results;

    // if (!empty($user->CompanyId)) {
    //     if(isset($param['withAll'])){
    //         $sales[0] = 'SELECT All';
    //     }
    //     $data = \common\models\cm\Division::find()->all();
    //     // print_r($data);
    //     foreach ($data as $child) {
    //         $results[$child->id] = $child->divCode.' - '.$child->divName;
    //     }
    // }

  }

  public static function getEmployee()
  {
    Yii::$app->cache->flush();

    $data = \common\models\hr\Employee::find()->all();

    foreach ($data as $e) {
      $level = !empty($e->level->level_name) ? " - " . $e->level->level_name : "";
      $results[$e->user_id] = $e->user_id . ' - ' . $e->full_name . $level;
    }

    return $results;
  }

  public static function getVempGroup()
  {
    // Yii::$app->cache->flush();

    $results = [];

    $data = \common\models\hr\VempGroup::find()->all();

    foreach ($data as $v) {
      $results[$v->user_id] = $v->user_id . ' - ' . $v->full_name . ' - ' . $v->level_name;
    }

    return $results;
  }

  public static function getEmployeeJob()
  {
    $results = [];

    $data = \common\models\tr\JobBudgeting::find()->all();

    foreach ($data as $v) {
      $results[$v->employee_id] = $v->employee_id . ' - ' . $v->employee->full_name;
    }

    return $results;
  }

  public function getAreaJob()
  {
    Yii::$app->cache->flush();

    $data = \common\models\cl\Client::find()->all();
    // print_r($data);
    foreach ($data as $child) {
      $results[$child->id] = $child->client_code . ' - ' . $child->client_areas;
    }
    return $results;
  }

  public static function emailMonthlySupervisor()
  {
    $sent_to = "";
    $header = Yii::$app->db->createCommand("call monthly_approval1_header('" . date('Y-m-01') . "', '" . date('Y-m-t') . "')")->queryAll();
    if (!empty($header)) {
      foreach ($header as $h) {
        if (!empty($h['approval_id'])) {
          $subject = 'Crowe TimeReport - Monthly Supervisor Pending Approval Summary';
          static::Email(
            $h['approval_email'],
            $subject,
            [
              'h' => $h,
            ],
            'timereport/monthly_supervisor'
          );
        }
        $sent_to .= " ".$h['approval_name'].",";
      }
    }
    echo !empty($sent_to) ? "Email Sent to: ".$sent_to : "Failed, call administrator";
  }
  public static function emailMonthlyManager()
  {
    $sent_to = "";
    $header = Yii::$app->db->createCommand("call monthly_approval2_header('" . date('Y-m-01') . "', '" . date('Y-m-t') . "')")->queryAll();
    if (!empty($header)) {
      foreach ($header as $h) {
        if (!empty($h['approval_id'])) {
          $subject = 'Crowe TimeReport - Monthly Manager Pending Approval Summary';
          static::Email(
            $h['approval_email'],
            $subject,
            [
              'h' => $h,
            ],
            'timereport/monthly_manager'
          );
        }
      }
      $sent_to .= " ".$h['approval_name'].",";
    }
    echo !empty($sent_to) ? "Email Sent to: ".$sent_to : "Failed, call administrator";
  }



  public static function Email($sendto, $subject, $param, $layouts = '')
  {
    Yii::$app->cache->flush();

    $from = Yii::$app->params['appNotificationEmail'];
    $cc = 'antariksa.org@gmail.com';//Yii::$app->params['appNotificationEmailCC'];

    $to = $sendto;

    if (!empty($to)) {
      $html = ['html' => $layouts];

      $compose = Yii::$app->mailer->compose($html, $param);
      $compose->setFrom($from);
      $compose->setTo($to);
      if (!empty($cc)) {
        $compose->setCc($cc);
      }
      $compose->setSubject($subject);

      $compose->send();
    }
  }
}
