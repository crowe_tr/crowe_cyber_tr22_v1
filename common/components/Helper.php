<?php

namespace common\components;

use Yii;
use yii\web\UploadedFile;

/****** common HELPER :
* fungsi-fungsi yang biasa dipakai / dibutuhkan di semua module buatan si AA
* "buat hidup lebih mudah"
*
* ada beberapa initial, yaitu :
* - get* => return berupa value (berbagai type),
* - set* => return berupa value juga tapi dikhususkan untuk ganti value,contoh: $a = setValue(123);
* - is* => return berupa boolean (1/0), contoh: if(isEmpty($a)) ? ...
* - js => return berupa function js
* *mau buat baru ? ikutin initial ini biar jadi standar !
*
* PENTING !!!!
* Jangan sebarang hapus/ganti fungsi dibawah jika tidak tau apa yang dilakukan !!!!
* banyak module yang menggunakan helper ini jika mau ubah sesuatu disarankan buat fungsi baru !
***********************/

class Helper
{

    public static function getIsMobile()
    {
        $mobile = false;

        $useragent=$_SERVER['HTTP_USER_AGENT'];
        if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', 
        substr($useragent,0,4))){
            $mobile = true;
        }
        return $mobile;
    }
    public function ResetMail($str)
    {
        $rmv = array('_');
        $str = str_replace($rmv, '', $str);

        return $str;
    }
    public function ResetPhoneOrFax($str)
    {
        $rmv = array(' ', '_', '(', ')');
        $str = str_replace($rmv, '', $str);

        return $str;
    }
    public function ResetDecimal($str)
    {
        $rmv = array(',');
        $str = str_replace($rmv, '', $str);

        return $str;
    }
    public static function setDefaultValueIfEmpty($val = '', $var_type = 'str')
    {
        if (empty($val)) {
            switch ($var_type) {
                case 'string':
                    $val = '(not set)';
                break;

                case 'int':
                    $val = 0;
                break;

                default:
                    $val = null;
                break;
            }
        }

        return $val;
    }

    public static function getEmployeeFromUser($_id = '')
    {
        if (empty($_id)) {
            $_id = CommonHelper::getSessionIdUser();
        }

        $_return = array();
        $_return['ID'] = '';
        $_return['NAME'] = '';
        $_return['OFFICE_ID'] = '';
        $_return['DEPARTEMENT_ID'] = '';

        $_user = \common\models\USERS::findOne($_id);
        if (!empty($_user->PERSON_ID)) {
            $_employee = \common\models\EMPLOYEE::findOne($_user->PERSON_ID);
            if (!empty($_employee->ID)) {
                $_return = array();
                $_return['ID'] = $_employee->ID;
                $_return['NAME'] = $_employee->NAME;
                $_return['OFFICE_ID'] = $_employee->OFFICE_ID;
                $_return['DEPARTEMENT_ID'] = $_employee->DEPARTEMENT_ID;
            }
        }

        return $_return;
    }
    public static function getSessionIdUser()
    {
        if (!empty(Yii::$app->user->identity->id)) {
            $_id = Yii::$app->user->identity->id;  //if id = null, set id = session_id :P
        } else {
            $_id = -1; //if session null juga set -1 => UNAUTHORIZED USER ketauan kalau perlu nantinya
        }

        return $_id;
    }
    public static function getUserLoginIdentity()
    {
        $data = array();

        $session = Yii::$app->session;
        $_user_employee_id = (!empty($session['__employee_id'])) ? $session['__employee_id'] : '(not set)';
        $_user_full_name = (!empty($session['__full_name'])) ? $session['__full_name'] : '(not set)';
        $_user_photo = Yii::$app->homeUrl.Yii::$app->params['appUserDefaultPhotoBaseurl'];

        if (!empty($session['__employee_photo'])) {
            $_path = Yii::$app->basePath.Yii::$app->params['hrEmployeeBasepath'];
            $_base_url = Yii::$app->homeUrl.Yii::$app->params['hrEmployeeBaseurl'];

            if (file_exists($_path.$session['__employee_photo'])) {
                $_user_photo = $_base_url.$session['__employee_photo'];
            }
        }

        $data['_user_employee_id'] = $_user_employee_id;
        $data['_user_full_name'] = $_user_full_name;
        $data['_user_photo'] = $_user_photo;

        return $data;
    }

    public static function jsClock()
    {
        $js = '
            function updateClock(id)
            {
                var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

                var d = new Date( );
                var currentHours = d.getHours( );
                var currentMinutes = d.getMinutes( );
                var currentSeconds = d.getSeconds( );

                var curr_date = d.getDate();
				if(curr_date < 10) curr_date = "0" + curr_date;
                var curr_month = months[d.getMonth()];
                var curr_year = d.getFullYear();

                // Pad the minutes and seconds with leading zeros, if required
                currentMinutes = (currentMinutes < 10 ? "0" : "") + currentMinutes;
                currentSeconds = (currentSeconds < 10 ? "0" : "") + currentSeconds;


                // Compose the string for display
                var currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds + "<br>" + curr_date + " " + curr_month + " " + curr_year;
                $("#"+id).html(currentTimeString);
            }';

        return $js;
    }

    public static function upload_model($_model, $_fieldname, $_default_filename = "", $_dir, $_filename)
    {
        $file = UploadedFile::getInstance($_model, $_fieldname);
        $filename = $_default_filename;
        if (!empty($file) && $file->size !== 0) {
            $filename = $_filename . date('ymdhis') . '.' . $file->extension;
            Helper::upload($file, $_dir . $filename);
        }

        return $filename;
    }


    public static function upload($file, $filepath)
    {
        return $file->saveAs(Yii::getAlias('@public_basepath') . '/' . $filepath);
    }
}
