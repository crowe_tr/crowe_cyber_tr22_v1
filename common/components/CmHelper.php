<?php

namespace common\components;

use Yii;
use yii\helpers\Html;
use kartik\grid\GridView;

class CmHelper
{
  public static function set_title($this_form, $title1, $title2 = null){
    $this_form->title = $title1;
    $this_form->params['breadcrumbs'][] = $this_form->title;
    $this_form->params['breadcrumbs'][] = $title2;
    $this_form->params['breadcrumbs_btn'] = Html::a(
                                                      '<i class="pg-plus"></i> <span class="hidden-xs">CREATE NEW</span>',
                                                      false,
                                                      [
                                                        'onclick' => 'FormCreate()',
                                                        'class' => 'btn btn-warning text-white ',
                                                      ]
                                                    );
  }

  public static function set_column($iattribute, $ilabel, $icol_sm, $ivalue=null, $ialign=null){
    return  [
              'attribute' => $iattribute,
              'label' => $ilabel,
              'headerOptions' => ['class' => 'col-sm-'.$icol_sm.' bg-success'],
              'filterOptions' => ['class' => 'b-b b-grey'],
              'contentOptions' => ['class' => 'kv-align-middle '.$ialign],
            ];
  }

  public static function set_column_search($iattribute, $ilabel, $icol_sm, $ifilter, $ivalue){
    return  [
              'attribute' => $iattribute,
              'label' => $ilabel,
              'headerOptions' => ['class' => 'col-sm-'.$icol_sm.' bg-success'],
              'filterOptions' => ['class' => 'b-b b-grey'],
              'contentOptions' => ['class' => 'kv-align-middle'],
              'filterType' => GridView::FILTER_SELECT2,
              'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
              ],
              'filterInputOptions' => ['placeholder' => ''],
              'format' => 'raw',
              'filter' => $ifilter,
              'value'=> $ivalue,
            ];
  }

  public static function set_column_no(){
    return  [
              'class' => 'kartik\grid\SerialColumn',
              'header' => 'NO',
              'mergeHeader' => false,
              'headerOptions' => ['class' => 'bg-success b-r'],
              'contentOptions' => ['class' => 'text-right b-r'],
              'filterOptions' => ['class' => 'b-b b-grey b-r'],
              'width' => '36px',
            ];
  }

  public static function set_column_bool($iattribute, $icol, $iinit)
  {
    if ($iinit == 1) {
      $vyes = 'Yes';
      $vno = 'No';
    } elseif ($iinit == 2) {
      $vyes = 'Active';
      $vno = 'Non Active';
    }

    return
      [
          'attribute' => $iattribute,
          'headerOptions' => ['class' => 'col-sm-'.$icol.' bg-success'],
          'filterOptions' => ['class' => 'b-b b-grey'],
          'contentOptions' => ['class' => 'kv-align-middle'],
          'filterType' => GridView::FILTER_SELECT2,
          'filterWidgetOptions' => [
              'pluginOptions' => ['allowClear' => true],
          ],
          'filterInputOptions' => ['placeholder' => ''],
          'contentOptions' => ['class' => 'kv-align-middle'],
          'filterInputOptions' => ['placeholder' => ''],
          'format' => 'raw',
          'filter' => [1 => $vyes , 0 => $vno ],
          'value' =>  function($model) use ($iattribute, $vyes, $vno){
                        if ($model->$iattribute == 1) {
                          return $vyes;
                        }
                        else{
                          return $vno;
                        }
                      },
      ];
  }

  public static function set_icon_cm($iid, $iform){
    return self::set_icon($iid, 'cm', $iform);

  }

  public static function set_icon($iid, $isub, $iform){
    $vform = $isub.'/'.$iform.'/form';

    $val = '<div class="tooltip-action">
              <div class="trigger">
                '.Html::a('<i class="fa fa-ellipsis-h"></i>', false, ['class' => 'text-info', 'data-toggle' => 'tooltip', 'data-original-title' => 'Up here!']).'
              </div>
              <div class="action-mask">
                <div class="action">
                  '.Html::a(
                    '<i class="fa fa-edit"></i>',
                    false,
                    [
                        'onclick' => "FormUpdate('".Yii::$app->urlManager->createAbsoluteUrl([$vform, 'id' => $iid])."')",
                        'class' => 'btn btn-success text-white padding-5 p-l-10 p-r-10',
                        'style' => 'bjob-radius: 5px !important',
                    ]
                  ).'
                '.Html::a('<i class="pg-trash"></i>',
                                        ['delete', 'id' => $iid],
                                        [
                                          'data-method' => 'post',
                                          'data-confirm' => 'Are you sure to delete this item?',
                                          'class' => 'btn btn-danger text-white padding-5 p-l-10 p-r-10',
                                        ]).'
                </div>
              </div>
            </div>';
    return $val;
  }

  public static function widget_pjax(){
    $data = '
              <div class="modal fade stick-up " id="crud-modal" role="dialog" aria-hidden="false" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog modal-lg ">
                  <div class="modal-content no-border">
                    <div class="modal-header bg-success text-white">
                    </div>
                    <div id="detail" class="modal-body padding-20">
                    </div>
                  </div>
                </div>
              </div>
            ';

    return $data;
  }

  public static function grid_view($idataProvider, $isearchModel, $icolumn){
    $data = [
              'dataProvider' => $idataProvider,
              'filterModel' => $isearchModel,
              'columns' => $icolumn,
              'layout'=>'
                <div class="card card-default">
                  <div class="row ">
                    <div class="col-md-12">
                      {items}
                    </div>
                  </div>
                  <div class="row padding-10">
                    <div class="col-md-4">{summary}</div>
                    <div class="col-md-8">{pager}</div>
                  </div>
                </div>
                  ',
              'emptyText' => '
                      <div class="text-center" style="padding: 2em 0">
                        <i class="fa fa-exclamation-circle fa-5x text-warning"></i>
                        <br>
                        <br>
                        '.Yii::t('backend', 'You do not have any data within your Filters.').'
                        <br>
                        '.Yii::t('backend', 'To create a new data, click <b><i class="icon-plus-circle"></i> ADD NEW</b> on the right top of corner').'
                    </div>',



              'resizableColumns' => true,
              'bordered' => false,
              'striped' => false,
              'condensed' => false,
              'responsive' => false,
              'hover' => true,
              'persistResize' => false,
            ];

    return $data;
  }

  public static function set_script($iform){
    $rform = '/'.'cm/'.$iform.'/form';
    $data = "
              var paramJs = (paramJs || {});
              paramJs.urlFormShowModal = '". Yii::$app->urlManager->createAbsoluteUrl([$rform]) ."';

              window.closeModal = function(){
                reload();
              };

            	function FormShowModal(link='') {
            		var link = (link || paramJs.urlFormShowModal);
            		$.ajax({
            			url: link,
            			data:  $('#crud-form').serialize(),
            			method: 'POST',
            			dataType: 'html',
            			success: function(data) {
            				$('#detail').html(data);
            			},
            		});
            	}

            	function FormCreate() {
            		FormShowModal();
            		$('#crud-modal').modal('show');
            	}
            	function FormUpdate(link) {
            		FormShowModal(link);
            		$('#crud-modal').modal('show');
            	}

            	function reload(){
            		$.pjax.defaults.timeout = false;
            		$.pjax.reload({
            			container: '#crud-pjax'
            		})
            		$('#crud-form').modal('hide');
            		$('#crud-form').data('modal', null);
                $('.modal-backdrop').modal('hide');
                $( '.modal-backdrop' ).removeClass( 'show' ).addClass( 'hide' );
            	}
            ";

    return $data;
  }

}
