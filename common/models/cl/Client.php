<?php

namespace common\models\cl;

use Yii;
use common\models\cm\Entity;
use common\models\cm\Division;
/**
 * This is the model class for table "clClient".
 *
 * @property int $Id
 * @property string $Code
 * @property int $Seq
 * @property string $Entity
 * @property string $Name
 * @property string $NPWP
 * @property string $VATOption
 * @property string $LatestJob
 * @property string $Industry
 * @property string $StatusClient
 * @property string $OfficeCP
 * @property string $OfficePhone
 * @property string $OfficeFax
 * @property string $OfficePhone2
 * @property string $OfficeEmail
 * @property string $OfficeCity
 * @property string* @property string $OfficeAddress
 * @property string $OfficeZipCode
 * @property string $BillingCP
 * @property string $BillingPhone
 * @property string $BillingFax
 * @property string $BillingPhone2
 * @property string $BillingEmail
 * @property string $BillingCity
 * @property string $BillingAddress
 * @property string $BillingZipCode
 * @property string $OtherCP
 * @property string $OtherPhone
 * @property string $OtherFax
 * @property string $OtherPhone2
 * @property string $OtherEmail
 * @property string $OtherAddress
 * @property string $OtherCity
 * @property string $OtherZipCode
 * @property string $ClientAreas
 * @property int $Status
 * @property int $Flag
 * @property string $CreatedBy
 * @property string $CreatedDate
 * @property string $LastUpdateBy
 * @property string $LastUpdateDate
 * @property string $DeletedBy
 * @property string $DeletedDate
 */
class Client extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cl_client';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
      return  [
                  [['seq'],
                      'autonumber',
                      'group' => $this->client_code,
                      'format' => function () {
                          return '?';
                      },
                      'digit' => 4,
                  ],
                  [['seq'], 'integer'],
                  [['alias'], 'string', 'max' => 3],
                  /*[['Seq'], 'unique'], disabled due a request 03 Maret 2020*/
                  [[ 'entity_id', 'div_id', 'client_name'
                      ], 'required'],
                  [['id'], 'integer'],
                  [['entity_id', 'client_name', 'npwp','div_id' , 'latest_job', 'industry_id'], 'string', 'max' => 255],
                  [['id'], 'unique'],
                  [['flag'], 'integer'],
                  [['created_by'], 'string', 'max' => 55],
                  [['flag', 'created_date', 'last_update_date', 'last_update_by', 'deleted_by', 'deleted_date', 'client_areas'], 'safe'],
              ];

        // return [
        //     [['Seq'],
        //         'autonumber',
        //         'group' => $this->Code,
        //         'format' => function () {
        //             return '?';
        //         },
        //         'digit' => 4,
        //     ],
        //     [['Seq'], 'integer'],
        //     [['Alias'], 'string', 'max' => 3],
        //     /*[['Seq'], 'unique'], disabled due a request 03 Maret 2020*/
        //     [[ 'Entity', 'Name'
        //         ], 'required'],
        //     [['Id'], 'integer'],
        //     [['Entity', 'Name', 'NPWP','divID' , 'LatestJob', 'Industry'], 'string', 'max' => 255],
        //     [['Id'], 'unique'],
        //     [['Flag'], 'integer'],
        //     [['CreatedBy'], 'string', 'max' => 55],
        //     [['Flag', 'CreatedDate', 'LastUpdateDate', 'LastUpdateBy', 'DeletedBy', 'DeletedDate', 'ClientAreas'], 'safe'],
        // ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
      return  [
                  'id' => Yii::t('app', 'ID'),
                  'client_code' => Yii::t('app', 'Code'),
                  'div_id' => Yii::t('app', 'Division'),
                  'alias' => Yii::t('app', 'Alias'),
                  'seq' => Yii::t('app', 'Client Number'),
                  'entity_id' => Yii::t('app', 'Entity'),
                  'client_name' => Yii::t('app', 'Name'),
                  'npwp' => Yii::t('app', 'Npwp'),
                  'latest_job' => Yii::t('app', 'Latest Job'),
                  'industry_id' => Yii::t('app', 'Industry'),
                  'client_areas' => Yii::t('app', 'Client Areas'),
                  'flag' => Yii::t('app', 'Status'),
                  'created_by' => Yii::t('app', 'Created By'),
                  'created_date' => Yii::t('app', 'Created Date'),
                  'last_update_by' => Yii::t('app', 'Last Update By'),
                  'last_update_date' => Yii::t('app', 'Last Update Date'),
                  'deleted_by' => Yii::t('app', 'Deleted By'),
                  'deleted_date' => Yii::t('app', 'Deleted Date'),
              ];

        // return [
        //     'Id' => Yii::t('app', 'ID'),
        //     'Code' => Yii::t('app', 'Code'),
        //     'divID' => Yii::t('app', 'Division'),
        //     'Alias' => Yii::t('app', 'Alias'),
        //     'Seq' => Yii::t('app', 'Client Number'),
        //     'Entity' => Yii::t('app', 'Entity'),
        //     'Name' => Yii::t('app', 'Name'),
        //     'NPWP' => Yii::t('app', 'Npwp'),
        //     'LatestJob' => Yii::t('app', 'Latest Job'),
        //     'Industry' => Yii::t('app', 'Industry'),
        //     'ClientAreas' => Yii::t('app', 'Client Areas'),
        //     'Flag' => Yii::t('app', 'Status'),
        //     'CreatedBy' => Yii::t('app', 'Created By'),
        //     'CreatedDate' => Yii::t('app', 'Created Date'),
        //     'LastUpdateBy' => Yii::t('app', 'Last Update By'),
        //     'LastUpdateDate' => Yii::t('app', 'Last Update Date'),
        //     'DeletedBy' => Yii::t('app', 'Deleted By'),
        //     'DeletedDate' => Yii::t('app', 'Deleted Date'),
        // ];
    }
    public function getIndustry()
    {
      return $this->hasOne(ClientIndustries::className(), ['id' => 'industry_id']);

        // return $this->hasOne(ClientIndustries::className(), ['Id' => 'Industry']);
    }
    public function getEntity()
    {
      return $this->hasOne(Entity::className(), ['id' => 'entity_id']);

        // return $this->hasOne(Entity::className(), ['Id' => 'Entity']);
    }
    public function getDivision()
    {
      return $this->hasOne(Division::className(), ['id' => 'div_id']);

        // return $this->hasOne(Division::className(), ['Id' => 'divID']);
    }

}
