<?php

namespace common\models\cl\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\cl\Client as ClientModel;

/**
 * Client represents the model behind the search form of `common\models\cl\Client`.
 */
class Client extends ClientModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
      return  [
                  [['id', 'seq', 'flag'], 'integer'],
                  [['client_code', 'entity_id','div_id', 'industry_id', 'client_name', 'NPWP',
                    'client_areas', 'created_by', 'created_date',
                    'last_update_by', 'last_update_date', 'deleted_by', 'deleted_date'], 'safe'],
              ];

        // return [
        //     [['Id', 'Seq', 'Flag'], 'integer'],
        //     [['Code', 'Entity','divID', 'Industry', 'Name', 'NPWP', 'ClientAreas', 'CreatedBy', 'CreatedDate', 'LastUpdateBy', 'LastUpdateDate', 'DeletedBy', 'DeletedDate'], 'safe'],
        // ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ClientModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'seq' => $this->seq,
            'entity_id' => $this->entity_id,
            'div_id' => $this->div_id,
            'flag' => $this->flag,
            'created_date' => $this->created_date,
            'last_update_date' => $this->last_update_date,
            'deleted_date' => $this->deleted_date,
        ]);

        $query->andFilterWhere(['like', 'client_code', $this->client_code])
            ->andFilterWhere(['like', 'entity_id', $this->entity_id])
            ->andFilterWhere(['like', 'client_name', $this->client_name])
            ->andFilterWhere(['like', 'npwp', $this->npwp])
            ->andFilterWhere(['like', 'latest_job', $this->latest_job])
            ->andFilterWhere(['like', 'industry_id', $this->industry_id])
            ->andFilterWhere(['like', 'client_areas', $this->client_areas])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'last_update_by', $this->last_update_by])
            ->andFilterWhere(['like', 'deleted_by', $this->deleted_by]);

        // $query->andFilterWhere([
        //     'Id' => $this->Id,
        //     'Seq' => $this->Seq,
        //     'Entity' => $this->Entity,
        //     'divID' => $this->divID,
        //     'Flag' => $this->Flag,
        //     'CreatedDate' => $this->CreatedDate,
        //     'LastUpdateDate' => $this->LastUpdateDate,
        //     'DeletedDate' => $this->DeletedDate,
        // ]);
        //
        // $query->andFilterWhere(['like', 'Code', $this->Code])
        //     ->andFilterWhere(['like', 'Entity', $this->Entity])
        //     ->andFilterWhere(['like', 'Name', $this->Name])
        //     ->andFilterWhere(['like', 'NPWP', $this->NPWP])
        //     ->andFilterWhere(['like', 'LatestJob', $this->LatestJob])
        //     ->andFilterWhere(['like', 'Industry', $this->Industry])
        //     ->andFilterWhere(['like', 'ClientAreas', $this->ClientAreas])
        //     ->andFilterWhere(['like', 'CreatedBy', $this->CreatedBy])
        //     ->andFilterWhere(['like', 'LastUpdateBy', $this->LastUpdateBy])
        //     ->andFilterWhere(['like', 'DeletedBy', $this->DeletedBy]);

        return $dataProvider;
    }
}
