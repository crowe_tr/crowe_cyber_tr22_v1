<?php

namespace common\models\cl;

use Yii;

/**
 * This is the model class for table "viewClientZone".
 *
 * @property string $termZoneName
 */
class viewClientZone extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client_zone';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['termZoneName'], 'required'],
            [['termZoneName'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'termZoneName' => 'Term Zone Name',
        ];
    }
}
