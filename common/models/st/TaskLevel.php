<?php

namespace common\models\st;

use Yii;

class TaskLevel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_level';
    }

}
