<?php

namespace common\models\st;

use Yii;

/**
 * This is the model class for table "stRulesLevel".
 *
 * @property int $id
 * @property int $rulesID
 * @property int $itemID
 * @property string $levelDescription
 *
 * @property StRuleItem $item
 * @property StRulesDetail $rules
 */
class RulesLevel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $TabularInput;
    public static function tableName()
    {
        return 'st_rules_level';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
      return  [
                  [['id', 'TabularInput', 'qty_claim', 'qty_claim_stayed', 'stayed_allowance'], 'safe'],
                  [['rules_id', 'item_id'], 'required'],
                  [['rules_id', 'item_id'], 'integer'],
                  [['level_description'], 'string', 'max' => 1000],
                  [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => RuleItem::className(), 'targetAttribute' => ['item_id' => 'id']],
                  [['rules_id'], 'exist', 'skipOnError' => true, 'targetClass' => RulesDetail::className(), 'targetAttribute' => ['rules_id' => 'id']],
              ];

        // return [
        //     [['id', 'TabularInput', 'qtyClaim', 'qtyClaimStayed', 'stayedAllowance'], 'safe'],
        //     [['rulesID', 'itemID'], 'required'],
        //     [['rulesID', 'itemID'], 'integer'],
        //     [['levelDescription'], 'string', 'max' => 1000],
        //     [['itemID'], 'exist', 'skipOnError' => true, 'targetClass' => RuleItem::className(), 'targetAttribute' => ['itemID' => 'id']],
        //     [['rulesID'], 'exist', 'skipOnError' => true, 'targetClass' => RulesDetail::className(), 'targetAttribute' => ['rulesID' => 'id']],
        // ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
      return  [
                  'id' => Yii::t('app', 'ID'),
                  'rules_id' => Yii::t('app', 'Rules ID'),
                  'item_id' => Yii::t('app', 'Item ID'),
                  'level_description' => Yii::t('app', 'Applied To'),
              ];

        // return [
        //     'id' => Yii::t('app', 'ID'),
        //     'rulesID' => Yii::t('app', 'Rules ID'),
        //     'itemID' => Yii::t('app', 'Item ID'),
        //     'levelDescription' => Yii::t('app', 'Applied To'),
        // ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRuleItem()
    {
      return $this->hasOne(RuleItem::className(), ['id' => 'item_id']);

        // return $this->hasOne(RuleItem::className(), ['id' => 'itemID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRulesDetail()
    {
      return $this->hasOne(RulesDetail::className(), ['id' => 'rules_id']);

        // return $this->hasOne(RulesDetail::className(), ['id' => 'rulesID']);
    }
}
