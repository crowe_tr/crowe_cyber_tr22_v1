<?php

namespace common\models\st;

use Yii;

/**
 * This is the model class for table "stRuleItem".
 *
 * @property int $id
 * @property string $termItemName
 * @property string $termItemDesc
 * @property string $termZoneName
 *
 * @property StRulesDetail[] $stRulesDetails
 * @property StRulesLevel[] $stRulesLevels
 */
class RuleItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'st_rule_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
      return  [
                  [['id','is_client_zone'], 'safe'],

                  [['term_item_name', 'term_item_desc', 'term_zone_name'], 'required'],
                  [['term_item_name'], 'string', 'max' => 80],
                  [['term_item_desc'], 'string', 'max' => 500],
                  [['term_item_name'], 'unique'],
              ];

        // return [
        //     [['id','isClientZone'], 'safe'],
        //
        //     [['termItemName', 'termItemDesc', 'termZoneName'], 'required'],
        //     [['termItemName'], 'string', 'max' => 80],
        //     [['termItemDesc'], 'string', 'max' => 500],
        //     [['termItemName'], 'unique'],
        // ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
      return  [
                  'id' => Yii::t('app', 'ID'),
                  'term_item_name' => Yii::t('app', 'Name'),
                  'term_item_desc' => Yii::t('app', 'Description'),
                  'term_zone_name' => Yii::t('app', 'Zone Name'),
                  'is_client_zone'=>'Client&Job Used'
              ];

        // return [
        //     'id' => Yii::t('app', 'ID'),
        //     'termItemName' => Yii::t('app', 'Name'),
        //     'termItemDesc' => Yii::t('app', 'Description'),
        //     'termZoneName' => Yii::t('app', 'Zone Name'),
        //     'isClientZone'=>'Client&Job Used'
        // ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRulesDetails()
    {
      return $this->hasMany(RulesDetail::className(), ['item_id' => 'id']);

        // return $this->hasMany(RulesDetail::className(), ['itemID' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRulesLevels()
    {
      return $this->hasMany(RulesLevel::className(), ['item_id' => 'id']);

        // return $this->hasMany(RulesLevel::className(), ['itemID' => 'id']);
    }
}
