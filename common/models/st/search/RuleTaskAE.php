<?php

namespace common\models\st\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\st\RuleTaskAE as RuleTaskAEModel;

/**
 * RuleTaskAE represents the model behind the search form of `common\models\st\RuleTaskAE`.
 */
class RuleTaskAE extends RuleTaskAEModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
      return  [
                  [['level_id', 'task_id', 'percentage'], 'integer'],
              ];

        // return [
        //     [['levelID', 'taskID', 'percentage'], 'integer'],
        // ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RuleTaskAEModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'level_id' => $this->level_id,
            'task_id' => $this->task_id,
            'percentage' => $this->percentage,
        ]);

        // $query->andFilterWhere([
        //     'levelID' => $this->levelID,
        //     'taskID' => $this->taskID,
        //     'percentage' => $this->percentage,
        // ]);

        return $dataProvider;
    }
}
