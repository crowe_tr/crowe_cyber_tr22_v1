<?php

namespace common\models\st\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\st\HolidayType as HolidayTypeModel;

/**
 * HolidayType represents the model behind the search form of `common\models\st\HolidayType`.
 */
class HolidayType extends HolidayTypeModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
      return  [
                  [['id', 'flag', 'is_holiday'], 'integer'],
                  [['holiday_type_name'], 'safe'],
              ];

        // return [
        //     [['id', 'flag'], 'integer'],
        //     [['holidayTypeName'], 'safe'],
        // ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HolidayTypeModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'flag' => $this->flag,
        ]);

        $query->andFilterWhere(['like', 'holiday_type_name', $this->holiday_type_name]);
        $query->andFilterWhere(['like', 'is_holiday', $this->is_holiday]);

        // $query->andFilterWhere(['like', 'holidayTypeName', $this->holidayTypeName]);

        return $dataProvider;
    }
}
