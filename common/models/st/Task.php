<?php

namespace common\models\st;

use Yii;

/**
 * This is the model class for table "stTask".
 *
 * @property int $id
 * @property int $seq
 * @property string $taskName
 * @property int $taskTypeID
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'st_task';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
      return  [
                  [['id'], 'safe'],
                  [['emp_dept', 'emp_division', 'emp_level', 'manager_id', 'supervisor_id'], 'safe'],
                  [['seq', 'task_type_id', 'task_name'], 'required'],
                  [['seq', 'task_type_id'], 'integer'],
                  [['task_name'], 'string', 'max' => 60],
                  [['task_type_id', 'task_name'], 'unique', 'targetAttribute' => ['task_type_id', 'task_name']],
              ];

        // return [
        //     [['id'], 'safe'],
        //     [['empDept', 'empDivision', 'empLevel', 'Manager', 'Supervisor'], 'safe'],
        //     [['seq', 'taskTypeID', 'taskName'], 'required'],
        //     [['seq', 'taskTypeID'], 'integer'],
        //     [['taskName'], 'string', 'max' => 60],
        //     [['taskTypeID', 'taskName'], 'unique', 'targetAttribute' => ['taskTypeID', 'taskName']],
        // ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
      return  [
                  'id' => Yii::t('app', 'ID'),
                  'seq' => Yii::t('app', 'Seq'),
                  'task_name' => Yii::t('app', 'Task Name'),
                  'task_type_id' => Yii::t('app', 'Type'),
                  'emp_dept' => 'Departement',
                  'emp_division' => 'Division',
                  'emp_level' => 'Level',
                  'manager_id' => 'Manager',
                  'supervisor_id' => 'Supervisor',
              ];

        // return [
        //     'id' => Yii::t('app', 'ID'),
        //     'seq' => Yii::t('app', 'Seq'),
        //     'taskName' => Yii::t('app', 'Task Name'),
        //     'taskTypeID' => Yii::t('app', 'Type'),
        //     'empDept' => 'Departement',
        //     'empDivision' => 'Division',
        //     'empLevel' => 'Level',
        //
        // ];
    }
    public function getType()
    {
      return $this->hasOne(TaskType::className(), ['id' => 'task_type_id']);

        // return $this->hasOne(TaskType::className(), ['id' => 'taskTypeID']);
    }
}
