<?php

namespace common\models\st;

use Yii;

/**
 * This is the model class for table "stTaskType".
 *
 * @property int $id
 * @property string $taskTypeName
 * @property int $flag
 */
class TaskType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'st_task_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
      return  [
                  [['id','is_meal','is_ope','is_taxi','used_project','emp_dept', 'emp_division'], 'safe'],
                  [['task_type_name'], 'required'],
                  [['flag'], 'integer'],
                  [['task_type_name'], 'string', 'max' => 30],
                  [['task_type_name'], 'unique'],
              ];

        // return [
        //     [['id','isMeal','isOPE','isTaxi','usedProject', 'empDept', 'empDivision'], 'safe'],
        //     [['taskTypeName'], 'required'],
        //     [['flag'], 'integer'],
        //     [['taskTypeName'], 'string', 'max' => 30],
        //     [['taskTypeName'], 'unique'],
        // ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
      return  [
                  'id' => Yii::t('app', 'ID'),
                  'task_type_name' => Yii::t('app', 'Type Name'),
                  'flag' => Yii::t('app', 'Status'),
                  'is_meal' => Yii::t('app', 'Meal'),
                  'is_ope' => Yii::t('app', 'OPE'),
                  'is_taxi' => Yii::t('app', 'Taxi'),
                  'used_project'=> 'Project',
                  'emp_dept'=> 'Departement',
                  'emp_division'=> 'Division',
              ];

        // return [
        //     'id' => Yii::t('app', 'ID'),
        //     'taskTypeName' => Yii::t('app', 'Type Name'),
        //     'flag' => Yii::t('app', 'Status'),
        //     'isMeal' => Yii::t('app', 'Meal'),
        //     'isOPE' => Yii::t('app', 'OPE'),
        //     'isTaxi' => Yii::t('app', 'Taxi'),
        //     'usedProject'=> 'Project',
        // ];
    }

    public function getTask()
    {
      return $this->hasMany(Task::className(), ['task_type_id' => 'id']);

        // return $this->hasMany(Task::className(), ['taskTypeID' => 'id']);
    }
}
