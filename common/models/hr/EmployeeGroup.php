<?php

namespace common\models\hr;

use Yii;

class EmployeeGroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employee_group';
    }

}
