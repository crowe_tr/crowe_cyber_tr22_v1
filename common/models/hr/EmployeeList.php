<?php

namespace common\models\hr;

use Yii;

/**
 * This is the model class for table "list_employee".
 *
 * @property string $employee_id
 * @property string $employee_name
 * @property string $employee_email
 * @property int $employee_status
 * @property string $employee_desc
 * @property string $employee_initial
 * @property string $gender
 * @property int $max_view_tr
 * @property int $level_id
 * @property string $level_code
 * @property string $level_name
 * @property int $level_auto
 * @property string $level_desc
 * @property int $entity_id
 * @property string $entity_code
 * @property string $entity_name
 * @property string $entity_desc
 * @property int $division_id
 * @property string $division_code
 * @property string $division_name
 * @property string $division_desc
 * @property int $dept_id
 * @property string $dept_code
 * @property string $dept_name
 * @property string $dept_desc
 * @property string $group_id
 * @property string $group_name
 * @property string $group_initial
 * @property string $group_desc
 * @property string $manager_id
 * @property string $manager_name
 * @property string $manager_initial
 * @property string $manager_email
 * @property string $spv_id
 * @property string $spv_name
 * @property string $spv_initial
 * @property string $spv_email
 */
class EmployeeList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employee_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['employee_id', 'employee_name', 'level_code', 'level_name', 'entity_code', 'entity_name', 'div_code', 'div_name'], 'required'],
            [['employee_status', 'max_view_tr', 'level_id', 'level_auto', 'entity_id', 'div_id', 'dept_id'], 'integer'],
            [['employee_id', 'group_id', 'manager_id', 'spv_id'], 'string', 'max' => 32],
            [['employee_name', 'dept_name', 'group_name', 'manager_name', 'spv_name'], 'string', 'max' => 128],
            [['employee_email', 'manager_email', 'spv_email'], 'string', 'max' => 64],
            [['employee_desc', 'group_desc'], 'string', 'max' => 211],
            [['employee_initial', 'div_code', 'group_initial', 'manager_initial', 'spv_initial'], 'string', 'max' => 4],
            [['gender'], 'string', 'max' => 1],
            [['level_code'], 'string', 'max' => 3],
            [['level_name'], 'string', 'max' => 45],
            [['level_desc'], 'string', 'max' => 51],
            [['entity_code', 'dept_code'], 'string', 'max' => 6],
            [['entity_name'], 'string', 'max' => 120],
            [['entity_desc'], 'string', 'max' => 129],
            [['div_name'], 'string', 'max' => 50],
            [['div_desc'], 'string', 'max' => 57],
            [['dept_desc'], 'string', 'max' => 137],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'employee_id' => 'Employee ID',
            'employee_name' => 'Employee Name',
            'employee_email' => 'Employee Email',
            'employee_status' => 'Employee Status',
            'employee_desc' => 'Employee Desc',
            'employee_initial' => 'Employee Initial',
            'gender' => 'Gender',
            'max_view_tr' => 'Max View Tr',
            'level_id' => 'Level ID',
            'level_code' => 'Level Code',
            'level_name' => 'Level Name',
            'level_auto' => 'Level Auto',
            'level_desc' => 'Level Desc',
            'entity_id' => 'Entity ID',
            'entity_code' => 'Entity Code',
            'entity_name' => 'Entity Name',
            'entity_desc' => 'Entity Desc',
            'div_id' => 'Division ID',
            'div_code' => 'Division Code',
            'div_name' => 'Division Name',
            'div_desc' => 'Division Desc',
            'dept_id' => 'Dept ID',
            'dept_code' => 'Dept Code',
            'dept_name' => 'Dept Name',
            'dept_desc' => 'Dept Desc',
            'group_id' => 'Group ID',
            'group_name' => 'Group Name',
            'group_initial' => 'Group Initial',
            'group_desc' => 'Group Desc',
            'manager_id' => 'Manager ID',
            'manager_name' => 'Manager Name',
            'manager_initial' => 'Manager Initial',
            'manager_email' => 'Manager Email',
            'spv_id' => 'Spv ID',
            'spv_name' => 'Spv Name',
            'spv_initial' => 'Spv Initial',
            'spv_email' => 'Spv Email',
        ];
    }
}
