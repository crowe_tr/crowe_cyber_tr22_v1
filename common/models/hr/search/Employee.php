<?php

namespace common\models\hr\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\hr\Employee as EmployeeModel;

class Employee extends EmployeeModel
{

    public function rules()
    {
      return  [
                  [['user_name', 'password_hash', 'auth_kkey', 'account_activation_token', 'user_email', 'user_id',
                    'full_name', 'initial', 'gender', 'place_of_birth', 'date_of_birth', 'join_date', 'education',
                    'certificate', 'religion', 'marital_status', 'nationality', 'resign_date',
                    'resign_description', 'address', 'postal_code', 'mobile_phone', 'photo', 'created_by',
                    'created_date', 'last_update_by', 'last_update_date', 'deleted_by', 'deleted_date',
                    'parent_id'], 'safe'],
                  [['password_reset_token', 'flag', 'entity_id', 'level_id', 'div_id', 'dept_id',
                    'keep_vacation_balance', 'annual_vacation'], 'integer'],
              ];

    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = EmployeeModel::find();
        $query->where("hr_employee.user_id<>'0'");

        // $query->where("hr_employee.Id<>'0'");
        $query->joinWith("level");
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'hr_employee.password_reset_token' => $this->password_reset_token,
            'hr_employee.flag' => $this->flag,
            'hr_employee.date_of_birth' => $this->date_of_birth,
            'hr_employee.join_date' => $this->join_date,
            'hr_employee.entity_id' => $this->entity_id,
            // 'hr_employee.level_id' => $this->level_id,
            'hr_employee.div_id' => $this->div_id,
            'hr_employee.dept_id' => $this->dept_id,
            'hr_employee.resign_date' => $this->resign_date,
            'hr_employee.keep_vacation_balance' => $this->keep_vacation_balance,
            'hr_employee.annual_vacation' => $this->annual_vacation,
            'hr_employee.created_date' => $this->created_date,
            'hr_employee.last_update_date' => $this->last_update_date,
            'hr_employee.deleted_date' => $this->deleted_date,
        ]);

        // $query->andFilterWhere([
        //     'hr_employee.PasswordResetToken' => $this->PasswordResetToken,
        //     'hr_employee.flag' => $this->flag,
        //     'hr_employee.dateOfBirth' => $this->dateOfBirth,
        //     'hr_employee.joinDate' => $this->joinDate,
        //     'hr_employee.entityId' => $this->entityId,
        //     'hr_employee.divisionID' => $this->divisionID,
        //     'hr_employee.deptID' => $this->deptID,
        //     'hr_employee.resignDate' => $this->resignDate,
        //     'hr_employee.keepVacationBalance' => $this->keepVacationBalance,
        //     'hr_employee.annualVacation' => $this->annualVacation,
        //     'hr_employee.CreatedDate' => $this->CreatedDate,
        //     'hr_employee.LastUpdateDate' => $this->LastUpdateDate,
        //     'hr_employee.DeletedDate' => $this->DeletedDate,
        // ]);
        //

        $query->andFilterWhere(['like', 'hr_employee.user_name', $this->user_name])
            ->andFilterWhere(['like', 'hr_employee.password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'hr_employee.auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'hr_employee.account_activation_token', $this->account_activation_token])
            ->andFilterWhere(['like', 'hr_employee.user_email', $this->user_email])
            ->andFilterWhere(['like', 'hr_employee.user_id', $this->user_id])
            ->andFilterWhere(['like', 'hr_employee.full_name', $this->full_name])
            ->andFilterWhere(['like', 'hr_employee.initial', $this->initial])
            ->andFilterWhere(['like', 'hr_employee.gender', $this->gender])
            ->andFilterWhere(['like', 'hr_employee.place_of_birth', $this->place_of_birth])
            ->andFilterWhere(['like', 'hr_employee.education', $this->education])
            ->andFilterWhere(['like', 'hr_employee.certificate', $this->certificate])
            ->andFilterWhere(['like', 'hr_employee.religion', $this->religion])
            ->andFilterWhere(['like', 'hr_employee.marital_status', $this->marital_status])
            ->andFilterWhere(['like', 'hr_employee.nationality', $this->nationality])
            ->andFilterWhere(['like', 'hr_employee.resign_description', $this->resign_description])
            ->andFilterWhere(['like', 'hr_employee.address', $this->address])
            ->andFilterWhere(['like', 'hr_employee.postal_code', $this->postal_code])
            ->andFilterWhere(['like', 'hr_employee.mobile_phone', $this->mobile_phone])
            ->andFilterWhere(['like', 'hr_employee.photo', $this->photo])
            ->andFilterWhere(['like', 'hr_employee.created_by', $this->created_by])
            ->andFilterWhere(['like', 'hr_employee.last_update_by', $this->last_update_by])
            ->andFilterWhere(['like', 'hr_employee.deleted_by', $this->deleted_by])
            ->andFilterWhere(['like', 'hr_employee.parent_id', $this->parent_id])
            ->andFilterWhere(['like', 'cm_level.level_name', $this->level_id]);;


        // $query->andFilterWhere(['like', 'hr_employee.Username', $this->Username])
        //     ->andFilterWhere(['like', 'hr_employee.passwordHash', $this->passwordHash])
        //     ->andFilterWhere(['like', 'hr_employee.AuthKey', $this->AuthKey])
        //     ->andFilterWhere(['like', 'hr_employee.AccountActivationToken', $this->AccountActivationToken])
        //     ->andFilterWhere(['like', 'hr_employee.email', $this->email])
        //     ->andFilterWhere(['like', 'hr_employee.Id', $this->Id])
        //     ->andFilterWhere(['like', 'hr_employee.fullName', $this->fullName])
        //     ->andFilterWhere(['like', 'hr_employee.initial', $this->initial])
        //     ->andFilterWhere(['like', 'hr_employee.gender', $this->gender])
        //     ->andFilterWhere(['like', 'hr_employee.placeOfBirth', $this->placeOfBirth])
        //     ->andFilterWhere(['like', 'hr_employee.education', $this->education])
        //     ->andFilterWhere(['like', 'hr_employee.certificate', $this->certificate])
        //     ->andFilterWhere(['like', 'hr_employee.religion', $this->religion])
        //     ->andFilterWhere(['like', 'hr_employee.maritalStatus', $this->maritalStatus])
        //     ->andFilterWhere(['like', 'hr_employee.nationality', $this->nationality])
        //     ->andFilterWhere(['like', 'hr_employee.resignDescription', $this->resignDescription])
        //     ->andFilterWhere(['like', 'hr_employee.address', $this->address])
        //     ->andFilterWhere(['like', 'hr_employee.postalCode', $this->postalCode])
        //     ->andFilterWhere(['like', 'hr_employee.mobilePhone', $this->mobilePhone])
        //     ->andFilterWhere(['like', 'hr_employee.photo', $this->photo])
        //     ->andFilterWhere(['like', 'hr_employee.CreatedBy', $this->CreatedBy])
        //     ->andFilterWhere(['like', 'hr_employee.LastUpdateBy', $this->LastUpdateBy])
        //     ->andFilterWhere(['like', 'hr_employee.DeletedBy', $this->DeletedBy])
        //     ->andFilterWhere(['like', 'hr_employee.parentID', $this->parentID])
        //     ->andFilterWhere(['like', 'cm_level.levelName', $this->levelID]);

        return $dataProvider;
    }
}
