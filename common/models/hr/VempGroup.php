<?php

namespace common\models\hr;

use Yii;
use common\models\cm\Level;


/**
 * This is the model class for table "vempGroup".
 *
 * @property string $id
 * @property string $fullName
 * @property int $entityID
 * @property int $divisionID
 */
class VempGroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employee_group';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'full_name'], 'required'],
            [['entity_id', 'div_id', 'level_id'], 'integer'],
            [['user_id'], 'string', 'max' => 32],
            [['full_name', 'level_name'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'ID',
            'full_name' => 'Full Name',
            'entity_id' => 'Entity ID',
            'div_id' => 'Division ID',
            'level_id' => 'level ID',
            'level_name' => 'level Name',
        ];
    }

    public function getLevel()
    {
      return $this->hasOne(Level::className(), ['id' => 'level_id']);
    }
}
