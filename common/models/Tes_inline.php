<?php

namespace common\models;

use yii\db\ActiveRecord;

class Tes_inline extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%a_tes_inline}}';
    }

    public function rules()
    {
        return [
            [['code', 'field_name'], 'safe'],
        ];
    }
}
