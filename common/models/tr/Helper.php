<?php

namespace common\models\tr;

use Yii;
use yii\base\Model;

/** Dummy Model untuk kepentingan reporting atau apapun yang gak perlu ke tabel **/
class Helper extends Model
{
    public $employee_id;
    public $tr_date;

    public function rules()
    {
      return [

          [['employee_id', 'tr_date'], 'required', 'on'=> 'open_tr'],
      ];

        // return [
        //
        //     [['EmployeeId', 'Date'], 'required', 'on'=> 'open_tr'],
        // ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
      return [
          'employee_id' => Yii::t('app', 'Employee'),
          'tr_date' => Yii::t('app', 'Date'),
      ];

        // return [
        //     'EmployeeId' => Yii::t('app', 'Employee'),
        //     'Date' => Yii::t('app', 'Date'),
        // ];
    }

}
