<?php

namespace common\models\tr;

use Yii;
use common\models\cm\Taxi;
use common\models\st\Task;
use common\models\st\TaskType;

/**
 * This is the model class for table "trTimeReportTaxi".
 *
 * @property int $TimeReportID
 * @property int $Seq
 * @property int $cmTaxiID
 * @property int $TaskTypeID
 * @property int $JobId
 * @property int $TaskID
 * @property string $Start
 * @property string $Finish
 * @property string $VoucherNo
 * @property string $Destination
 * @property string $Amount
 * @property string $Description
 *
 * @property Taxi $cmTaxi
 * @property Job $job
 * @property Task $task
 * @property TaskType $taskType
 * @property TimeReport $timeReport
 */
class TimeReportTaxi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tr_time_report_taxi';
    }
    public $EmployeeId;
    public $Date;
    public $isStayed;


    public $Taxi;
    public $TaxiProvided;
    public $TaxiAllocation;
    public $TaxiClaimQty;
    public $TaxiClaimQtyStayed;
    public $TaxiMinOvertime;
    public $TaxiMinClock;
    public $TaxiStart;
    public $TaxiFinish;


    public $lookup;

    public function rules()
    {
        return [
            // [['EmployeeId', 'Date', 'isStayed', 'tr_det_id'], 'safe'],
            [['tr_det_id'], 'safe'],

            [['seq'], 'safe'],
            [['time_report_id', 'taxi_id', 'taxi_start', 'taxi_finish', 'voucher_no', 'destination', 'amount'], 'required'],
            [['time_report_id', 'seq', 'taxi_id'], 'integer'],

            //[['Start', 'Finish'], 'date', 'dateFormat' => 'H:i:s'],
            [['description'], 'string'],
            [['voucher_no'], 'string', 'max' => 30],
            [['destination'], 'string', 'max' => 100],
            [['time_report_id', 'seq'], 'unique', 'targetAttribute' => ['time_report_id', 'seq']],
            [['taxi_id'], 'exist', 'skipOnError' => true, 'targetClass' => Taxi::className(), 'targetAttribute' => ['taxi_id' => 'id']],
            [['time_report_id'], 'exist', 'skipOnError' => true, 'targetClass' => TimeReport::className(), 'targetAttribute' => ['time_report_id' => 'id']],

            //['Start', 'compare', 'compareAttribute' => 'Finish', 'operator' => '<'],
            [['tr_det_id'], 'required'],
            /*
            [['TrDetID'], 'required'
            , 'when'=>function($model) {
                $return = false;

                if($model->cmTaxiID == 5){
                    $return = true;
                }
                return $return;
            }, 'enableClientValidation' => false],
            */

            [['description'], 'required'
            , 'when'=>function($model) {
                $return = false;

                if($model->taxi_id == 6){
                    $return = true;
                }
                return $return;
            }, 'enableClientValidation' => false],

            [['job_id'], 'required'
            , 'when'=>function($model) {
                $return = false;

                if($model->task_type_id == 1){
                    $return = true;
                }
                return $return;
            }, 'enableClientValidation' => false],
            [['approval1','approval2'], 'safe'],
            [['tr_date','employee_id'], 'safe'],

            // [['Taxi', 'TaxiProvided', 'TaxiAllocation','TaxiClaimQty', 'TaxiClaimQtyStayed', 'TaxiMinOvertime', 'TaxiMinClock', 'TaxiStart', 'TaxiEnd'], 'safe'],
            //[['Taxi'], 'validateTaxi', 'enableClientValidation' => false],

        ];
    }
    public function validateTaxi($attribute_name, $params){
        $valid = true;
        if($this->Taxi > 0){
            if($this->TaxiProvided == true){
                $ClaimQty = ($this->isStayed == 0) ? $this->TaxiClaimQty : $this->TaxiClaimQtyStayed;
                if ($ClaimQty >= $this->TaxiAllocation){
                    if($this->TaxiMinOvertime <= $this->OvertimeAllocation) {
                        if($this->TaxiMinClock <= $this->TaxiStart){
                            $this->addError($attribute_name, "Total Minimal Jam Taxi tidak memenuhi syarat untuk claim taxi, minimal Time :".$this->TaxiMinClock);
                            $valid = false;
                        }
                    } else {
                        $this->addError($attribute_name, "Total Overtime tidak memenuhi syarat untuk claim taxi, minimal Overtime : ".$this->TaxiMinOvertime);
                        $valid = false;
                    }
                }else{
                    $this->addError($attribute_name, "Batas claim Taxi sudah mencapai batas, hak claim taxi : ".$ClaimQty);
                    $valid = false;
                }
            }else{
                $this->addError($attribute_name, "Anda tidak bisa claim Taxi");
                $valid = false;
            }
        }
        return $valid;
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'time_report_id' => Yii::t('app', 'Time Report ID'),
            'seq' => Yii::t('app', 'Seq'),
            'taxi_id' => Yii::t('app', 'Taxi'),
            'task_type_id' => Yii::t('app', 'Class / Task Type'),
            'job_id' => Yii::t('app', 'Job'),
            'task_id' => Yii::t('app', 'Task'),
            'taxi_start' => Yii::t('app', 'Start'),
            'taxi_finish' => Yii::t('app', 'Finish'),
            'voucher_no' => Yii::t('app', 'Voucher No'),
            'destination' => Yii::t('app', 'Destination'),
            'amount' => Yii::t('app', 'Amount'),
            'description' => Yii::t('app', 'Description'),
            'tr_det_id' => 'Task Description',

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxi()
    {
        return $this->hasOne(Taxi::className(), ['id' => 'taxi_id']);
    }

    public function getJob()
    {
        return $this->hasOne(Job::className(), ['id' => 'job_id']);
    }
    public function getTasktype()
    {
        return $this->hasOne(TaskType::className(), ['id' => 'task_type_id']);
    }
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimeReport()
    {
        return $this->hasOne(TimeReport::className(), ['id' => 'time_report_id']);
    }
}
