<?php

namespace common\models\tr;

use Yii;

/**
 * This is the model class for table "vJob".
 *
 * @property int $JobID
 * @property string $JobCode
 * @property string $Description
 * @property int $ClientID
 * @property string $JobArea
 * @property string $JobCreated
 * @property string $StartDate
 * @property string $EndDate
 * @property string $Partner
 * @property string $Manager
 * @property string $Supervisor
 * @property int $IncludeOPE
 * @property int $IsMeal
 * @property int $IsOutOfOffice
 * @property int $IsTaxi
 * @property string $MealAllowance
 * @property string $MealAllowanceAct
 * @property string $OutOfOfficeAllowance
 * @property string $OutOfOfficeAllowanceAct
 * @property string $TaxiAllowance
 * @property string $TaxiAllowanceAct
 * @property string $AdministrativeCharge
 * @property string $AdministrativeChargeAct
 * @property string $OtherExpenseAllowance
 * @property string $OtherExpenseAllowanceAct
 * @property string $Fee
 * @property string $FeeAct
 * @property string $TimeCharges
 * @property string $TimeChargesAct
 * @property string $Percentage
 * @property string $PercentageAct
 * @property int $Status 0:Draff; 1:Submit; 2:Approve; 3:Reject; 4:Close; 5:Finish
 * @property int $Flag 0:Nothing; 1:Draff Revise; 2:Submit Revise; 3:Approve Revise; 4:Reject Revise
 * @property string $CreatedBy
 * @property string $CreatedAt
 * @property string $UpdateBy
 * @property string $UpdateAt
 * @property string $Client
 * @property string $Entity
 * @property string $Division
 * @property string $PartnerName
 * @property string $ManagerName
 * @property string $SupervisorName
 * @property string $status_job
 */
class VJob extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'job_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['JobID', 'ClientID', 'IncludeOPE', 'IsMeal', 'IsOutOfOffice', 'IsTaxi', 'Status', 'Flag'], 'integer'],
            [['Description', 'ClientID', 'StartDate', 'EndDate'], 'required'],
            [['JobCreated', 'StartDate', 'EndDate', 'CreatedAt', 'UpdateAt'], 'safe'],
            [['MealAllowance', 'MealAllowanceAct', 'OutOfOfficeAllowance', 'OutOfOfficeAllowanceAct', 'TaxiAllowance', 'TaxiAllowanceAct', 'AdministrativeCharge', 'AdministrativeChargeAct', 'OtherExpenseAllowance', 'OtherExpenseAllowanceAct', 'Fee', 'FeeAct', 'TimeCharges', 'TimeChargesAct'], 'number'],
            [['JobCode'], 'string', 'max' => 16],
            [['Description', 'JobArea', 'status_job'], 'string', 'max' => 255],
            [['Partner', 'Manager', 'Supervisor', 'Percentage', 'PercentageAct', 'CreatedBy', 'UpdateBy'], 'string', 'max' => 32],
            [['Client'], 'string', 'max' => 100],
            [['Entity'], 'string', 'max' => 120],
            [['Division'], 'string', 'max' => 50],
            [['PartnerName', 'ManagerName', 'SupervisorName'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'JobID' => 'Job ID',
            'JobCode' => 'Job Code',
            'Description' => 'Description',
            'ClientID' => 'Client ID',
            'JobArea' => 'Job Area',
            'JobCreated' => 'Job Created',
            'StartDate' => 'Start Date',
            'EndDate' => 'End Date',
            'Partner' => 'Partner',
            'Manager' => 'Manager',
            'Supervisor' => 'Supervisor',
            'IncludeOPE' => 'Include Ope',
            'IsMeal' => 'Is Meal',
            'IsOutOfOffice' => 'Is Out Of Office',
            'IsTaxi' => 'Is Taxi',
            'MealAllowance' => 'Meal Allowance',
            'MealAllowanceAct' => 'Meal Allowance Act',
            'OutOfOfficeAllowance' => 'Out Of Office Allowance',
            'OutOfOfficeAllowanceAct' => 'Out Of Office Allowance Act',
            'TaxiAllowance' => 'Taxi Allowance',
            'TaxiAllowanceAct' => 'Taxi Allowance Act',
            'AdministrativeCharge' => 'Administrative Charge',
            'AdministrativeChargeAct' => 'Administrative Charge Act',
            'OtherExpenseAllowance' => 'Other Expense Allowance',
            'OtherExpenseAllowanceAct' => 'Other Expense Allowance Act',
            'Fee' => 'Fee',
            'FeeAct' => 'Fee Act',
            'TimeCharges' => 'Time Charges',
            'TimeChargesAct' => 'Time Charges Act',
            'Percentage' => 'Percentage',
            'PercentageAct' => 'Percentage Act',
            'Status' => 'Status',
            'Flag' => 'Flag',
            'CreatedBy' => 'Created By',
            'CreatedAt' => 'Created At',
            'UpdateBy' => 'Update By',
            'UpdateAt' => 'Update At',
            'Client' => 'Client',
            'Entity' => 'Entity',
            'Division' => 'Division',
            'PartnerName' => 'Partner Name',
            'ManagerName' => 'Manager Name',
            'SupervisorName' => 'Supervisor Name',
            'status_job' => 'Status Job',
        ];
    }
}
