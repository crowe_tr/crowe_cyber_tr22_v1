<?php

namespace common\models\tr;

use Yii;
use yii\helpers\Html;
use yii\db\ActiveRecord;
use common\components\CommonHelper;
use common\models\cl\Client;
use common\models\cm\Dept;
use common\models\tr\ms\Entity;
use common\models\hr\Employee;

class Job extends \yii\db\ActiveRecord
{

    const StatusSubmit = 1;
    const StatusApproved = 2;
    const StatusOnProgress = 21;
    const StatusDenied = 3;
    const StatusCanceled = 4;
    const StatusSuccess = 22;


    const StatusMyPending = 0;

    public $Qty;
    public $Subtotal;
    public $Discount;
    public $Total;
    public $VAT;
    public $Grandtotal;

    public $QtyPending;
    public $QtyApproved;
    public $QtyDenie;
    public $GrandtotalPending;
    public $GrandtotalApproved;
    public $GrandtotalDenie;

    public $_Total;
    public $_TotalDraft;
    public $_TotalSubmit;
    public $_TotalMyPending;
    public $_TotalApproved;
    public $_TotalDenied;
    public $_TotalOnProgress;
    public $_TotalCanceled;
    public $_TotalSuccess;
    // public $TimeCharges;
    public $RecoveryRate;




    public $Level;
    public $isMyOnlyRequest;

    public static function tableName()
    {
        return 'tr_job';
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
      return  [
                [['description', 'client_id', 'start_date', 'end_date', 'job_fee'], 'required'],
                [['time_charges_actual', 'meal_allowance_actual', 'taxi_allowance_actual', 'ope_allowance_actual',
                  'administrative_charge_actual', 'other_expense_allowance_actual', 'percentage_actual'], 'safe'],
                [['id', 'include_ope', 'is_meal', 'is_taxi', 'is_ope', 'job_status'], 'integer'],
                [['job_code', 'partner_id', 'manager_id', 'supervisor_id', 'created_by', 'updated_by'], 'string', 'max' => 16],
                [['is_meal', 'is_taxi', 'is_ope', 'job_created', 'start_date', 'end_date',
                  'created_at', 'updated_at', 'Total', 'taxi_allowance',
                  'other_expense_allowance', 'meal_allowance', 'ope_allowance', 'time_charges',
                  'recovery_rate', 'job_area', 'administrative_charge', 'client_id'], 'safe'],
                [['description'], 'string', 'max' => 128],
                [['id', 'job_code'], 'unique'],

                [['job_code'], 'required', 'on' => 'update_code'],
                [['job_code', 'manager_id', 'description', 'client_id', 'start_date', 'end_date', 'job_fee'], 'required', 'on' => 'create'],
                [['job_code', 'partner_id', 'manager_id', 'supervisor_id', 'job_area'], 'required', 'on' => 'update'],
              ];

        // return [
        //     [['Description', 'ClientID', 'StartDate', 'EndDate', 'Fee'], 'required'],
        //     [['TimeChargesAct', 'MealAllowanceAct', 'TaxiAllowanceAct', 'OutOfOfficeAllowanceAct', 'AdministrativeChargeAct', 'OtherExpenseAllowanceAct', 'PercentageAct'], 'safe'],
        //     [['JobID', 'IncludeOPE', 'IsMeal', 'IsTaxi', 'IsOutOfOffice', 'Status'], 'integer'],
        //     [['JobCode', 'Partner', 'Manager', 'Supervisor', 'CreatedBy', 'UpdateBy'], 'string', 'max' => 16],
        //     [['IsMeal', 'IsTaxi', 'IsOutOfOffice', 'JobCreated', 'StartDate', 'EndDate', 'CreatedAt', 'UpdateAt', 'Total', 'MealAllowance', 'TaxiAllowance', 'OtherExpenseAllowance', 'OutOfOfficeAllowance', 'TimeCharges', 'RecoveryRate', 'JobArea', 'AdministrativeCharge', 'ClientID'], 'safe'],
        //     [['Description'], 'string', 'max' => 128],
        //     [['JobID', 'JobCode'], 'unique'],
        //
        //
        //
        //     [['JobCode'], 'required', 'on' => 'update_code'],
        //     [['JobCode', 'Manager', 'Description', 'ClientID', 'StartDate', 'EndDate', 'Fee'], 'required', 'on' => 'create'],
        //     [['JobCode', 'Partner', 'Manager', 'Supervisor', 'JobArea'], 'required', 'on' => 'update'],
        // ];
    }
    public function attributeLabels()
    {
      return  [
                'id' => Yii::t('app', 'Job ID'),
                'job_code' => Yii::t('app', 'Job Code'),
                'description' => Yii::t('app', 'Description'),
                'client_id' => Yii::t('app', 'Client'),
                // 'ClassId' => Yii::t('app', 'ClassId'),
                'job_area' => Yii::t('app', 'Job Area'),
                'job_created' => Yii::t('app', 'Job Created'),
                'start_date' => Yii::t('app', 'Start Date'),
                'end_date' => Yii::t('app', 'End Date'),
                'partner_id' => Yii::t('app', 'Partner'),
                'manager_id' => Yii::t('app', 'Manager'),
                'supervisor_id' => Yii::t('app', 'Supervisor'),
                'include_ope' => Yii::t('app', 'Include OPE'),
                'is_meal' => Yii::t('app', 'Meals'),
                'is_taxi' => Yii::t('app', 'Taxi Transportasion'),
                'is_ope' => Yii::t('app', 'Out office Allowance'),
                'ope_allowance' => Yii::t('app', 'Out Of Office Allowance'),
                'meal_allowance' => Yii::t('app', 'Meal Allowance'),
                'taxi_allowance' => Yii::t('app', 'Taxi Allowed'),
                'administrative_charge' => Yii::t('app', 'Administrative Charge'),
                'other_expense_allowance' => Yii::t('app', 'Other Expense'),
                'job_fee' => Yii::t('app', 'Fee'),
                'Total' => Yii::t('app', 'Total'),
                'job_status' => Yii::t('app', 'Status'),
                'created_by' => Yii::t('app', 'Created By'),
                'created_at' => Yii::t('app', 'Created At'),
                'updated_by' => Yii::t('app', 'Update By'),
                'updated_at' => Yii::t('app', 'Update At'),
            ];

        // return [
        //     'JobID' => Yii::t('app', 'Job ID'),
        //     'JobCode' => Yii::t('app', 'Job Code'),
        //     'Description' => Yii::t('app', 'Description'),
        //     'ClientID' => Yii::t('app', 'Client'),
        //     // 'ClassId' => Yii::t('app', 'ClassId'),
        //     'JobArea' => Yii::t('app', 'Job Area'),
        //     'JobCreated' => Yii::t('app', 'Job Created'),
        //     'StartDate' => Yii::t('app', 'Start Date'),
        //     'EndDate' => Yii::t('app', 'End Date'),
        //     'Partner' => Yii::t('app', 'Partner'),
        //     'Manager' => Yii::t('app', 'Manager'),
        //     'Supervisor' => Yii::t('app', 'Supervisor'),
        //     'IncludeOPE' => Yii::t('app', 'Include OPE'),
        //     'IsMeal' => Yii::t('app', 'Meals'),
        //     'IsTaxi' => Yii::t('app', 'Taxi Transportasion'),
        //     'IsOutOfOffice' => Yii::t('app', 'Out office Allowance'),
        //     'OutOfOfficeAllowance' => Yii::t('app', 'Out Of Office Allowance'),
        //     'MealAllowance' => Yii::t('app', 'Meal Allowance'),
        //     'TaxiAllowance' => Yii::t('app', 'Taxi Allowed'),
        //     'AdministrativeCharge' => Yii::t('app', 'Administrative Charge'),
        //     'OtherExpenseAllowance' => Yii::t('app', 'Other Expense'),
        //     'Fee' => Yii::t('app', 'Fee'),
        //     'Total' => Yii::t('app', 'Total'),
        //     'Status' => Yii::t('app', 'Status'),
        //     'CreatedBy' => Yii::t('app', 'Created By'),
        //     'CreatedAt' => Yii::t('app', 'Created At'),
        //     'UpdateBy' => Yii::t('app', 'Update By'),
        //     'UpdateAt' => Yii::t('app', 'Update At'),
        // ];
    }

    public function pkGroup($ClientID)
    {
        return date('y');
    }

    public function getBudgeting()
    {
      return $this->hasMany(TrJobBudgeting::className(), ['job_id' => 'id']);

        // return $this->hasMany(TrJobBudgeting::className(), ['JobID' => 'JobID']);
    }

    public function getComments()
    {
      return $this->hasMany(TrJobComment::className(), ['job_id' => 'id']);

        // return $this->hasMany(TrJobComment::className(), ['JobID' => 'JobID']);
    }

    public static function countTotalGoods($model)
    {
        foreach ($model->goods as $good) {
            if (($good->Status != Self::StatusDenied) and ($good->Status != Self::StatusCanceled)) {
                $model->Qty += $good->Qty;
                $model->Subtotal += $good->Subtotal;
                $model->Discount += $good->Discount;
                $model->Total += $good->Total;
                $model->VAT += $good->VAT;
                $model->Grandtotal += $good->Grandtotal;
            }
        }

        return $model;
    }

    public static function getTotalJobFromStatus()
    {
        $user = CommonHelper::getUserIndentity();
        $StatusSubmit = Self::StatusSubmit;
        $StatusApproved = Self::StatusApproved;
        $StatusDenied = Self::StatusDenied;
        $StatusOnProgress = Self::StatusOnProgress;
        $StatusCanceled = Self::StatusCanceled;
        $StatusSuccess = Self::StatusSuccess;

        $where = [];
        $or = [];
        if ($user->is_admin != 1) {
            $where = [
              'manager_id' => $user->user_id,
            ];
            $or = ['or',['created_by' => $user->user_id ],$where];
        }

        // if ($user->IsAdmin != 1) {
        //     $where = [
        //         'Manager' => $user->Id,
        //     ];
        //     $or = ['or', ['CreatedBy' => $user->Id], $where];
        // }

        // if($user->canChangeDept){
        // 	$where = [
        // 		  'CompanyId' => $user->CompanyId,
        // 		  'BranchId' => $user->BranchId,
        //             ];
        // }else{
        // 	$where = [
        // 		  'CompanyId' => $user->CompanyId,
        // 		  'BranchId' => $user->BranchId,
        // 		  'DeptId' => $user->DeptId,
        //             ];
        // }

        $data = self::find()
                ->select(
                "count(id) as _Total
                  , sum(case when job_status = 0 and flag = 0 then 1 else 0 end) as _TotalDraft
                  , sum(case when job_status = {$StatusSubmit}  then 1 else 0 end) as _TotalSubmit
                  , sum(case when job_status = {$StatusApproved}  and start_date > NOW() then 1 else 0 end) as _TotalApproved
                  , sum(case when job_status = {$StatusDenied}  then 1 else 0 end) as _TotalDenied
                  , sum(case when job_status = {$StatusOnProgress} - 19  and start_date < NOW() and end_date > NOW()  then 1 else 0 end) as _TotalOnProgress
                  , sum(case when job_status = {$StatusCanceled}   then 1 else 0 end) as _TotalCanceled
                  , sum(case when job_status = {$StatusSuccess} - 20   and end_date < NOW() then 1 else 0 end)  as _TotalSuccess
                ")->andWhere($or)->One();

        // $data = self::find()
        //     ->select(
        //         "count(JobID) as _Total
        //           , sum(case when Status = 0 and Flag = 0 then 1 else 0 end) as _TotalDraft
        //           , sum(case when Status = {$StatusSubmit}  then 1 else 0 end) as _TotalSubmit
        //           , sum(case when Status = {$StatusApproved}  and StartDate > NOW() then 1 else 0 end) as _TotalApproved
        //           , sum(case when Status = {$StatusDenied}  then 1 else 0 end) as _TotalDenied
        //           , sum(case when Status = {$StatusOnProgress} - 19  and StartDate < NOW() and EndDate > NOW()  then 1 else 0 end) as _TotalOnProgress
        //           , sum(case when Status = {$StatusCanceled}   then 1 else 0 end) as _TotalCanceled
        //           , sum(case when Status = {$StatusSuccess} - 20   and EndDate < NOW() then 1 else 0 end)  as _TotalSuccess
        //         "
        //     )->andWhere($or)->One();

        return $data;
    }
    public static function getTotalApprovalFromStatus($id = '')
    {
        $user = CommonHelper::getUserIndentity();
        $StatusSubmit = Self::StatusSubmit;
        $StatusMyPending = Self::StatusMyPending;
        $StatusApproved = Self::StatusApproved;
        $StatusDenied = Self::StatusDenied;
        $StatusOnProgress = Self::StatusOnProgress;
        $StatusCanceled = Self::StatusCanceled;
        $StatusSuccess = Self::StatusSuccess;

        // $data = self::find()
        //         ->select(
        //         "count(trJob.JobID) as _Total
        //           , sum(case when trJob.Flag = 0 ) as _TotalDraft
        //           , sum(case when trJob.Status = {$StatusSubmit}  then 1 else 0 end) as _TotalSubmit
        //           , sum(case when (trJob.Status = {$StatusMyPending}  and trJob.Approver = '{$user->EmployeeID}') then 1 else 0 end) as _TotalMyPending
        //           , sum(case when trJob.Status = {$StatusApproved}  then 1 else 0 end) as _TotalApproved
        //           , sum(case when trJob.Status = {$StatusDenied}  then 1 else 0 end) as _TotalDenied
        //           , sum(case when trJob.Status = {$StatusCanceled}  then 1 else 0 end) as _TotalCanceled
        //         ")->one();
        $where = [];
        if ($user->is_admin != 1) {
            $where = [
                'Partner' => $user->user_id,

            ];
        }

        // if ($user->IsAdmin != 1) {
        //     $where = [
        //         'Partner' => $user->Id,
        //
        //     ];
        // }

        $data = self::find()
            ->select(
                "count(id) as _Total
                    , sum(case when job_status = 0 and flag = 0 then 1 else 0 end) as _TotalDraft
                    , sum(case when job_status = {$StatusSubmit}  then 1 else 0 end) as _TotalSubmit
                    , sum(case when job_status = {$StatusApproved}  and start_date > NOW() then 1 else 0 end) as _TotalApproved
                    , sum(case when job_status = {$StatusDenied}  then 1 else 0 end) as _TotalDenied
                    , sum(case when job_status = {$StatusOnProgress} - 19  and start_date < NOW() and end_date > NOW()  then 1 else 0 end) as _TotalOnProgress
                    , sum(case when job_status = {$StatusCanceled}   then 1 else 0 end) as _TotalCanceled
                    , sum(case when job_status = {$StatusSuccess} - 20   and end_date < NOW() then 1 else 0 end)  as _TotalSuccess
                "
            )->where($where)->andWhere(['<>', 'Status', 0])->one();

        // $data = self::find()
        //     ->select(
        //         "count(JobID) as _Total
        //             , sum(case when Status = 0 and Flag = 0 then 1 else 0 end) as _TotalDraft
        //             , sum(case when Status = {$StatusSubmit}  then 1 else 0 end) as _TotalSubmit
        //             , sum(case when Status = {$StatusApproved}  and StartDate > NOW() then 1 else 0 end) as _TotalApproved
        //             , sum(case when Status = {$StatusDenied}  then 1 else 0 end) as _TotalDenied
        //             , sum(case when Status = {$StatusOnProgress} - 19  and StartDate < NOW() and EndDate > NOW()  then 1 else 0 end) as _TotalOnProgress
        //             , sum(case when Status = {$StatusCanceled}   then 1 else 0 end) as _TotalCanceled
        //             , sum(case when Status = {$StatusSuccess} - 20   and EndDate < NOW() then 1 else 0 end)  as _TotalSuccess
        //         "
        //     )->where($where)->andWhere(['<>', 'Status', 0])->one();
        /*
            ->joinwith('approvers');
            ->where([
              //'trJob.CompanyId' => $user->CompanyId,
              //'trJob.BranchId' => $user->BranchId,
              //'trJobApprover.Approver' => $user->EmployeeID,
            ])->One();
                */
        return $data;
    }

    public static function getButtonTotalJobFromStatus($action = 9)
    {
        $user = CommonHelper::getUserIndentity();
        $class = new self();
        $data = $class->getTotalJobFromStatus();
        $total_active = ($action == 9) ? ' active' : '';
        $draft_active = ($action == 8) ? ' active' : '';
        $submit_active = ($action == Self::StatusSubmit) ? ' active' : '';
        $approved_active = ($action == Self::StatusApproved) ? ' active' : '';
        $denied_active = ($action == Self::StatusDenied) ? ' active' : '';
        $canceled_active = ($action == Self::StatusCanceled) ? ' active' : '';
        $success_active = ($action == Self::StatusSuccess) ? ' active' : '';
        $onprogress_active = ($action == Self::StatusOnProgress) ? ' active' : '';

        $total = isset($data->_Total) ? $data->_Total : 0;
        $draft = isset($data->_TotalDraft) ? $data->_TotalDraft : 0;
        $submit = isset($data->_TotalSubmit) ? $data->_TotalSubmit : 0;
        $approved = isset($data->_TotalApproved) ? $data->_TotalApproved : 0;
        $denied = isset($data->_TotalDenied) ? $data->_TotalDenied : 0;
        $onprogress = isset($data->_TotalOnProgress) ? $data->_TotalOnProgress : 0;
        $canceled = isset($data->_TotalCanceled) ? $data->_TotalCanceled : 0;
        $success = isset($data->_TotalSuccess) ? $data->_TotalSuccess : 0;

        $button = '';

        $button .= ' ' . Html::a(
            Yii::t('app', 'ALL') . ' | ' . intval($total),
            ['all'],
            ['class' => 'btn btn-primary btn-cons' . $total_active]
        );
        // print_r($user->levelID);
        $button .= ' ' . Html::a(
            Yii::t('app', 'DRAFT') . ' | ' . intval($draft),
            ['draft'],
            ['class' => 'btn btn-primary btn-cons' . $draft_active]
        );
        $button .= ' ' . Html::a(
            Yii::t('app', 'SUBMITTED') . ' | ' . intval($submit),
            ['submit'],
            ['class' => 'btn btn-primary btn-cons' . $submit_active]
        );
        $button .= ' ' . Html::a(
            Yii::t('app', 'APPROVED') . ' | ' . intval($approved),
            ['approved'],
            ['class' => 'btn btn-primary btn-cons' . $approved_active]
        );
        // $button .= ' '.Html::a(
        //         Yii::t('app', 'OnProgress').' | '.intval($approved), ['onprogress'], ['class' => 'btn btn-primary btn-cons'.$approved_active]
        //     );
        $button .= ' ' . Html::a(
            Yii::t('app', 'REJECTED') . ' | ' . intval($denied),
            ['denied'],
            ['class' => 'btn btn-primary btn-cons' . $denied_active]
        );
        $button .= ' ' . Html::a(
            Yii::t('app', 'ON GOING') . ' | ' . intval($onprogress),
            ['onprogress'],
            ['class' => 'btn btn-primary btn-cons' . $onprogress_active]
        );
        $button .= ' ' . Html::a(
            Yii::t('app', 'EXPIRED') . ' | ' . intval($success),
            ['success'],
            ['class' => 'btn btn-primary btn-cons' . $success_active]
        );
        $button .= ' ' . Html::a(
            Yii::t('app', 'CLOSED') . ' | ' . intval($canceled),
            ['canceled'],
            ['class' => 'btn btn-primary btn-cons' . $canceled_active]
        );

        return $button;
    }

    public static function getButtonTotalApprovalFromStatus($action)
    {
        $user = CommonHelper::getUserIndentity();
        $class = new self();
        $data = $class->getTotalApprovalFromStatus();
        $total_active = ($action == 9) ? ' active' : '';
        $draft_active = ($action == 8) ? ' active' : '';
        $submit_active = ($action == Self::StatusSubmit) ? ' active' : '';
        $approved_active = ($action == Self::StatusApproved) ? ' active' : '';
        $denied_active = ($action == Self::StatusDenied) ? ' active' : '';
        $canceled_active = ($action == Self::StatusCanceled) ? ' active' : '';
        $success_active = ($action == Self::StatusSuccess) ? ' active' : '';
        $onprogress_active = ($action == Self::StatusOnProgress) ? ' active' : '';

        $total = isset($data->_Total) ? $data->_Total : 0;
        $draft = isset($data->_TotalDraft) ? $data->_TotalDraft : 0;
        $submit = isset($data->_TotalSubmit) ? $data->_TotalSubmit : 0;
        $approved = isset($data->_TotalApproved) ? $data->_TotalApproved : 0;
        $denied = isset($data->_TotalDenied) ? $data->_TotalDenied : 0;
        $onprogress = isset($data->_TotalOnProgress) ? $data->_TotalOnProgress : 0;
        $canceled = isset($data->_TotalCanceled) ? $data->_TotalCanceled : 0;
        $success = isset($data->_TotalSuccess) ? $data->_TotalSuccess : 0;

        $button = '';

        $button .= ' ' . Html::a(
            Yii::t('app', 'ALL') . ' | ' . intval($total),
            ['all'],
            ['class' => 'btn btn-primary btn-cons mb-2' . $total_active]
        );
        // print_r($user->levelID);
        $button .= ' ' . Html::a(
            Yii::t('app', 'SUBMIT') . ' | ' . intval($submit),
            ['submit'],
            ['class' => 'btn btn-primary btn-cons mb-2' . $submit_active]
        );
        $button .= ' ' . Html::a(
            Yii::t('app', 'APPROVED') . ' | ' . intval($approved),
            ['approved'],
            ['class' => 'btn btn-primary btn-cons mb-2' . $approved_active]
        );
        // $button .= ' '.Html::a(
        //         Yii::t('app', 'OnProgress').' | '.intval($approved), ['onprogress'], ['class' => 'btn btn-primary btn-cons'.$approved_active]
        //     );
        $button .= ' ' . Html::a(
            Yii::t('app', 'REJECT') . ' | ' . intval($denied),
            ['denied'],
            ['class' => 'btn btn-primary btn-cons mb-2' . $denied_active]
        );
        $button .= ' ' . Html::a(
            Yii::t('app', 'ON GOING') . ' | ' . intval($onprogress),
            ['ongoing'],
            ['class' => 'btn btn-primary btn-cons mb-2' . $onprogress_active]
        );
        $button .= ' ' . Html::a(
            Yii::t('app', 'EXPIRED') . ' | ' . intval($success),
            ['success'],
            ['class' => 'btn btn-primary btn-cons mb-2' . $success_active]
        );
        $button .= ' ' . Html::a(
            Yii::t('app', 'CLOSED') . ' | ' . intval($canceled),
            ['canceled'],
            ['class' => 'btn btn-primary btn-cons mb-2' . $canceled_active]
        );

        return $button;
    }

    public static function switchStatus($data, $showicon = 1, $onlyicon = 0, $flag = 1)
    {
        $return = '';
        if ($flag == 1) {
            switch ($data) {
                case 0:
                    $icon = ($showicon == 1 || $onlyicon == 1) ? ' <i class="fa fa-ellipsis-h "></i>' : '';
                    $label = ($onlyicon == 0) ? ' PENDING APPROVAL' : '';
                    $return = '<font class="bold text-warning">' . $icon . $label . ' </font>';
                    break;
                case 1:
                    $icon = ($showicon == 1 || $onlyicon == 1) ? ' <i class="fa fa-check-circle"></i>' : '';
                    $label = ($onlyicon == 0) ? ' APPROVED' : '';
                    $return = '<font class="bold text-complete">' . $icon . $label . ' </font>';
                    break;
                case -1:
                    $icon = ($showicon == 1 || $onlyicon == 1) ? ' <i class="fa fa-minus-circle"></i>' : '';
                    $label = ($onlyicon == 0) ? ' DENIED' : '';
                    $return = '<font class="bold text-danger">' . $icon . $label . ' </font>';
                    break;
                default:
                    $icon = '';
                    $label = '';
                    $return = '';
            }
        } elseif ($flag == 0) {
            $icon = ($showicon == 1 || $onlyicon == 1) ? ' <i class="fa fa-close"></i>' : '';
            $label = ($onlyicon == 0) ? ' CANCELED' : '';
            $return = '<font class="bold text-danger">' . $icon . $label . ' </font>';
        } elseif ($flag == -1) {
            $icon = ($showicon == 1 || $onlyicon == 1) ? ' <i class="fa fa-close"></i>' : '';
            $label = ($onlyicon == 0) ? ' DELETED' : '';
            $return = '<font class="bold text-danger">' . $icon . $label . ' </font>';
        }

        return $return;
    }

    public function getClient()
    {
      return $this->hasOne(Client::className(), ['id' => 'client_id']);
        // return $this->hasOne(Client::className(), ['Id' => 'ClientID']);
    }

    // public function getDivision()
    // {
    //     return $this->hasOne(Dept::className(), ['Id' => 'Id']);
    // }

    public function getDept()
    {
      return $this->hasOne(Dept::className(), ['id' => 'dept_id']);
    }

    public function getPartner()
    {
      return $this->hasOne(Employee::className(), ['user_id' => 'partner_id']);
        // return $this->hasOne(Employee::className(), ['Id' => 'Partner']);
    }

    public function getManager()
    {
        return $this->hasOne(Employee::className(), ['user_id' => 'manager_id']);
    }

    public function getSupervisor()
    {
        return $this->hasOne(Employee::className(), ['user_id' => 'supervisor_id']);
    }

    public static function status_email($status)
    {
        $results = [1 => 'NEW VERSION', 2 => 'HAS APPROVED', 3 => 'REVISED VERSION'];
        return !isset($results[$status]) ? "NEW VERSION" : $results[$status];
    }

    public static function status_list()
    {
        return [
            0 => "DRAFT",
            1 => "SUBMIT",
            2 => "APPROVE",
            3 => "REJECT",
            4 => "CLOSE",
        ];
    }
    public static function status_label($status)
    {
        $data = static::status_list();
        return !isset($data[$status]) ? "DRAFT" : $data[$status];
    }
}
