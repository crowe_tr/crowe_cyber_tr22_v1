<?php

namespace common\models\tr\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\tr\JobBudgetingTemp;

/**
 * JobBudgetingTempSearch represents the model behind the search form of `common\models\tr\JobBudgetingTemp`.
 */
class JobBudgetingTempSearch extends JobBudgetingTemp
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['job_id', 'seq'], 'integer'],
            [['employee_id', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'safe'],
            [['planning', 'field_work', 'reporting', 'wrap_up', 'over_time', 'total_wh'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = JobBudgetingTemp::find();
        $query->joinWith('employee');
        $query->leftJoin('cm_level','cm_level.id=hr_employee.level_id');
        // $query->orderBy(['cm_level.level_code' => SORT_ASC, 'hr_employee.full_name' => SORT_ASC]);
        $query->orderBy(['hr_employee.full_name' => SORT_ASC]);


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
              'pageSize' => 100,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'job_id' => $this->job_id,
            'seq' => $this->seq,
            'planning' => $this->planning,
            'field_work' => $this->field_work,
            'reporting' => $this->reporting,
            'wrap_up' => $this->wrap_up,
            'over_time' => $this->over_time,
            'total_wh' => $this->total_wh,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'employee_id', $this->employee_id])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'updated_by', $this->updated_by]);

        return $dataProvider;
    }
}
