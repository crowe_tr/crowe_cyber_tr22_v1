<?php

namespace common\models\tr\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\tr\TimeReport as TimeReportModel;

/**
 * TimeReport represents the model behind the search form of `common\models\tr\TimeReport`.
 */
class TimeReport extends TimeReportModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'is_stayed', 'tr_status'], 'integer'],
            [['employee_id', 'tr_date', 'work_hour'], 'safe'],
            // [['employee_id', 'tr_date', 'maxOverTime'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TimeReportModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tr_date' => $this->tr_date,
            'is_stayed' => $this->is_stayed,
            // 'employee_rate' => $this->employee_rate,
            // 'maxMeal' => $this->maxMeal,
            // 'maxOPE' => $this->maxOPE,
            'tr_status' => $this->tr_status,
        ]);

        $query->andFilterWhere(['like', 'employee_id', $this->employee_id]);
            // ->andFilterWhere(['like', 'maxOverTime', $this->maxOverTime]);

        return $dataProvider;
    }
}
