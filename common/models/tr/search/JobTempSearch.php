<?php

namespace common\models\tr\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\tr\JobTemp;

/**
 * JobTempSearch represents the model behind the search form of `common\models\tr\JobTemp`.
 */
class JobTempSearch extends JobTemp
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'client_id', 'include_ope', 'is_meal', 'is_ope', 'is_taxi', 'job_status', 'flag'], 'integer'],
            [['job_code', 'description', 'job_area', 'job_created',
              'start_date', 'end_date', 'partner_id', 'manager_id', 'supervisor_id',
              'percentage', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'safe'],
            [['meal_allowance', 'ope_allowance', 'taxi_allowance', 'administrative_charge', 'other_expense_allowance',
              'job_fee', 'time_charges'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = JobTemp::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
            'job_created' => $this->job_created,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'include_ope' => $this->include_ope,
            'is_meal' => $this->is_meal,
            'is_ope' => $this->is_ope,
            'is_taxi' => $this->is_taxi,
            'meal_allowance' => $this->meal_allowance,
            'ope_allowance' => $this->ope_allowance,
            'taxi_allowance' => $this->taxi_allowance,
            'administrative_charge' => $this->administrative_charge,
            'other_expense_allowance' => $this->other_expense_allowance,
            'job_fee' => $this->job_fee,
            'time_charges' => $this->time_charges,
            'job_status' => $this->job_status,
            'flag' => $this->flag,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'job_code', $this->job_code])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'job_area', $this->job_area])
            ->andFilterWhere(['like', 'partner_id', $this->partner_id])
            ->andFilterWhere(['like', 'manager_id', $this->manager_id])
            ->andFilterWhere(['like', 'supervisor_id', $this->supervisor_id])
            ->andFilterWhere(['like', 'percentage', $this->percentage])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'updated_by', $this->updated_by]);

        return $dataProvider;
    }
}
