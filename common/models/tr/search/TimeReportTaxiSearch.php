<?php

namespace common\models\tr\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\tr\TimeReportTaxi;

/**
 * TimeReportTaxiSearch represents the model behind the search form of `common\models\tr\TimeReportTaxi`.
 */
class TimeReportTaxiSearch extends TimeReportTaxi
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['time_report_id', 'seq', 'taxi_id', 'task_type_id', 'job_id', 'task_id'], 'integer'],
            [['taxi_start', 'taxi_finish', 'voucher_no', 'destination', 'description'], 'safe'],
            [['amount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TimeReportTaxi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'time_report_id' => $this->time_report_id,
            'seq' => $this->seq,
            'taxi_id' => $this->taxi_id,
            'task_type_id' => $this->task_type_id,
            'job_id' => $this->job_id,
            'task_id' => $this->task_id,
            'taxi_start' => $this->taxi_start,
            'taxi_finish' => $this->taxi_finish,
            'amount' => $this->amount,
        ]);

        $query->andFilterWhere(['like', 'voucher_no', $this->voucher_no])
            ->andFilterWhere(['like', 'destination', $this->destination])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
