<?php

namespace common\models\tr\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\tr\TimeReportNote;

class TimeReportNoteSearch extends TimeReportNote
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TimeReportNote::find()->orderBy([
            'created_at' => SORT_DESC //specify sort order ASC for ascending DESC for descending
            ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'time_report_id' => $this->time_report_id,
            'parent_id' => $this->parent_id,
            'comment_type' => $this->comment_type,
            'comment_for' => $this->comment_for,
        ]);

        return $dataProvider;
    }
}
