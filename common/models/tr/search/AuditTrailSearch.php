<?php

namespace common\models\tr\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\tr\AuditTrail;

/**
 * AuditTrailSearch represents the model behind the search form of `common\models\tr\AuditTrail`.
 */
class AuditTrailSearch extends AuditTrail
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'trans_id', 'audit_status'], 'integer'],
            [['source', 'trans_type', 'transaction_type', 'source_no', 'audit_description',
              'created_at', 'created_by', 'updated_at', 'updated_by'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $query = AuditTrail::find()->orderBy('created_at Desc');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
          'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'trans_id' => $params['trans_id'],
            'audit_status' => $this->audit_status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'source', $params['source']])
            ->andFilterWhere(['like', 'trans_type', $params['trans_type']])
            ->andFilterWhere(['like', 'transaction_type', $this->transaction_type])
            ->andFilterWhere(['like', 'source_no', $this->source_no])
            ->andFilterWhere(['like', 'audit_description', $this->audit_description])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'updated_by', $this->updated_by]);

        return $dataProvider;
    }
}
