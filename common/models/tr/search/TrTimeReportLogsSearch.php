<?php

namespace common\models\tr\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\tr\TrTimeReportLogs;


class TrTimeReportLogsSearch extends TrTimeReportLogs
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'tr_id', 'tr_det_id'], 'integer'],
            // [['tr_date','employee_name','tr_type','tr_status','tr_memo',
            //   'user_action','created_at','created_by'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        
        // die();
        $query = TrTimeReportLogs::find()->orderBy('created_at Desc');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
          'query' => $query,
        ]);

        $this->load($params);
        // var_dump($params, $this);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tr_id' => $params['tr_id'],
            // 'tr_type' => $this->tr_type,
            // 'created_at' => $this->created_at,
        //     // 'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'tr_type', $this->tr_type])
            // ->andFilterWhere(['like', 'transaction_type', $this->transaction_type])
            // ->andFilterWhere(['like', 'source_no', $this->source_no])
            // ->andFilterWhere(['like', 'audit_description', $this->audit_description])
            ->andFilterWhere(['like', 'created_by', $this->created_by]);

        return $dataProvider;
    }
}
