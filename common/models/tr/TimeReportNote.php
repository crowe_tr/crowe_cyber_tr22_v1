<?php

namespace common\models\tr;

use Yii;

/**
 * This is the model class for table "trTimeReportNote".
 *
 * @property int $CommentId
 * @property string $EmployeeId
 * @property string $Date
 * @property string $Notes
 * @property string $CreatedBy
 * @property string $CreatedDate
 */
use common\models\hr\Employee;

class TimeReportNote extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tr_time_report_note';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['comment_date', 'created_at', 'time_report_id', 'parent_id', 'comment_type', 'comment_for'], 'safe'],
            [['comment_notes'], 'string'],
            [['employee_id', 'created_by'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Comment ID'),
            'employee_id' => Yii::t('app', 'Employee'),
            'parent_id' => Yii::t('app', 'Parent'),
            'comment_date' => Yii::t('app', 'Date'),
            'comment_notes' => Yii::t('app', 'Notes'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created Date'),
        ];
    }
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['user_id' => 'employee_id']);
    }

}
