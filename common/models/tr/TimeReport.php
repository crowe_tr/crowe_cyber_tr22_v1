<?php

namespace common\models\tr;

use Yii;

use common\models\hr\Employee;
use common\models\cm\Level;

class TimeReport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $YearDate;
    public $employee_level;
    public $Date_label;

    public $employee_name;
    // public $employee_rate;
    // public $work_hour;
    public static function tableName()
    {
        return 'tr_time_report';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [['id', 'YearDate', 'employee_name', 'is_stayed'], 'safe'],

            [['employee_id', 'tr_date', 'is_stayed', 'tr_status'], 'required'],
            [['tr_date','work_hour','employee_rate'], 'safe'],
            [['is_stayed', 'tr_status'], 'integer'],
            [['employee_id'], 'string', 'max' => 32],
            // [['maxOverTime'], 'string', 'max' => 45],
            [['employee_id', 'tr_date'], 'unique', 'targetAttribute' => ['employee_id', 'tr_date']],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['employee_id' => 'user_id']],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'employee_id' => Yii::t('app', 'Employee'),
            'tr_date' => Yii::t('app', 'Date'),
            'employee_rate' => Yii::t('app', 'Employee Rate'),
            'is_stayed' => Yii::t('app', 'Stayed'),
            // 'maxOverTime' => Yii::t('app', 'Max Over Time'),
            // 'maxMeal' => Yii::t('app', 'Max Meal'),
            // 'maxOPE' => Yii::t('app', 'Max Ope'),
            'tr_status' => Yii::t('app', 'Status'),
            'Date_label'=>'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['user_id' => 'employee_id']);
    }
    public function getLevel()
    {
        return $this->hasOne(Level::className(), ['id' => 'level_id'])->via('employee');
    }

}
