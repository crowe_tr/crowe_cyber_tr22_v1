<?php

namespace common\models\tr;

use Yii;
use common\models\st\Task;
use common\models\st\TaskType;
use common\models\st\RuleTermZone;

/**
 * This is the model class for table "trTimeReportOutOfOffice".
 *
 * @property int $TimeReportID
 * @property int $Seq
 * @property int $TaskTypeID
 * @property int $JobId
 * @property int $TaskID
 * @property string $ZoneID
 *
 * @property Job $job
 * @property Task $task
 * @property TaskType $taskType
 * @property TimeReport $timeReport
 */
class TimeReportOutOfOffice extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tr_time_report_ope';
    }
    // public $EmployeeId;
    // public $Date;
    // public $isStayed;


    public $OPEProvided;
    public $OPEAllocation;
    public $OPEClaimQty;
    public $OPEClaimQtyStayed;

    public $lookup;
    public function rules()
    {
        return [
            // [['EmployeeId', 'Date', 'isStayed', 'Description'], 'safe'],
            [['description'], 'safe'],

            [['time_report_id', 'seq', 'tr_det_id', 'zone_id'], 'safe'],
            [['time_report_id', 'tr_det_id', 'zone_id'], 'required'],

            [['time_report_id', 'seq', 'tr_det_id'], 'integer'],

            [['zone_id'], 'string', 'max' => 255],
            [['time_report_id', 'seq'], 'unique', 'targetAttribute' => ['time_report_id', 'seq']],
            // [['time_report_id', 'tr_det_id'], 'unique', 'targetAttribute' => ['time_report_id', 'tr_det_id']],
            [['time_report_id'], 'exist', 'skipOnError' => true, 'targetClass' => TimeReport::className(), 'targetAttribute' => ['time_report_id' => 'id']],


            [['job_id'], 'required'
            , 'when'=>function($model) {
                $return = false;

                if($model->task_type_id == 1){
                    $return = true;
                }
                return $return;
            }, 'enableClientValidation' => false],
            [['zone_id'], 'required'
            , 'when'=>function($model) {
                $return = false;

                if($model->task_type_id == 1){
                    $return = true;
                }
                return $return;
            }, 'enableClientValidation' => false],
            [['tr_date','employee_id','inter_city'], 'safe'],
            [['approval1','approval2'], 'safe'],

            [['ope_value', 'OPEProvided', 'OPEAllocation','OPEClaimQty', 'OPEClaimQtyStayed'], 'safe'],
            //[['OutOfOffice'], 'validateOPE', 'enableClientValidation' => false],
        ];
    }
    public function validateOPE($attribute_name, $params){
        $valid = true;
        if($this->ope_value > 0){
            if($this->OPEProvided == true){
                $OPEClaimQty = ($this->isStayed == 0) ? $this->OPEClaimQty : $this->OPEClaimQtyStayed;
                if ($OPEClaimQty < $this->OPEAllocation){
                    $this->addError($attribute_name, "OPE: ".$this->OPEAllocation.", Batas claim OPE sudah mencapai batas, hak claim OPE : ".$OPEClaimQty);
                    $valid = false;
                }
            }else{
                $this->addError($attribute_name, "Anda tidak bisa claim ope");
                $valid = false;
            }
        }
        return $valid;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'time_report_id' => Yii::t('app', 'Time Report'),
            'seq' => Yii::t('app', 'Seq'),
            'tr_det_id' => Yii::t('app', 'Task Description'),
        ];
    }

    public function getJob()
    {
        return $this->hasOne(Job::className(), ['id' => 'job_id']);
    }
    public function getTasktype()
    {
        return $this->hasOne(TaskType::className(), ['id' => 'task_type_id']);
    }
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimeReport()
    {
        return $this->hasOne(TimeReport::className(), ['id' => 'time_report_id']);
    }
    public function getZone()
    {
        return $this->hasOne(RuleTermZone::className(), ['id' => 'zone_id']);
    }

}
