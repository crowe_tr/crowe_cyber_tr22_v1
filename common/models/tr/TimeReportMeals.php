<?php

namespace common\models\tr;
use common\models\st\Task;
use common\models\st\TaskType;

use Yii;

/**
 * This is the model class for table "trTimeReportMeals".
 *
 * @property int $TimeReportID
 * @property int $Seq
 * @property int $TaskTypeID
 * @property int $JobId
 * @property int $TaskID
 * @property int $Status
 *
 * @property Job $job
 * @property Task $task
 * @property TaskType $taskType
 * @property TimeReport $timeReport
 */
class TimeReportMeals extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tr_time_report_meals';
    }

    // public $EmployeeId;
    // public $TrDate;
    // public $isStayed;

    public $Meal;
    public $MealProvided;
    public $MealAllocation;
    public $MealClaimQty;
    public $MealClaimQtyStayed;
    public $MealMinOvertime;

    public $lookup;

    public function rules()
    {
        return [

            // [['EmployeeId', 'TrDate', 'isStayed', 'lookup', 'description'], 'safe'],
            [['lookup', 'description'], 'safe'],
            [['time_report_id', 'seq'], 'safe'],
            [['time_report_id', 'tr_det_id'], 'required'],
            //[['TimeReportID', 'TaskTypeID', 'TaskID'], 'integer'],

            [['time_report_id', 'tr_det_id'], 'unique', 'targetAttribute' => ['time_report_id', 'tr_det_id']],
            // [['time_report_id', 'seq'], 'unique', 'targetAttribute' => ['time_report_id', 'seq']],


            [['job_id'], 'exist', 'skipOnError' => true, 'targetClass' => Job::className(), 'targetAttribute' => ['job_id' => 'id']],
            [['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['task_id' => 'id']],
            [['task_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaskType::className(), 'targetAttribute' => ['task_type_id' => 'id']],
            [['time_report_id'], 'exist', 'skipOnError' => true, 'targetClass' => TimeReport::className(), 'targetAttribute' => ['time_report_id' => 'id']],
            [['tr_date','employee_id'], 'safe'],
            [['approval1','approval2'], 'safe'],

            //[['Meal', 'MealProvided', 'MealAllocation', 'MealClaimQty', 'MealClaimQtyStayed','MealMinOvertime'], 'safe'],
            //[['Meal'], 'validateMeals', 'enableClientValidation' => false],

        ];
    }

    public function validateMeals($attribute_name, $params){
        // var_dump('b');
        // die();
        $valid = true;
        if($this->Meal > 0){
            if($this->MealProvided == true){
                $ClaimQty = ($this->is_stayed == 0) ? $this->MealClaimQty : $this->MealClaimQtyStayed;
                if ($ClaimQty >= $this->MealAllocation){
                    if($this->MealMinOvertime <= $this->OvertimeAllocation){
                        $this->addError($attribute_name, "Total Overtime tidak memenuhi syarat untuk claim makan, minimal Overtime : ".$this->TaxiMinOvertime);
                        $valid = false;
                    }
                }else{
                    $this->addError($attribute_name, "Batas claim meal sudah mencapai batas, hak claim meal : ".$ClaimQty);
                    $valid = false;
                }
            }else{
                $this->addError($attribute_name, "Anda tidak bisa claim meal");
                $valid = false;
            }
        }
        return $valid;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'time_report_id' => Yii::t('app', 'Time Report'),
            'seq' => Yii::t('app', 'Seq'),
            'task_type_id' => Yii::t('app', 'Class/Task Type'),
            'job_id' => Yii::t('app', 'Job'),
            'task_id' => Yii::t('app', 'Task'),
            'tr_det_id' => 'Task Description',
        ];
    }

    public function getJob()
    {
        return $this->hasOne(Job::className(), ['id' => 'job_id']);
    }
    public function getTasktype()
    {
        return $this->hasOne(TaskType::className(), ['id' => 'task_type_id']);
    }
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimeReport()
    {
        return $this->hasOne(TimeReport::className(), ['id' => 'time_report_id']);
    }
}
