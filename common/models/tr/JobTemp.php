<?php

namespace common\models\tr;

use common\models\cl\Client;
use common\models\hr\Employee;

use Yii;

/**
 * This is the model class for table "trJobTemp".
 *
 * @property int $JobID
 * @property string $JobCode
 * @property string $Description
 * @property int $ClientID
 * @property string $JobArea
 * @property string $JobCreated
 * @property string $StartDate
 * @property string $EndDate
 * @property string $Partner
 * @property string $Manager
 * @property string $Supervisor
 * @property int $IncludeOPE
 * @property int $IsMeal
 * @property int $IsOutOfOffice
 * @property int $IsTaxi
 * @property string $MealAllowance
 * @property string $OutOfOfficeAllowance
 * @property string $TaxiAllowance
 * @property string $AdministrativeCharge
 * @property string $OtherExpenseAllowance
 * @property string $Fee
 * @property string $TimeCharges
 * @property string $Percentage
 * @property int $Status 0:Draff; 1:Submit; 2:Reject; 3:Approve; 4:Finish; 5:Close
 * @property int $Flag
 * @property string $CreatedBy
 * @property string $CreatedAt
 * @property string $UpdateBy
 * @property string $UpdateAt
 *
 * @property ClClient $client
 */
class JobTemp extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    // public $Total;
    public static function tableName()
    {
        return 'tr_job_temp';
    }

    /**
     * {@inheritdoc}
     */

    // public $AdministrativeChargeAct;
    // public $OtherExpenseAllowanceAct;
    public function rules()
    {
        return [
            [['description', 'client_id', 'start_date', 'end_date', 'job_fee'], 'required'],
            [['time_charges_actual', 'meal_allowance_actual', 'taxi_allowance_actual', 'ope_allowance_actual',
              'administrative_charge_actual', 'other_expense_allowance_actual'], 'safe'],
            [['id', 'include_ope', 'is_meal', 'is_taxi', 'is_ope', 'job_status'], 'integer'],
            [['job_code', 'partner_id', 'manager_id', 'supervisor_id', 'created_by', 'updated_by'], 'string', 'max' => 32],
            [['is_meal', 'is_taxi', 'is_ope', 'job_created', 'start_date', 'end_date',
              'created_at', 'updated_at', 'total_wh', 'meal_allowance', 'taxi_allowance', 'other_expense_allowance',
              'ope_allowance', 'time_charges',
              // 'RecoveryRate',
              'percentage',
              'job_area', 'administrative_charge', 'client_id'], 'safe'],
            [['description'], 'string', 'max' => 128],
            [['id', 'job_code'], 'unique'],



            [['job_code', 'manager_id', 'description', 'client_id', 'start_date', 'end_date', 'job_fee'], 'required', 'on' => 'create'],
            [['job_code', 'partner_id', 'manager_id', 'supervisor_id', 'job_area'], 'required', 'on' => 'update'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Job ID',
            'job_code' => 'Job Code',
            'description' => 'Description',
            'client_id' => 'Client ID',
            'job_area' => 'Job Area',
            'job_created' => 'Job Created',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'partner_id' => 'Partner',
            'manager_id' => 'Manager',
            'supervisor_id' => 'Supervisor',
            'include_ope' => 'Include Ope',
            'is_meal' => 'Is Meal',
            'is_ope' => 'Is Out Of Office',
            'is_taxi' => 'Is Taxi',
            'meal_allowance' => 'Meal Allowance',
            'ope_allowance' => 'Out Of Office Allowance',
            'taxi_allowance' => 'Taxi Allowance',
            'administrative_charge' => 'Administrative Charge',
            'other_expense_allowance' => 'Other Expense Allowance',
            'job_fee' => 'Fee',
            'time_charges' => 'Time Charges',
            'percentage' => 'Percentage',
            'job_status' => 'Status',
            'flag' => 'Flag',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Update By',
            'updated_at' => 'Update At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }
    public function getPartner()
    {
        return $this->hasOne(Employee::className(), ['user_id' => 'partner_id']);
    }
    public function getManager()
    {
        return $this->hasOne(Employee::className(), ['user_id' => 'manager_id']);
    }
    public function getSupervisor()
    {
        return $this->hasOne(Employee::className(), ['user_id' => 'supervisor_id']);
    }
}
