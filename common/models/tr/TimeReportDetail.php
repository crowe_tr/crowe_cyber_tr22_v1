<?php

namespace common\models\tr;

use Yii;
use common\models\tr\TimeReport;
use common\models\st\Task;
use common\models\st\TaskType;
use common\models\tr\Job;


class TimeReportDetail extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'tr_time_report_detail';
    }

    // public $employee_idx;
    // public $tr_datex;
    public $is_stayed;

    public $max_over_time;
    public $over_time_allocation;

    public $min_work_hour;
    public $max_work_hour;
    public $work_hour_allocation;
    public $lookup;

    public function rules()
    {
        return [
            [['id','work_hour', 'over_time', 'is_stayed', 'lookup', 'description', 'attachment'], 'safe'],
            ['attachment', 'file', 'extensions' => ['png', 'jpg', 'pdf', 'jpeg'], 'maxSize' => 1024 * 1000 * 3],


            [['time_report_id', 'task_type_id', 'task_id', 'work_hour'], 'required'],

            [['time_report_id', 'task_type_id', 'job_id', 'task_id'], 'integer'],

            [['time_report_id', 'task_type_id', 'task_id'], 'unique',
                'targetAttribute' => ['time_report_id', 'task_type_id', 'task_id'],
                'message' => 'Tasks has already been taken',
                'when' => function ($model) {
                    return $model->task_id == 1;
                },
            ],
            [['time_report_id', 'task_type_id', 'task_id', 'job_id'], 'unique',
                'targetAttribute' => ['time_report_id', 'task_type_id', 'task_id', 'job_id'],
                'message' => 'Projects has already been taken',
                'when' => function ($model) {
                    return $model->task_id <> 1;
                },
            ],

            [['job_id'], 'required'
            , 'when'=>function($model) {
                $return = false;

                if($model->task_type_id == 1){
                    $return = true;
                }
                return $return;
            }, 'enableClientValidation' => false],
            [['approval1','approval2'], 'safe'],
            [['tr_date','employee_id'], 'safe'],
            [['tr_detail_value'], 'safe'],
            // [['tr_datex','employee_idx'], 'safe'],
            // [['MaxOvertime', 'MinWorkhour', 'MaxWorkhour','OvertimeAllocation', 'WorkHourAllocation'], 'safe'],
            [['over_time'], 'validateOvertime', 'enableClientValidation' => true],
            [['work_hour'], 'validateWorkHour', 'enableClientValidation' => true],

        ];
    }
    public function beforeSave($insert) {
        $this->work_hour = abs(round($this->work_hour, 1));
        $this->over_time = abs(round($this->over_time, 1));

        $this->work_hour = Yii::$app->formatter->asDecimal($this->work_hour);
        $this->over_time = Yii::$app->formatter->asDecimal($this->over_time);

        $sql = "CALL `common_list`('trdet-before-save', '{$this->employee_id};{$this->tr_date};{$this->task_id};{$this->work_hour}')";

        $rule = Yii::$app->db->createCommand($sql)->queryOne();
        // var_dump($rule);
        // die();
        if(!empty($rule)) {
            $return = true;
            if($rule['is_day_valid']==0){
                $taskName = $rule['task_name'];
                $this->addError('Description', "Hari ini tidak diijinkan untuk mengajukan permohonan : ".$taskName);
                $return = false;
            }
            if($rule['is_max_days_valid']==0){
                $max_days = $rule['max_days'];
                $total_day = $rule['total_day'];
                $this->addError('Description', "Maximal mengajukan permohonan ini adalah ".$max_days.". Anda sudah mengajukan sebanyak : ".$total_day);
                $return = false;
            }
            if($rule['is_max_hours_valid']==0){
                $WorkHour = $rule['max_hours'];
                $this->addError('Description', "Jam melebihi ".$WorkHour);
                $return = false;
            }
            if($rule['is_attachment']==1 && empty($this->attachment)){
                $this->addError('attachment', "Attachment wajib diisi");
                $return = false;
            }
            return $return;
        }else{
            return true;
        }
    }

    public function validateOvertime($attribute_name, $params){
        $valid = true;
        /*
        if(($this->WorkHourAllocation < $this->MaxWorkhour) && ($this->OvertimeAllocation > 0 AND $this->OvertimeAllocation <> NULL)){
            $valid = false;
            $this->addError($attribute_name, "Tidak bisa claim Overtime karena Total WorkHour : ".$this->WorkHourAllocation." Jam, belum mencapai : ".$this->MaxWorkhour." Jam");
        }else{
            if($this->MaxOvertime < $this->OvertimeAllocation){
                $valid = false;
                $this->addError($attribute_name, "Total Overtime : ".$this->OvertimeAllocation." Jam, dalam sehari tidak boleh melebihi : ".$this->MaxOvertime." Jam");
            }
        }
        */
        if(fmod($this->over_time,0.5) != 0){
            $valid = false;
            $this->addError($attribute_name, "Overtime must be a multiple of 0.5");
        }

        return $valid;
    }
    public function validateWorkHour($attribute_name, $params){
        $valid = true;
        /*
        $WorkHour = $this->WorkHourAllocation + $this->OvertimeAllocation;
        if($this->MinWorkhour > $WorkHour){
            $valid = false;
            $this->addError($attribute_name, "Total WorkHour : ".$this->WorkHourAllocation." Jam, dalam sehari tidak boleh kurang dari : ".$this->MinWorkhour." Jam");
        } else if($this->MaxWorkhour < $WorkHour){
            $valid = false;
            $this->addError($attribute_name, "Total WorkHour : ".$this->WorkHourAllocation." Jam, dalam sehari tidak boleh melebihi : ".$this->MaxWorkhour." Jam");
        }
        */
        if(fmod($this->work_hour,0.5) != 0){
            $valid = false;
            $this->addError($attribute_name, "WorkHour must be a multiple of 0.5");
        }
        return $valid;
    }


    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'time_report_id' => Yii::t('app', 'Time Report ID'),
            'employee_id' => Yii::t('app', 'Employee'),
            'tr_date' => Yii::t('app', 'Date'),
            'task_type_id' => Yii::t('app', 'Class / Task Type'),
            'job_id' => Yii::t('app', 'Job'),
            'task_id' => Yii::t('app', 'Task'),
            'work_hour' => Yii::t('app', 'Work Hours'),
            'over_time' => Yii::t('app', 'Overtimes'),
            'attachment' => Yii::t('app', 'File Attachment'),
        ];
    }

    public function getJob()
    {
        return $this->hasOne(Job::className(), ['id' => 'job_id']);
    }
    public function getTasktype()
    {
        return $this->hasOne(TaskType::className(), ['id' => 'task_type_id']);
    }
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }

    public function getParent()
    {
        return $this->hasOne(TimeReport::className(), ['id' => 'time_report_id']);
    }
    public function getTimeReport()
    {
        return $this->hasOne(TimeReport::className(), ['id' => 'time_report_id']);
    }


}
