<?php

namespace common\models\tr;

use Yii;
use common\models\hr\Employee;

/**
 * This is the model class for table "JobComment".
 *
 * @property int $CommentId
 * @property int $JobID
 * @property string $Comments
 * @property string $CreatedBy
 * @property string $CreatedDate
 *
 * @property Job $job
 */
class JobComment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tr_job_comment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
      return  [
                  [['job_id'], 'integer'],
                  [['comments'], 'string'],
                  [['created_date'], 'safe'],
                  [['created_by'], 'string', 'max' => 16],
                  [['id'], 'exist', 'skipOnError' => true, 'targetClass' => Job::className(), 'targetAttribute' => ['id' => 'job_id']],
              ];

        // return [
        //     [['JobID'], 'integer'],
        //     [['Comments'], 'string'],
        //     [['CreatedDate'], 'safe'],
        //     [['CreatedBy'], 'string', 'max' => 16],
        //     [['JobID'], 'exist', 'skipOnError' => true, 'targetClass' => Job::className(), 'targetAttribute' => ['JobID' => 'JobID']],
        // ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
      return  [
                  'id' => Yii::t('app', 'Comment ID'),
                  'job_id' => Yii::t('app', 'Job ID'),
                  'comments' => Yii::t('app', 'Comments'),
                  'created_by' => Yii::t('app', 'Created By'),
                  'created_date' => Yii::t('app', 'Created Date'),
              ];

        // return [
        //     'CommentId' => Yii::t('app', 'Comment ID'),
        //     'JobID' => Yii::t('app', 'Job ID'),
        //     'Comments' => Yii::t('app', 'Comments'),
        //     'CreatedBy' => Yii::t('app', 'Created By'),
        //     'CreatedDate' => Yii::t('app', 'Created Date'),
        // ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJob()
    {
      return $this->hasOne(Job::className(), ['id' => 'job_id']);

        // return $this->hasOne(Job::className(), ['JobID' => 'JobID']);
    }

    public function getEmployee()
    {
      return $this->hasOne(Employee::className(), ['user_id' => 'created_by']);

      // return $this->hasOne(Employee::className(), ['Id' => 'CreatedBy']);
    }
}
