<?php

namespace common\models\tr;

use Yii;

class TimeReportOutOfTown extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'tr_time_report_out_of_town';
    }
    public function rules()
    {
        return [
            [[
                'tr_id',
                'tr_date',
                'employee_id',
                'job_id',
                'approval1',
                'approval2',
                'out_of_town_value',
                'description',
                'created_at',
                'updated_at',
                'deleted_at',
            ], 'safe'],
            [[
                'tr_id',
                'tr_date',
                'employee_id',
                'job_id',
                'description',
            ], 'required'],
        ];
    }
    public function attributeLabels()
    {
        return [
            'tr_id' => Yii::t('app', 'Time Report'),
            'tr_date' => Yii::t('app', 'Date'),
            'employee_id' => Yii::t('app', 'Employee'),
            'job_id' => Yii::t('app', 'Job'),
            'out_of_town_value' => Yii::t('app', 'Value'),
        ];
    }

    public function getTimeReport()
    {
        return $this->hasOne(TimeReport::className(), ['ID' => 'tr_id']);
    }
}
