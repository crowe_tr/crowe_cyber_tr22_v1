<?php

namespace common\models\tr\ms;

use common\components\CommonHelper;
class Division extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'cmDept';
    }

    public function rules()
    {
        return [
            [['Id', 'Departement'], 'required'],
            [['Id'], 'string', 'max' => 3],
            [['Departement'], 'string', 'max' => 128],
        ];
    }

    public function attributeLabels()
    {
        return [
            'Id' => 'Code',
            'Departement' => 'Departement',
        ];
    }
}
