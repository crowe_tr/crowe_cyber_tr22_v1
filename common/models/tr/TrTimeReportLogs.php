<?php

namespace common\models\tr;

use Yii;


class TrTimeReportLogs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tr_time_report_logs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tr_id','tr_det_id'], 'integer'],
            [['employee_name','tr_type','tr_status','tr_memo','user_action'], 'string'],
            [['created_at','tr_date'], 'safe'],
            [['created_by'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tr_id' => 'TR ID',
            'tr_date' => 'Date',
            'employee_name' => 'Employee Name',
            'tr_det_id' => 'TR DET ID',
            'tr_type' => 'TYPE',
            'tr_status' => 'STATUS',
            'tr_memo' => 'MEMO',
            'user_action' => 'Action',
            'created_at' => 'Action At',
            'created_by' => 'Action By',
        ];
    }
}
