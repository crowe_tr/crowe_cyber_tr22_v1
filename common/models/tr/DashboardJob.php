<?php

namespace common\models\tr;

use Yii;

class DashboardJob extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dashboard_job';
    }

}
