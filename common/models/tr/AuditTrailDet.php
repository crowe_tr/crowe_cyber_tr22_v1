<?php

namespace common\models\tr;

use Yii;

/**
 * This is the model class for table "audit_trail_det".
 *
 * @property int $ID
 * @property int $AuditTrailID
 * @property string $Source
 * @property string $TransType
 * @property string $TransactionType
 * @property int $TransID
 * @property string $TransEmpID
 * @property int $TransSeq
 * @property int $Status
 * @property string $AuditDescription
 * @property string $AuditSQL
 * @property string $DateCreated
 * @property string $CreatedBy
 * @property string $DateModified
 * @property string $ModifiedBy
 */
class AuditTrailDet extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'audit_trail_det';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['audit_trail_id'], 'required'],
            [['audit_trail_id', 'trans_id', 'trans_seq', 'audit_status'], 'integer'],
            [['audit_description', 'audit_sql'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['source', 'trans_type'], 'string', 'max' => 3],
            [['transaction_type'], 'string', 'max' => 60],
            [['trans_employee_id'], 'string', 'max' => 32],
            [['created_by', 'updated_by'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'audit_trail_id' => 'Audit Trail ID',
            'source' => 'Source',
            'trans_type' => 'Trans Type',
            'transaction_type' => 'Transaction Type',
            'trans_id' => 'Trans ID',
            'trans_employee_id' => 'Trans Emp ID',
            'trans_seq' => 'Trans Seq',
            'audit_status' => 'Status',
            'audit_description' => 'Audit Description',
            'audit_sql' => 'Audit Sql',
            'created_at' => 'Date Created',
            'created_by' => 'Created By',
            'updated_at' => 'Date Modified',
            'updated_by' => 'Modified By',
        ];
    }
}
