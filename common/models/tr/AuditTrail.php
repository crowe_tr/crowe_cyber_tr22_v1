<?php

namespace common\models\tr;

use Yii;

/**
 * This is the model class for table "audit_trail".
 *
 * @property int $ID
 * @property string $Source
 * @property string $TransType
 * @property string $TransactionType
 * @property string $SourceNo
 * @property int $TransID
 * @property string $AuditDescription
 * @property int $Status
 * @property string $DateCreated
 * @property string $CreatedBy
 * @property string $DateModified
 * @property string $ModifiedBy
 */
class AuditTrail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'audit_trail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['trans_id', 'audit_status'], 'integer'],
            [['audit_description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['source', 'trans_type'], 'string', 'max' => 3],
            [['transaction_type', 'created_by', 'updated_by'], 'string', 'max' => 128],
            [['source_no'], 'string', 'max' => 60],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'source' => 'Source',
            'trans_type' => 'Trans Type',
            'transaction_type' => 'Transaction Type',
            'source_no' => 'Source No',
            'trans_id' => 'Trans ID',
            'audit_description' => 'Audit Description',
            'audit_status' => 'Status',
            'created_at' => 'Date Created',
            'created_by' => 'Created By',
            'updated_at' => 'Date Modified',
            'updated_by' => 'Modified By',
        ];
    }
}
