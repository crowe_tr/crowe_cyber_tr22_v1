<?php

namespace common\models\tr;

use common\models\hr\Employee;
use common\models\hr\EmployeeList;
use common\models\tr\JobBudgeting;

use Yii;

/**
 * This is the model class for table "trJobBudgetingTemp".
 *
 * @property int $JobID
 * @property int $Seq
 * @property string $EmployeeID
 * @property string $Planning
 * @property string $FieldWork
 * @property string $Reporting
 * @property string $WrapUp
 * @property string $OverTime
 * @property string $Total
 * @property string $CreatedBy
 * @property string $CreatedAt
 * @property string $UpdateBy
 * @property string $UpdateAt
 */
class JobBudgetingTemp extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tr_job_budgeting_temp';
    }

    public $entity_name;
    public $div_name;
    public $group_name;
    public $level_name;
    public $full_name;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['job_id', 'employee_id'], 'required'],
            [['job_id'], 'integer'],
            [['planning', 'field_work', 'reporting', 'wrap_up', 'over_time', 'total_wh'], 'number'],
            [['entity_name', 'div_name', 'group_name', 'level_name', 'full_name', 'created_at', 'updated_at'], 'safe'],
            [['employee_id'], 'string', 'max' => 32],
            [['created_by', 'updated_by'], 'string', 'max' => 32],
            // [['JobID'], 'unique', 'targetAttribute' => ['JobID', 'Seq']],
            [['job_id', 'employee_id'], 'unique', 'targetAttribute' => ['job_id', 'employee_id']],
        ];
    }
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if (intval($this->over_time) != 0) {
                $old = JobBudgeting::find()->where(['job_id' => $this->job_id, 'employee_id'=>$this->employee_id])->one();
                $OverTimeUsed = !empty($old->over_time_used) ? $old->over_time_used : null;
                if (!empty($OverTimeUsed) && $this->over_time == $OverTimeUsed) {
                    return true;
                } else {
                    $isNotBoss = Yii::$app->db->createCommand(
                        "
                          select `is_true`('is-not-boss-emp', '" . $this->employee_id . "') as value
                        "
                    )->queryOne();
                    if ($isNotBoss['value'] == 1) {
                        return true;
                    } else {
                      if (!empty($OverTimeUsed)) {
                        return true;
                      } else {
                        $this->addError($this->over_time, 'User can not charge Overtime');
                        return false;
                      }
                    }
                }
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'job_id' => 'Job ID',
            // 'Seq' => 'Seq',
            'employee_id' => 'Employee ID',
            'planning' => 'Planning',
            'field_work' => 'Field Work',
            'reporting' => 'Reporting',
            'wrap_up' => 'Wrap Up',
            'over_time' => 'Over Time',
            'total_wh' => 'Total',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Update By',
            'updated_at' => 'Update At',
        ];
    }

    public function getJob()
    {
        return $this->hasOne(Job::className(), ['id' => 'job_id']);
    }
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['user_id' => 'employee_id']);
    }
}
