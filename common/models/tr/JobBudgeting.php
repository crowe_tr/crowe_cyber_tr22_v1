<?php

namespace common\models\tr;

use Yii;
use common\models\hr\Employee;
use common\models\hr\EmployeeList;

class JobBudgeting extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'tr_job_budgeting';
    }

    public $Entity;
    public $Division;
    public $Grup;
    public $level_name;
    public $full_name;

    public function rules()
    {
        return [
            [['job_id', 'employee_id'], 'required'],
            [['job_id'], 'integer'],
            [['created_at', 'update_at',
              'Entity', 'Division', 'Grup', 'level_name', 'full_name',
              'planning', 'field_work', 'reporting', 'wrap_up', 'over_time', 'total_wh'], 'safe'],
            [['employee_id', 'created_by', 'update_by'], 'string', 'max' => 16],
            [['job_id', 'employee_id'], 'unique', 'targetAttribute' => ['job_id', 'employee_id']],
            // [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['employee_id' => 'Id']],

        ];
    }
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if (intval($this->over_time) != 0) {
                $old = JobBudgeting::find()->where(['job_id' => $this->job_id, 'employee_id' => $this->employee_id])->one();
                $over_time_used = !empty($old->over_time_used) ? $old->over_time_used : null;

                if (!empty($over_time_used) && $this->over_time == $over_time_used) {
                    return true;
                } else {
                    $sql_not_boss = "select `is_true`('is-not-boss-emp', '" . $this->employee_id . "') as value";
                    $isNotBoss = Yii::$app->db->createCommand($sql_not_boss)->queryOne();

                    if ($isNotBoss['value'] == 1) {
                        return true;
                    } else {
                        if ($over_time_used > 0) {
                          return true;
                        } else {
                          $this->addError($this->over_time, 'User can not charge Overtime');
                          return false;
                        }
                    }
                }
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    public function attributeLabels()
    {
        return [
            'job_id' => Yii::t('app', 'Job ID'),
            'employee_id' => Yii::t('app', 'Employee'),
            'planning' => Yii::t('app', 'planning'),
            'field_work' => Yii::t('app', 'Field Work'),
            'reporting' => Yii::t('app', 'reporting'),
            'wrap_up' => Yii::t('app', 'WRAP UP'),
            'over_time' => Yii::t('app', 'Over Time'),
            'total_wh' => Yii::t('app', 'total_wh'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'update_by' => Yii::t('app', 'Update By'),
            'update_at' => Yii::t('app', 'Update At'),
        ];
    }

    public function getJob()
    {
        return $this->hasOne(Job::className(), ['id' => 'job_id']);
    }
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['user_id' => 'employee_id']);
    }

    public function getEmployee_list()
    {
        return $this->hasOne(EmployeeList::className(), ['employee_id' => 'employee_id']);
    }
}
