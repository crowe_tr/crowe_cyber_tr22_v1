<?php

namespace common\models\rg\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\rg\rgUser;

/**
 * rgUserSearch represents the model behind the search form about `common\models\rg\rgUser`.
 */
class rgUserSearch extends rgUser
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Id', 'CompanyId', 'BranchId', 'DeptId', 'EmployeeId', 'Email', 'PasswordHash', 'AuthKey', 'AccountActivationToken', 'LastUpdateBy', 'LastUpdateDate', 'CreatedBy', 'CreatedDate'], 'safe'],
            [['PasswordResetToken', 'Status', 'Flag'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied.
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = rgUser::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'PasswordResetToken' => $this->PasswordResetToken,
            'Status' => $this->Status,
            'LastUpdateDate' => $this->LastUpdateDate,
            'CreatedDate' => $this->CreatedDate,
            'Flag' => $this->Flag,
        ]);

        $query->andFilterWhere(['like', 'Id', $this->Id])
            ->andFilterWhere(['like', 'CompanyId', $this->CompanyId])
            ->andFilterWhere(['like', 'BranchId', $this->BranchId])
            ->andFilterWhere(['like', 'DeptId', $this->DeptId])
            ->andFilterWhere(['like', 'EmployeeId', $this->EmployeeId])
            ->andFilterWhere(['like', 'Email', $this->Email])
            ->andFilterWhere(['like', 'PasswordHash', $this->PasswordHash])
            ->andFilterWhere(['like', 'AuthKey', $this->AuthKey])
            ->andFilterWhere(['like', 'AccountActivationToken', $this->AccountActivationToken])
            ->andFilterWhere(['like', 'LastUpdateBy', $this->LastUpdateBy])
            ->andFilterWhere(['like', 'CreatedBy', $this->CreatedBy]);

        return $dataProvider;
    }
}
