<?php

namespace common\models\cm;

use Yii;

/**
 * This is the model class for table "cmOptions".
 *
 * @property string $optionsName
 * @property string $optionsValue
 */
class Options extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cm_options';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
      return  [
                  [['options_value', 'effective_date'], 'required'],
                  [['options_name'], 'required', 'on'=>'new'],

                  [['options_name'], 'string', 'max' => 60],
                  [['options_value'], 'string', 'max' => 45],
                  [['options_name', 'effective_date'], 'unique', 'targetAttribute' => ['options_name', 'effective_date'], 'on'=>'new']
              ];

        // return [
        //     [['optionsName', 'optionsValue'], 'safe'],
        //     [['optionsValue', 'effectiveDate'], 'required'],
        //     [['optionsName'], 'required', 'on'=>'new'],
        //
        //     [['optionsName'], 'string', 'max' => 60],
        //     [['optionsValue'], 'string', 'max' => 45],
        //     [['optionsName', 'effectiveDate'], 'unique', 'targetAttribute' => ['optionsName', 'effectiveDate'], 'on'=>'new']
        // ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
      return  [
                  'options_name' => 'Options Name',
                  'options_value' => 'Options Value',
              ];

        // return [
        //     'optionsName' => 'Options Name',
        //     'optionsValue' => 'Options Value',
        // ];
    }
}
