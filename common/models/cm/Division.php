<?php

namespace common\models\cm;

use Yii;

/**
 * This is the model class for table "trMsDivision".
 *
 * @property int $id
 * @property string $divCode
 * @property string $divName
 * @property int $suspended
 */
class Division extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cm_division';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
      return  [
                  [['div_code'], 'string', 'max' => 4],
                  [['div_name'], 'string', 'max' => 50],
                  [['div_code'], 'unique'],
                  [['div_name'], 'unique'],
                //   [['overtime_project'], 'integer'],
                  [['recovery_rate'], 'integer'],
                  [['div_code','div_name'], 'required'],

              ];
        // return [
        //     [['divCode'], 'string', 'max' => 4],
        //     [['divName'], 'string', 'max' => 50],
        //     [['divCode'], 'unique'],
        //     [['divName'], 'unique'],
        //     [['divCode','divName'], 'required'],
        // ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
      return  [
                  'id' => 'ID',
                  'div_code' => 'Div Code',
                  'div_name' => 'Div Name',
                  'recovery_rate' => 'Min Recovery Rate',
                //   'overtime_project' => 'OT Project',
              ];
        // return [
        //     'id' => 'ID',
        //     'divCode' => 'Div Code',
        //     'divName' => 'Div Name',
        // ];
    }
}
