<?php

namespace common\models\cm\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\cm\Level as levelModel;

/**
 * level represents the model behind the search form of `common\models\cm\level`.
 */
class Level extends levelModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
      return  [
                  [['id'], 'integer'],
                  [['level_code', 'level_name', 'level_notes',
                    'auto_approve', 'is_not_boss', 'is_not_manager'], 'safe'],
              ];
        // return [
        //     [['id'], 'integer'],
        //     [['levelCode', 'levelName', 'notes'], 'safe'],
        // ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = levelModel::find();
        // add conditions that should always apply here
        $query->orderBy([
            'level_code' => SORT_ASC,
        ]);

        // $query->orderBy([
        //     'levelCode' => SORT_ASC,
        // ]);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'level_code', $this->level_code])
            ->andFilterWhere(['like', 'level_name', $this->level_name])
            ->andFilterWhere(['like', 'level_notes', $this->level_notes])
            ->andFilterWhere(['like', 'auto_approve', $this->auto_approve])
            ->andFilterWhere(['like', 'is_not_boss', $this->is_not_boss])
            ->andFilterWhere(['like', 'is_not_manager', $this->is_not_manager]);

        // $query->andFilterWhere([
        //     'id' => $this->id,
        // ]);
        //
        // $query->andFilterWhere(['like', 'levelCode', $this->levelCode])
        //     ->andFilterWhere(['like', 'levelName', $this->levelName])
        //     ->andFilterWhere(['like', 'notes', $this->notes]);

        return $dataProvider;
    }
}
