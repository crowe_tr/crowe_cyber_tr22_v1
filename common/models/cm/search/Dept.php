<?php

namespace common\models\cm\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\cm\Dept as DeptModel;

/**
 * Dept represents the model behind the search form of `common\models\cm\Dept`.
 */
class Dept extends DeptModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
      return  [
                  [['id', 'div_id', 'flag'], 'integer'],
                  [['dept_code', 'dept_name'], 'safe'],
              ];
        // return [
        //     [['id', 'divID', 'Flag'], 'integer'],
        //     [['deptCode', 'deptName'], 'safe'],
        // ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DeptModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'div_id' => $this->div_id,
            'flag' => $this->flag,
        ]);

        $query->andFilterWhere(['like', 'dept_code', $this->dept_code])
            ->andFilterWhere(['like', 'dept_name', $this->dept_name]);

        return $dataProvider;

        // $query->andFilterWhere([
        //     'id' => $this->id,
        //     'divID' => $this->divID,
        //     'Flag' => $this->Flag,
        // ]);
        //
        // $query->andFilterWhere(['like', 'deptCode', $this->deptCode])
        //     ->andFilterWhere(['like', 'deptName', $this->deptName]);

        // return $dataProvider;
    }
}
