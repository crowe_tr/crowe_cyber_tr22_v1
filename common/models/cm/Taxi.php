<?php

namespace common\models\cm;

use Yii;

/**
 * This is the model class for table "cmTaxi".
 *
 * @property int $id
 * @property string $name
 * @property int $overtime
 */
class Taxi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cm_taxi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['taxi_name'], 'required'],
            [['is_over_time', 'is_cut_ope'], 'integer'],
            [['taxi_name'], 'string', 'max' => 30],
            [['taxi_name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'taxi_name' => Yii::t('app', 'Name'),
            'is_over_time' => Yii::t('app', 'Overtime'),
            'is_cut_ope' => Yii::t('app', 'Cut OPE'),
        ];
    }
}
