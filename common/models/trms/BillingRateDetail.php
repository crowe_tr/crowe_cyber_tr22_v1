<?php

namespace common\models\trms;

use Yii;

/**
 * This is the model class for table "trMsBillingRateDetail".
 *
 * @property int $id
 * @property int $billingRateID
 * @property int $projectPositionID
 * @property string $BillingRateValue
 *
 * @property TrMsBillingRate $billingRate
 * @property TrMsPosition $projectPosition
 */
class BillingRateDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trMsBillingRateDetail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['billingRateID', 'projectPositionID', 'BillingRateValue'], 'required'],
            [['billingRateID', 'projectPositionID'], 'integer'],
            [['BillingRateValue'], 'number'],
            [['billingRateID', 'projectPositionID'], 'unique', 'targetAttribute' => ['billingRateID', 'projectPositionID']],
            [['billingRateID'], 'exist', 'skipOnError' => true, 'targetClass' => BillingRate::className(), 'targetAttribute' => ['billingRateID' => 'id']],
            [['projectPositionID'], 'exist', 'skipOnError' => true, 'targetClass' => Position::className(), 'targetAttribute' => ['projectPositionID' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'billingRateID' => 'Billing Rate ID',
            'projectPositionID' => 'Project Position ID',
            'BillingRateValue' => 'Billing Rate Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillingRate()
    {
        return $this->hasOne(BillingRate::className(), ['id' => 'billingRateID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectPosition()
    {
        return $this->hasOne(Position::className(), ['id' => 'projectPositionID']);
    }
}
