<?php

namespace common\models\trms;

use Yii;

/**
 * This is the model class for table "trMsTask".
 *
 * @property int $id
 * @property string $name
 * @property int $taskType
 *
 * @property TrMsTaskType $taskType0
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trMsTask';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['taskType'], 'integer'],
            [['name'], 'string', 'max' => 60],
            [['taskType', 'name'], 'unique', 'targetAttribute' => ['taskType', 'name']],
            [['taskType'], 'exist', 'skipOnError' => true, 'targetClass' => TaskType::className(), 'targetAttribute' => ['taskType' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'taskType' => 'Task Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskType0()
    {
        return $this->hasOne(TaskType::className(), ['id' => 'taskType']);
    }
}
