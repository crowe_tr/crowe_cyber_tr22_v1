<?php

namespace common\models\trms;

use Yii;

/**
 * This is the model class for table "trMsRule".
 *
 * @property int $id
 * @property string $name
 * @property string $ruleType
 * @property string $claimMeal
 * @property string $useTaxi
 * @property string $claimTransportation
 * @property string $claimOutOffice
 * @property string $day
 * @property string $area
 * @property string $workHour
 * @property string $totalOvertimeHour
 * @property string $itemOvertime
 * @property string $isMealProvided
 * @property string $isTransportationProvided
 * @property string $isAccomodationProvided
 * @property string $claimTransportationAmount
 * @property string $withOvertimeTransport
 * @property string $applicablePosition
 * @property string $source
 * @property string $allowance
 */
class Rule extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trMsRule';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'ruleType', 'claimMeal', 'useTaxi', 'claimTransportation', 'claimOutOffice', 'day', 'area', 'workHour', 'totalOvertimeHour', 'itemOvertime', 'isMealProvided', 'isTransportationProvided', 'isAccomodationProvided', 'claimTransportationAmount', 'withOvertimeTransport', 'applicablePosition', 'source', 'allowance'], 'string', 'max' => 45],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'ruleType' => 'Rule Type',
            'claimMeal' => 'Claim Meal',
            'useTaxi' => 'Use Taxi',
            'claimTransportation' => 'Claim Transportation',
            'claimOutOffice' => 'Claim Out Office',
            'day' => 'Day',
            'area' => 'Area',
            'workHour' => 'Work Hour',
            'totalOvertimeHour' => 'Total Overtime Hour',
            'itemOvertime' => 'Item Overtime',
            'isMealProvided' => 'Is Meal Provided',
            'isTransportationProvided' => 'Is Transportation Provided',
            'isAccomodationProvided' => 'Is Accomodation Provided',
            'claimTransportationAmount' => 'Claim Transportation Amount',
            'withOvertimeTransport' => 'With Overtime Transport',
            'applicablePosition' => 'Applicable Position',
            'source' => 'Source',
            'allowance' => 'Allowance',
        ];
    }
}
