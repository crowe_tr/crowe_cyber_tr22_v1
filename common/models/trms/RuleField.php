<?php

namespace common\models\trms;

use Yii;

/**
 * This is the model class for table "trMsRuleField".
 *
 * @property int $id
 * @property string $sourceRule
 * @property string $attribute
 * @property string $label
 * @property string $filedType
 * @property string $range
 * @property string $widget
 * @property string $position
 */
class RuleField extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trMsRuleField';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sourceRule', 'attribute', 'label', 'filedType', 'range', 'widget', 'position'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sourceRule' => 'Source Rule',
            'attribute' => 'Attribute',
            'label' => 'Label',
            'filedType' => 'Filed Type',
            'range' => 'Range',
            'widget' => 'Widget',
            'position' => 'Position',
        ];
    }
}
