<?php

namespace common\models\trms;

use Yii;

/**
 * This is the model class for table "trMsPosition".
 *
 * @property int $id
 * @property string $positionCode
 * @property string $positionName
 * @property string $notes
 * @property int $positionTypeId
 *
 * @property TrMsAreaEffectivePosition[] $trMsAreaEffectivePositions
 * @property TrMsBillingRateDetail[] $trMsBillingRateDetails
 * @property TrMsBillingRate[] $billingRates
 * @property TrMsPositionType $positionType
 */
class Position extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trMsPosition';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['positionCode'], 'required'],
            [['positionTypeId'], 'integer'],
            [['positionCode'], 'string', 'max' => 10],
            [['positionName', 'notes'], 'string', 'max' => 45],
            [['positionCode'], 'unique'],
            [['positionTypeId', 'positionName'], 'unique', 'targetAttribute' => ['positionTypeId', 'positionName']],
            [['positionTypeId'], 'exist', 'skipOnError' => true, 'targetClass' => PositionType::className(), 'targetAttribute' => ['positionTypeId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'positionCode' => 'Position Code',
            'positionName' => 'Position Name',
            'notes' => 'Notes',
            'positionTypeId' => 'Position Type ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrMsAreaEffectivePositions()
    {
        return $this->hasMany(AreaEffectivePosition::className(), ['empPositionID' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrMsBillingRateDetails()
    {
        return $this->hasMany(BillingRateDetail::className(), ['projectPositionID' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillingRates()
    {
        return $this->hasMany(BillingRate::className(), ['id' => 'billingRateID'])->viaTable('trMsBillingRateDetail', ['projectPositionID' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPositionType()
    {
        // return $this->hasOne(TrMsPositionType::className(), ['id' => 'positionTypeId']);
        return $this->hasOne(PositionType::className(), ['id' => 'positionTypeId']);
    }
}
