<?php

namespace common\models\trms;

use Yii;

/**
 * This is the model class for table "trMsAreaItem".
 *
 * @property int $id
 * @property string $areaItemName
 * @property string $areaItemDesc
 * @property string $areatermname
 * @property int $conditionType 0-Berdasarkan Term, 1-Berdasarkan Zona, 2-Berdasarkan Posisi
 * @property int $areaID
 * @property int $suspended
 *
 * @property AreaEffectiveDet[] $trMsAreaEffectiveDets
 * @property Area $area
 */
class AreaItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trMsAreaItem';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['areaItemName', 'areaItemDesc', 'areatermname', 'areaID'], 'required'],
            [['conditionType', 'areaID', 'suspended'], 'integer'],
            [['areaItemName'], 'string', 'max' => 80],
            [['areaItemDesc'], 'string', 'max' => 500],
            [['areatermname'], 'string', 'max' => 240],
            [['areaItemName'], 'unique'],
            [['areaID'], 'exist', 'skipOnError' => true, 'targetClass' => Area::className(), 'targetAttribute' => ['areaID' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'areaItemName' => 'Area Item Name',
            'areaItemDesc' => 'Area Item Desc',
            'areatermname' => 'Areatermname',
            'conditionType' => 'Condition Type',
            'areaID' => 'Area ID',
            'suspended' => 'Suspended',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrMsAreaEffectiveDets()
    {
        return $this->hasMany(TrMsAreaEffectiveDet::className(), ['itemID' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArea()
    {
        return $this->hasOne(Area::className(), ['id' => 'areaID']);
    }
}
