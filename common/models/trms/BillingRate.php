<?php

namespace common\models\trms;

use Yii;

/**
 * This is the model class for table "trMsBillingRate".
 *
 * @property int $id
 * @property string $description
 * @property string $effectiveDate
 *
 * @property TrMsBillingRateDetail[] $trMsBillingRateDetails
 * @property TrMsPosition[] $projectPositions
 */
class BillingRate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trMsBillingRate';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['effectiveDate'], 'safe'],
            [['description'], 'string', 'max' => 255],
            [['effectiveDate'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Description',
            'effectiveDate' => 'Efective Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrMsBillingRateDetails()
    {
        return $this->hasMany(TrMsBillingRateDetail::className(), ['billingRateID' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectPositions()
    {
        return $this->hasMany(TrMsPosition::className(), ['id' => 'projectPositionID'])->viaTable('trMsBillingRateDetail', ['billingRateID' => 'id']);
    }
}
