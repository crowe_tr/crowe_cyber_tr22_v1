<?php

namespace common\models\trms;

use Yii;

/**
 * This is the model class for table "trMsPositionType".
 *
 * @property int $id
 * @property string $name
 * @property int $suspended
 *
 * @property TrMsPosition[] $trMsPositions
 */
class PositionType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trMsPositionType';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['suspended'], 'integer'],
            [['name'], 'string', 'max' => 45],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'suspended' => 'Suspended',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrMsPositions()
    {
        return $this->hasMany(Position::className(), ['positionTypeId' => 'id']);
    }
}
