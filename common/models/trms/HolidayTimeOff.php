<?php

namespace common\models\trms;

use Yii;

/**
 * This is the model class for table "trMsHolidayTimeOff".
 *
 * @property int $id
 * @property string $date
 * @property int $type
 * @property string $description
 *
 * @property TrMsHolidayType $type0
 */
class HolidayTimeOff extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trMsHolidayTimeOff';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['type'], 'integer'],
            [['description'], 'string', 'max' => 45],
            [['date'], 'unique'],
            [['type'], 'exist', 'skipOnError' => true, 'targetClass' => HolidayType::className(), 'targetAttribute' => ['type' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'type' => 'Type',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType0()
    {
        return $this->hasOne(HolidayType::className(), ['id' => 'type']);
    }
}
