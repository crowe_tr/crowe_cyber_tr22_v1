<?php

namespace common\models\trms;

use Yii;

/**
 * This is the model class for table "trMsAreaEffective".
 *
 * @property int $id
 * @property string $effectivedate
 * @property string $description
 *
 * @property TrMsAreaEffectiveDet[] $trMsAreaEffectiveDets
 */
class AreaEffective extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trMsAreaEffective';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['effectivedate', 'description'], 'required'],
            [['effectivedate'], 'safe'],
            [['description'], 'string', 'max' => 240],
            [['effectivedate'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'effectivedate' => 'Effectivedate',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrMsAreaEffectiveDets()
    {
        return $this->hasMany(TrMsAreaEffectiveDet::className(), ['areaEffectiveID' => 'id']);
    }
}
