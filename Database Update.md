20 Sept 2022
A.  UNTUK KEPERLUAN "ANNUAL LEAVE"
    1.  Update Trigger di Table "tr_time_report_detail" untuk BI,BU,AI,AD
    2.  Buat Table baru untuk menampung Annual Leave

18 Sept 2022
A.  UNTUK KEPERLUAN "REPORT SUMMARY RECOVERY RATE" ==> "DONE"
    1.  Update procedure "rpt_val" di bagian "tc-value-group"
    2.  Buat procedure "update_job_actual"
    3.  Update Trigger di Table "tr_job_budgeting_ob" untuk AI,AU,AD
    4.  Update procedure "set_tr_val" di bagian :
        a.  "tr-job-detail"
        b.  "tr-job-meal"
        c.  "tr-job-ope"
        d.  "tr-job-taxi"
        dan tambahkan variable "vdata_tc_ob"
